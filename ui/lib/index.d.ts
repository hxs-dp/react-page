import { BottomToolbarProps } from './BottomToolbar/types';
import { colorToString } from './ColorPicker/colorToString';
import darkTheme from './ThemeProvider/DarkTheme';
import { ImageUploadType, ImageUploaded, ImageLoaded } from './ImageUpload/types';
import { RGBColor } from './ColorPicker/types';
declare const Trash: any;
declare const PluginDrawer: any;
declare const DisplayModeToggle: any;
declare const BottomToolbar: any;
declare const EditorUI: any;
declare const ThemeProvider: any;
declare const ImageUpload: any;
declare const ColorPicker: any;
export default EditorUI;
export { BottomToolbarProps, EditorUI, Trash, PluginDrawer, DisplayModeToggle, BottomToolbar, ThemeProvider, darkTheme, ImageUpload, ImageUploaded, ImageLoaded, ImageUploadType, ColorPicker, colorToString, RGBColor, };
declare const Toolbar: any;
/**
 * @deprecated, use PluginDrawer instead
 */
export { Toolbar };
//# sourceMappingURL=index.d.ts.map