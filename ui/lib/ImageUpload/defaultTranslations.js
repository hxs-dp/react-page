"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.defaultTranslations = void 0;
exports.defaultTranslations = {
    buttonContent: 'Upload image',
    noFileError: 'No file selected',
    badExtensionError: 'Bad file type',
    tooBigError: 'Too big',
    uploadingError: 'Error while uploading',
    unknownError: 'Unknown error',
};
//# sourceMappingURL=defaultTranslations.js.map