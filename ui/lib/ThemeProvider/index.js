"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.darkTheme = void 0;
var React = __importStar(require("react"));
var styles_1 = require("@material-ui/core/styles");
var styles_2 = require("@material-ui/styles");
var styles_3 = require("@material-ui/styles");
var index_1 = __importDefault(require("./DarkTheme/index"));
exports.darkTheme = index_1.default;
var themeOptions_1 = require("./themeOptions");
var generateClassName = styles_3.createGenerateClassName({
    disableGlobal: true,
    productionPrefix: 'ory',
});
var theme = styles_1.createMuiTheme(themeOptions_1.themeOptions);
var ThemeProvider = /** @class */ (function (_super) {
    __extends(ThemeProvider, _super);
    function ThemeProvider() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    ThemeProvider.prototype.render = function () {
        return (React.createElement(styles_2.StylesProvider, { injectFirst: true, generateClassName: generateClassName },
            React.createElement(styles_2.ThemeProvider, { theme: this.props.theme || theme }, this.props.children)));
    };
    return ThemeProvider;
}(React.Component));
exports.default = ThemeProvider;
//# sourceMappingURL=index.js.map