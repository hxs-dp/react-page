"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var styles_1 = require("@material-ui/core/styles");
var theme = styles_1.createMuiTheme({
    palette: {
        type: 'dark',
    },
});
exports.default = theme;
//# sourceMappingURL=index.js.map