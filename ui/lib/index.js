"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Toolbar = exports.colorToString = exports.ColorPicker = exports.ImageUpload = exports.darkTheme = exports.ThemeProvider = exports.BottomToolbar = exports.DisplayModeToggle = exports.PluginDrawer = exports.Trash = exports.EditorUI = void 0;
// something is wrong with lerna, typescript and this import: import { lazyLoad } from '@react-page/core';
var component_1 = __importDefault(require("@loadable/component"));
var colorToString_1 = require("./ColorPicker/colorToString");
Object.defineProperty(exports, "colorToString", { enumerable: true, get: function () { return colorToString_1.colorToString; } });
var DarkTheme_1 = __importDefault(require("./ThemeProvider/DarkTheme"));
exports.darkTheme = DarkTheme_1.default;
var Trash = component_1.default(function () { return Promise.resolve().then(function () { return __importStar(require('./Trash/index')); }); });
exports.Trash = Trash;
var PluginDrawer = component_1.default(function () { return Promise.resolve().then(function () { return __importStar(require('./PluginDrawer/index')); }); });
exports.PluginDrawer = PluginDrawer;
var DisplayModeToggle = component_1.default(function () { return Promise.resolve().then(function () { return __importStar(require('./DisplayModeToggle/index')); }); });
exports.DisplayModeToggle = DisplayModeToggle;
var BottomToolbar = component_1.default(function () { return Promise.resolve().then(function () { return __importStar(require('./BottomToolbar/index')); }); });
exports.BottomToolbar = BottomToolbar;
var EditorUI = component_1.default(function () { return Promise.resolve().then(function () { return __importStar(require('./EditorUI/index')); }); });
exports.EditorUI = EditorUI;
var ThemeProvider = component_1.default(function () { return Promise.resolve().then(function () { return __importStar(require('./ThemeProvider/index')); }); });
exports.ThemeProvider = ThemeProvider;
var ImageUpload = component_1.default(function () { return Promise.resolve().then(function () { return __importStar(require('./ImageUpload/index')); }); });
exports.ImageUpload = ImageUpload;
var ColorPicker = component_1.default(function () { return Promise.resolve().then(function () { return __importStar(require('./ColorPicker/index')); }); });
exports.ColorPicker = ColorPicker;
exports.default = EditorUI;
var Toolbar = PluginDrawer;
exports.Toolbar = Toolbar;
//# sourceMappingURL=index.js.map