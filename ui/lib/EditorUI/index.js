"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var index_1 = __importDefault(require("../DisplayModeToggle/index"));
var ThemeProvider_1 = __importDefault(require("../ThemeProvider"));
var index_2 = __importDefault(require("../PluginDrawer/index"));
var index_3 = __importDefault(require("../Trash/index"));
exports.default = react_1.default.memo(function (_a) {
    var _b = _a.stickyNess, stickyNess = _b === void 0 ? {
        shouldStickToTop: false,
        shouldStickToBottom: false,
        rightOffset: 0,
    } : _b, _c = _a.hideEditorSidebar, hideEditorSidebar = _c === void 0 ? false : _c;
    return (react_1.default.createElement(ThemeProvider_1.default, null,
        react_1.default.createElement(index_3.default, null),
        !hideEditorSidebar && react_1.default.createElement(index_1.default, { stickyNess: stickyNess }),
        react_1.default.createElement(index_2.default, null)));
});
//# sourceMappingURL=index.js.map