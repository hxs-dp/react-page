"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@material-ui/core");
var Delete_1 = __importDefault(require("@material-ui/icons/Delete"));
var React = __importStar(require("react"));
var DraftSwitch_1 = __importDefault(require("../DraftSwitch"));
var DuplicateButton_1 = __importDefault(require("../DuplicateButton"));
var I18nTools_1 = __importDefault(require("../I18nTools"));
var SelectParentButton_1 = __importDefault(require("../SelectParentButton"));
var core_2 = require("@react-page/core");
var Tools = function (_a) {
    var id = _a.id;
    var removeCell = core_2.useRemoveCell();
    return (React.createElement("div", { style: { display: 'flex', alignItems: 'center' } },
        React.createElement(I18nTools_1.default, { id: id }),
        React.createElement(DraftSwitch_1.default, { id: id }),
        React.createElement(DuplicateButton_1.default, { id: id }),
        React.createElement(SelectParentButton_1.default, { id: id }),
        React.createElement(core_1.Tooltip, { title: "Remove Plugin" },
            React.createElement(core_1.IconButton, { onClick: function () { return removeCell(id); }, "aria-label": "delete", color: "secondary" },
                React.createElement(Delete_1.default, null)))));
};
exports.default = React.memo(Tools);
//# sourceMappingURL=Tools.js.map