"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@material-ui/core");
var Drawer_1 = __importDefault(require("@material-ui/core/Drawer"));
var FormatSize_1 = __importDefault(require("@material-ui/icons/FormatSize"));
var React = __importStar(require("react"));
var ThemeProvider_1 = __importStar(require("../ThemeProvider"));
var Tools_1 = __importDefault(require("./Tools"));
var darkBlack = 'rgba(0, 0, 0, 0.87)';
var bright = 'rgba(255,255,255, 0.98)';
var brightBorder = 'rgba(0, 0, 0, 0.12)';
var SIZES = [1, 0.8, 0.6, 1.2];
var lastSize = SIZES[0]; // poor mans redux
var BottomToolbar = function (_a) {
    var _b = _a.open, open = _b === void 0 ? false : _b, children = _a.children, className = _a.className, _c = _a.dark, dark = _c === void 0 ? false : _c, theme = _a.theme, _d = _a.anchor, anchor = _d === void 0 ? 'bottom' : _d, title = _a.title, _e = _a.icon, icon = _e === void 0 ? null : _e, id = _a.id;
    var _f = __read(React.useState(lastSize), 2), size = _f[0], setSize = _f[1];
    var toggleSize = function () {
        var newSize = SIZES[(SIZES.indexOf(size) + 1) % SIZES.length];
        setSize(newSize);
        // poor man's redux
        lastSize = newSize;
    };
    return (React.createElement(ThemeProvider_1.default, { theme: theme ? theme : dark ? ThemeProvider_1.darkTheme : null },
        React.createElement(Drawer_1.default, { SlideProps: {
                unmountOnExit: true,
            }, variant: "persistent", className: className, open: open, anchor: anchor, PaperProps: {
                style: {
                    backgroundColor: 'transparent',
                    border: 'none',
                    overflow: 'visible',
                    pointerEvents: 'none',
                },
            } },
            React.createElement("div", { style: {
                    pointerEvents: 'all',
                    border: (dark ? darkBlack : brightBorder) + " 1px solid",
                    borderRadius: '4px 4px 0 0',
                    backgroundColor: dark ? darkBlack : bright,
                    padding: '12px 24px',
                    margin: 'auto',
                    boxShadow: '0px 1px 8px -1px rgba(0,0,0,0.4)',
                    position: 'relative',
                    minWidth: '50vw',
                    maxWidth: 'calc(100vw - 220px)',
                    transformOrigin: 'bottom',
                    transform: "scale(" + size + ")",
                    transition: '0.3s',
                } },
                children,
                React.createElement(core_1.Divider, { style: {
                        marginLeft: -24,
                        marginRight: -24,
                        marginTop: 12,
                        marginBottom: 12,
                    } }),
                React.createElement(React.Fragment, null,
                    React.createElement(core_1.Grid, { container: true, direction: "row", alignItems: "center" },
                        icon || title ? (React.createElement(core_1.Grid, { item: true },
                            React.createElement(core_1.Avatar, { children: icon || (title ? title[0] : ''), style: {
                                    marginRight: 16,
                                } }))) : null,
                        React.createElement(core_1.Grid, { item: true },
                            React.createElement(core_1.Typography, { variant: "subtitle1" }, title)),
                        React.createElement(core_1.Grid, { item: true },
                            React.createElement(core_1.Tooltip, { title: "Toggle Size" },
                                React.createElement(core_1.IconButton, { onClick: toggleSize, "aria-label": "toggle Size", color: "primary" },
                                    React.createElement(FormatSize_1.default, null)))),
                        React.createElement(core_1.Grid, { item: true, style: { marginLeft: 'auto' } },
                            React.createElement(Tools_1.default, { id: id }))))))));
};
exports.default = React.memo(BottomToolbar);
//# sourceMappingURL=index.js.map