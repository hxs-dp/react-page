import * as React from 'react';
export interface Translations {
    noPluginFoundContent: string | JSX.Element;
    searchPlaceholder: string;
    layoutPlugins: string | JSX.Element;
    contentPlugins: string | JSX.Element;
    insertPlugin: string | JSX.Element;
    dragMe: string;
}
declare const _default: React.NamedExoticComponent<{}>;
export default _default;
//# sourceMappingURL=index.d.ts.map