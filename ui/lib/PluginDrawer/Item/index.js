"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var Avatar_1 = __importDefault(require("@material-ui/core/Avatar"));
var ListItem_1 = __importDefault(require("@material-ui/core/ListItem"));
var ListItemText_1 = __importDefault(require("@material-ui/core/ListItemText"));
var core_1 = require("@react-page/core");
var React = __importStar(require("react"));
var index_1 = __importDefault(require("../Draggable/index"));
var Item = function (_a) {
    var plugin = _a.plugin, insert = _a.insert, translations = _a.translations;
    if (!plugin.IconComponent && !plugin.text) {
        return null;
    }
    var insertAtEnd = core_1.useInsertCellAtTheEnd();
    var Draggable = index_1.default(plugin.name);
    return (React.createElement(Draggable, { insert: insert },
        React.createElement(ListItem_1.default, { title: "Click to add or drag and drop it somwhere on your page!", className: "ory-plugin-drawer-item", onClick: function () { return insertAtEnd(insert); } },
            React.createElement(Avatar_1.default, { children: plugin.IconComponent || plugin.text[0], style: {
                    marginRight: 16,
                } }),
            React.createElement(ListItemText_1.default, { primary: plugin.text, secondary: plugin.description }))));
};
exports.default = Item;
//# sourceMappingURL=index.js.map