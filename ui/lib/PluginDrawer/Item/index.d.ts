import { Plugin } from '@react-page/core';
import * as React from 'react';
import { Translations } from '..';
interface ItemProps {
    plugin: Plugin;
    insert: any;
    translations: Translations;
}
declare const Item: React.FC<ItemProps>;
export default Item;
//# sourceMappingURL=index.d.ts.map