"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@material-ui/core");
var Translate_1 = __importDefault(require("@material-ui/icons/Translate"));
var core_2 = require("@react-page/core");
var react_1 = __importStar(require("react"));
var SelectLang_1 = __importDefault(require("./SelectLang"));
var I18nDialog_1 = __importDefault(require("./I18nDialog"));
var I18nTools = function (_a) {
    var _b;
    var id = _a.id;
    var editor = core_2.useEditor();
    var _c = __read(react_1.useState(false), 2), showI18nDialog = _c[0], setShowI18nDialog = _c[1];
    var hasI18n = ((_b = editor.languages) === null || _b === void 0 ? void 0 : _b.length) > 0;
    var onClose = function () { return setShowI18nDialog(false); };
    if (!hasI18n) {
        return null;
    }
    return (react_1.default.createElement(react_1.default.Fragment, null,
        react_1.default.createElement(core_1.Dialog, { open: showI18nDialog, onClose: onClose },
            react_1.default.createElement(I18nDialog_1.default, { id: id, onClose: onClose })),
        react_1.default.createElement("div", { style: { display: 'flex', alignItems: 'center' } },
            react_1.default.createElement(core_1.IconButton, { onClick: function () { return setShowI18nDialog(true); }, "aria-label": "i18n", color: "secondary" },
                react_1.default.createElement(Translate_1.default, null)),
            react_1.default.createElement(SelectLang_1.default, null))));
};
exports.default = I18nTools;
//# sourceMappingURL=index.js.map