"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@material-ui/core");
var Translate_1 = __importDefault(require("@material-ui/icons/Translate"));
var core_2 = require("@react-page/core");
var react_1 = __importDefault(require("react"));
var DraftSwitch_1 = __importDefault(require("../DraftSwitch"));
var SelectLang_1 = __importDefault(require("./SelectLang"));
var I18nDialog = function (_a) {
    var _b, _c, _d;
    var id = _a.id, onClose = _a.onClose;
    var currentLang = core_2.useLang();
    var editor = core_2.useEditor();
    var node = core_2.useCell(id);
    var setLang = core_2.useSetLang();
    var contentOrLayout = (_b = node.layout) !== null && _b !== void 0 ? _b : node.content;
    var updateCellContent = core_2.useUpdateCellContent();
    var updateCellLayout = core_2.useUpdateCellLayout();
    var reset = function (lang) {
        if (node.layout) {
            updateCellLayout(id, null, lang);
        }
        else {
            updateCellContent(id, null, lang);
        }
    };
    var defaultLangLabel = (_d = (_c = editor.languages) === null || _c === void 0 ? void 0 : _c[0]) === null || _d === void 0 ? void 0 : _d.label;
    return (react_1.default.createElement(core_1.DialogContent, null,
        react_1.default.createElement("div", { style: { display: 'flex', alignItems: 'center' } },
            react_1.default.createElement(Translate_1.default, { style: { marginRight: 'auto' } }),
            " ",
            react_1.default.createElement(SelectLang_1.default, null)),
        react_1.default.createElement("hr", null),
        react_1.default.createElement(core_1.Table, null,
            react_1.default.createElement("tbody", null, editor.languages.map(function (l, index) {
                var _a;
                var state = (_a = contentOrLayout.stateI18n) === null || _a === void 0 ? void 0 : _a[l.lang];
                var isCurrent = currentLang === l.lang;
                var hasState = Boolean(state);
                return (react_1.default.createElement("tr", { key: l.lang },
                    react_1.default.createElement("th", { style: {
                            textAlign: 'left',
                            textDecoration: isCurrent ? 'underline' : undefined,
                        } },
                        react_1.default.createElement(core_1.Button, { onClick: function () { return setLang(l.lang); } },
                            l.label,
                            " ",
                            index === 0 ? '(default)' : null)),
                    react_1.default.createElement("td", null,
                        react_1.default.createElement(DraftSwitch_1.default, { id: id, lang: l.lang })),
                    react_1.default.createElement("td", null, hasState ? '✔️' : ' '),
                    react_1.default.createElement("td", null, hasState && index !== 0 ? (react_1.default.createElement(core_1.Button, { onClick: function () {
                            reset(l.lang);
                        } },
                        "Reset to ",
                        defaultLangLabel,
                        " \u26A0\uFE0F")) : null)));
            }))),
        react_1.default.createElement(core_1.Button, { onClick: function () { return onClose(); } }, "Close")));
};
exports.default = I18nDialog;
//# sourceMappingURL=I18nDialog.js.map