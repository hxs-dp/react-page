"use strict";
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@react-page/core");
var react_1 = __importDefault(require("react"));
var reselect_1 = require("reselect");
var core_2 = require("@material-ui/core");
var SelectLang = function (_a) {
    var _b;
    var _c = _a.lang, lang = _c === void 0 ? null : _c, setLang = _a.setLang, rest = __rest(_a, ["lang", "setLang"]);
    var editor = core_1.useEditor();
    if (((_b = editor.languages) === null || _b === void 0 ? void 0 : _b.length) > 0) {
        return (react_1.default.createElement(core_2.Select, { value: lang, onChange: function (e) { return setLang(e.target.value); } }, editor.languages.map(function (l) { return (react_1.default.createElement("option", { key: l.lang, value: l.lang }, l.label)); })));
    }
    return null;
};
var mapStateToProps = reselect_1.createStructuredSelector({
    lang: core_1.Selectors.Setting.getLang,
});
var mapDispatchToProps = {
    setLang: core_1.Actions.Setting.setLang,
};
exports.default = core_1.connect(mapStateToProps, mapDispatchToProps)(SelectLang);
//# sourceMappingURL=SelectLang.js.map