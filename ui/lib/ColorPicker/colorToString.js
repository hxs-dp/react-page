"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.colorToString = void 0;
exports.colorToString = function (c) {
    return c && 'rgba(' + c.r + ', ' + c.g + ', ' + c.b + ', ' + c.a + ')';
};
//# sourceMappingURL=colorToString.js.map