"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@material-ui/core");
var Visibility_1 = __importDefault(require("@material-ui/icons/Visibility"));
var VisibilityOff_1 = __importDefault(require("@material-ui/icons/VisibilityOff"));
var core_2 = require("@react-page/core");
var react_1 = __importDefault(require("react"));
var DraftSwitch = function (_a) {
    var _b, _c;
    var id = _a.id, lang = _a.lang;
    var node = core_2.useCell(id);
    var setDraft = core_2.useSetDraft();
    var currentLang = core_2.useLang();
    if (!node) {
        return null;
    }
    var theLang = lang !== null && lang !== void 0 ? lang : currentLang;
    var hasI18n = Boolean(node.isDraftI18n);
    var isDraft = (_c = (_b = node === null || node === void 0 ? void 0 : node.isDraftI18n) === null || _b === void 0 ? void 0 : _b[theLang]) !== null && _c !== void 0 ? _c : node === null || node === void 0 ? void 0 : node.isDraft; // fallback to legacy
    var title = isDraft ? 'Content is hidden' : 'Content is visible';
    return node ? (react_1.default.createElement(core_1.Tooltip, { title: title + (hasI18n ? ' in ' + theLang : '') },
        react_1.default.createElement(core_1.FormControlLabel, { style: { marginRight: 5 }, labelPlacement: "start", control: react_1.default.createElement(core_1.Switch, { color: "primary", checked: !isDraft, onChange: function (e) {
                    setDraft(id, !e.target.checked, theLang);
                } }), label: isDraft ? (react_1.default.createElement(VisibilityOff_1.default, { style: { marginTop: 5 } })) : (react_1.default.createElement(Visibility_1.default, { style: { marginTop: 5 } })) }))) : null;
};
exports.default = DraftSwitch;
//# sourceMappingURL=index.js.map