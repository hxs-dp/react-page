/// <reference types="react" />
declare const DraftSwitch: ({ id, lang }: {
    id: string;
    lang?: string;
}) => JSX.Element;
export default DraftSwitch;
//# sourceMappingURL=index.d.ts.map