"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var IconButton_1 = __importDefault(require("@material-ui/core/IconButton"));
var VerticalAlignTop_1 = __importDefault(require("@material-ui/icons/VerticalAlignTop"));
var core_1 = require("@react-page/core");
var react_1 = __importStar(require("react"));
var SelectParentButton = function (_a) {
    var id = _a.id;
    var parentCell = core_1.useParentCell(id);
    var focusCell = core_1.useFocusCell();
    var onClick = react_1.useCallback(function () { return focusCell(parentCell === null || parentCell === void 0 ? void 0 : parentCell.id); }, [
        parentCell === null || parentCell === void 0 ? void 0 : parentCell.id,
    ]);
    return parentCell ? (react_1.default.createElement(IconButton_1.default, { className: "bottomToolbar__selectParentButton", onClick: onClick, color: "default", title: "Select parent" },
        react_1.default.createElement(VerticalAlignTop_1.default, null))) : null;
};
exports.default = SelectParentButton;
//# sourceMappingURL=index.js.map