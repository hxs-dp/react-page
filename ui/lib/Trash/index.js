"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var Fab_1 = __importDefault(require("@material-ui/core/Fab"));
var Delete_1 = __importDefault(require("@material-ui/icons/Delete"));
var core_1 = require("@react-page/core");
var classnames_1 = __importDefault(require("classnames"));
var lodash_throttle_1 = __importDefault(require("lodash.throttle"));
var React = __importStar(require("react"));
var target = {
    hover: lodash_throttle_1.default(
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    function (props, monitor) {
        var item = monitor.getItem();
        if (monitor.isOver({ shallow: true })) {
            item.clearHover();
        }
    }, 200, { trailing: false }),
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    drop: function (props, monitor) {
        var item = monitor.getItem();
        if (monitor.didDrop() || !monitor.isOver({ shallow: true })) {
            // If the item drop occurred deeper down the tree, don't do anything
            return;
        }
        props.removeCell(item.id);
    },
};
// eslint-disable-next-line @typescript-eslint/no-explicit-any
var connectMonitor = function (_connect, monitor) { return ({
    connectDropTarget: _connect.dropTarget(),
    isOverCurrent: monitor.isOver({ shallow: true }),
}); };
var Raw = function (_a) {
    var isOverCurrent = _a.isOverCurrent, connectDropTarget = _a.connectDropTarget;
    var isLayoutMode = core_1.useIsLayoutMode();
    return connectDropTarget(React.createElement("div", { className: classnames_1.default('ory-controls-trash', {
            'ory-controls-trash-active': isLayoutMode,
        }) },
        React.createElement(Fab_1.default, { color: "secondary", disabled: !isOverCurrent },
            React.createElement(Delete_1.default, null))));
};
var types = function (_a) {
    var editor = _a.editor;
    var plugins = editor.plugins.getRegisteredNames();
    if (editor.plugins.hasNativePlugin()) {
        plugins.push(editor.plugins.getNativePlugin()().name);
    }
    return plugins;
};
// eslint-disable-next-line @typescript-eslint/no-explicit-any
var Decorated = core_1.DropTarget(types, target, connectMonitor)(Raw);
var Trash = function () {
    var editor = core_1.useEditor();
    var removeCell = core_1.useRemoveCell();
    return React.createElement(Decorated, { editor: editor, removeCell: removeCell });
};
exports.default = Trash;
//# sourceMappingURL=index.js.map