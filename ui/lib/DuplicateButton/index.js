"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@material-ui/core");
var FileCopy_1 = __importDefault(require("@material-ui/icons/FileCopy"));
var core_2 = require("@react-page/core");
var react_1 = __importDefault(require("react"));
var DuplicateButton = function (_a) {
    var id = _a.id;
    var duplicateCell = core_2.useDuplicateCell();
    return (react_1.default.createElement(core_1.Tooltip, { title: "Duplicate Plugin" },
        react_1.default.createElement(core_1.IconButton, { onClick: function () { return duplicateCell(id); }, "aria-label": "delete", color: "default" },
            react_1.default.createElement(FileCopy_1.default, null))));
};
exports.default = DuplicateButton;
//# sourceMappingURL=index.js.map