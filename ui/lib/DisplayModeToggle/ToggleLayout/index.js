"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = __importStar(require("react"));
var ViewQuilt_1 = __importDefault(require("@material-ui/icons/ViewQuilt"));
var Button_1 = __importDefault(require("../Button"));
var core_1 = require("@react-page/core");
var reselect_1 = require("reselect");
var Inner = function (props) { return (React.createElement(Button_1.default, { icon: React.createElement(ViewQuilt_1.default, null), description: props.label, active: props.isLayoutMode, onClick: props.layoutMode })); };
var mapStateToProps = reselect_1.createStructuredSelector({
    isLayoutMode: core_1.Selectors.Display.isLayoutMode,
});
var mapDispatchToProps = { layoutMode: core_1.Actions.Display.layoutMode };
exports.default = core_1.connect(mapStateToProps, mapDispatchToProps)(Inner);
//# sourceMappingURL=index.js.map