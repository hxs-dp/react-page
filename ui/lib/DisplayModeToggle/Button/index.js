"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = __importStar(require("react"));
var Fab_1 = __importDefault(require("@material-ui/core/Fab"));
var core_1 = require("@material-ui/core");
var DisplayModeToggle = function (_a) {
    var description = _a.description, icon = _a.icon, onClick = _a.onClick, active = _a.active, disabled = _a.disabled;
    var theme = core_1.useTheme();
    var isLarge = core_1.useMediaQuery(theme.breakpoints.up('sm'));
    return (React.createElement("div", { className: "ory-controls-mode-toggle-button" },
        React.createElement("div", { className: "ory-controls-mode-toggle-button-inner" },
            React.createElement(Fab_1.default, { color: active ? 'secondary' : 'default', size: isLarge ? 'large' : 'small', onClick: onClick, disabled: disabled }, icon)),
        React.createElement("div", { className: "ory-controls-mode-toggle-button-description" }, description)));
};
exports.default = DisplayModeToggle;
//# sourceMappingURL=index.js.map