"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = __importStar(require("react"));
var index_1 = __importDefault(require("./ToggleEdit/index"));
var index_2 = __importDefault(require("./ToggleInsert/index"));
var index_3 = __importDefault(require("./ToggleLayout/index"));
var index_4 = __importDefault(require("./TogglePreview/index"));
var index_5 = __importDefault(require("./ToggleResize/index"));
var defaultTranslations = {
    edit: 'Edycja',
    insert: 'Dodaj',
    layout: 'Przesuń',
    resize: 'Zmień rozmiar',
    preview: 'Podgląd',
};
var getStickyNessstyle = function (stickyness) {
    if (!stickyness ||
        (!stickyness.shouldStickToBottom && !stickyness.shouldStickToTop)) {
        return {
            position: 'fixed',
        };
    }
    return {
        position: 'absolute',
        bottom: stickyness.shouldStickToBottom ? 0 : 'auto',
        top: stickyness.shouldStickToTop ? 0 : 'auto',
        right: -stickyness.rightOffset || 0,
    };
};
var Inner = function (_a) {
    var stickyNess = _a.stickyNess, _b = _a.translations, translations = _b === void 0 ? defaultTranslations : _b;
    return (React.createElement("div", { className: "ory-controls-mode-toggle-control-group", style: __assign({ position: 'fixed', zIndex: 10001, bottom: 0, right: 0, display: 'flex', maxHeight: '100%' }, getStickyNessstyle(stickyNess)) },
        React.createElement("div", { ref: stickyNess.stickyElRef, style: {
                padding: 16,
                position: 'relative',
                flexFlow: 'column wrap',
                direction: 'rtl',
                display: 'flex',
            } },
            React.createElement("div", { className: "ory-controls-mode-toggle-control" },
                React.createElement(index_1.default, { label: translations.edit }),
                React.createElement("div", { className: "ory-controls-mode-toggle-clearfix" })),
            React.createElement("div", { className: "ory-controls-mode-toggle-control" },
                React.createElement(index_2.default, { label: translations.insert }),
                React.createElement("div", { className: "ory-controls-mode-toggle-clearfix" })),
            React.createElement("div", { className: "ory-controls-mode-toggle-control" },
                React.createElement(index_3.default, { label: translations.layout }),
                React.createElement("div", { className: "ory-controls-mode-toggle-clearfix" })),
            React.createElement("div", { className: "ory-controls-mode-toggle-control" },
                React.createElement(index_5.default, { label: translations.resize }),
                React.createElement("div", { className: "ory-controls-mode-toggle-clearfix" })),
            React.createElement("div", { className: "ory-controls-mode-toggle-control" },
                React.createElement(index_4.default, { label: translations.preview }),
                React.createElement("div", { className: "ory-controls-mode-toggle-clearfix" })))));
};
exports.default = Inner;
//# sourceMappingURL=index.js.map