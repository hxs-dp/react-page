"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = __importStar(require("react"));
var SettingsOverscan_1 = __importDefault(require("@material-ui/icons/SettingsOverscan"));
var core_1 = require("@react-page/core");
var reselect_1 = require("reselect");
var index_1 = __importDefault(require("../Button/index"));
var Inner = function (props) { return (React.createElement(index_1.default, { icon: React.createElement(SettingsOverscan_1.default, null), description: props.label, active: props.isResizeMode, onClick: props.resizeMode })); };
var mapStateToProps = reselect_1.createStructuredSelector({
    isResizeMode: core_1.Selectors.Display.isResizeMode,
});
var mapDispatchToProps = { resizeMode: core_1.Actions.Display.resizeMode };
exports.default = core_1.connect(mapStateToProps, mapDispatchToProps)(Inner);
//# sourceMappingURL=index.js.map