export declare const defaultTranslations: {
    buttonContent: string;
    noFileError: string;
    badExtensionError: string;
    tooBigError: string;
    uploadingError: string;
    unknownError: string;
};
//# sourceMappingURL=defaultTranslations.d.ts.map