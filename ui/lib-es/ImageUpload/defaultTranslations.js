export var defaultTranslations = {
    buttonContent: 'Upload image',
    noFileError: 'No file selected',
    badExtensionError: 'Bad file type',
    tooBigError: 'Too big',
    uploadingError: 'Error while uploading',
    unknownError: 'Unknown error',
};
//# sourceMappingURL=defaultTranslations.js.map