import { FormControlLabel, Switch, Tooltip } from '@material-ui/core';
import VisibleIcon from '@material-ui/icons/Visibility';
import NonVisibleIcon from '@material-ui/icons/VisibilityOff';
import { useCell, useLang, useSetDraft } from '@react-page/core';
import React from 'react';
var DraftSwitch = function (_a) {
    var _b, _c;
    var id = _a.id, lang = _a.lang;
    var node = useCell(id);
    var setDraft = useSetDraft();
    var currentLang = useLang();
    if (!node) {
        return null;
    }
    var theLang = lang !== null && lang !== void 0 ? lang : currentLang;
    var hasI18n = Boolean(node.isDraftI18n);
    var isDraft = (_c = (_b = node === null || node === void 0 ? void 0 : node.isDraftI18n) === null || _b === void 0 ? void 0 : _b[theLang]) !== null && _c !== void 0 ? _c : node === null || node === void 0 ? void 0 : node.isDraft; // fallback to legacy
    var title = isDraft ? 'Content is hidden' : 'Content is visible';
    return node ? (React.createElement(Tooltip, { title: title + (hasI18n ? ' in ' + theLang : '') },
        React.createElement(FormControlLabel, { style: { marginRight: 5 }, labelPlacement: "start", control: React.createElement(Switch, { color: "primary", checked: !isDraft, onChange: function (e) {
                    setDraft(id, !e.target.checked, theLang);
                } }), label: isDraft ? (React.createElement(NonVisibleIcon, { style: { marginTop: 5 } })) : (React.createElement(VisibleIcon, { style: { marginTop: 5 } })) }))) : null;
};
export default DraftSwitch;
//# sourceMappingURL=index.js.map