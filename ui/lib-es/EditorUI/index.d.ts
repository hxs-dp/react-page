import React from 'react';
import { StickyNess } from '../DisplayModeToggle/index';
declare const _default: React.MemoExoticComponent<({ stickyNess, hideEditorSidebar, }: {
    stickyNess?: StickyNess;
    hideEditorSidebar?: boolean;
}) => JSX.Element>;
export default _default;
//# sourceMappingURL=index.d.ts.map