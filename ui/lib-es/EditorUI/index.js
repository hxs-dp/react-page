import React from 'react';
import DisplayModeToggle from '../DisplayModeToggle/index';
import ThemeProvider from '../ThemeProvider';
import PluginDrawer from '../PluginDrawer/index';
import Trash from '../Trash/index';
export default React.memo(function (_a) {
    var _b = _a.stickyNess, stickyNess = _b === void 0 ? {
        shouldStickToTop: false,
        shouldStickToBottom: false,
        rightOffset: 0,
    } : _b, _c = _a.hideEditorSidebar, hideEditorSidebar = _c === void 0 ? false : _c;
    return (React.createElement(ThemeProvider, null,
        React.createElement(Trash, null),
        !hideEditorSidebar && React.createElement(DisplayModeToggle, { stickyNess: stickyNess }),
        React.createElement(PluginDrawer, null)));
});
//# sourceMappingURL=index.js.map