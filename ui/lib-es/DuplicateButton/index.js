import { IconButton, Tooltip } from '@material-ui/core';
import Icon from '@material-ui/icons/FileCopy';
import { useDuplicateCell } from '@react-page/core';
import React from 'react';
var DuplicateButton = function (_a) {
    var id = _a.id;
    var duplicateCell = useDuplicateCell();
    return (React.createElement(Tooltip, { title: "Duplicate Plugin" },
        React.createElement(IconButton, { onClick: function () { return duplicateCell(id); }, "aria-label": "delete", color: "default" },
            React.createElement(Icon, null))));
};
export default DuplicateButton;
//# sourceMappingURL=index.js.map