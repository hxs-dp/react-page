import * as React from 'react';
declare const defaultTranslations: {
    edit: string;
    insert: string;
    layout: string;
    resize: string;
    preview: string;
};
export declare type StickyNess = {
    shouldStickToTop: boolean;
    shouldStickToBottom: boolean;
    rightOffset: number;
    stickyElRef?: React.Ref<HTMLDivElement>;
};
declare const Inner: React.SFC<{
    translations?: typeof defaultTranslations;
    stickyNess?: StickyNess;
}>;
export default Inner;
//# sourceMappingURL=index.d.ts.map