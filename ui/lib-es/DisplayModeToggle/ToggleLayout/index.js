import * as React from 'react';
import ViewQuilt from '@material-ui/icons/ViewQuilt';
import Button from '../Button';
import { connect, Selectors, Actions } from '@react-page/core';
import { createStructuredSelector } from 'reselect';
var Inner = function (props) { return (React.createElement(Button, { icon: React.createElement(ViewQuilt, null), description: props.label, active: props.isLayoutMode, onClick: props.layoutMode })); };
var mapStateToProps = createStructuredSelector({
    isLayoutMode: Selectors.Display.isLayoutMode,
});
var mapDispatchToProps = { layoutMode: Actions.Display.layoutMode };
export default connect(mapStateToProps, mapDispatchToProps)(Inner);
//# sourceMappingURL=index.js.map