import * as React from 'react';
import ContentAdd from '@material-ui/icons/Add';
import Button from '../Button/index';
import { connect, Actions, Selectors } from '@react-page/core';
import { createStructuredSelector } from 'reselect';
var Inner = function (props) { return (React.createElement(Button, { icon: React.createElement(ContentAdd, null), description: props.label, active: props.isInsertMode, onClick: props.insertMode })); };
var mapStateToProps = createStructuredSelector({
    isInsertMode: Selectors.Display.isInsertMode,
});
var mapDispatchToProps = { insertMode: Actions.Display.insertMode };
export default connect(mapStateToProps, mapDispatchToProps)(Inner);
//# sourceMappingURL=index.js.map