import * as React from 'react';
import Resize from '@material-ui/icons/SettingsOverscan';
import { connect, Selectors, Actions } from '@react-page/core';
import { createStructuredSelector } from 'reselect';
import Button from '../Button/index';
var Inner = function (props) { return (React.createElement(Button, { icon: React.createElement(Resize, null), description: props.label, active: props.isResizeMode, onClick: props.resizeMode })); };
var mapStateToProps = createStructuredSelector({
    isResizeMode: Selectors.Display.isResizeMode,
});
var mapDispatchToProps = { resizeMode: Actions.Display.resizeMode };
export default connect(mapStateToProps, mapDispatchToProps)(Inner);
//# sourceMappingURL=index.js.map