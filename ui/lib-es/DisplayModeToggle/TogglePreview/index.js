import * as React from 'react';
import Devices from '@material-ui/icons/Devices';
import { connect, Actions, Selectors } from '@react-page/core';
import { createStructuredSelector } from 'reselect';
import Button from '../Button/index';
var Inner = function (props) { return (React.createElement(Button, { icon: React.createElement(Devices, null), description: props.label, active: props.isPreviewMode, onClick: props.previewMode })); };
var mapStateToProps = createStructuredSelector({
    isPreviewMode: Selectors.Display.isPreviewMode,
});
var mapDispatchToProps = { previewMode: Actions.Display.previewMode };
export default connect(mapStateToProps, mapDispatchToProps)(Inner);
//# sourceMappingURL=index.js.map