var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import * as React from 'react';
import ToggleEdit from './ToggleEdit/index';
import ToggleInsert from './ToggleInsert/index';
import ToggleLayout from './ToggleLayout/index';
import TogglePreview from './TogglePreview/index';
import ToggleResize from './ToggleResize/index';
var defaultTranslations = {
    edit: 'Edycja',
    insert: 'Dodaj',
    layout: 'Przesuń',
    resize: 'Zmień rozmiar',
    preview: 'Podgląd',
};
var getStickyNessstyle = function (stickyness) {
    if (!stickyness ||
        (!stickyness.shouldStickToBottom && !stickyness.shouldStickToTop)) {
        return {
            position: 'fixed',
        };
    }
    return {
        position: 'absolute',
        bottom: stickyness.shouldStickToBottom ? 0 : 'auto',
        top: stickyness.shouldStickToTop ? 0 : 'auto',
        right: -stickyness.rightOffset || 0,
    };
};
var Inner = function (_a) {
    var stickyNess = _a.stickyNess, _b = _a.translations, translations = _b === void 0 ? defaultTranslations : _b;
    return (React.createElement("div", { className: "ory-controls-mode-toggle-control-group", style: __assign({ position: 'fixed', zIndex: 10001, bottom: 0, right: 0, display: 'flex', maxHeight: '100%' }, getStickyNessstyle(stickyNess)) },
        React.createElement("div", { ref: stickyNess.stickyElRef, style: {
                padding: 16,
                position: 'relative',
                flexFlow: 'column wrap',
                direction: 'rtl',
                display: 'flex',
            } },
            React.createElement("div", { className: "ory-controls-mode-toggle-control" },
                React.createElement(ToggleEdit, { label: translations.edit }),
                React.createElement("div", { className: "ory-controls-mode-toggle-clearfix" })),
            React.createElement("div", { className: "ory-controls-mode-toggle-control" },
                React.createElement(ToggleInsert, { label: translations.insert }),
                React.createElement("div", { className: "ory-controls-mode-toggle-clearfix" })),
            React.createElement("div", { className: "ory-controls-mode-toggle-control" },
                React.createElement(ToggleLayout, { label: translations.layout }),
                React.createElement("div", { className: "ory-controls-mode-toggle-clearfix" })),
            React.createElement("div", { className: "ory-controls-mode-toggle-control" },
                React.createElement(ToggleResize, { label: translations.resize }),
                React.createElement("div", { className: "ory-controls-mode-toggle-clearfix" })),
            React.createElement("div", { className: "ory-controls-mode-toggle-control" },
                React.createElement(TogglePreview, { label: translations.preview }),
                React.createElement("div", { className: "ory-controls-mode-toggle-clearfix" })))));
};
export default Inner;
//# sourceMappingURL=index.js.map