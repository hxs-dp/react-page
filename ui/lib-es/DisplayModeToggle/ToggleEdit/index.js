import * as React from 'react';
import Create from '@material-ui/icons/Create';
import Button from '../Button/index';
import { connect, Actions, Selectors } from '@react-page/core';
import { createStructuredSelector } from 'reselect';
var Inner = function (props) { return (React.createElement(Button, { icon: React.createElement(Create, null), description: props.label, active: props.isEditMode, onClick: props.editMode })); };
var mapStateToProps = createStructuredSelector({
    isEditMode: Selectors.Display.isEditMode,
});
var mapDispatchToProps = { editMode: Actions.Display.editMode };
export default connect(mapStateToProps, mapDispatchToProps)(Inner);
//# sourceMappingURL=index.js.map