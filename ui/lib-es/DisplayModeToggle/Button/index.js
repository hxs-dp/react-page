import * as React from 'react';
import Fab from '@material-ui/core/Fab';
import { useTheme, useMediaQuery } from '@material-ui/core';
var DisplayModeToggle = function (_a) {
    var description = _a.description, icon = _a.icon, onClick = _a.onClick, active = _a.active, disabled = _a.disabled;
    var theme = useTheme();
    var isLarge = useMediaQuery(theme.breakpoints.up('sm'));
    return (React.createElement("div", { className: "ory-controls-mode-toggle-button" },
        React.createElement("div", { className: "ory-controls-mode-toggle-button-inner" },
            React.createElement(Fab, { color: active ? 'secondary' : 'default', size: isLarge ? 'large' : 'small', onClick: onClick, disabled: disabled }, icon)),
        React.createElement("div", { className: "ory-controls-mode-toggle-button-description" }, description)));
};
export default DisplayModeToggle;
//# sourceMappingURL=index.js.map