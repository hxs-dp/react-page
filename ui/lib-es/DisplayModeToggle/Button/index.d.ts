import * as React from 'react';
declare const DisplayModeToggle: ({ description, icon, onClick, active, disabled, }: {
    description: string;
    icon: JSX.Element;
    active: boolean;
    disabled?: boolean;
    onClick: React.MouseEventHandler<HTMLElement>;
}) => JSX.Element;
export default DisplayModeToggle;
//# sourceMappingURL=index.d.ts.map