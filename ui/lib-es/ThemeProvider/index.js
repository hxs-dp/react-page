var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import * as React from 'react';
import { createMuiTheme } from '@material-ui/core/styles';
import { ThemeProvider as MaterialUiThemeProvider, StylesProvider, } from '@material-ui/styles';
import { createGenerateClassName } from '@material-ui/styles';
import darkTheme from './DarkTheme/index';
import { themeOptions } from './themeOptions';
export { darkTheme };
var generateClassName = createGenerateClassName({
    disableGlobal: true,
    productionPrefix: 'ory',
});
var theme = createMuiTheme(themeOptions);
var ThemeProvider = /** @class */ (function (_super) {
    __extends(ThemeProvider, _super);
    function ThemeProvider() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    ThemeProvider.prototype.render = function () {
        return (React.createElement(StylesProvider, { injectFirst: true, generateClassName: generateClassName },
            React.createElement(MaterialUiThemeProvider, { theme: this.props.theme || theme }, this.props.children)));
    };
    return ThemeProvider;
}(React.Component));
export default ThemeProvider;
//# sourceMappingURL=index.js.map