import { createMuiTheme } from '@material-ui/core/styles';
var theme = createMuiTheme({
    palette: {
        type: 'dark',
    },
});
export default theme;
//# sourceMappingURL=index.js.map