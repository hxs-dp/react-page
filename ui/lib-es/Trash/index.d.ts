import { Editor } from '@react-page/core';
import * as React from 'react';
export interface RawProps {
    editor: Editor;
    isLayoutMode: boolean;
    isOverCurrent: boolean;
    connectDropTarget: (node: JSX.Element) => JSX.Element;
}
declare const Trash: React.SFC;
export default Trash;
//# sourceMappingURL=index.d.ts.map