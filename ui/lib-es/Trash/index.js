import Fab from '@material-ui/core/Fab';
import Delete from '@material-ui/icons/Delete';
import { DropTarget, useEditor, useIsLayoutMode, useRemoveCell, } from '@react-page/core';
import classNames from 'classnames';
import throttle from 'lodash.throttle';
import * as React from 'react';
var target = {
    hover: throttle(
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    function (props, monitor) {
        var item = monitor.getItem();
        if (monitor.isOver({ shallow: true })) {
            item.clearHover();
        }
    }, 200, { trailing: false }),
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    drop: function (props, monitor) {
        var item = monitor.getItem();
        if (monitor.didDrop() || !monitor.isOver({ shallow: true })) {
            // If the item drop occurred deeper down the tree, don't do anything
            return;
        }
        props.removeCell(item.id);
    },
};
// eslint-disable-next-line @typescript-eslint/no-explicit-any
var connectMonitor = function (_connect, monitor) { return ({
    connectDropTarget: _connect.dropTarget(),
    isOverCurrent: monitor.isOver({ shallow: true }),
}); };
var Raw = function (_a) {
    var isOverCurrent = _a.isOverCurrent, connectDropTarget = _a.connectDropTarget;
    var isLayoutMode = useIsLayoutMode();
    return connectDropTarget(React.createElement("div", { className: classNames('ory-controls-trash', {
            'ory-controls-trash-active': isLayoutMode,
        }) },
        React.createElement(Fab, { color: "secondary", disabled: !isOverCurrent },
            React.createElement(Delete, null))));
};
var types = function (_a) {
    var editor = _a.editor;
    var plugins = editor.plugins.getRegisteredNames();
    if (editor.plugins.hasNativePlugin()) {
        plugins.push(editor.plugins.getNativePlugin()().name);
    }
    return plugins;
};
// eslint-disable-next-line @typescript-eslint/no-explicit-any
var Decorated = DropTarget(types, target, connectMonitor)(Raw);
var Trash = function () {
    var editor = useEditor();
    var removeCell = useRemoveCell();
    return React.createElement(Decorated, { editor: editor, removeCell: removeCell });
};
export default Trash;
//# sourceMappingURL=index.js.map