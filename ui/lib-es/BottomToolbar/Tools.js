import { IconButton, Tooltip } from '@material-ui/core';
import Delete from '@material-ui/icons/Delete';
import * as React from 'react';
import DraftSwitch from '../DraftSwitch';
import DuplicateButton from '../DuplicateButton';
import I18nTools from '../I18nTools';
import SelectParentButton from '../SelectParentButton';
import { useRemoveCell } from '@react-page/core';
var Tools = function (_a) {
    var id = _a.id;
    var removeCell = useRemoveCell();
    return (React.createElement("div", { style: { display: 'flex', alignItems: 'center' } },
        React.createElement(I18nTools, { id: id }),
        React.createElement(DraftSwitch, { id: id }),
        React.createElement(DuplicateButton, { id: id }),
        React.createElement(SelectParentButton, { id: id }),
        React.createElement(Tooltip, { title: "Remove Plugin" },
            React.createElement(IconButton, { onClick: function () { return removeCell(id); }, "aria-label": "delete", color: "secondary" },
                React.createElement(Delete, null)))));
};
export default React.memo(Tools);
//# sourceMappingURL=Tools.js.map