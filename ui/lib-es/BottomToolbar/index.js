var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
import { Avatar, Divider, Grid, IconButton, Tooltip, Typography, } from '@material-ui/core';
import Drawer from '@material-ui/core/Drawer';
import FormatSize from '@material-ui/icons/FormatSize';
import * as React from 'react';
import ThemeProvider, { darkTheme } from '../ThemeProvider';
import Tools from './Tools';
var darkBlack = 'rgba(0, 0, 0, 0.87)';
var bright = 'rgba(255,255,255, 0.98)';
var brightBorder = 'rgba(0, 0, 0, 0.12)';
var SIZES = [1, 0.8, 0.6, 1.2];
var lastSize = SIZES[0]; // poor mans redux
var BottomToolbar = function (_a) {
    var _b = _a.open, open = _b === void 0 ? false : _b, children = _a.children, className = _a.className, _c = _a.dark, dark = _c === void 0 ? false : _c, theme = _a.theme, _d = _a.anchor, anchor = _d === void 0 ? 'bottom' : _d, title = _a.title, _e = _a.icon, icon = _e === void 0 ? null : _e, id = _a.id;
    var _f = __read(React.useState(lastSize), 2), size = _f[0], setSize = _f[1];
    var toggleSize = function () {
        var newSize = SIZES[(SIZES.indexOf(size) + 1) % SIZES.length];
        setSize(newSize);
        // poor man's redux
        lastSize = newSize;
    };
    return (React.createElement(ThemeProvider, { theme: theme ? theme : dark ? darkTheme : null },
        React.createElement(Drawer, { SlideProps: {
                unmountOnExit: true,
            }, variant: "persistent", className: className, open: open, anchor: anchor, PaperProps: {
                style: {
                    backgroundColor: 'transparent',
                    border: 'none',
                    overflow: 'visible',
                    pointerEvents: 'none',
                },
            } },
            React.createElement("div", { style: {
                    pointerEvents: 'all',
                    border: (dark ? darkBlack : brightBorder) + " 1px solid",
                    borderRadius: '4px 4px 0 0',
                    backgroundColor: dark ? darkBlack : bright,
                    padding: '12px 24px',
                    margin: 'auto',
                    boxShadow: '0px 1px 8px -1px rgba(0,0,0,0.4)',
                    position: 'relative',
                    minWidth: '50vw',
                    maxWidth: 'calc(100vw - 220px)',
                    transformOrigin: 'bottom',
                    transform: "scale(" + size + ")",
                    transition: '0.3s',
                } },
                children,
                React.createElement(Divider, { style: {
                        marginLeft: -24,
                        marginRight: -24,
                        marginTop: 12,
                        marginBottom: 12,
                    } }),
                React.createElement(React.Fragment, null,
                    React.createElement(Grid, { container: true, direction: "row", alignItems: "center" },
                        icon || title ? (React.createElement(Grid, { item: true },
                            React.createElement(Avatar, { children: icon || (title ? title[0] : ''), style: {
                                    marginRight: 16,
                                } }))) : null,
                        React.createElement(Grid, { item: true },
                            React.createElement(Typography, { variant: "subtitle1" }, title)),
                        React.createElement(Grid, { item: true },
                            React.createElement(Tooltip, { title: "Toggle Size" },
                                React.createElement(IconButton, { onClick: toggleSize, "aria-label": "toggle Size", color: "primary" },
                                    React.createElement(FormatSize, null)))),
                        React.createElement(Grid, { item: true, style: { marginLeft: 'auto' } },
                            React.createElement(Tools, { id: id }))))))));
};
export default React.memo(BottomToolbar);
//# sourceMappingURL=index.js.map