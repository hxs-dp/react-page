/// <reference types="react" />
export declare type BottomToolbarProps = {
    open?: boolean;
    children?: React.ReactNode;
    className?: string;
    dark: boolean;
    title?: string;
    anchor?: 'top' | 'bottom' | 'left' | 'right';
    icon?: any;
    theme?: any;
} & ToolsProps;
export declare type ToolsProps = {
    id: string;
};
//# sourceMappingURL=types.d.ts.map