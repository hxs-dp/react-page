import IconButton from '@material-ui/core/IconButton';
import VerticalAlignTopIcon from '@material-ui/icons/VerticalAlignTop';
import { useFocusCell, useParentCell } from '@react-page/core';
import React, { useCallback } from 'react';
var SelectParentButton = function (_a) {
    var id = _a.id;
    var parentCell = useParentCell(id);
    var focusCell = useFocusCell();
    var onClick = useCallback(function () { return focusCell(parentCell === null || parentCell === void 0 ? void 0 : parentCell.id); }, [
        parentCell === null || parentCell === void 0 ? void 0 : parentCell.id,
    ]);
    return parentCell ? (React.createElement(IconButton, { className: "bottomToolbar__selectParentButton", onClick: onClick, color: "default", title: "Select parent" },
        React.createElement(VerticalAlignTopIcon, null))) : null;
};
export default SelectParentButton;
//# sourceMappingURL=index.js.map