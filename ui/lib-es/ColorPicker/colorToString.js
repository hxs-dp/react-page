export var colorToString = function (c) {
    return c && 'rgba(' + c.r + ', ' + c.g + ', ' + c.b + ', ' + c.a + ')';
};
//# sourceMappingURL=colorToString.js.map