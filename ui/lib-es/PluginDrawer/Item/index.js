import Avatar from '@material-ui/core/Avatar';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import { useInsertCellAtTheEnd } from '@react-page/core';
import * as React from 'react';
import draggable from '../Draggable/index';
var Item = function (_a) {
    var plugin = _a.plugin, insert = _a.insert, translations = _a.translations;
    if (!plugin.IconComponent && !plugin.text) {
        return null;
    }
    var insertAtEnd = useInsertCellAtTheEnd();
    var Draggable = draggable(plugin.name);
    return (React.createElement(Draggable, { insert: insert },
        React.createElement(ListItem, { title: "Click to add or drag and drop it somwhere on your page!", className: "ory-plugin-drawer-item", onClick: function () { return insertAtEnd(insert); } },
            React.createElement(Avatar, { children: plugin.IconComponent || plugin.text[0], style: {
                    marginRight: 16,
                } }),
            React.createElement(ListItemText, { primary: plugin.text, secondary: plugin.description }))));
};
export default Item;
//# sourceMappingURL=index.js.map