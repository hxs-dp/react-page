var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListSubheader from '@material-ui/core/ListSubheader';
import TextField from '@material-ui/core/TextField';
import { connect, sanitizeInitialChildren, Selectors, useEditor, } from '@react-page/core';
import * as React from 'react';
import { Portal } from 'react-portal';
import { createStructuredSelector } from 'reselect';
import Item from './Item/index';
var defaultTranslations = {
    noPluginFoundContent: 'No plugins found',
    searchPlaceholder: 'Search plugins',
    layoutPlugins: 'Layout plugins',
    contentPlugins: 'Content plugins',
    insertPlugin: 'Add plugin to content',
    dragMe: 'Drag me!',
};
var Raw = /** @class */ (function (_super) {
    __extends(Raw, _super);
    function Raw(props) {
        var _this = _super.call(this, props) || this;
        _this.onRef = function (component) {
            _this.input = component;
        };
        _this.onSearch = function (e) {
            var target = e.target;
            if (target instanceof HTMLInputElement) {
                _this.setState({
                    isSearching: target.value.length > 0,
                    searchText: target.value,
                });
            }
        };
        _this.state = {
            isSearching: false,
            searchText: '',
        };
        _this.onSearch = _this.onSearch.bind(_this);
        _this.searchFilter = _this.searchFilter.bind(_this);
        return _this;
    }
    Raw.prototype.componentDidUpdate = function () {
        var input = this.input;
        if (input && this.props.isInsertMode && input instanceof HTMLElement) {
            setTimeout(function () {
                var e = input.querySelector('input');
                if (e) {
                    e.focus();
                }
            }, 100);
        }
    };
    Raw.prototype.searchFilter = function (plugin) {
        return (plugin &&
            plugin.name &&
            !plugin.hideInMenu &&
            (plugin.name
                .toLowerCase()
                .startsWith(this.state.searchText.toLowerCase()) ||
                (plugin.description &&
                    plugin.description
                        .toLowerCase()
                        .startsWith(this.state.searchText.toLowerCase())) ||
                (plugin.text &&
                    plugin.text
                        .toLowerCase()
                        .startsWith(this.state.searchText.toLowerCase()))));
    };
    Raw.prototype.render = function () {
        var _this = this;
        var plugins = this.props.editor.plugins;
        var content = plugins.plugins.content.filter(this.searchFilter);
        var layout = plugins.plugins.layout.filter(this.searchFilter);
        return (React.createElement(Portal, null,
            React.createElement(Drawer, { variant: "persistent", className: "ory-plugin-drawer", open: this.props.isInsertMode, PaperProps: {
                    style: {
                        width: 320,
                    },
                } },
                React.createElement(List, { subheader: React.createElement(ListSubheader, null, this.props.translations.insertPlugin) },
                    React.createElement(ListItem, null,
                        React.createElement(TextField, { inputRef: this.onRef, placeholder: this.props.translations.searchPlaceholder, fullWidth: true, onChange: this.onSearch })),
                    layout.length + content.length === 0 && (React.createElement(ListSubheader, null, this.props.translations.noPluginFoundContent))),
                content.length > 0 && (React.createElement(List, { subheader: React.createElement(ListSubheader, null, this.props.translations.contentPlugins) }, content.map(function (plugin, k) {
                    var initialState = plugin.createInitialState();
                    return (React.createElement(Item, { translations: _this.props.translations, plugin: plugin, key: k.toString(), insert: {
                            content: {
                                plugin: plugin,
                                state: initialState,
                            },
                        } }));
                }))),
                layout.length > 0 && (React.createElement(List, { subheader: React.createElement(ListSubheader, null, this.props.translations.layoutPlugins) }, layout.map(function (plugin, k) {
                    var initialState = plugin.createInitialState();
                    var children = sanitizeInitialChildren(plugin.createInitialChildren());
                    return (React.createElement(Item, { translations: _this.props.translations, plugin: plugin, key: k.toString(), insert: __assign(__assign({}, children), { layout: {
                                plugin: plugin,
                                state: initialState,
                            } }) }));
                }))))));
    };
    Raw.defaultProps = {
        translations: defaultTranslations,
    };
    return Raw;
}(React.Component));
var mapStateToProps = createStructuredSelector({
    isInsertMode: Selectors.Display.isInsertMode,
});
var Decorated = connect(mapStateToProps)(Raw);
var Toolbar = function () {
    var editor = useEditor();
    return React.createElement(Decorated, { editor: editor });
};
export default React.memo(Toolbar);
//# sourceMappingURL=index.js.map