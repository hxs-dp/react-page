export declare const source: {
    beginDrag({ insert, ...props }: {
        insert: Record<string, unknown>;
        layoutMode(): void;
    }): {
        layoutMode(): void;
        node: Record<string, unknown>;
        rawNode: () => Record<string, unknown>;
    };
};
export declare const collect: (connect: any, monitor: any) => {
    connectDragSource: any;
    isDragging: any;
    connectDragPreview: any;
};
//# sourceMappingURL=index.d.ts.map