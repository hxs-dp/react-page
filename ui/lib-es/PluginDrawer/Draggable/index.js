var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { Actions, connect, DragSource } from '@react-page/core';
import classNames from 'classnames';
import * as React from 'react';
import { collect, source } from './helper/index';
var instances = {};
var Draggable = /** @class */ (function (_super) {
    __extends(Draggable, _super);
    function Draggable() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Draggable.prototype.componentDidMount = function () {
        var _this = this;
        var img = new Image();
        img.onload = function () { return _this.props.connectDragPreview(img); };
        img.src =
            // tslint:disable-next-line:max-line-length
            'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAhCAYAAACbffiEAAAA6UlEQVRYhe2ZQQ6CMBBFX0njHg7ESXTp1p3uvIBewc3Em3AfdelSFwRDCAm01JRO+pa0lP8zzc9kMCKyAa7AFqhIixdwB44WuACHuHq8KWm1vwtgF1lMCPaWkevUNE3Qr9R17XTu1P5uvUdV+IpbG2qMGBH5xBYRAjUVUWPEjj10SS3XRFry3kha/VBTETVGcmqtDTVGFqdWn7k9ku96f88QNRVRYySn1tpQY8QptXz7qinmnpt7rZTIqbU21BgJ2mv1+XfCDVFTETVGjIg8SG8KP+RZ0I7lU+dmgRNgaKfyZVw9znT/R85fOHJJE77U6UcAAAAASUVORK5CYII=';
    };
    Draggable.prototype.render = function () {
        var _a = this.props, connectDragSource = _a.connectDragSource, isDragging = _a.isDragging, children = _a.children, className = _a.className;
        var classes = classNames(className, { 'ory-toolbar-draggable-is-dragged': isDragging }, 'ory-toolbar-draggable');
        return connectDragSource(React.createElement("div", { className: classes }, children));
    };
    return Draggable;
}(React.PureComponent));
var mapStateToProps = null;
var _a = Actions.Display, insertMode = _a.insertMode, editMode = _a.editMode, layoutMode = _a.layoutMode;
var clearHover = Actions.Cell.clearHover;
var mapDispatchToProps = { insertMode: insertMode, editMode: editMode, layoutMode: layoutMode, clearHover: clearHover };
export default (function (dragType) {
    if (dragType === void 0) { dragType = 'CELL'; }
    if (!instances[dragType]) {
        instances[dragType] = connect(mapStateToProps, mapDispatchToProps)(DragSource(dragType, source, collect)(Draggable));
    }
    return instances[dragType];
});
//# sourceMappingURL=index.js.map