// something is wrong with lerna, typescript and this import: import { lazyLoad } from '@react-page/core';
import loadable from '@loadable/component';
import { colorToString } from './ColorPicker/colorToString';
import darkTheme from './ThemeProvider/DarkTheme';
var Trash = loadable(function () { return import('./Trash/index'); });
var PluginDrawer = loadable(function () { return import('./PluginDrawer/index'); });
var DisplayModeToggle = loadable(function () { return import('./DisplayModeToggle/index'); });
var BottomToolbar = loadable(function () { return import('./BottomToolbar/index'); });
var EditorUI = loadable(function () { return import('./EditorUI/index'); });
var ThemeProvider = loadable(function () { return import('./ThemeProvider/index'); });
var ImageUpload = loadable(function () { return import('./ImageUpload/index'); });
var ColorPicker = loadable(function () { return import('./ColorPicker/index'); });
export default EditorUI;
export { EditorUI, Trash, PluginDrawer, DisplayModeToggle, BottomToolbar, ThemeProvider, darkTheme, ImageUpload, ColorPicker, colorToString, };
var Toolbar = PluginDrawer;
/**
 * @deprecated, use PluginDrawer instead
 */
export { Toolbar };
//# sourceMappingURL=index.js.map