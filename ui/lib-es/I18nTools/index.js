var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
import { IconButton, Dialog } from '@material-ui/core';
import Translate from '@material-ui/icons/Translate';
import { useEditor } from '@react-page/core';
import React, { useState } from 'react';
import SelectLang from './SelectLang';
import I18nDialog from './I18nDialog';
var I18nTools = function (_a) {
    var _b;
    var id = _a.id;
    var editor = useEditor();
    var _c = __read(useState(false), 2), showI18nDialog = _c[0], setShowI18nDialog = _c[1];
    var hasI18n = ((_b = editor.languages) === null || _b === void 0 ? void 0 : _b.length) > 0;
    var onClose = function () { return setShowI18nDialog(false); };
    if (!hasI18n) {
        return null;
    }
    return (React.createElement(React.Fragment, null,
        React.createElement(Dialog, { open: showI18nDialog, onClose: onClose },
            React.createElement(I18nDialog, { id: id, onClose: onClose })),
        React.createElement("div", { style: { display: 'flex', alignItems: 'center' } },
            React.createElement(IconButton, { onClick: function () { return setShowI18nDialog(true); }, "aria-label": "i18n", color: "secondary" },
                React.createElement(Translate, null)),
            React.createElement(SelectLang, null))));
};
export default I18nTools;
//# sourceMappingURL=index.js.map