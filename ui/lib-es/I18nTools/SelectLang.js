var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
import { Actions, connect, Selectors, useEditor } from '@react-page/core';
import React from 'react';
import { createStructuredSelector } from 'reselect';
import { Select } from '@material-ui/core';
var SelectLang = function (_a) {
    var _b;
    var _c = _a.lang, lang = _c === void 0 ? null : _c, setLang = _a.setLang, rest = __rest(_a, ["lang", "setLang"]);
    var editor = useEditor();
    if (((_b = editor.languages) === null || _b === void 0 ? void 0 : _b.length) > 0) {
        return (React.createElement(Select, { value: lang, onChange: function (e) { return setLang(e.target.value); } }, editor.languages.map(function (l) { return (React.createElement("option", { key: l.lang, value: l.lang }, l.label)); })));
    }
    return null;
};
var mapStateToProps = createStructuredSelector({
    lang: Selectors.Setting.getLang,
});
var mapDispatchToProps = {
    setLang: Actions.Setting.setLang,
};
export default connect(mapStateToProps, mapDispatchToProps)(SelectLang);
//# sourceMappingURL=SelectLang.js.map