import { Button, DialogContent, Table } from '@material-ui/core';
import Translate from '@material-ui/icons/Translate';
import { useCell, useEditor, useLang, useSetLang, useUpdateCellContent, useUpdateCellLayout, } from '@react-page/core';
import React from 'react';
import DraftSwitch from '../DraftSwitch';
import SelectLang from './SelectLang';
var I18nDialog = function (_a) {
    var _b, _c, _d;
    var id = _a.id, onClose = _a.onClose;
    var currentLang = useLang();
    var editor = useEditor();
    var node = useCell(id);
    var setLang = useSetLang();
    var contentOrLayout = (_b = node.layout) !== null && _b !== void 0 ? _b : node.content;
    var updateCellContent = useUpdateCellContent();
    var updateCellLayout = useUpdateCellLayout();
    var reset = function (lang) {
        if (node.layout) {
            updateCellLayout(id, null, lang);
        }
        else {
            updateCellContent(id, null, lang);
        }
    };
    var defaultLangLabel = (_d = (_c = editor.languages) === null || _c === void 0 ? void 0 : _c[0]) === null || _d === void 0 ? void 0 : _d.label;
    return (React.createElement(DialogContent, null,
        React.createElement("div", { style: { display: 'flex', alignItems: 'center' } },
            React.createElement(Translate, { style: { marginRight: 'auto' } }),
            " ",
            React.createElement(SelectLang, null)),
        React.createElement("hr", null),
        React.createElement(Table, null,
            React.createElement("tbody", null, editor.languages.map(function (l, index) {
                var _a;
                var state = (_a = contentOrLayout.stateI18n) === null || _a === void 0 ? void 0 : _a[l.lang];
                var isCurrent = currentLang === l.lang;
                var hasState = Boolean(state);
                return (React.createElement("tr", { key: l.lang },
                    React.createElement("th", { style: {
                            textAlign: 'left',
                            textDecoration: isCurrent ? 'underline' : undefined,
                        } },
                        React.createElement(Button, { onClick: function () { return setLang(l.lang); } },
                            l.label,
                            " ",
                            index === 0 ? '(default)' : null)),
                    React.createElement("td", null,
                        React.createElement(DraftSwitch, { id: id, lang: l.lang })),
                    React.createElement("td", null, hasState ? '✔️' : ' '),
                    React.createElement("td", null, hasState && index !== 0 ? (React.createElement(Button, { onClick: function () {
                            reset(l.lang);
                        } },
                        "Reset to ",
                        defaultLangLabel,
                        " \u26A0\uFE0F")) : null)));
            }))),
        React.createElement(Button, { onClick: function () { return onClose(); } }, "Close")));
};
export default I18nDialog;
//# sourceMappingURL=I18nDialog.js.map