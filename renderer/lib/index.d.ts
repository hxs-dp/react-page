import { EditableType, Plugins } from '@react-page/core';
import * as React from 'react';
export interface HTMLRendererProps {
    state: EditableType;
    plugins?: Plugins;
    lang?: string;
}
export declare const HTMLRenderer: React.SFC<HTMLRendererProps>;
//# sourceMappingURL=index.d.ts.map