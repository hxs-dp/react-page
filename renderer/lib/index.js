"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.HTMLRenderer = void 0;
var core_1 = require("@react-page/core");
var classnames_1 = __importDefault(require("classnames"));
var React = __importStar(require("react"));
var getI18nState = function (_a) {
    var _b, _c;
    var stateI18n = _a.stateI18n, state = _a.state, lang = _a.lang;
    if (!stateI18n || !lang) {
        return state;
    }
    return ((_c = (_b = stateI18n === null || stateI18n === void 0 ? void 0 : stateI18n[lang]) !== null && _b !== void 0 ? _b : 
    // find first non-empty
    stateI18n === null || 
    // find first non-empty
    stateI18n === void 0 ? void 0 : 
    // find first non-empty
    stateI18n[Object.keys(stateI18n).find(function (l) { return stateI18n[l]; })]) !== null && _c !== void 0 ? _c : state);
};
var gridClass = function (size) {
    if (size === void 0) { size = 12; }
    return "ory-cell-sm-" + size + " ory-cell-xs-12";
};
var rowHasInlineChildren = function (_a) {
    var cells = _a.cells;
    return Boolean(cells.length === 2 && Boolean(cells[0].inline));
};
var HTMLRow = React.memo(function (_a) {
    var _b = _a.cells, cells = _b === void 0 ? [] : _b, className = _a.className, lang = _a.lang;
    return (React.createElement("div", { className: classnames_1.default('ory-row', className, {
            'ory-row-has-floating-children': rowHasInlineChildren({ cells: cells }),
        }) }, cells.map(function (c) { return (React.createElement(HTMLCell, __assign({ key: c.id }, c, { lang: lang }))); })));
});
// eslint-disable-next-line no-empty-function
var noop = function () {
    return;
};
var HTMLCell = React.memo(function (props) {
    var _a;
    var _b;
    var _c = props.rows, rows = _c === void 0 ? [] : _c, _d = props.layout, layout = _d === void 0 ? {} : _d, _e = props.content, content = _e === void 0 ? {} : _e, hasInlineNeighbour = props.hasInlineNeighbour, inline = props.inline, size = props.size, id = props.id, isDraft = props.isDraft, isDraftI18n = props.isDraftI18n, lang = props.lang;
    var cn = classnames_1.default('ory-cell', gridClass(size), (_a = {
            'ory-cell-has-inline-neighbour': hasInlineNeighbour
        },
        _a["ory-cell-inline-" + (inline || '')] = inline,
        _a));
    if ((_b = isDraftI18n === null || isDraftI18n === void 0 ? void 0 : isDraftI18n[lang]) !== null && _b !== void 0 ? _b : isDraft) {
        return null;
    }
    if (layout.plugin) {
        var state = layout.state, stateI18n = layout.stateI18n, _f = layout.plugin, Component = _f.Component, name_1 = _f.name, version = _f.version;
        return (React.createElement("div", { className: cn },
            React.createElement("div", { className: "ory-cell-inner" },
                React.createElement(Component, { readOnly: true, lang: lang, state: getI18nState({ state: state, stateI18n: stateI18n, lang: lang }), onChange: noop, id: id, name: name_1, focused: false, version: version }, rows.map(function (r) { return (React.createElement(HTMLRow, __assign({ key: r.id }, r, { lang: lang, className: "ory-cell-inner" }))); })))));
    }
    else if (content.plugin) {
        var state = content.state, stateI18n = content.stateI18n, _g = content.plugin, Component = _g.Component, name_2 = _g.name, version = _g.version;
        return (React.createElement("div", { className: cn },
            React.createElement("div", { className: "ory-cell-inner ory-cell-leaf" },
                React.createElement(Component, { isPreviewMode: true, readOnly: true, lang: lang, state: getI18nState({ state: state, stateI18n: stateI18n, lang: lang }), onChange: noop, id: id, name: name_2, focused: false, version: version, isEditMode: false, isLayoutMode: false, isResizeMode: false, isInsertMode: false }))));
    }
    else if (rows.length > 0) {
        return (React.createElement("div", { className: cn }, rows.map(function (r) { return (React.createElement(HTMLRow, __assign({ key: r.id }, r, { lang: lang, className: "ory-cell-inner" }))); })));
    }
    return (React.createElement("div", { className: cn },
        React.createElement("div", { className: "ory-cell-inner" })));
});
exports.HTMLRenderer = React.memo(function (_a) {
    var state = _a.state, plugins = _a.plugins, _b = _a.lang, lang = _b === void 0 ? null : _b;
    var service = new core_1.PluginService(plugins);
    var _c = service.unserialize(state), cells = _c.cells, props = __rest(_c, ["cells"]);
    return (React.createElement(HTMLRow, __assign({ lang: lang, cells: core_1.setAllSizesAndOptimize(cells) }, props)));
});
//# sourceMappingURL=index.js.map