"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createRow = exports.createCell = void 0;
exports.createCell = function () { return ({
    id: '',
    rows: [],
    size: 12,
    hover: null,
    inline: null,
    focused: false,
    focusSource: '',
    resizable: false,
    bounds: { left: 0, right: 0 },
    hasInlineNeighbour: null,
    levels: {
        above: 0,
        below: 0,
        right: 0,
        left: 0,
    },
}); };
exports.createRow = function () { return ({
    id: '',
    hover: null,
    cells: [],
}); };
//# sourceMappingURL=editable.js.map