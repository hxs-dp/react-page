"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = __importStar(require("react"));
var react_dnd_1 = require("react-dnd");
var react_dnd_html5_backend_1 = require("react-dnd-html5-backend");
var BlurGate_1 = __importDefault(require("../components/BlurGate"));
var Editor_1 = require("../Editor");
var reduxConnect_1 = require("../reduxConnect");
var Provider = function (_a) {
    var editor = _a.editor, _b = _a.children, children = _b === void 0 ? [] : _b, _c = _a.dndBackend, dndBackend = _c === void 0 ? react_dnd_html5_backend_1.HTML5Backend : _c, _d = _a.blurGateDisabled, blurGateDisabled = _d === void 0 ? false : _d, blurGateDefaultMode = _a.blurGateDefaultMode;
    return (React.createElement(react_dnd_1.DndProvider, { backend: dndBackend },
        React.createElement(reduxConnect_1.ReduxProvider, { store: editor.store },
            React.createElement(Editor_1.EditorContext.Provider, { value: editor },
                React.createElement(BlurGate_1.default, { disabled: blurGateDisabled, defaultMode: blurGateDefaultMode }, children)))));
};
exports.default = Provider;
//# sourceMappingURL=index.js.map