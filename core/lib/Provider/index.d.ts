import { BackendFactory } from 'dnd-core';
import * as React from 'react';
import Editor from '../Editor';
import { DisplayModes } from '../actions/display';
interface ProviderProps {
    editor: Editor;
    dndBackend?: BackendFactory;
    blurGateDisabled?: boolean;
    blurGateDefaultMode?: DisplayModes;
}
declare const Provider: React.FC<ProviderProps>;
export default Provider;
//# sourceMappingURL=index.d.ts.map