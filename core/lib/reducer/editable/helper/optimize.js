"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
var __spread = (this && this.__spread) || function () {
    for (var ar = [], i = 0; i < arguments.length; i++) ar = ar.concat(__read(arguments[i]));
    return ar;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.optimizeRow = exports.optimizeCell = exports.optimizeRows = exports.optimizeCells = exports.flatten = void 0;
var empty_1 = require("./empty");
exports.flatten = function (c, n) {
    return __spread(c, n);
};
exports.optimizeCells = function (cells) {
    if (cells === void 0) { cells = []; }
    return cells.filter(empty_1.emptyFilter);
};
exports.optimizeRows = function (rows) {
    if (rows === void 0) { rows = []; }
    return rows.filter(empty_1.emptyFilter);
};
exports.optimizeCell = function (cell) {
    var rows = cell.rows, other = __rest(cell, ["rows"]);
    var optimized = __assign(__assign({}, other), { rows: (rows || [])
            .map(function (r) {
            var _a = r.cells, cells = _a === void 0 ? [] : _a;
            if (cells.length !== 1) {
                return [r];
            }
            var _b = cells[0], _c = _b.rows, cellRows = _c === void 0 ? [] : _c, layout = _b.layout;
            if (cellRows.length > 0 && !layout) {
                return cellRows;
            }
            return [r];
        })
            .reduce(exports.flatten, []) });
    return optimized;
};
exports.optimizeRow = function (_a) {
    var cells = _a.cells, other = __rest(_a, ["cells"]);
    return (__assign(__assign({}, other), { cells: (cells || [])
            .map(function (c) {
            var _a = c.rows, rows = _a === void 0 ? [] : _a, size = c.size;
            if (rows.length !== 1 || c.layout) {
                return [c];
            }
            var _b = rows[0].cells, rowCells = _b === void 0 ? [] : _b;
            if (rowCells.length === 1) {
                return rowCells.map(function (r) { return (__assign(__assign({}, r), { size: size })); });
            }
            return [c];
        })
            .reduce(exports.flatten, []) }));
};
//# sourceMappingURL=optimize.js.map