"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.emptyFilter = exports.isEmpty = void 0;
exports.isEmpty = function (_a) {
    var cells = _a.cells, rows = _a.rows, _b = _a.layout, _c = (_b === void 0 ? {} : _b).plugin, _d = (_c === void 0 ? {} : _c).name, layout = _d === void 0 ? undefined : _d, _e = _a.content, _f = (_e === void 0 ? {} : _e).plugin, _g = (_f === void 0 ? {} : _f).name, content = _g === void 0 ? undefined : _g;
    return !(cells || []).filter(exports.emptyFilter).length &&
        !(rows || []).filter(exports.emptyFilter).length &&
        !content &&
        !(layout && (rows || []).filter(exports.emptyFilter).length);
};
// eslint-disable-next-line @typescript-eslint/no-explicit-any
exports.emptyFilter = function (state) { return !exports.isEmpty(state); };
//# sourceMappingURL=empty.js.map