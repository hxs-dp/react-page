import { Cell } from '../../../types/editable';
/**
 * Sum up cell sizes: Σ(cell[size]).
 */
export declare const sumSizes: (cells?: Array<Cell>) => number;
/**
 * Updates each cell's size boundaries.
 */
export declare const computeBounds: (cells?: Array<Cell>) => Array<Cell>;
/**
 * Computes if a cell is resizable.
 */
export declare const computeResizeable: (cells?: Array<Cell>) => Array<Cell>;
/**
 * Computes sizes an inline element was found.
 */
export declare const computeInlines: (cells?: Array<Cell>) => Array<Cell>;
/**
 * Resize cells.
 */
export declare const resizeCells: (cells: Array<Cell>, { id, size }: Cell) => Array<Cell>;
/**
 * Balance cell sizes.
 *
 * @param {[...cell]} cells
 * @return {[...cell]}
 */
export declare const computeSizes: (cells?: Array<Cell>) => Array<Cell>;
//# sourceMappingURL=sizing.d.ts.map