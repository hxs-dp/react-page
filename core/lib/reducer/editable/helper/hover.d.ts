import { CellHoverAction } from './../../../actions/cell/drag';
import { EditorState } from '../../../types/editor';
/**
 * Check if this item is currently being hovered.
 */
export declare const isHoveringThis: (state: EditorState, action: CellHoverAction) => boolean;
//# sourceMappingURL=hover.d.ts.map