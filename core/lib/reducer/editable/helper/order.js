"use strict";
var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
var __spread = (this && this.__spread) || function () {
    for (var ar = [], i = 0; i < arguments.length; i++) ar = ar.concat(__read(arguments[i]));
    return ar;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.cellOrder = void 0;
var computeOrder = function (_a) {
    var rows = _a.rows, cells = _a.cells, _b = _a.content, _c = (_b === void 0 ? {} : _b).plugin, _d = (_c === void 0 ? {} : _c).name, name = _d === void 0 ? '' : _d, id = _a.id;
    return __spread([
        [
            {
                id: id,
                isLeaf: Boolean(name),
            },
        ]
    ], (rows || []).map(computeOrder), (cells || []).map(computeOrder)).reduce(function (p, n) { return __spread(p, n); }, []);
};
// eslint-disable-next-line @typescript-eslint/no-explicit-any
exports.cellOrder = function (os) {
    return os
        .map(computeOrder)
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        .reduce(function (p, n) { return __spread(p, n); }, []);
};
//# sourceMappingURL=order.js.map