"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.setAllSizesAndOptimize = void 0;
var sizing_1 = require("./sizing");
var optimize_1 = require("./optimize");
var level_1 = require("./level");
exports.setAllSizesAndOptimize = function (cells) {
    if (cells === void 0) { cells = []; }
    return sizing_1.computeInlines(sizing_1.computeResizeable(sizing_1.computeBounds(sizing_1.computeSizes(optimize_1.optimizeCells(cells))))).map(function (cell) {
        if (cell.rows) {
            cell.rows = optimize_1.optimizeRows(cell.rows).map(function (r) {
                var optimized = optimize_1.optimizeRow(r);
                if (optimized.cells) {
                    optimized.cells = exports.setAllSizesAndOptimize(optimized.cells);
                }
                return optimized;
            });
        }
        return level_1.computeDropLevels(optimize_1.optimizeCell(cell));
    });
};
//# sourceMappingURL=setAllSizesAndOptimize.js.map