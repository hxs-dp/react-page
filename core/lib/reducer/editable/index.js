"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.editable = exports.rawEditableReducer = void 0;
var cell_1 = require("../../actions/cell");
var sanitizeInitialChildren_1 = __importDefault(require("../../helper/sanitizeInitialChildren"));
var editable_1 = require("../../types/editable");
var order_1 = require("./helper/order");
var setAllSizesAndOptimize_1 = require("./helper/setAllSizesAndOptimize");
var tree_1 = require("./tree");
exports.rawEditableReducer = function (state, action) {
    if (state === void 0) { state = {
        id: null,
        cells: [],
        config: {
            whitelist: [],
        },
    }; }
    var newCells = setAllSizesAndOptimize_1.setAllSizesAndOptimize(tree_1.cells(state.cells, action));
    // eslint-disable-next-line default-case
    switch (action.type) {
        case cell_1.CELL_CREATE_FALLBACK:
            if (action.editable === state.id) {
                if (action.fallback.createInitialChildren) {
                    var children = sanitizeInitialChildren_1.default(action.fallback.createInitialChildren());
                    var c = __assign(__assign(__assign({}, editable_1.createCell()), children), { layout: {
                            plugin: action.fallback,
                            state: action.fallback.createInitialState(),
                        }, id: action.ids.cell });
                    newCells = setAllSizesAndOptimize_1.setAllSizesAndOptimize(tree_1.cells([c], action));
                }
                else {
                    var c = __assign(__assign({}, editable_1.createCell()), { content: {
                            plugin: action.fallback,
                            state: action.fallback.createInitialState(),
                        }, id: action.ids.cell });
                    newCells = setAllSizesAndOptimize_1.setAllSizesAndOptimize(tree_1.cells([c], action));
                }
            }
            break;
        default:
            break;
    }
    return __assign(__assign({}, state), { cells: newCells, cellOrder: order_1.cellOrder(newCells || []) });
};
exports.editable = exports.rawEditableReducer;
//# sourceMappingURL=index.js.map