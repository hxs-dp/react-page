"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.reducer = void 0;
var redux_1 = require("redux");
var editables_1 = require("./editables");
var display_1 = require("./display");
var focus_1 = require("./focus");
var settings_1 = require("./settings");
var reducer = redux_1.combineReducers({
    editables: editables_1.editables,
    display: display_1.display,
    focus: focus_1.focus,
    settings: settings_1.settings,
});
exports.reducer = reducer;
exports.default = redux_1.combineReducers({ reactPage: reducer });
//# sourceMappingURL=index.js.map