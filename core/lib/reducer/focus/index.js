"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.focus = void 0;
var cell_1 = require("../../actions/cell");
exports.focus = function (state, action) {
    if (state === void 0) { state = ''; }
    switch (action.type) {
        case cell_1.CELL_FOCUS:
            return action.id;
        case cell_1.CELL_BLUR_ALL:
            return '';
        case cell_1.CELL_BLUR:
            return action.id === state ? '' : state;
        default:
            return state;
    }
};
//# sourceMappingURL=index.js.map