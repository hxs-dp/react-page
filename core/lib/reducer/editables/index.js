"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
var __spread = (this && this.__spread) || function () {
    for (var ar = [], i = 0; i < arguments.length; i++) ar = ar.concat(__read(arguments[i]));
    return ar;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.editables = void 0;
var redux_undo_1 = __importStar(require("redux-undo"));
var debug_1 = require("redux-undo/lib/debug");
var core_1 = require("../../actions/cell/core");
var insert_1 = require("../../actions/cell/insert");
var editables_1 = require("../../actions/editables");
var const_1 = require("../../const");
var editable_1 = require("../editable");
if (!const_1.isProduction) {
    debug_1.set(true);
}
var inner = redux_undo_1.default(function (
// eslint-disable-next-line @typescript-eslint/no-explicit-any
state, action) {
    if (state === void 0) { state = []; }
    switch (action.type) {
        default:
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            return state.map(function (e) { return editable_1.editable(e, action); });
    }
}, {
    filter: redux_undo_1.includeAction([
        core_1.CELL_UPDATE_CONTENT,
        core_1.CELL_UPDATE_LAYOUT,
        core_1.CELL_REMOVE,
        core_1.CELL_RESIZE,
        insert_1.CELL_INSERT_ABOVE,
        insert_1.CELL_INSERT_BELOW,
        insert_1.CELL_INSERT_LEFT_OF,
        insert_1.CELL_INSERT_RIGHT_OF,
        insert_1.CELL_INSERT_INLINE_LEFT,
        insert_1.CELL_INSERT_INLINE_RIGHT,
        insert_1.CELL_INSERT_AT_END,
    ]),
    // initTypes: [UPDATE_EDITABLE],
    neverSkipReducer: true,
});
exports.editables = function (state, action) {
    if (state === void 0) { state = {
        past: [],
        present: [],
        future: [],
    }; }
    var _a = state.past, past = _a === void 0 ? [] : _a, _b = state.present, present = _b === void 0 ? [] : _b, _c = state.future, future = _c === void 0 ? [] : _c;
    switch (action.type) {
        case editables_1.UPDATE_EDITABLE:
            return inner({
                past: past.map(function (e) { return __spread(e.filter(function (_a) {
                    var id = _a.id;
                    return id !== action.editable.id;
                }), [
                    // we need to run the rawreducer once or the history initial state will be inconsistent.
                    // resolves https://github.com/ory/editor/pull/117#issuecomment-242942796
                    // ...past,
                    editable_1.editable(action.editable, action),
                ]); }),
                present: inner(__spread(present.filter(function (_a) {
                    var id = _a.id;
                    return id !== action.editable.id;
                }), [
                    // we need to run the rawreducer once or the history initial state will be inconsistent.
                    // resolves https://github.com/ory/editor/pull/117#issuecomment-242942796
                    editable_1.editable(action.editable, action),
                ]), undefined),
                future: future,
            }, undefined);
        default:
            return inner(state, action);
    }
};
//# sourceMappingURL=index.js.map