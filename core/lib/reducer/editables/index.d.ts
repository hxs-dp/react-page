/// <reference types="redux-undo" />
import { AnyAction } from 'redux';
import { Editables } from '../../types/editable';
export declare const editables: (state: Editables, action: AnyAction) => import("redux-undo").StateWithHistory<any>;
//# sourceMappingURL=index.d.ts.map