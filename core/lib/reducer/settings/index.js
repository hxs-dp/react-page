"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.settings = void 0;
var setting_1 = require("../../actions/setting");
exports.settings = function (state, action) {
    if (state === void 0) { state = {
        lang: null,
    }; }
    switch (action.type) {
        case setting_1.SET_LANG:
            return __assign(__assign({}, state), { lang: action.lang });
        default:
            return state;
    }
};
//# sourceMappingURL=index.js.map