"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var createInitialChildren_1 = __importDefault(require("../createInitialChildren"));
exports.default = (function (childrenOrRowDef) {
    if (Array.isArray(childrenOrRowDef)) {
        return createInitialChildren_1.default(childrenOrRowDef);
    }
    return childrenOrRowDef;
});
//# sourceMappingURL=index.js.map