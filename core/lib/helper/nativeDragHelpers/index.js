"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createNativeCellReplacement = exports.isNativeHTMLElementDrag = void 0;
var react_dnd_html5_backend_1 = require("react-dnd-html5-backend");
exports.isNativeHTMLElementDrag = function (monitor) {
    switch (monitor.getItemType()) {
        case react_dnd_html5_backend_1.NativeTypes.URL:
        case react_dnd_html5_backend_1.NativeTypes.FILE:
        case react_dnd_html5_backend_1.NativeTypes.TEXT:
            return true;
        default:
            return false;
    }
};
exports.createNativeCellReplacement = function () {
    var id = 'ory-native-drag';
    return {
        id: id,
        rawNode: function () { return ({ id: id }); },
        node: { content: { plugin: { isInlineable: false } } },
    };
};
//# sourceMappingURL=index.js.map