import { ContentPlugin, ContentPluginConfig, LayoutPlugin, LayoutPluginConfig } from '../../service/plugin/classes';
declare type CellDef = {
    content?: {
        plugin: ContentPluginConfig;
        state?: object;
    };
    layout?: {
        plugin: LayoutPluginConfig;
        state?: object;
    };
    size?: number;
    [key: string]: any;
};
export declare type RowDef = CellDef[];
export declare type ChildrenDef = {
    id: string;
    rows: {
        id: string;
        cells: {
            id: string;
            content?: {
                plugin: ContentPlugin;
                state?: object;
            };
            layout?: {
                plugin: LayoutPlugin;
                state?: object;
            };
            size?: number;
            [key: string]: any;
        }[];
    }[];
};
export declare type InitialChildrenDef = RowDef[] | ChildrenDef;
declare const _default: (rows: RowDef[]) => ChildrenDef;
export default _default;
//# sourceMappingURL=index.d.ts.map