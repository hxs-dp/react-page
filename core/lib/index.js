"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __exportStar = (this && this.__exportStar) || function(m, exports) {
    for (var p in m) if (p !== "default" && !Object.prototype.hasOwnProperty.call(exports, p)) __createBinding(exports, m, p);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.lazyLoad = exports.Editable = exports.Provider = exports.ReduxContext = exports.connect = exports.ReduxProvider = exports.editableReducer = exports.reducer = exports.Editor = exports.LayoutPlugin = exports.ContentPlugin = exports.PluginService = exports.sanitizeInitialChildren = exports.i18n = exports.Selectors = exports.Actions = exports.Plugin = exports.createEmptyState = exports.Migration = exports.setAllSizesAndOptimize = exports.DragSource = exports.DropTarget = void 0;
var react_dnd_1 = require("react-dnd");
Object.defineProperty(exports, "DragSource", { enumerable: true, get: function () { return react_dnd_1.DragSource; } });
Object.defineProperty(exports, "DropTarget", { enumerable: true, get: function () { return react_dnd_1.DropTarget; } });
var actions_1 = require("./actions");
Object.defineProperty(exports, "Actions", { enumerable: true, get: function () { return actions_1.Actions; } });
var Editable_1 = __importDefault(require("./components/Editable"));
exports.Editable = Editable_1.default;
var Editor_1 = __importStar(require("./Editor"));
exports.Editor = Editor_1.default;
Object.defineProperty(exports, "createEmptyState", { enumerable: true, get: function () { return Editor_1.createEmptyState; } });
var lazyLoad_1 = __importDefault(require("./helper/lazyLoad"));
exports.lazyLoad = lazyLoad_1.default;
var sanitizeInitialChildren_1 = __importDefault(require("./helper/sanitizeInitialChildren"));
exports.sanitizeInitialChildren = sanitizeInitialChildren_1.default;
var Provider_1 = __importDefault(require("./Provider"));
exports.Provider = Provider_1.default;
var reducer_1 = require("./reducer");
Object.defineProperty(exports, "reducer", { enumerable: true, get: function () { return reducer_1.reducer; } });
var editable_1 = require("./reducer/editable");
Object.defineProperty(exports, "editableReducer", { enumerable: true, get: function () { return editable_1.editable; } });
var reduxConnect_1 = require("./reduxConnect");
Object.defineProperty(exports, "connect", { enumerable: true, get: function () { return reduxConnect_1.connect; } });
Object.defineProperty(exports, "ReduxContext", { enumerable: true, get: function () { return reduxConnect_1.ReduxContext; } });
Object.defineProperty(exports, "ReduxProvider", { enumerable: true, get: function () { return reduxConnect_1.ReduxProvider; } });
var selector_1 = require("./selector");
Object.defineProperty(exports, "Selectors", { enumerable: true, get: function () { return selector_1.Selectors; } });
var i18n_1 = __importDefault(require("./service/i18n"));
exports.i18n = i18n_1.default;
var plugin_1 = __importDefault(require("./service/plugin"));
exports.PluginService = plugin_1.default;
var classes_1 = require("./service/plugin/classes");
Object.defineProperty(exports, "ContentPlugin", { enumerable: true, get: function () { return classes_1.ContentPlugin; } });
Object.defineProperty(exports, "LayoutPlugin", { enumerable: true, get: function () { return classes_1.LayoutPlugin; } });
Object.defineProperty(exports, "Migration", { enumerable: true, get: function () { return classes_1.Migration; } });
Object.defineProperty(exports, "Plugin", { enumerable: true, get: function () { return classes_1.Plugin; } });
__exportStar(require("./components/hooks"), exports);
var setAllSizesAndOptimize_1 = require("./reducer/editable/helper/setAllSizesAndOptimize");
Object.defineProperty(exports, "setAllSizesAndOptimize", { enumerable: true, get: function () { return setAllSizesAndOptimize_1.setAllSizesAndOptimize; } });
exports.default = Editor_1.default;
//# sourceMappingURL=index.js.map