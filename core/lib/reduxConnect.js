"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.connect = exports.useSelector = exports.useDispatch = exports.useStore = exports.ReduxProvider = exports.ReduxContext = void 0;
/* eslint-disable @typescript-eslint/ban-types */
var react_1 = __importDefault(require("react"));
var react_redux_1 = require("react-redux");
exports.ReduxContext = react_1.default.createContext(null);
exports.ReduxProvider = function (_a) {
    var store = _a.store, props = __rest(_a, ["store"]);
    return (react_1.default.createElement(react_redux_1.Provider, __assign({ store: store, context: exports.ReduxContext }, props)));
};
exports.useStore = react_redux_1.createStoreHook(exports.ReduxContext);
exports.useDispatch = react_redux_1.createDispatchHook(exports.ReduxContext);
exports.useSelector = react_redux_1.createSelectorHook(exports.ReduxContext);
exports.connect = function (
// eslint-disable-next-line @typescript-eslint/no-explicit-any
mapStateToProps, mapDispatchToProps, 
// eslint-disable-next-line @typescript-eslint/no-explicit-any
mergeProps, options) {
    if (options === void 0) { options = {}; }
    return react_redux_1.connect(mapStateToProps, mapDispatchToProps, mergeProps, __assign(__assign({}, options), { context: exports.ReduxContext }));
};
//# sourceMappingURL=reduxConnect.js.map