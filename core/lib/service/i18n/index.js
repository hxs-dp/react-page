"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.I18n = void 0;
var I18n = /** @class */ (function () {
    function I18n() {
        this.t = function (message) { return message; };
    }
    return I18n;
}());
exports.I18n = I18n;
exports.default = new I18n();
//# sourceMappingURL=index.js.map