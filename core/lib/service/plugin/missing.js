"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.layoutMissing = exports.contentMissing = void 0;
var React = __importStar(require("react"));
var ContentMissingComponent = function (props) { return (React.createElement("div", { style: {
        backgroundColor: 'red',
        padding: '8px',
        border: '1px solid black',
        margin: '2px',
        overflowX: 'scroll',
    } },
    "The requested content plugin could not be found.",
    React.createElement("button", { onClick: props.remove }, "Delete Plugin"),
    React.createElement("pre", null, JSON.stringify(props, null, 2)))); };
exports.contentMissing = function (_a) {
    var name = _a.name, version = _a.version;
    return ({
        Component: ContentMissingComponent,
        name: name,
        version: version,
    });
};
var LayoutMissingComponent = function (_a) {
    var children = _a.children, props = __rest(_a, ["children"]);
    return (React.createElement("div", null,
        React.createElement("div", { style: {
                backgroundColor: 'red',
                padding: '8px',
                border: '1px solid black',
                margin: '2px',
                overflowX: 'scroll',
            } },
            "The requested layout plugin could not be found.",
            React.createElement("button", { onClick: props.remove }, "Delete Plugin"),
            React.createElement("pre", null, JSON.stringify(props, null, 2))),
        children));
};
exports.layoutMissing = function (_a) {
    var name = _a.name, version = _a.version;
    return ({
        Component: LayoutMissingComponent,
        name: name,
        version: version,
    });
};
//# sourceMappingURL=missing.js.map