import { ContentPluginConfig, LayoutPluginConfig } from './classes';
export declare const contentMissing: ({ name, version, }: Pick<ContentPluginConfig, 'name' | 'version'>) => ContentPluginConfig;
export declare const layoutMissing: ({ name, version, }: Pick<LayoutPluginConfig, 'name' | 'version'>) => LayoutPluginConfig;
//# sourceMappingURL=missing.d.ts.map