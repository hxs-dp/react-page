"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = __importStar(require("react"));
var handleChange = function (onChange) { return function (e) {
    if (e.target instanceof HTMLInputElement) {
        onChange({ value: e.target.value });
    }
}; };
var Default = function (_a) {
    var readOnly = _a.readOnly, value = _a.state.value, onChange = _a.onChange;
    return readOnly ? (React.createElement("div", null, value)) : (React.createElement("textarea", { style: { width: '100%' }, value: value, onChange: handleChange(onChange) }));
};
var _defaultContentPlugin = {
    Component: Default,
    name: 'ory/editor/core/default',
    version: '0.0.1',
    createInitialState: function () { return ({
        value: 'This is the default plugin from the core package. To replace it, set the "defaultPlugin" value in the editor config.',
    }); },
};
exports.default = _defaultContentPlugin;
//# sourceMappingURL=default.js.map