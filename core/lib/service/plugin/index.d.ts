import { ComponetizedCell, EditableType } from '../../types/editable';
import { EditorState } from '../../types/editor';
import { ContentPlugin, ContentPluginConfig, LayoutPlugin, LayoutPluginConfig, Plugin, Plugins, PluginsInternal } from './classes';
/**
 * Iterate through an editable content tree and generate ids where missing.
 */
export declare const generateMissingIds: (props: EditorState) => EditorState;
/**
 * PluginService is a registry of all content and layout plugins known to the editor.
 */
export default class PluginService {
    plugins: PluginsInternal;
    /**
     * Instantiate a new PluginService instance. You can provide your own set of content and layout plugins here.
     */
    constructor({ content, layout, native }?: Plugins);
    hasNativePlugin: () => boolean;
    getNativePlugin: () => import("../../types/editable").NativeFactory;
    createNativePlugin: (hover?: any, monitor?: any, component?: any) => ComponetizedCell;
    setLayoutPlugins: (plugins?: LayoutPluginConfig[]) => void;
    addLayoutPlugin: (config: LayoutPluginConfig) => void;
    removeLayoutPlugin: (name: string) => void;
    setContentPlugins: (plugins?: ContentPluginConfig[]) => void;
    addContentPlugin: (config: ContentPluginConfig) => void;
    removeContentPlugin: (name: string) => void;
    /**
     * Finds a layout plugin based on its name and version.
     */
    findLayoutPlugin: (name: string, version: string) => {
        plugin: LayoutPlugin;
        pluginWrongVersion?: LayoutPlugin;
    };
    /**
     * Finds a content plugin based on its name and version.
     */
    findContentPlugin: (name: string, version: string) => {
        plugin: ContentPlugin;
        pluginWrongVersion?: ContentPlugin;
    };
    /**
     * Returns a list of all known plugin names.
     */
    getRegisteredNames: () => Array<string>;
    migratePluginState: (state: any, plugin: Plugin, dataVersion: string) => any;
    getNewPluginState: (found: {
        plugin: Plugin;
        pluginWrongVersion?: Plugin;
    }, state: unknown, stateI18n: {
        [lang: string]: unknown;
    }, version: string) => {
        plugin: Plugin;
        state: unknown;
        stateI18n: {
            [lang: string]: unknown;
        };
    };
    unserialize: (state: any) => any;
    serialize: (state: any) => EditableType;
}
//# sourceMappingURL=index.d.ts.map