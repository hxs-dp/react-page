import { PluginsInternal } from './classes';
declare const _default: (pluginsToMergeIn: PluginsInternal, plugins: PluginsInternal) => PluginsInternal;
export default _default;
//# sourceMappingURL=mergePlugins.d.ts.map