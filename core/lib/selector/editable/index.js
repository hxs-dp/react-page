"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
var __spread = (this && this.__spread) || function () {
    for (var ar = [], i = 0; i < arguments.length; i++) ar = ar.concat(__read(arguments[i]));
    return ar;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.parentCellSelector = exports.purifiedNode = exports.searchNodeEverywhere = exports.node = exports.editableConfig = exports.purifiedEditable = exports.editables = exports.editable = exports.findNodeInState = void 0;
// eslint-disable-next-line @typescript-eslint/no-explicit-any
var findNode = function (current, nodeId) {
    var id = current.id, _a = current.rows, rows = _a === void 0 ? [] : _a, _b = current.cells, cells = _b === void 0 ? [] : _b;
    if (id === nodeId) {
        return current;
    }
    var found = undefined;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    __spread(rows, cells).find(function (n) {
        var f = findNode(n, nodeId);
        if (f) {
            found = f;
        }
        return Boolean(f);
    });
    return found;
};
exports.findNodeInState = function (state, editableId, nodeId) {
    var tree = exports.editable(state, { id: editableId });
    if (!tree) {
        throw new Error("Could not find editable: " + editableId);
    }
    return findNode(tree, nodeId);
};
exports.editable = function (state, _a) {
    var id = _a.id;
    return state &&
        state.reactPage &&
        state.reactPage.editables &&
        state.reactPage.editables.present.find(function (_a) {
            var current = (_a === void 0 ? {} : _a).id;
            return current === id;
        });
};
exports.editables = function (_a) {
    var present = _a.reactPage.editables.present;
    return present;
};
exports.purifiedEditable = function (state, props) {
    var found = exports.editable(state, props);
    if (!found) {
        return null;
    }
    return __assign(__assign({}, found), { cells: (found.cells || []).map(function (c) {
            return typeof c === 'string' ? c : c.id;
        }) });
};
exports.editableConfig = function (state, _a) {
    var id = _a.editable;
    return exports.editable(state, { id: id }).config;
};
exports.node = function (state, props) {
    return __assign({}, exports.findNodeInState(state, props.editable, props.id));
};
exports.searchNodeEverywhere = function (state, id) {
    for (var i = 0; i < state.reactPage.editables.present.length; i++) {
        var n = exports.node(state, {
            id: id,
            editable: state.reactPage.editables.present[i].id,
        });
        if (n.id) {
            return {
                node: n,
                editable: state.reactPage.editables.present[i],
            };
        }
    }
    return null;
};
exports.purifiedNode = function (state, props) {
    var found = exports.node(state, props);
    if (!found) {
        return null;
    }
    if (found.cells) {
        found.cells = found.cells.map(function (c) { return c.id; }
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        );
    }
    if (found.rows) {
        found.rows = found.rows.map(function (r) { return r.id; }
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        );
    }
    return found;
};
var parentInner = function (current, props) {
    var id = current.id, _a = current.rows, rows = _a === void 0 ? [] : _a, _b = current.cells, cells = _b === void 0 ? [] : _b;
    if (id === props.id) {
        return current;
    }
    var found = undefined;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    __spread(rows, cells).find(function (n) {
        var f = parentInner(n, props);
        if (f) {
            found = f;
        }
        return Boolean(f);
    });
    return found
        ? __assign(__assign({}, found), { ancestors: __spread((found.ancestors || []), [current]) }) : found;
};
exports.parentCellSelector = function (state, props
// eslint-disable-next-line @typescript-eslint/no-explicit-any
) {
    var tree = exports.editable(state, { id: props.editable });
    if (!tree) {
        throw new Error("Could not find editable: " + props.editable);
    }
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    var parent = parentInner(tree, props);
    var ancestor = ((parent === null || parent === void 0 ? void 0 : parent.ancestors) || []).find(function (a) { return a.content || a.layout; });
    return ancestor;
};
//# sourceMappingURL=index.js.map