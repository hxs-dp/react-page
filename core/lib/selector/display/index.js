"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.isResizeMode = exports.isInsertMode = exports.isEditMode = exports.isLayoutMode = exports.isPreviewMode = void 0;
var display_1 = require("../../actions/display");
exports.isPreviewMode = function (_a) {
    var mode = _a.reactPage.display.mode;
    return mode === display_1.DISPLAY_MODE_PREVIEW;
};
exports.isLayoutMode = function (_a) {
    var mode = _a.reactPage.display.mode;
    return mode === display_1.DISPLAY_MODE_LAYOUT;
};
exports.isEditMode = function (_a) {
    var mode = _a.reactPage.display.mode;
    return mode === display_1.DISPLAY_MODE_EDIT;
};
exports.isInsertMode = function (_a) {
    var mode = _a.reactPage.display.mode;
    return mode === display_1.DISPLAY_MODE_INSERT;
};
exports.isResizeMode = function (_a) {
    var mode = _a.reactPage.display.mode;
    return mode === display_1.DISPLAY_MODE_RESIZING;
};
//# sourceMappingURL=index.js.map