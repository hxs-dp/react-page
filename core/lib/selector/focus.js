"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.focus = void 0;
exports.focus = function (state) {
    return state && state.reactPage && state.reactPage.focus;
};
//# sourceMappingURL=focus.js.map