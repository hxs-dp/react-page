"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.editables = exports.editable = exports.selectors = exports.Selectors = void 0;
var DisplaySelectors = __importStar(require("./display"));
var EditableSelectors = __importStar(require("./editable"));
var editable_1 = require("./editable");
Object.defineProperty(exports, "editable", { enumerable: true, get: function () { return editable_1.editable; } });
Object.defineProperty(exports, "editables", { enumerable: true, get: function () { return editable_1.editables; } });
var SettingSelectors = __importStar(require("./setting"));
exports.Selectors = {
    Display: DisplaySelectors,
    Setting: SettingSelectors,
    Editable: EditableSelectors,
};
exports.selectors = function (store) { return ({
    editable: function (id) { return editable_1.editable(store.getState(), { id: id }); },
    editables: function (id) { return editable_1.editables(store.getState()); },
}); };
//# sourceMappingURL=index.js.map