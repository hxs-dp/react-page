"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.setLang = exports.SET_LANG = void 0;
exports.SET_LANG = 'SET_LANG';
exports.setLang = function (lang) { return ({
    type: exports.SET_LANG,
    lang: lang,
}); };
//# sourceMappingURL=setting.js.map