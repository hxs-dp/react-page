"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.updateEditable = exports.UPDATE_EDITABLE = void 0;
var helpers_1 = require("./helpers");
exports.UPDATE_EDITABLE = 'UPDATE_EDITABLE';
exports.updateEditable = function (editable) { return ({
    type: exports.UPDATE_EDITABLE,
    ts: new Date(),
    editable: editable,
    ids: helpers_1.generateIds(),
}); };
//# sourceMappingURL=editables.js.map