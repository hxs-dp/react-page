"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.previousMode = exports.resizeMode = exports.layoutMode = exports.previewMode = exports.editMode = exports.insertMode = exports.setMode = exports.DEFAULT_DISPLAY_MODE = exports.DISPLAY_MODE_RESIZING = exports.DISPLAY_MODE_INSERT = exports.DISPLAY_MODE_EDIT = exports.DISPLAY_MODE_LAYOUT = exports.DISPLAY_MODE_PREVIEW = exports.SET_PREVIOUS_DISPLAY_MODE = exports.SET_DISPLAY_MODE = void 0;
exports.SET_DISPLAY_MODE = 'SET_DISPLAY_MODE';
exports.SET_PREVIOUS_DISPLAY_MODE = 'SET_PREVIOUS_DISPLAY_MODE';
exports.DISPLAY_MODE_PREVIEW = 'preview';
exports.DISPLAY_MODE_LAYOUT = 'layout';
exports.DISPLAY_MODE_EDIT = 'edit';
exports.DISPLAY_MODE_INSERT = 'insert';
exports.DISPLAY_MODE_RESIZING = 'resizing';
exports.DEFAULT_DISPLAY_MODE = exports.DISPLAY_MODE_EDIT;
var setDisplayMode = function (mode, remember) {
    if (remember === void 0) { remember = false; }
    return function () { return ({
        type: exports.SET_DISPLAY_MODE,
        ts: new Date(),
        mode: mode,
        remember: remember,
    }); };
};
/**
 * Dispatch to switch to arbitrary mode.
 */
exports.setMode = function (mode, remember) {
    if (remember === void 0) { remember = false; }
    return ({
        type: exports.SET_DISPLAY_MODE,
        ts: new Date(),
        mode: mode,
        remember: remember,
    });
};
/**
 * Dispatch to switch to insert display mode.
 */
exports.insertMode = setDisplayMode(exports.DISPLAY_MODE_INSERT);
/**
 * Dispatch to switch to edit display mode.
 */
exports.editMode = setDisplayMode(exports.DISPLAY_MODE_EDIT);
/**
 * Dispatch to switch to preview display mode.
 */
exports.previewMode = setDisplayMode(exports.DISPLAY_MODE_PREVIEW);
/**
 * Dispatch to switch to layout display mode.
 */
exports.layoutMode = setDisplayMode(exports.DISPLAY_MODE_LAYOUT);
/**
 * Dispatch to switch to resize display mode.
 */
exports.resizeMode = setDisplayMode(exports.DISPLAY_MODE_RESIZING);
/**
 * Dispatch to switch to the last display mode, or the fallback if reverting is not possible.
 */
exports.previousMode = function (fallback) { return ({
    type: exports.SET_PREVIOUS_DISPLAY_MODE,
    fallback: fallback,
}); };
//# sourceMappingURL=display.js.map