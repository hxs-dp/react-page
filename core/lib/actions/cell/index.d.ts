export declare const cellActions: {
    createFallbackCell: (fallback: any, editable: string) => import("./core").CreateFallbackCellAction;
    blurAllCells: () => import("./core").BlurAllCellsAction;
    blurCell: (id: string) => () => import("./core").BlurCellAction;
    focusPreviousCell: (id: string) => () => import("./core").FocusPreviousCellAction;
    focusNextCell: (id: string) => () => import("./core").FocusNextCellAction;
    focusCell: (id: string, scrollToCell?: boolean) => ({ source, }?: {
        source?: string;
    }) => import("./core").FocusCellAction;
    resizeCell: (id: string) => (size?: number) => import("./core").ResizeCellAction;
    removeCell: (id: string, ids?: import("../../types/editable").NewIds) => import("./core").RemoveCellAction;
    updateCellLayout: (id: string) => (state?: import("../../types/editor").EditorState, lang?: string) => import("./core").UpdateCellLayoutAction;
    updateCellContent: (id: string) => (state?: import("../../types/editor").EditorState, lang?: string) => import("./core").UpdateCellContentAction;
    updateCellIsDraft: (id: string, isDraft?: boolean, lang?: string) => import("./core").UpdateCellIsDraftAction;
    insertCellRightInline: (item: Partial<import("../../types/editable").AbstractCell<import("../..").Row>>, { id: hover, inline, hasInlineNeighbour }: Partial<import("../../types/editable").AbstractCell<import("../..").Row>>, level?: number, ids?: import("../../types/editable").NewIds) => (dispatch: any) => void;
    insertCellLeftInline: (item: Partial<import("../../types/editable").AbstractCell<import("../..").Row>>, { id: hover, inline, hasInlineNeighbour }: Partial<import("../../types/editable").AbstractCell<import("../..").Row>>, level?: number, ids?: import("../../types/editable").NewIds) => (dispatch: any) => void;
    insertCellLeftOf: (item: Partial<import("../../types/editable").AbstractCell<import("../..").Row>>, { id: hover, inline, hasInlineNeighbour }: Partial<import("../../types/editable").AbstractCell<import("../..").Row>>, level?: number, ids?: import("../../types/editable").NewIds) => (dispatch: any) => void;
    insertCellRightOf: (item: Partial<import("../../types/editable").AbstractCell<import("../..").Row>>, { id: hover, inline, hasInlineNeighbour }: Partial<import("../../types/editable").AbstractCell<import("../..").Row>>, level?: number, ids?: import("../../types/editable").NewIds) => (dispatch: any) => void;
    insertCellAbove: (item: Partial<import("../../types/editable").AbstractCell<import("../..").Row>>, { id: hover, inline, hasInlineNeighbour }: Partial<import("../../types/editable").AbstractCell<import("../..").Row>>, level?: number, ids?: import("../../types/editable").NewIds) => (dispatch: any) => void;
    insertCellBelow: (item: Partial<import("../../types/editable").AbstractCell<import("../..").Row>>, { id: hover, inline, hasInlineNeighbour }: Partial<import("../../types/editable").AbstractCell<import("../..").Row>>, level?: number, ids?: import("../../types/editable").NewIds) => (dispatch: any) => void;
    duplicateCell: (item: any) => (dispatch: any) => void;
    insertCellAtTheEnd: (item: Partial<import("../../types/editable").AbstractCell<import("../..").Row>>, { id: hover, inline, hasInlineNeighbour }: Partial<import("../../types/editable").AbstractCell<import("../..").Row>>, level?: number, ids?: import("../../types/editable").NewIds) => (dispatch: any) => void;
    insert: (type: string) => (item: Partial<import("../../types/editable").AbstractCell<import("../..").Row>>, { id: hover, inline, hasInlineNeighbour }: Partial<import("../../types/editable").AbstractCell<import("../..").Row>>, level?: number, ids?: import("../../types/editable").NewIds) => (dispatch: any) => void;
    cancelCellDrag: () => import("./drag").CancelCellDragAction;
    clearHover: () => import("./drag").ClearHoverAction;
    dragCell: (id: string) => import("./drag").DragCellAction;
    cellHoverInlineRight: (drag: import("../../types/editable").AbstractCell<import("../..").Row>, hover: import("../../types/editable").AbstractCell<import("../..").Row>) => import("./drag").CellHoverAction;
    cellHoverInlineLeft: (drag: import("../../types/editable").AbstractCell<import("../..").Row>, hover: import("../../types/editable").AbstractCell<import("../..").Row>) => import("./drag").CellHoverAction;
    cellHoverBelow: (drag: import("../../types/editable").AbstractCell<import("../..").Row>, hover: import("../../types/editable").AbstractCell<import("../..").Row>, level: number) => import("./drag").CellHoverAction;
    cellHoverAbove: (drag: import("../../types/editable").AbstractCell<import("../..").Row>, hover: import("../../types/editable").AbstractCell<import("../..").Row>, level: number) => import("./drag").CellHoverAction;
    cellHoverRightOf: (drag: Partial<import("../../types/editable").AbstractCell<import("../..").Row>>, hover: Partial<import("../../types/editable").AbstractCell<import("../..").Row>>, level: number) => import("./drag").CellHoverAction;
    cellHoverLeftOf: (drag: Partial<import("../../types/editable").AbstractCell<import("../..").Row>>, hover: Partial<import("../../types/editable").AbstractCell<import("../..").Row>>, level: number) => import("./drag").CellHoverAction;
    cellHover: ({ id: drag }: Partial<import("../../types/editable").AbstractCell<import("../..").Row>>, { id: hover }: Partial<import("../../types/editable").AbstractCell<import("../..").Row>>, level: number, position: import("../../const").PositionEnum) => import("./drag").CellHoverAction;
};
export * from './insert';
export * from './core';
export * from './drag';
//# sourceMappingURL=index.d.ts.map