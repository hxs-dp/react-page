"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.coreActions = exports.createFallbackCell = exports.blurAllCells = exports.blurCell = exports.focusPreviousCell = exports.focusNextCell = exports.focusCell = exports.resizeCell = exports.removeCell = exports.updateCellLayout = exports.updateCellIsDraft = exports.updateCellContent = exports.CELL_CREATE_FALLBACK = exports.CELL_FOCUS_NEXT = exports.CELL_FOCUS_PREV = exports.CELL_BLUR_ALL = exports.CELL_BLUR = exports.CELL_FOCUS = exports.CELL_RESIZE = exports.CELL_REMOVE = exports.CELL_UPDATE_LAYOUT = exports.CELL_UPDATE_IS_DRAFT = exports.CELL_UPDATE_CONTENT = void 0;
var uuid_1 = __importDefault(require("uuid"));
var helpers_1 = require("../helpers");
exports.CELL_UPDATE_CONTENT = 'CELL_UPDATE_CONTENT';
exports.CELL_UPDATE_IS_DRAFT = 'CELL_UPDATE_IS_DRAFT';
exports.CELL_UPDATE_LAYOUT = 'CELL_UPDATE_LAYOUT';
exports.CELL_REMOVE = 'CELL_REMOVE';
exports.CELL_RESIZE = 'CELL_RESIZE';
exports.CELL_FOCUS = 'CELL_FOCUS';
exports.CELL_BLUR = 'CELL_BLUR';
exports.CELL_BLUR_ALL = 'CELL_BLUR_ALL';
exports.CELL_FOCUS_PREV = 'CELL_FOCUS_PREV';
exports.CELL_FOCUS_NEXT = 'CELL_FOCUS_NEXT';
exports.CELL_CREATE_FALLBACK = 'CELL_CREATE_FALLBACK';
/**
 * An action creator for updating a cell's content data.
 *
 * @example
 * // const store = redux.createStore()
 * // const cell = { id: '1', ... }
 * store.dispatch(updateCellContent(cell.id, { foo: 'bar' }))
 *
 * @param {string} id The id of the cell that should be updated
 * @return {Action}
 */
exports.updateCellContent = function (id) { return function (state, lang) {
    if (state === void 0) { state = {}; }
    return ({
        type: exports.CELL_UPDATE_CONTENT,
        ts: new Date(),
        id: id,
        state: state,
        lang: lang,
    });
}; };
/**
 * An action creator for setting the cell's isDraft property
 *
 * @example
 * // const store = redux.createStore()
 * // const cell = { id: '1', ... }
 * store.dispatch(updateCellContent(cell.id, { foo: 'bar' }))
 *
 * @param {string} id The id of the cell that should be updated
 * @return {Action}
 */
exports.updateCellIsDraft = function (id, isDraft, lang) {
    if (isDraft === void 0) { isDraft = false; }
    if (lang === void 0) { lang = null; }
    return ({
        type: exports.CELL_UPDATE_IS_DRAFT,
        ts: new Date(),
        id: id,
        isDraft: isDraft,
        lang: lang,
    });
};
/**
 * An action creator for updating a cell's layout data.
 *
 * @example
 * // const store = redux.createStore()
 * // const cell = { id: '1', ... }
 * store.dispatch(updateCellLayout(cell.id, { foo: 'bar' }))
 *
 * @param {string} id The id of the cell that should be updated
 * @return {Action}
 */
exports.updateCellLayout = function (id) { return function (state, lang) {
    if (state === void 0) { state = {}; }
    return ({
        type: exports.CELL_UPDATE_LAYOUT,
        ts: new Date(),
        id: id,
        state: state,
        lang: lang,
    });
}; };
/**
 * An action creator for removing a cell.
 *
 * @example
 * // const store = redux.createStore()
 * // const cell = { id: '1', ... }
 * store.dispatch(removeCell(cell.id, ['1', '2', '3', '4', ...]))
 *
 * @param {string} id The id of the cell that should be removed.
 * @param {string} ids An object of IDs for new cells that might be created.
 * @return {Action}
 */
exports.removeCell = function (id, ids) {
    if (ids === void 0) { ids = null; }
    return ({
        type: exports.CELL_REMOVE,
        ts: new Date(),
        id: id,
        ids: ids ? ids : helpers_1.generateIds(),
    });
};
/**
 * An action creator for resizing a cell.
 *
 * @example
 * // const store = redux.createStore()
 * // const cell = { id: '1', ... }
 * store.dispatch(resizeCell(cell.id)(size))
 *
 * @param {string} id The id of the cell that should be removed.
 * @param {number} size The cell's new size.
 * @return {Function}
 */
exports.resizeCell = function (id) { return function (size) {
    if (size === void 0) { size = 1; }
    return ({
        type: exports.CELL_RESIZE,
        ts: new Date(),
        id: id,
        size: size,
    });
}; };
/**
 * Dispatch to focus a cell.
 */
exports.focusCell = function (id, scrollToCell) {
    if (scrollToCell === void 0) { scrollToCell = false; }
    return function (_a) {
        var source = (_a === void 0 ? {} : _a).source;
        return ({
            type: exports.CELL_FOCUS,
            ts: new Date(),
            id: id,
            scrollToCell: scrollToCell,
            source: source,
        });
    };
};
/**
 * Dispatch to focus a cell.
 */
exports.focusNextCell = function (id) { return function () { return ({
    type: exports.CELL_FOCUS_NEXT,
    ts: new Date(),
    id: id,
}); }; };
/**
 * Dispatch to focus a cell.
 */
exports.focusPreviousCell = function (id) { return function () { return ({
    type: exports.CELL_FOCUS_PREV,
    ts: new Date(),
    id: id,
}); }; };
/**
 * Dispatch to blur a cell.
 */
exports.blurCell = function (id) { return function () { return ({
    type: exports.CELL_BLUR,
    ts: new Date(),
    id: id,
}); }; };
/**
 * Dispatch to blur all cells. For example when clicking on document body.
 */
exports.blurAllCells = function () { return ({
    type: exports.CELL_BLUR_ALL,
    ts: new Date(),
}); };
/**
 * Creates a fallback cell, usually done when an editable is empty.
 */
exports.createFallbackCell = function (
// eslint-disable-next-line @typescript-eslint/no-explicit-any
fallback, editable) { return ({
    type: exports.CELL_CREATE_FALLBACK,
    ts: new Date(),
    editable: editable,
    ids: {
        cell: uuid_1.default.v4(),
    },
    fallback: fallback,
}); };
exports.coreActions = {
    createFallbackCell: exports.createFallbackCell,
    blurAllCells: exports.blurAllCells,
    blurCell: exports.blurCell,
    focusPreviousCell: exports.focusPreviousCell,
    focusNextCell: exports.focusNextCell,
    focusCell: exports.focusCell,
    resizeCell: exports.resizeCell,
    removeCell: exports.removeCell,
    updateCellLayout: exports.updateCellLayout,
    updateCellContent: exports.updateCellContent,
    updateCellIsDraft: exports.updateCellIsDraft,
};
//# sourceMappingURL=core.js.map