"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.insertActions = exports.duplicateCell = exports.insertCellAtTheEnd = exports.insertCellRightInline = exports.insertCellLeftInline = exports.insertCellLeftOf = exports.insertCellRightOf = exports.insertCellAbove = exports.insertCellBelow = exports.CELL_INSERT_AT_END = exports.CELL_INSERT_INLINE_RIGHT = exports.CELL_INSERT_INLINE_LEFT = exports.CELL_INSERT_RIGHT_OF = exports.CELL_INSERT_LEFT_OF = exports.CELL_INSERT_BELOW = exports.CELL_INSERT_ABOVE = void 0;
var uuid_1 = require("uuid");
var display_1 = require("../display");
var helpers_1 = require("../helpers");
var core_1 = require("./core");
exports.CELL_INSERT_ABOVE = 'CELL_INSERT_ABOVE';
exports.CELL_INSERT_BELOW = 'CELL_INSERT_BELOW';
exports.CELL_INSERT_LEFT_OF = 'CELL_INSERT_LEFT_OF';
exports.CELL_INSERT_RIGHT_OF = 'CELL_INSERT_RIGHT_OF';
exports.CELL_INSERT_INLINE_LEFT = 'CELL_INSERT_INLINE_LEFT';
exports.CELL_INSERT_INLINE_RIGHT = 'CELL_INSERT_INLINE_RIGHT';
exports.CELL_INSERT_AT_END = 'CELL_INSERT_AT_END';
var insert = function (type) { return function (item, _a, level, ids) {
    var hover = _a.id, inline = _a.inline, hasInlineNeighbour = _a.hasInlineNeighbour;
    if (level === void 0) { level = 0; }
    if (ids === void 0) { ids = null; }
    var l = level;
    switch (type) {
        case exports.CELL_INSERT_ABOVE:
        case exports.CELL_INSERT_BELOW: {
            if ((inline || hasInlineNeighbour) && level < 1) {
                l = 1;
            }
            break;
        }
        case exports.CELL_INSERT_LEFT_OF:
        case exports.CELL_INSERT_RIGHT_OF: {
            if ((inline || hasInlineNeighbour) && level < 1) {
                l = 1;
            }
            break;
        }
        default:
    }
    var insertAction = {
        type: type,
        ts: new Date(),
        item: item,
        hover: hover,
        level: l,
        // FIXME: item handling is a bit confusing,
        // we now give some of them a name like "cell" or "item",
        // but the purpose of the others is unclear
        ids: ids ? ids : helpers_1.generateIds(),
    };
    return function (dispatch) {
        dispatch(insertAction);
        // FIXME: checking if an item is new or just moved around is a bit awkward
        var isNew = !item.id || (item.rows && !item.levels);
        if (isNew) {
            dispatch(display_1.editMode());
        }
        setTimeout(function () {
            dispatch(core_1.focusCell(insertAction.ids.item, true)());
        }, 300);
    };
}; };
/**
 * Insert a cell below of the hovering cell.
 */
exports.insertCellBelow = insert(exports.CELL_INSERT_BELOW);
/**
 * Insert a cell above of the hovering cell.
 */
exports.insertCellAbove = insert(exports.CELL_INSERT_ABOVE);
/**
 * Insert a cell right of the hovering cell.
 */
exports.insertCellRightOf = insert(exports.CELL_INSERT_RIGHT_OF);
/**
 * Insert a cell left of the hovering cell.
 */
exports.insertCellLeftOf = insert(exports.CELL_INSERT_LEFT_OF);
/**
 * Insert a cell inside the hovering cell, on the left.
 */
exports.insertCellLeftInline = insert(exports.CELL_INSERT_INLINE_LEFT);
/**
 * Insert a cell inside the hovering cell, on the right.
 */
exports.insertCellRightInline = insert(exports.CELL_INSERT_INLINE_RIGHT);
exports.insertCellAtTheEnd = insert(exports.CELL_INSERT_AT_END);
// set new ids recursivly
var newIds = function (_a) {
    var id = _a.id, item = __rest(_a, ["id"]);
    return __assign(__assign({}, item), { content: item.content && {
            plugin: item.content.plugin,
            state: JSON.parse(JSON.stringify(item.content.state)),
        }, layout: item.layout && {
            plugin: item.layout.plugin,
            state: JSON.parse(JSON.stringify(item.layout.state)),
        }, id: uuid_1.v4(), rows: item.rows
            ? item.rows.map(function (row) { return (__assign(__assign({}, row), { id: uuid_1.v4(), cells: row.cells ? row.cells.map(newIds) : undefined })); })
            : undefined });
};
exports.duplicateCell = function (item) { return exports.insertCellBelow(newIds(item), item); };
exports.insertActions = {
    insertCellRightInline: exports.insertCellRightInline,
    insertCellLeftInline: exports.insertCellLeftInline,
    insertCellLeftOf: exports.insertCellLeftOf,
    insertCellRightOf: exports.insertCellRightOf,
    insertCellAbove: exports.insertCellAbove,
    insertCellBelow: exports.insertCellBelow,
    duplicateCell: exports.duplicateCell,
    insertCellAtTheEnd: exports.insertCellAtTheEnd,
    insert: insert,
};
//# sourceMappingURL=insert.js.map