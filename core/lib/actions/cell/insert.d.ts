import { Action } from 'redux';
import { Cell, NewIds } from '../../types/editable';
export declare const CELL_INSERT_ABOVE = "CELL_INSERT_ABOVE";
export declare const CELL_INSERT_BELOW = "CELL_INSERT_BELOW";
export declare const CELL_INSERT_LEFT_OF = "CELL_INSERT_LEFT_OF";
export declare const CELL_INSERT_RIGHT_OF = "CELL_INSERT_RIGHT_OF";
export declare const CELL_INSERT_INLINE_LEFT = "CELL_INSERT_INLINE_LEFT";
export declare const CELL_INSERT_INLINE_RIGHT = "CELL_INSERT_INLINE_RIGHT";
export declare const CELL_INSERT_AT_END = "CELL_INSERT_AT_END";
export interface InsertAction extends Action {
    ts: Date;
    item: Partial<Cell>;
    hover: string;
    level: number;
    ids: NewIds;
}
/**
 * Insert a cell below of the hovering cell.
 */
export declare const insertCellBelow: (item: Partial<Cell>, { id: hover, inline, hasInlineNeighbour }: Partial<Cell>, level?: number, ids?: NewIds) => (dispatch: any) => void;
/**
 * Insert a cell above of the hovering cell.
 */
export declare const insertCellAbove: (item: Partial<Cell>, { id: hover, inline, hasInlineNeighbour }: Partial<Cell>, level?: number, ids?: NewIds) => (dispatch: any) => void;
/**
 * Insert a cell right of the hovering cell.
 */
export declare const insertCellRightOf: (item: Partial<Cell>, { id: hover, inline, hasInlineNeighbour }: Partial<Cell>, level?: number, ids?: NewIds) => (dispatch: any) => void;
/**
 * Insert a cell left of the hovering cell.
 */
export declare const insertCellLeftOf: (item: Partial<Cell>, { id: hover, inline, hasInlineNeighbour }: Partial<Cell>, level?: number, ids?: NewIds) => (dispatch: any) => void;
/**
 * Insert a cell inside the hovering cell, on the left.
 */
export declare const insertCellLeftInline: (item: Partial<Cell>, { id: hover, inline, hasInlineNeighbour }: Partial<Cell>, level?: number, ids?: NewIds) => (dispatch: any) => void;
/**
 * Insert a cell inside the hovering cell, on the right.
 */
export declare const insertCellRightInline: (item: Partial<Cell>, { id: hover, inline, hasInlineNeighbour }: Partial<Cell>, level?: number, ids?: NewIds) => (dispatch: any) => void;
export declare const insertCellAtTheEnd: (item: Partial<Cell>, { id: hover, inline, hasInlineNeighbour }: Partial<Cell>, level?: number, ids?: NewIds) => (dispatch: any) => void;
export declare const duplicateCell: (item: any) => (dispatch: any) => void;
export declare const insertActions: {
    insertCellRightInline: (item: Partial<Cell>, { id: hover, inline, hasInlineNeighbour }: Partial<Cell>, level?: number, ids?: NewIds) => (dispatch: any) => void;
    insertCellLeftInline: (item: Partial<Cell>, { id: hover, inline, hasInlineNeighbour }: Partial<Cell>, level?: number, ids?: NewIds) => (dispatch: any) => void;
    insertCellLeftOf: (item: Partial<Cell>, { id: hover, inline, hasInlineNeighbour }: Partial<Cell>, level?: number, ids?: NewIds) => (dispatch: any) => void;
    insertCellRightOf: (item: Partial<Cell>, { id: hover, inline, hasInlineNeighbour }: Partial<Cell>, level?: number, ids?: NewIds) => (dispatch: any) => void;
    insertCellAbove: (item: Partial<Cell>, { id: hover, inline, hasInlineNeighbour }: Partial<Cell>, level?: number, ids?: NewIds) => (dispatch: any) => void;
    insertCellBelow: (item: Partial<Cell>, { id: hover, inline, hasInlineNeighbour }: Partial<Cell>, level?: number, ids?: NewIds) => (dispatch: any) => void;
    duplicateCell: (item: any) => (dispatch: any) => void;
    insertCellAtTheEnd: (item: Partial<Cell>, { id: hover, inline, hasInlineNeighbour }: Partial<Cell>, level?: number, ids?: NewIds) => (dispatch: any) => void;
    insert: (type: string) => (item: Partial<Cell>, { id: hover, inline, hasInlineNeighbour }: Partial<Cell>, level?: number, ids?: NewIds) => (dispatch: any) => void;
};
//# sourceMappingURL=insert.d.ts.map