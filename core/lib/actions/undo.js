"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.redo = exports.undo = void 0;
var redux_undo_1 = require("redux-undo");
exports.undo = function () { return ({
    type: redux_undo_1.ActionTypes.UNDO,
}); };
exports.redo = function () { return ({
    type: redux_undo_1.ActionTypes.REDO,
}); };
//# sourceMappingURL=undo.js.map