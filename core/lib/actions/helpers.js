"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.generateIds = void 0;
var uuid_1 = require("uuid");
exports.generateIds = function () {
    return {
        cell: uuid_1.v4(),
        item: uuid_1.v4(),
        others: [uuid_1.v4(), uuid_1.v4(), uuid_1.v4()],
    };
};
//# sourceMappingURL=helpers.js.map