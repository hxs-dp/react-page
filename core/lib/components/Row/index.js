"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = __importStar(require("react"));
var reduxConnect_1 = require("../../reduxConnect");
var reselect_1 = require("reselect");
var Droppable_1 = __importDefault(require("./Droppable"));
var inner_1 = __importDefault(require("./inner"));
var Dimensions_1 = __importDefault(require("../Dimensions"));
var display_1 = require("../../selector/display");
var editable_1 = require("../../selector/editable");
var cell_1 = require("../../actions/cell");
var Row = /** @class */ (function (_super) {
    __extends(Row, _super);
    function Row(props) {
        var _this = _super.call(this, props) || this;
        var whitelist = props.config.whitelist;
        _this.Droppable = Droppable_1.default(whitelist);
        return _this;
    }
    Row.prototype.render = function () {
        var Droppable = this.Droppable;
        var props = this.props;
        return (React.createElement(Droppable, __assign({}, props),
            React.createElement(inner_1.default, __assign({}, props))));
    };
    return Row;
}(React.PureComponent));
var mapStateToProps = reselect_1.createStructuredSelector({
    isLayoutMode: display_1.isLayoutMode,
    config: editable_1.editableConfig,
    isResizeMode: display_1.isResizeMode,
    isInsertMode: display_1.isInsertMode,
    isEditMode: display_1.isEditMode,
    node: editable_1.purifiedNode,
    rawNode: function (state, props) { return function () {
        return editable_1.node(state, props);
    }; },
});
var mapDispatchToProps = {
    blurAllCells: cell_1.blurAllCells,
};
exports.default = Dimensions_1.default()(reduxConnect_1.connect(mapStateToProps, mapDispatchToProps)(Row));
//# sourceMappingURL=index.js.map