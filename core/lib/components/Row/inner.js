"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
var __spread = (this && this.__spread) || function () {
    for (var ar = [], i = 0; i < arguments.length; i++) ar = ar.concat(__read(arguments[i]));
    return ar;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = __importStar(require("react"));
var classnames_1 = __importDefault(require("classnames"));
var Cell_1 = __importDefault(require("../Cell"));
var rowHasInlineChildren = function (_a) {
    var cells = _a.cells;
    return Boolean(cells.length === 2 && Boolean(cells[0].inline));
};
var Inner = function (_a) {
    var _b;
    var editable = _a.editable, ancestors = _a.ancestors, _c = _a.node, id = _c.id, hover = _c.hover, _d = _c.cells, cells = _d === void 0 ? [] : _d, containerHeight = _a.containerHeight, blurAllCells = _a.blurAllCells, containerWidth = _a.containerWidth, allowMoveInEditMode = _a.allowMoveInEditMode, allowResizeInEditMode = _a.allowResizeInEditMode, editModeResizeHandle = _a.editModeResizeHandle, rawNode = _a.rawNode;
    return (React.createElement("div", { className: classnames_1.default('ory-row', (_b = {
                'ory-row-is-hovering-this': Boolean(hover)
            },
            _b["ory-row-is-hovering-" + (hover || '')] = Boolean(hover),
            _b['ory-row-has-floating-children'] = rowHasInlineChildren(rawNode()),
            _b)), onClick: blurAllCells }, cells.map(function (c) { return (React.createElement(Cell_1.default, { rowWidth: containerWidth, rowHeight: containerHeight, ancestors: __spread(ancestors, [id]), editable: editable, key: c, id: c, allowMoveInEditMode: allowMoveInEditMode, allowResizeInEditMode: allowResizeInEditMode, editModeResizeHandle: editModeResizeHandle })); })));
};
exports.default = Inner;
//# sourceMappingURL=inner.js.map