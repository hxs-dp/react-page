"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Droppable = void 0;
var React = __importStar(require("react"));
var react_dnd_1 = require("react-dnd");
var drag_1 = require("../../../actions/cell/drag");
var insert_1 = require("../../../actions/cell/insert");
var reduxConnect_1 = require("../../../reduxConnect");
var dnd_1 = require("./dnd");
var Droppable = /** @class */ (function (_super) {
    __extends(Droppable, _super);
    function Droppable() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Droppable.prototype.render = function () {
        if (!(this.props.isLayoutMode || this.props.isInsertMode)) {
            return (React.createElement("div", { className: "ory-row-droppable-container" }, this.props.children));
        }
        return this.props.connectDropTarget(React.createElement("div", { className: "ory-row-droppable" }, this.props.children));
    };
    return Droppable;
}(React.Component));
exports.Droppable = Droppable;
var mapDispatchToProps = __assign(__assign({}, drag_1.dragActions), insert_1.insertActions);
exports.default = (function (dropTypes) {
    if (dropTypes === void 0) { dropTypes = ['CELL']; }
    return reduxConnect_1.connect(null, mapDispatchToProps)(react_dnd_1.DropTarget(dropTypes, dnd_1.target, dnd_1.connect)(Droppable));
});
//# sourceMappingURL=index.js.map