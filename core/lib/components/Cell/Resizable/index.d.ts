export interface ResizableState {
    stepWidth: number;
    width: number;
    steps: number;
}
declare const _default: any;
export default _default;
//# sourceMappingURL=index.d.ts.map