import { ComponetizedCell } from '../../../types/editable';
export declare const computeStepWidth: ({ rowWidth, steps, }: {
    rowWidth: number;
    steps: number;
}) => number;
export declare const widthToSize: ({ stepWidth, steps }: {
    stepWidth: number;
    steps: number;
}, { node: { inline } }: ComponetizedCell, result: {
    width: number;
}) => number;
//# sourceMappingURL=helper.d.ts.map