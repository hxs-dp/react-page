"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var classnames_1 = __importDefault(require("classnames"));
var throttle_1 = __importDefault(require("lodash/throttle"));
var React = __importStar(require("react"));
var react_resizable_1 = require("react-resizable");
var reselect_1 = require("reselect");
var display_1 = require("../../../actions/display");
var reduxConnect_1 = require("../../../reduxConnect");
var helper_1 = require("./helper");
var Resizable = /** @class */ (function (_super) {
    __extends(Resizable, _super);
    function Resizable(props) {
        var _this = _super.call(this, props) || this;
        _this.onChangeSize = function (size) {
            if (isNaN(size.width)) {
                return;
            }
            var newSize = helper_1.widthToSize(_this.state, _this.props, size);
            _this.props.onChange(newSize);
        };
        _this.onResize = function (event, _a) {
            var size = _a.size;
            if (isNaN(size.width)) {
                return;
            }
            _this.setState({ width: size.width });
            _this.onChangeSizeThrottled(size);
        };
        _this.onResizeStop = function (event, _a) {
            var size = _a.size;
            if (isNaN(size.width)) {
                return;
            }
            _this.onChangeSize(size);
            var newSize = helper_1.widthToSize(_this.state, _this.props, size);
            _this.setState({ width: newSize * _this.state.stepWidth });
        };
        var sw = helper_1.computeStepWidth(props);
        _this.onChangeSizeThrottled = throttle_1.default(_this.onChangeSize, 100);
        _this.state = {
            stepWidth: sw,
            width: props.node.size * sw,
            steps: props.steps - 1 || 11,
        };
        return _this;
    }
    Resizable.prototype.render = function () {
        var _a;
        var _b = this.props, _c = _b.node, bounds = _c.bounds, inline = _c.inline, children = _b.children;
        return (React.createElement(react_resizable_1.Resizable, { className: classnames_1.default('ory-cell-inner', 'ory-cell-resizable', (_a = {},
                _a["ory-cell-resizable-inline-" + (inline || '')] = inline,
                _a)), onResize: this.onResize, onResizeStop: this.onResizeStop, minConstraints: inline ? null : [this.state.stepWidth, Infinity], maxConstraints: inline ? null : [bounds.right * this.state.stepWidth, Infinity], draggableOpts: { axis: 'none', offsetParent: document.body }, width: this.state.width, height: 0 },
            React.createElement("div", null, children)));
    };
    return Resizable;
}(React.PureComponent));
var mapStateToProps = reselect_1.createStructuredSelector({});
var mapDispatchToProps = { resizeMode: display_1.resizeMode, editMode: display_1.editMode };
exports.default = reduxConnect_1.connect(mapStateToProps, mapDispatchToProps)(Resizable);
//# sourceMappingURL=index.js.map