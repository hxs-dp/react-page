"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var classnames_1 = __importDefault(require("classnames"));
var React = __importStar(require("react"));
var react_dnd_1 = require("react-dnd");
var drag_1 = require("../../../actions/cell/drag");
var insert_1 = require("../../../actions/cell/insert");
var reduxConnect_1 = require("../../../reduxConnect");
var dnd_1 = require("./helper/dnd");
var Droppable = /** @class */ (function (_super) {
    __extends(Droppable, _super);
    function Droppable() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Droppable.prototype.render = function () {
        var _a;
        var _b = this.props, connectDropTarget = _b.connectDropTarget, isLayoutMode = _b.isLayoutMode, isInsertMode = _b.isInsertMode, className = _b.className, isLeaf = _b.isLeaf, hover = _b.node.hover, children = _b.children, allowMoveInEditMode = _b.allowMoveInEditMode;
        if (!(isLayoutMode || isInsertMode) && !allowMoveInEditMode) {
            return (React.createElement("div", { className: classnames_1.default(className, 'ory-cell-droppable-container') }, children));
        }
        return connectDropTarget(React.createElement("div", { className: classnames_1.default(className, 'ory-cell-droppable', (_a = {
                    'ory-cell-droppable-is-over-current': hover
                },
                _a["ory-cell-droppable-is-over-" + hover] = hover,
                _a['ory-cell-droppable-leaf'] = isLeaf,
                _a)) }, children));
    };
    return Droppable;
}(React.PureComponent));
var mapDispatchToProps = __assign(__assign({}, drag_1.dragActions), insert_1.insertActions);
exports.default = reduxConnect_1.connect(null, mapDispatchToProps)(react_dnd_1.DropTarget(function (_a) {
    var dropTypes = _a.dropTypes;
    return dropTypes;
}, dnd_1.target, dnd_1.connect)(Droppable));
//# sourceMappingURL=index.js.map