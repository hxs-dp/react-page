import * as React from 'react';
import { ComponetizedCell, SimplifiedModesProps } from '../../../types/editable';
export declare type CellInnerProps = ComponetizedCell & SimplifiedModesProps;
declare class Inner extends React.PureComponent<CellInnerProps, {
    error: Error;
}> {
    state: {
        error: any;
    };
    componentDidCatch(error: Error): void;
    render(): JSX.Element;
}
export default Inner;
//# sourceMappingURL=index.d.ts.map