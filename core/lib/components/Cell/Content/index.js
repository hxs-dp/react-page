"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getI18nState = void 0;
var React = __importStar(require("react"));
var redux_1 = require("redux");
var reselect_1 = require("reselect");
var cell_1 = require("../../../actions/cell");
var reduxConnect_1 = require("../../../reduxConnect");
var display_1 = require("../../../selector/display");
var scrollIntoViewWithOffset_1 = __importDefault(require("../utils/scrollIntoViewWithOffset"));
exports.getI18nState = function (_a) {
    var _b, _c;
    var stateI18n = _a.stateI18n, state = _a.state, lang = _a.lang;
    if (!stateI18n || !lang) {
        return state;
    }
    return ((_c = (_b = stateI18n === null || stateI18n === void 0 ? void 0 : stateI18n[lang]) !== null && _b !== void 0 ? _b : 
    // find first non-empty
    stateI18n === null || 
    // find first non-empty
    stateI18n === void 0 ? void 0 : 
    // find first non-empty
    stateI18n[Object.keys(stateI18n).find(function (l) { return stateI18n[l]; })]) !== null && _c !== void 0 ? _c : state);
};
// TODO clean me up #157
var Content = /** @class */ (function (_super) {
    __extends(Content, _super);
    function Content() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.onRef = function (ref) {
            _this.ref = ref;
        };
        _this.onChange = function (state) {
            _this.props.updateCellContent(state, _this.props.lang);
        };
        _this.remove = function () {
            _this.props.removeCell();
        };
        _this.focus = function (args) {
            _this.props.focusCell(args);
        };
        return _this;
    }
    Content.prototype.UNSAFE_componentWillReceiveProps = function (nextProps) {
        var _a = this.props.node, was = _a.focused, scrollToCellWas = _a.scrollToCell;
        var lang = nextProps.lang, _b = nextProps.node, is = _b.focused, scrollToCellIs = _b.scrollToCell, focusSource = _b.focusSource;
        var editable = nextProps.editable, id = nextProps.id, _c = nextProps.node, _d = _c.content, _e = _d === void 0 ? {} : _d, _f = _e.plugin, _g = _f === void 0 ? {} : _f, _h = _g.handleFocus, handleFocus = _h === void 0 ? function () { return null; } : _h, _j = _g.handleBlur, handleBlur = _j === void 0 ? function () { return null; } : _j, _k = _g.name, name = _k === void 0 ? 'N/A' : _k, _l = _g.version, version = _l === void 0 ? 'N/A' : _l, _m = _e.state, state = _m === void 0 ? {} : _m, _o = _e.stateI18n, stateI18n = _o === void 0 ? {} : _o, focused = _c.focused;
        // FIXME this is really shitty because it will break when the state changes before the blur comes through, see #157
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        var pass = {
            editable: editable,
            id: id,
            state: exports.getI18nState({ lang: lang, state: state, stateI18n: stateI18n }),
            focused: this.props.isEditMode && focused,
            readOnly: !display_1.isEditMode,
            onChange: this.onChange,
            name: name,
            lang: lang,
            version: version,
            isEditMode: nextProps.isEditMode,
            isResizeMode: nextProps.isResizeMode,
            isPreviewMode: nextProps.isPreviewMode,
            isInsertMode: nextProps.isInsertMode,
            isLayoutMode: nextProps.isLayoutMode,
            remove: nextProps.removeCell,
        };
        // Basically we check if the focus state changed and if yes, we execute the callback handler from the plugin, that
        // can set some side effects.
        if (scrollToCellIs && scrollToCellWas !== scrollToCellIs) {
            if (this.ref) {
                scrollIntoViewWithOffset_1.default(this.ref, 100);
            }
        }
        if (!was && is) {
            // We need this because otherwise we lose hotkey focus on elements like spoilers.
            // This could probably be solved in an easier way by listening to window.document?
            handleFocus(pass, focusSource, this.ref);
        }
        else if (was && !is) {
            handleBlur(pass);
        }
    };
    Content.prototype.render = function () {
        var _this = this;
        var _a = this.props, lang = _a.lang, editable = _a.editable, id = _a.id, _b = _a.node, _c = _b.content, _d = _c === void 0 ? {} : _c, _e = _d.plugin, _f = _e === void 0 ? {} : _e, _g = _f.Component, Component = _g === void 0 ? function () { return null; } : _g, _h = _f.name, name = _h === void 0 ? 'N/A' : _h, _j = _f.version, version = _j === void 0 ? 'N/A' : _j, _k = _f.text, text = _k === void 0 ? 'unnamed plugin' : _k, _l = _d.state, state = _l === void 0 ? {} : _l, _m = _d.stateI18n, stateI18n = _m === void 0 ? null : _m, focused = _b.focused;
        var blurCell = this.props.blurCell;
        var focusProps;
        if (!this.props.isPreviewMode) {
            focusProps = {
                onMouseDown: function () {
                    if (!focused) {
                        _this.focus({ source: 'onMouseDown' });
                    }
                    return true;
                },
            };
        }
        // has in translation? if not, fall back to first nonEmpty or fallback to non i18n
        return (React.createElement("div", __assign({}, focusProps, { tabIndex: "-1", style: { outline: 'none' }, ref: this.onRef, className: "ory-cell-inner ory-cell-leaf" }),
            React.createElement(Component, { editable: editable, id: id, lang: lang, state: exports.getI18nState({ lang: lang, state: state, stateI18n: stateI18n }), focused: Boolean(this.props.isEditMode && focused), name: name, text: text, version: version, readOnly: !this.props.isEditMode, onChange: this.onChange, focus: this.focus, blur: blurCell, isInsertMode: this.props.isInsertMode, isResizeMode: this.props.isResizeMode, isPreviewMode: this.props.isPreviewMode, isEditMode: this.props.isEditMode, isLayoutMode: this.props.isLayoutMode, remove: this.remove })));
    };
    return Content;
}(React.PureComponent));
var mapStateToProps = reselect_1.createStructuredSelector({
    isEditMode: display_1.isEditMode,
    isLayoutMode: display_1.isLayoutMode,
    isPreviewMode: display_1.isPreviewMode,
    isInsertMode: display_1.isInsertMode,
    isResizeMode: display_1.isResizeMode,
});
var mapDispatchToProps = function (dispatch, _a) {
    var id = _a.id;
    return redux_1.bindActionCreators({
        updateCellContent: cell_1.updateCellContent(id),
    }, 
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    dispatch);
};
exports.default = reduxConnect_1.connect(mapStateToProps, mapDispatchToProps)(Content);
//# sourceMappingURL=index.js.map