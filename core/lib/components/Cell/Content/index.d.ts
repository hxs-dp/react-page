import { I18nField } from '../../../types/editable';
export declare const getI18nState: ({ stateI18n, state, lang, }: {
    stateI18n: I18nField<unknown>;
    state: unknown;
    lang?: string;
}) => unknown;
declare const _default: any;
export default _default;
//# sourceMappingURL=index.d.ts.map