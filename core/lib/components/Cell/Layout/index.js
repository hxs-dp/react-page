"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
var __spread = (this && this.__spread) || function () {
    for (var ar = [], i = 0; i < arguments.length; i++) ar = ar.concat(__read(arguments[i]));
    return ar;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = __importStar(require("react"));
var react_dom_1 = require("react-dom");
var redux_1 = require("redux");
var reselect_1 = require("reselect");
var cell_1 = require("../../../actions/cell");
var reduxConnect_1 = require("../../../reduxConnect");
var display_1 = require("../../../selector/display");
var Row_1 = __importDefault(require("../../Row"));
var scrollIntoViewWithOffset_1 = __importDefault(require("../utils/scrollIntoViewWithOffset"));
var selector_1 = require("../../../selector");
var Content_1 = require("../Content");
// TODO clean me up #157
var Layout = /** @class */ (function (_super) {
    __extends(Layout, _super);
    function Layout() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.onRef = function (ref) {
            _this.ref = ref;
        };
        _this.onChange = function (state) {
            _this.props.updateCellLayout(state, _this.props.lang);
        };
        return _this;
    }
    Layout.prototype.UNSAFE_componentWillReceiveProps = function (nextProps) {
        var _a = this.props.node, was = _a.focused, scrollToCellWas = _a.scrollToCell;
        var _b = nextProps.node, is = _b.focused, scrollToCellIs = _b.scrollToCell, focusSource = _b.focusSource;
        var lang = nextProps.lang, editable = nextProps.editable, id = nextProps.id, _c = nextProps.node, _d = _c.layout, _e = _d === void 0 ? {} : _d, _f = _e.plugin, _g = _f === void 0 ? {} : _f, _h = _g.handleFocus, handleFocus = _h === void 0 ? function () { return null; } : _h, _j = _g.handleBlur, handleBlur = _j === void 0 ? function () { return null; } : _j, _k = _g.name, name = _k === void 0 ? 'N/A' : _k, _l = _g.version, version = _l === void 0 ? 'N/A' : _l, _m = _e.state, state = _m === void 0 ? {} : _m, _o = _e.stateI18n, stateI18n = _o === void 0 ? null : _o, focused = _c.focused;
        // FIXME this is really shitty because it will break when the state changes before the blur comes through, see #157
        var pass = {
            editable: editable,
            id: id,
            lang: lang,
            state: Content_1.getI18nState({ lang: lang, state: state, stateI18n: stateI18n }),
            focused: Boolean(this.props.isEditMode && focused),
            readOnly: !this.props.isEditMode,
            onChange: this.onChange,
            name: name,
            version: version,
            remove: this.props.removeCell,
        };
        // Basically we check if the focus state changed and if yes, we execute the callback handler from the plugin, that
        // can set some side effects.
        if (scrollToCellIs && scrollToCellWas !== scrollToCellIs) {
            if (this.ref) {
                scrollIntoViewWithOffset_1.default(this.ref, 100);
            }
        }
        if (!was && is) {
            // We need this because otherwise we lose hotkey focus on elements like spoilers.
            // This could probably be solved in an easier way by listening to window.document?
            handleFocus(pass, focusSource, this.ref);
        }
        else if (was && !is) {
            handleBlur(pass);
        }
    };
    Layout.prototype.render = function () {
        var _this = this;
        var _a = this.props, id = _a.id, lang = _a.lang, _b = _a.node, _c = _b.rows, rows = _c === void 0 ? [] : _c, layout = _b.layout, focused = _b.focused, editable = _a.editable, _d = _a.ancestors, ancestors = _d === void 0 ? [] : _d, allowMoveInEditMode = _a.allowMoveInEditMode, allowResizeInEditMode = _a.allowResizeInEditMode, editModeResizeHandle = _a.editModeResizeHandle;
        var plugin = layout.plugin, state = layout.state, stateI18n = layout.stateI18n;
        var Component = plugin.Component, version = plugin.version, name = plugin.name, text = plugin.text;
        var _e = this.props, focusCell = _e.focusCell, blurCell = _e.blurCell, removeCell = _e.removeCell;
        var focusProps;
        if (!this.props.isPreviewMode) {
            focusProps = {
                // FIXME this should be MouseEvent
                onMouseDown: function (e) {
                    if (!focused &&
                        e.target.closest('.ory-cell-inner') ===
                            // eslint-disable-next-line react/no-find-dom-node
                            react_dom_1.findDOMNode(_this.ref)) {
                        focusCell({ source: 'onMouseDown' });
                    }
                    return true;
                },
            };
        }
        return (React.createElement("div", __assign({}, focusProps, { tabIndex: "-1", className: "ory-cell-inner", ref: this.onRef }),
            React.createElement(Component, { id: id, lang: lang, state: Content_1.getI18nState({ lang: lang, state: state, stateI18n: stateI18n }), focus: focusCell, blur: blurCell, editable: editable, focused: this.props.isEditMode && focused, name: name, text: text, version: version, readOnly: !this.props.isEditMode, onChange: this.onChange, remove: removeCell }, rows.map(function (r) { return (React.createElement(Row_1.default, { editable: editable, ancestors: __spread(ancestors, [id]), key: r, id: r, allowMoveInEditMode: allowMoveInEditMode, allowResizeInEditMode: allowResizeInEditMode, editModeResizeHandle: editModeResizeHandle })); }))));
    };
    return Layout;
}(React.PureComponent));
var mapStateToProps = reselect_1.createStructuredSelector({
    isEditMode: display_1.isEditMode,
    isPreviewMode: display_1.isPreviewMode,
    lang: selector_1.Selectors.Setting.getLang,
});
var mapDispatchToProps = function (dispatch, _a) {
    var id = _a.id;
    return redux_1.bindActionCreators({
        updateCellLayout: cell_1.updateCellLayout(id),
    }, 
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    dispatch);
};
exports.default = reduxConnect_1.connect(mapStateToProps, mapDispatchToProps)(Layout);
//# sourceMappingURL=index.js.map