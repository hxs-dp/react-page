"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = __importStar(require("react"));
var ErrorCell = function (_a) {
    var _b = _a.id, id = _b === void 0 ? 'no id given' : _b, error = _a.error, props = __rest(_a, ["id", "error"]);
    return (React.createElement("div", { className: "ory-cell-error" },
        React.createElement("strong", null, "An error occurred!"),
        React.createElement("small", null,
            React.createElement("dl", null,
                React.createElement("dt", null, "Cause:"),
                React.createElement("dd", null, error.message),
                React.createElement("dt", null, "Cell:"),
                React.createElement("dd", null, id))),
        props.isEditMode ? (React.createElement("button", { onClick: function () { return props.removeCell(); } }, "Remove")) : null));
};
exports.default = ErrorCell;
//# sourceMappingURL=index.js.map