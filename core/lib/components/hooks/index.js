"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.useInsertCellAtTheEnd = exports.useFocusCell = exports.useDuplicateCell = exports.useRemoveCell = exports.useUpdateCellLayout = exports.useUpdateCellContent = exports.useSetLang = exports.useSetDraft = exports.useRedo = exports.useUndo = exports.useLang = exports.useIsLayoutMode = exports.useIsInsertMode = exports.useIsEditMode = exports.useParentCell = exports.useCell = exports.useNode = exports.useEditor = exports.useEditableId = exports.EditableContext = void 0;
var reduxConnect_1 = require("../../reduxConnect");
var undo_1 = require("../../actions/undo");
var react_1 = require("react");
var insert_1 = require("../../actions/cell/insert");
var display_1 = require("../../selector/display");
var reduxConnect_2 = require("../../reduxConnect");
var editable_1 = require("../../selector/editable");
var selector_1 = require("../../selector");
var Editor_1 = require("../../Editor");
var core_1 = require("../../actions/cell/core");
var setting_1 = require("../../actions/setting");
exports.EditableContext = react_1.createContext(null);
exports.useEditableId = function () { return react_1.useContext(exports.EditableContext); };
exports.useEditor = function () { return react_1.useContext(Editor_1.EditorContext); };
exports.useNode = function (id) {
    var editableId = exports.useEditableId();
    return reduxConnect_2.useSelector(function (state) {
        return editable_1.node(state, { id: id, editable: editableId });
    });
};
var isCell = function (node) {
    var _a, _b;
    return Boolean(((_a = node) === null || _a === void 0 ? void 0 : _a.content) || ((_b = node) === null || _b === void 0 ? void 0 : _b.layout));
};
exports.useCell = function (id) {
    var node = exports.useNode(id);
    if (isCell(node)) {
        return node;
    }
    return null;
};
exports.useParentCell = function (id) {
    var editableId = exports.useEditableId();
    return reduxConnect_2.useSelector(function (state) {
        return editable_1.parentCellSelector(state, { id: id, editable: editableId });
    });
};
exports.useIsEditMode = function () {
    return reduxConnect_2.useSelector(display_1.isEditMode);
};
exports.useIsInsertMode = function () {
    return reduxConnect_2.useSelector(display_1.isInsertMode);
};
exports.useIsLayoutMode = function () {
    return reduxConnect_2.useSelector(display_1.isLayoutMode);
};
exports.useLang = function () {
    return reduxConnect_2.useSelector(selector_1.Selectors.Setting.getLang);
};
// actions
exports.useUndo = function () {
    var dispatch = reduxConnect_1.useDispatch();
    return react_1.useCallback(function () { return dispatch(undo_1.undo()); }, [dispatch]);
};
exports.useRedo = function () {
    var dispatch = reduxConnect_1.useDispatch();
    return react_1.useCallback(function () { return dispatch(undo_1.redo()); }, [dispatch]);
};
exports.useSetDraft = function () {
    var dispatch = reduxConnect_1.useDispatch();
    return react_1.useCallback(function (id, isDraft, lang) {
        return dispatch(core_1.updateCellIsDraft(id, isDraft, lang));
    }, [dispatch]);
};
exports.useSetLang = function () {
    var dispatch = reduxConnect_1.useDispatch();
    return react_1.useCallback(function (lang) { return dispatch(setting_1.setLang(lang)); }, [dispatch]);
};
exports.useUpdateCellContent = function () {
    var dispatch = reduxConnect_1.useDispatch();
    return react_1.useCallback(
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    function (id, state, lang) {
        return dispatch(core_1.updateCellContent(id)(state, lang));
    }, [dispatch]);
};
exports.useUpdateCellLayout = function () {
    var dispatch = reduxConnect_1.useDispatch();
    return react_1.useCallback(
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    function (id, state, lang) {
        return dispatch(core_1.updateCellLayout(id)(state, lang));
    }, [dispatch]);
};
exports.useRemoveCell = function () {
    var dispatch = reduxConnect_1.useDispatch();
    return react_1.useCallback(
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    function (id) { return dispatch(core_1.removeCell(id)); }, [dispatch]);
};
exports.useDuplicateCell = function () {
    var dispatch = reduxConnect_1.useDispatch();
    var editor = exports.useEditor();
    var editableId = exports.useEditableId();
    return react_1.useCallback(function (nodeId) {
        return dispatch(insert_1.duplicateCell(editor.getNode(editableId, nodeId)));
    }, [dispatch, editableId]);
};
exports.useFocusCell = function () {
    var dispatch = reduxConnect_1.useDispatch();
    return react_1.useCallback(function (id, scrollToCell) {
        dispatch(core_1.focusCell(id, scrollToCell)());
    }, [dispatch]);
};
exports.useInsertCellAtTheEnd = function () {
    var dispatch = reduxConnect_1.useDispatch();
    return react_1.useCallback(function (node) {
        dispatch(insert_1.insertCellAtTheEnd(node, {}));
    }, [dispatch]);
};
//# sourceMappingURL=index.js.map