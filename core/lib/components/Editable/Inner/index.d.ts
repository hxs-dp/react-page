import { RootState } from '../../../selector';
import { EditableComponentState, SimplifiedModesProps } from '../../../types/editable';
export declare type EditableInnerProps = EditableComponentState & SimplifiedModesProps;
export declare const displayMode: ({ reactPage: { display: { mode }, }, }: RootState) => string;
declare const _default: (props: any) => JSX.Element;
export default _default;
//# sourceMappingURL=index.d.ts.map