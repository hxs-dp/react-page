"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.displayMode = void 0;
var lodash_throttle_1 = __importDefault(require("lodash.throttle"));
var React = __importStar(require("react"));
var reselect_1 = require("reselect");
var cell_1 = require("../../../actions/cell");
var scrollIntoViewWithOffset_1 = __importDefault(require("../../../components/Cell/utils/scrollIntoViewWithOffset"));
var reduxConnect_1 = require("../../../reduxConnect");
var editable_1 = require("../../../selector/editable");
var classes_1 = require("../../../service/plugin/classes");
var Cell_1 = __importDefault(require("../../Cell"));
var Dimensions_1 = __importDefault(require("../../Dimensions"));
function isElementInViewport(el) {
    var rect = el.getBoundingClientRect();
    return (rect.top >= 0 &&
        rect.left >= 0 &&
        rect.bottom <=
            (window.innerHeight ||
                document.documentElement.clientHeight) /*or $(window).height() */ &&
        rect.right <=
            (window.innerWidth ||
                document.documentElement.clientWidth) /*or $(window).width() */);
}
var Inner = /** @class */ (function (_super) {
    __extends(Inner, _super);
    function Inner() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.ref = React.createRef();
        _this.firstElementInViewport = null;
        _this.onScroll = lodash_throttle_1.default(function () {
            if (_this.ref.current) {
                var firstInViewport = Array.prototype.find.call(_this.ref.current.getElementsByClassName('ory-cell'), function (cell) { return isElementInViewport(cell); });
                if (firstInViewport) {
                    _this.firstElementInViewport = {
                        el: firstInViewport,
                        topOffset: firstInViewport.getBoundingClientRect().top,
                    };
                }
                else {
                    _this.firstElementInViewport = null;
                }
            }
        }, 600);
        _this.createFallbackCell = function () {
            var _a = _this.props, node = _a.node, defaultPlugin = _a.defaultPlugin, id = _a.id;
            if (!node) {
                return;
            }
            var _b = node.cells, cells = _b === void 0 ? [] : _b;
            if (cells.length === 0) {
                // FIXME: one more reason to unify layout and content plugins...
                if (defaultPlugin.createInitialChildren) {
                    _this.props.createFallbackCell(new classes_1.LayoutPlugin(defaultPlugin), id);
                }
                else {
                    _this.props.createFallbackCell(new classes_1.ContentPlugin(defaultPlugin), id);
                }
            }
        };
        return _this;
    }
    Inner.prototype.componentDidMount = function () {
        this.createFallbackCell();
        window.addEventListener('scroll', this.onScroll);
    };
    Inner.prototype.componentDidUpdate = function (oldProps) {
        this.createFallbackCell();
        if (oldProps.displayMode !== this.props.displayMode) {
            if (this.firstElementInViewport) {
                var _a = this.firstElementInViewport, el_1 = _a.el, topOffset_1 = _a.topOffset;
                setTimeout(function () {
                    scrollIntoViewWithOffset_1.default(el_1, topOffset_1, 'auto');
                }, 0);
            }
        }
    };
    Inner.prototype.componentWillUnmount = function () {
        window.removeEventListener('scroll', this.onScroll);
    };
    Inner.prototype.render = function () {
        var _a = this.props, id = _a.id, containerWidth = _a.containerWidth, containerHeight = _a.containerHeight, node = _a.node, rest = __rest(_a, ["id", "containerWidth", "containerHeight", "node"]);
        if (!node) {
            return null;
        }
        var _b = node.cells, cells = _b === void 0 ? [] : _b;
        return (React.createElement("div", { ref: this.ref, className: "ory-editable" }, cells.map(function (c) { return (React.createElement(Cell_1.default, __assign({ rowWidth: containerWidth, rowHeight: containerHeight, editable: id, ancestors: [], key: c, id: c }, rest))); })));
    };
    return Inner;
}(React.PureComponent));
exports.displayMode = function (_a) {
    var mode = _a.reactPage.display.mode;
    return mode;
};
var mapStateToProps = reselect_1.createStructuredSelector({
    node: editable_1.purifiedEditable,
    displayMode: exports.displayMode,
});
var mapDispatchToProps = { createFallbackCell: cell_1.createFallbackCell };
exports.default = Dimensions_1.default()(reduxConnect_1.connect(mapStateToProps, mapDispatchToProps)(Inner));
//# sourceMappingURL=index.js.map