"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var fast_deep_equal_1 = __importDefault(require("fast-deep-equal"));
var react_1 = __importStar(require("react"));
var editable_1 = require("../../selector/editable");
var hooks_1 = require("../hooks");
var Decorator_1 = __importDefault(require("../HotKey/Decorator"));
var FallbackDropArea_1 = __importDefault(require("./FallbackDropArea"));
var Inner_1 = __importDefault(require("./Inner"));
var Editable = function (_a) {
    var id = _a.id, onChange = _a.onChange, onChangeLang = _a.onChangeLang, lang = _a.lang, t = _a.t, rest = __rest(_a, ["id", "onChange", "onChangeLang", "lang", "t"]);
    var editor = hooks_1.useEditor();
    // update lang when changed from outside
    react_1.useEffect(function () {
        editor.setLang(lang);
    }, [lang]);
    var previousSerializedRef = react_1.useRef();
    react_1.useEffect(function () {
        var oldLang = lang;
        var handleChanges = function () {
            // notify outsiders to new language, when chagned in ui
            var newLang = editor.store.getState().reactPage.settings.lang;
            if (newLang !== oldLang || newLang !== lang) {
                oldLang = newLang;
                onChangeLang === null || onChangeLang === void 0 ? void 0 : onChangeLang(newLang);
            }
            // check also if lang has changed internally, to call callback when controled from outside
            var state = editable_1.editable(editor.store.getState(), {
                id: id,
            });
            if (!state) {
                return;
            }
            // prevent uneeded updates
            var serialized = editor.plugins.serialize(state);
            var serializedEqual = fast_deep_equal_1.default(previousSerializedRef.current, serialized);
            if (serializedEqual) {
                return;
            }
            previousSerializedRef.current = serialized;
            onChange(serialized);
        };
        var unsubscribe = editor.store.subscribe(handleChanges);
        return function () {
            unsubscribe();
        };
    }, [editor, id, onChange]);
    return (react_1.default.createElement(hooks_1.EditableContext.Provider, { value: id },
        react_1.default.createElement(Decorator_1.default, { id: id },
            react_1.default.createElement(FallbackDropArea_1.default, null,
                react_1.default.createElement(Inner_1.default, __assign({ id: id, defaultPlugin: editor.defaultPlugin }, rest, { t: t }))))));
};
exports.default = react_1.default.memo(Editable);
//# sourceMappingURL=index.js.map