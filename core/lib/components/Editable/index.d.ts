import React from 'react';
import { AbstractCell, AbstractEditable, Row, SimplifiedModesProps } from '../../types/editable';
declare type Serialized = AbstractEditable<AbstractCell<Row>>;
export declare type EditableProps = {
    id: string;
    onChange: (value: Serialized) => void;
    lang?: string;
    t?: any;
    onChangeLang?: (lang: string) => void;
} & SimplifiedModesProps;
declare const _default: React.NamedExoticComponent<EditableProps>;
export default _default;
//# sourceMappingURL=index.d.ts.map