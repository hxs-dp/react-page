import React from 'react';
import { DisplayModes } from '../actions/display';
export interface BlurGateProps {
    disabled?: boolean;
    defaultMode?: DisplayModes;
}
declare const BlurGate: React.FC<BlurGateProps>;
export default BlurGate;
//# sourceMappingURL=BlurGate.d.ts.map