"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importStar(require("react"));
var cell_1 = require("../actions/cell");
var display_1 = require("../actions/display");
var reduxConnect_1 = require("../reduxConnect");
var reselect_1 = require("reselect");
var display_2 = require("../selector/display");
// this might break in future, but its better than nothing
// eslint-disable-next-line @typescript-eslint/no-explicit-any
function findReactElement(node) {
    for (var key in node) {
        if (key.startsWith('__reactInternalInstance$')) {
            return node[key];
        }
    }
    return null;
}
// we go up the reac-tree. This works even through portals, which would not be possible with traversing the dom tree!
var isInSameTree = function (parent, child) {
    if (!parent) {
        return false;
    }
    var element = findReactElement(child);
    while (element) {
        if (element.stateNode === parent) {
            return true;
        }
        element = element.return;
    }
    return false;
};
var useBlurAll = function (blurAllCellsDispatch, setMode, isInInsertMode, defaultMode, disabled) {
    if (defaultMode === void 0) { defaultMode = display_1.DISPLAY_MODE_EDIT; }
    var ref = react_1.default.useRef();
    react_1.useEffect(function () {
        if (disabled) {
            return;
        }
        if (!ref.current) {
            return;
        }
        if (!document && !document.body) {
            return;
        }
        var onMouseDown = function (e) {
            if (!isInSameTree(ref.current, e.target)) {
                blurAllCellsDispatch();
                // set us in default mode if current mode is "insert"
                if (isInInsertMode) {
                    setMode(defaultMode);
                }
            }
        };
        document.body.addEventListener('mousedown', onMouseDown);
        return function () {
            document.body.removeEventListener('mousedown', onMouseDown);
        };
    }, [ref.current, disabled, isInInsertMode]);
    return ref;
};
var mapStateToProps = reselect_1.createStructuredSelector({ isInsertMode: display_2.isInsertMode });
var mapDispatchToProps = { blurAllCells: cell_1.blurAllCells, setMode: display_1.setMode };
var BlurGate = reduxConnect_1.connect(mapStateToProps, mapDispatchToProps)(function (props) {
    var ref = useBlurAll(props.blurAllCells, props.setMode, props.defaultMode, props.isInsertMode, props.disabled);
    return react_1.default.createElement("div", { ref: ref }, props.children);
});
exports.default = BlurGate;
//# sourceMappingURL=BlurGate.js.map