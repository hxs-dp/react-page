"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
var __spread = (this && this.__spread) || function () {
    for (var ar = [], i = 0; i < arguments.length; i++) ar = ar.concat(__read(arguments[i]));
    return ar;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.createEmptyState = exports.EditorContext = void 0;
var react_dnd_html5_backend_1 = require("react-dnd-html5-backend");
var uuid_1 = require("uuid");
var actions_1 = require("./actions");
var const_1 = require("./const");
var selector_1 = require("./selector");
var plugin_1 = __importDefault(require("./service/plugin"));
var default_1 = __importDefault(require("./service/plugin/default"));
var store_1 = __importDefault(require("./store"));
var setting_1 = require("./actions/setting");
var editable_1 = require("./selector/editable");
var react_1 = require("react");
exports.EditorContext = react_1.createContext(null);
var initialState = function (_a) {
    var lang = _a.lang;
    return ({
        reactPage: {
            settings: {
                lang: lang,
            },
            editables: {
                past: [],
                present: [],
                future: [],
            },
        },
    });
};
var nativeTypes = function (editor) {
    return editor.plugins.hasNativePlugin()
        ? [react_dnd_html5_backend_1.NativeTypes.URL, react_dnd_html5_backend_1.NativeTypes.FILE, react_dnd_html5_backend_1.NativeTypes.TEXT]
        : [];
};
var update = function (editor) { return function (editable) {
    var state = editor.plugins.unserialize(editable);
    actions_1.actions(editor.store.dispatch).editable.update(__assign(__assign({}, state), { config: {
            plugins: editor.plugins,
            whitelist: __spread(editor.plugins.getRegisteredNames(), nativeTypes(editor)),
        } }));
}; };
/**
 * Editor is the core interface for dealing with the editor.
 */
var Editor = /** @class */ (function () {
    function Editor(_a) {
        var _this = this;
        var _b = _a === void 0 ? {} : _a, plugins = _b.plugins, _c = _b.middleware, middleware = _c === void 0 ? [] : _c, _d = _b.editables, editables = _d === void 0 ? [] : _d, _e = _b.defaultPlugin, defaultPlugin = _e === void 0 ? default_1.default : _e, store = _b.store, _f = _b.languages, languages = _f === void 0 ? [] : _f, lang = _b.lang;
        this.query = {};
        /**
         * @deprecated in order to reduce api surface, this api gets removed in the future. Please file an issue with your use case if you still need it
         */
        this.refreshEditables = function () {
            var _a;
            (_a = _this.store.getState().reactPage.editables.present) === null || _a === void 0 ? void 0 : _a.forEach(function (editable) {
                if (!const_1.isProduction) {
                    // tslint:disable-next-line:no-console
                    console.log(_this.plugins.serialize(editable));
                }
                // eslint-disable-next-line @typescript-eslint/no-explicit-any
                _this.trigger.editable.update(_this.plugins.serialize(editable));
            });
        };
        this.getNode = function (editableId, nodeId) {
            return editable_1.findNodeInState(_this.store.getState(), editableId, nodeId);
        };
        this.store =
            store ||
                store_1.default(initialState({ lang: lang || languages[0] }), middleware);
        this.plugins = new plugin_1.default(plugins);
        this.middleware = middleware;
        this.trigger = actions_1.actions(this.store.dispatch);
        this.query = selector_1.selectors(this.store);
        this.defaultPlugin = defaultPlugin;
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        this.trigger.editable.add = update(this);
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        this.trigger.editable.update = update(this);
        this.languages = languages;
        editables.forEach(this.trigger.editable.add);
    }
    Editor.prototype.setLang = function (lang) {
        this.store.dispatch(setting_1.setLang(lang));
    };
    return Editor;
}());
exports.createEmptyState = function () {
    return ({ id: uuid_1.v4(), cells: [] });
};
exports.default = Editor;
//# sourceMappingURL=Editor.js.map