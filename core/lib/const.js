"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.isProduction = exports.PositionEnum = void 0;
/**
 * A list of positions in the layout space.
 */
var PositionEnum;
(function (PositionEnum) {
    PositionEnum["LEFT_OF"] = "left-of";
    PositionEnum["RIGHT_OF"] = "right-of";
    PositionEnum["ABOVE"] = "above";
    PositionEnum["BELOW"] = "below";
    PositionEnum["INLINE_LEFT"] = "inline-left";
    PositionEnum["INLINE_RIGHT"] = "inline-right";
})(PositionEnum = exports.PositionEnum || (exports.PositionEnum = {}));
/**
 * Is true if built in production mode.
 */
exports.isProduction = process.env.NODE_ENV === 'production';
//# sourceMappingURL=const.js.map