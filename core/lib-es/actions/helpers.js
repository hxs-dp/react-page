import { v4 } from 'uuid';
export var generateIds = function () {
    return {
        cell: v4(),
        item: v4(),
        others: [v4(), v4(), v4()],
    };
};
//# sourceMappingURL=helpers.js.map