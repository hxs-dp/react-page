import { Action } from 'redux';
import { EditableType, NewIds } from '../types/editable';
export declare const UPDATE_EDITABLE = "UPDATE_EDITABLE";
export interface UpdateEditableAction extends Action {
    ts: Date;
    editable: EditableType;
    ids: NewIds;
}
export declare const updateEditable: (editable: EditableType) => UpdateEditableAction;
//# sourceMappingURL=editables.d.ts.map