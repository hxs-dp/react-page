import { generateIds } from './helpers';
export var UPDATE_EDITABLE = 'UPDATE_EDITABLE';
export var updateEditable = function (editable) { return ({
    type: UPDATE_EDITABLE,
    ts: new Date(),
    editable: editable,
    ids: generateIds(),
}); };
//# sourceMappingURL=editables.js.map