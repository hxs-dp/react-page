export declare const Actions: {
    Display: {
        insertMode: () => import("./display").SetDisplayModeAction;
        editMode: () => import("./display").SetDisplayModeAction;
        previewMode: () => import("./display").SetDisplayModeAction;
        layoutMode: () => import("./display").SetDisplayModeAction;
        resizeMode: () => import("./display").SetDisplayModeAction;
    };
    Cell: {
        createFallbackCell: (fallback: any, editable: string) => import("./cell").CreateFallbackCellAction;
        blurAllCells: () => import("./cell").BlurAllCellsAction;
        blurCell: (id: string) => () => import("./cell").BlurCellAction;
        focusPreviousCell: (id: string) => () => import("./cell").FocusPreviousCellAction;
        focusNextCell: (id: string) => () => import("./cell").FocusNextCellAction;
        focusCell: (id: string, scrollToCell?: boolean) => ({ source, }?: {
            source?: string;
        }) => import("./cell").FocusCellAction;
        resizeCell: (id: string) => (size?: number) => import("./cell").ResizeCellAction;
        removeCell: (id: string, ids?: import("../types/editable").NewIds) => import("./cell").RemoveCellAction;
        updateCellLayout: (id: string) => (state?: import("../types/editor").EditorState, lang?: string) => import("./cell").UpdateCellLayoutAction;
        updateCellContent: (id: string) => (state?: import("../types/editor").EditorState, lang?: string) => import("./cell").UpdateCellContentAction;
        updateCellIsDraft: (id: string, isDraft?: boolean, lang?: string) => import("./cell").UpdateCellIsDraftAction;
        insertCellRightInline: (item: Partial<import("../types/editable").AbstractCell<import("..").Row>>, { id: hover, inline, hasInlineNeighbour }: Partial<import("../types/editable").AbstractCell<import("..").Row>>, level?: number, ids?: import("../types/editable").NewIds) => (dispatch: any) => void;
        insertCellLeftInline: (item: Partial<import("../types/editable").AbstractCell<import("..").Row>>, { id: hover, inline, hasInlineNeighbour }: Partial<import("../types/editable").AbstractCell<import("..").Row>>, level?: number, ids?: import("../types/editable").NewIds) => (dispatch: any) => void;
        insertCellLeftOf: (item: Partial<import("../types/editable").AbstractCell<import("..").Row>>, { id: hover, inline, hasInlineNeighbour }: Partial<import("../types/editable").AbstractCell<import("..").Row>>, level?: number, ids?: import("../types/editable").NewIds) => (dispatch: any) => void;
        insertCellRightOf: (item: Partial<import("../types/editable").AbstractCell<import("..").Row>>, { id: hover, inline, hasInlineNeighbour }: Partial<import("../types/editable").AbstractCell<import("..").Row>>, level?: number, ids?: import("../types/editable").NewIds) => (dispatch: any) => void;
        insertCellAbove: (item: Partial<import("../types/editable").AbstractCell<import("..").Row>>, { id: hover, inline, hasInlineNeighbour }: Partial<import("../types/editable").AbstractCell<import("..").Row>>, level?: number, ids?: import("../types/editable").NewIds) => (dispatch: any) => void;
        insertCellBelow: (item: Partial<import("../types/editable").AbstractCell<import("..").Row>>, { id: hover, inline, hasInlineNeighbour }: Partial<import("../types/editable").AbstractCell<import("..").Row>>, level?: number, ids?: import("../types/editable").NewIds) => (dispatch: any) => void;
        duplicateCell: (item: any) => (dispatch: any) => void;
        insertCellAtTheEnd: (item: Partial<import("../types/editable").AbstractCell<import("..").Row>>, { id: hover, inline, hasInlineNeighbour }: Partial<import("../types/editable").AbstractCell<import("..").Row>>, level?: number, ids?: import("../types/editable").NewIds) => (dispatch: any) => void;
        insert: (type: string) => (item: Partial<import("../types/editable").AbstractCell<import("..").Row>>, { id: hover, inline, hasInlineNeighbour }: Partial<import("../types/editable").AbstractCell<import("..").Row>>, level?: number, ids?: import("../types/editable").NewIds) => (dispatch: any) => void;
        cancelCellDrag: () => import("./cell").CancelCellDragAction;
        clearHover: () => import("./cell").ClearHoverAction;
        dragCell: (id: string) => import("./cell").DragCellAction;
        cellHoverInlineRight: (drag: import("../types/editable").AbstractCell<import("..").Row>, hover: import("../types/editable").AbstractCell<import("..").Row>) => import("./cell").CellHoverAction;
        cellHoverInlineLeft: (drag: import("../types/editable").AbstractCell<import("..").Row>, hover: import("../types/editable").AbstractCell<import("..").Row>) => import("./cell").CellHoverAction;
        cellHoverBelow: (drag: import("../types/editable").AbstractCell<import("..").Row>, hover: import("../types/editable").AbstractCell<import("..").Row>, level: number) => import("./cell").CellHoverAction;
        cellHoverAbove: (drag: import("../types/editable").AbstractCell<import("..").Row>, hover: import("../types/editable").AbstractCell<import("..").Row>, level: number) => import("./cell").CellHoverAction;
        cellHoverRightOf: (drag: Partial<import("../types/editable").AbstractCell<import("..").Row>>, hover: Partial<import("../types/editable").AbstractCell<import("..").Row>>, level: number) => import("./cell").CellHoverAction;
        cellHoverLeftOf: (drag: Partial<import("../types/editable").AbstractCell<import("..").Row>>, hover: Partial<import("../types/editable").AbstractCell<import("..").Row>>, level: number) => import("./cell").CellHoverAction;
        cellHover: ({ id: drag }: Partial<import("../types/editable").AbstractCell<import("..").Row>>, { id: hover }: Partial<import("../types/editable").AbstractCell<import("..").Row>>, level: number, position: import("../const").PositionEnum) => import("./cell").CellHoverAction;
    };
    Setting: {
        setLang: (lang: string) => import("./setting").SetLangAction;
    };
};
export declare const actions: (dispatch: any) => {
    cell: {
        updateContent: (id: string, state: unknown, lang: any) => any;
        updateLayout: (id: string, state: unknown, lang: any) => any;
        remove: (id: string, ids?: import("../types/editable").NewIds) => import("./cell").RemoveCellAction;
        resize: (id: string, size: number) => any;
        focus: (id: string, source: unknown) => any;
        focusNext: (id: string) => any;
        focusPrevious: (id: string) => any;
        blurAll: () => import("./cell").BlurAllCellsAction;
        drag: (id: string) => import("./cell").DragCellAction;
        cancelDrag: () => import("./cell").CancelCellDragAction;
        hoverLeftOf: (drag: Partial<import("../types/editable").AbstractCell<import("..").Row>>, hover: Partial<import("../types/editable").AbstractCell<import("..").Row>>, level: number) => import("./cell").CellHoverAction;
        hoverRightOf: (drag: Partial<import("../types/editable").AbstractCell<import("..").Row>>, hover: Partial<import("../types/editable").AbstractCell<import("..").Row>>, level: number) => import("./cell").CellHoverAction;
        hoverAbove: (drag: import("../types/editable").AbstractCell<import("..").Row>, hover: import("../types/editable").AbstractCell<import("..").Row>, level: number) => import("./cell").CellHoverAction;
        hoverBelow: (drag: import("../types/editable").AbstractCell<import("..").Row>, hover: import("../types/editable").AbstractCell<import("..").Row>, level: number) => import("./cell").CellHoverAction;
        hoverFloatingLeft: (drag: import("../types/editable").AbstractCell<import("..").Row>, hover: import("../types/editable").AbstractCell<import("..").Row>) => import("./cell").CellHoverAction;
        hoverFloatingRight: (drag: import("../types/editable").AbstractCell<import("..").Row>, hover: import("../types/editable").AbstractCell<import("..").Row>) => import("./cell").CellHoverAction;
        clearHover: () => import("./cell").ClearHoverAction;
        insertBelow: (item: Partial<import("../types/editable").AbstractCell<import("..").Row>>, { id: hover, inline, hasInlineNeighbour }: Partial<import("../types/editable").AbstractCell<import("..").Row>>, level?: number, ids?: import("../types/editable").NewIds) => (dispatch: any) => void;
        insertAbove: (item: Partial<import("../types/editable").AbstractCell<import("..").Row>>, { id: hover, inline, hasInlineNeighbour }: Partial<import("../types/editable").AbstractCell<import("..").Row>>, level?: number, ids?: import("../types/editable").NewIds) => (dispatch: any) => void;
        insertRightOf: (item: Partial<import("../types/editable").AbstractCell<import("..").Row>>, { id: hover, inline, hasInlineNeighbour }: Partial<import("../types/editable").AbstractCell<import("..").Row>>, level?: number, ids?: import("../types/editable").NewIds) => (dispatch: any) => void;
        insertLeftOf: (item: Partial<import("../types/editable").AbstractCell<import("..").Row>>, { id: hover, inline, hasInlineNeighbour }: Partial<import("../types/editable").AbstractCell<import("..").Row>>, level?: number, ids?: import("../types/editable").NewIds) => (dispatch: any) => void;
        insertFloatingLeft: (item: Partial<import("../types/editable").AbstractCell<import("..").Row>>, { id: hover, inline, hasInlineNeighbour }: Partial<import("../types/editable").AbstractCell<import("..").Row>>, level?: number, ids?: import("../types/editable").NewIds) => (dispatch: any) => void;
        insertFloatingRight: (item: Partial<import("../types/editable").AbstractCell<import("..").Row>>, { id: hover, inline, hasInlineNeighbour }: Partial<import("../types/editable").AbstractCell<import("..").Row>>, level?: number, ids?: import("../types/editable").NewIds) => (dispatch: any) => void;
    };
    editable: {
        add: (editable: import("../types/editable").AbstractEditable<import("../types/editable").AbstractCell<import("..").Row>>) => import("./editables").UpdateEditableAction;
        update: (editable: import("../types/editable").AbstractEditable<import("../types/editable").AbstractCell<import("..").Row>>) => import("./editables").UpdateEditableAction;
    };
    mode: {
        insert: () => import("./display").SetDisplayModeAction;
        edit: () => import("./display").SetDisplayModeAction;
        preview: () => import("./display").SetDisplayModeAction;
        layout: () => import("./display").SetDisplayModeAction;
        resize: () => import("./display").SetDisplayModeAction;
    };
    undo: () => import("redux").Action<any>;
    redo: () => import("redux").Action<any>;
};
declare type ReturnType<T> = T extends (...args: any[]) => infer R ? R : any;
export declare type ActionsTypes = ReturnType<typeof actions>;
export {};
//# sourceMappingURL=index.d.ts.map