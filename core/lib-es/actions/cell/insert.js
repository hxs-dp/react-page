var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
import { v4 } from 'uuid';
import { editMode } from '../display';
import { generateIds } from '../helpers';
import { focusCell } from './core';
export var CELL_INSERT_ABOVE = 'CELL_INSERT_ABOVE';
export var CELL_INSERT_BELOW = 'CELL_INSERT_BELOW';
export var CELL_INSERT_LEFT_OF = 'CELL_INSERT_LEFT_OF';
export var CELL_INSERT_RIGHT_OF = 'CELL_INSERT_RIGHT_OF';
export var CELL_INSERT_INLINE_LEFT = 'CELL_INSERT_INLINE_LEFT';
export var CELL_INSERT_INLINE_RIGHT = 'CELL_INSERT_INLINE_RIGHT';
export var CELL_INSERT_AT_END = 'CELL_INSERT_AT_END';
var insert = function (type) { return function (item, _a, level, ids) {
    var hover = _a.id, inline = _a.inline, hasInlineNeighbour = _a.hasInlineNeighbour;
    if (level === void 0) { level = 0; }
    if (ids === void 0) { ids = null; }
    var l = level;
    switch (type) {
        case CELL_INSERT_ABOVE:
        case CELL_INSERT_BELOW: {
            if ((inline || hasInlineNeighbour) && level < 1) {
                l = 1;
            }
            break;
        }
        case CELL_INSERT_LEFT_OF:
        case CELL_INSERT_RIGHT_OF: {
            if ((inline || hasInlineNeighbour) && level < 1) {
                l = 1;
            }
            break;
        }
        default:
    }
    var insertAction = {
        type: type,
        ts: new Date(),
        item: item,
        hover: hover,
        level: l,
        // FIXME: item handling is a bit confusing,
        // we now give some of them a name like "cell" or "item",
        // but the purpose of the others is unclear
        ids: ids ? ids : generateIds(),
    };
    return function (dispatch) {
        dispatch(insertAction);
        // FIXME: checking if an item is new or just moved around is a bit awkward
        var isNew = !item.id || (item.rows && !item.levels);
        if (isNew) {
            dispatch(editMode());
        }
        setTimeout(function () {
            dispatch(focusCell(insertAction.ids.item, true)());
        }, 300);
    };
}; };
/**
 * Insert a cell below of the hovering cell.
 */
export var insertCellBelow = insert(CELL_INSERT_BELOW);
/**
 * Insert a cell above of the hovering cell.
 */
export var insertCellAbove = insert(CELL_INSERT_ABOVE);
/**
 * Insert a cell right of the hovering cell.
 */
export var insertCellRightOf = insert(CELL_INSERT_RIGHT_OF);
/**
 * Insert a cell left of the hovering cell.
 */
export var insertCellLeftOf = insert(CELL_INSERT_LEFT_OF);
/**
 * Insert a cell inside the hovering cell, on the left.
 */
export var insertCellLeftInline = insert(CELL_INSERT_INLINE_LEFT);
/**
 * Insert a cell inside the hovering cell, on the right.
 */
export var insertCellRightInline = insert(CELL_INSERT_INLINE_RIGHT);
export var insertCellAtTheEnd = insert(CELL_INSERT_AT_END);
// set new ids recursivly
var newIds = function (_a) {
    var id = _a.id, item = __rest(_a, ["id"]);
    return __assign(__assign({}, item), { content: item.content && {
            plugin: item.content.plugin,
            state: JSON.parse(JSON.stringify(item.content.state)),
        }, layout: item.layout && {
            plugin: item.layout.plugin,
            state: JSON.parse(JSON.stringify(item.layout.state)),
        }, id: v4(), rows: item.rows
            ? item.rows.map(function (row) { return (__assign(__assign({}, row), { id: v4(), cells: row.cells ? row.cells.map(newIds) : undefined })); })
            : undefined });
};
export var duplicateCell = function (item) { return insertCellBelow(newIds(item), item); };
export var insertActions = {
    insertCellRightInline: insertCellRightInline,
    insertCellLeftInline: insertCellLeftInline,
    insertCellLeftOf: insertCellLeftOf,
    insertCellRightOf: insertCellRightOf,
    insertCellAbove: insertCellAbove,
    insertCellBelow: insertCellBelow,
    duplicateCell: duplicateCell,
    insertCellAtTheEnd: insertCellAtTheEnd,
    insert: insert,
};
//# sourceMappingURL=insert.js.map