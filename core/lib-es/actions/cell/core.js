import uuid from 'uuid';
import { generateIds } from '../helpers';
export var CELL_UPDATE_CONTENT = 'CELL_UPDATE_CONTENT';
export var CELL_UPDATE_IS_DRAFT = 'CELL_UPDATE_IS_DRAFT';
export var CELL_UPDATE_LAYOUT = 'CELL_UPDATE_LAYOUT';
export var CELL_REMOVE = 'CELL_REMOVE';
export var CELL_RESIZE = 'CELL_RESIZE';
export var CELL_FOCUS = 'CELL_FOCUS';
export var CELL_BLUR = 'CELL_BLUR';
export var CELL_BLUR_ALL = 'CELL_BLUR_ALL';
export var CELL_FOCUS_PREV = 'CELL_FOCUS_PREV';
export var CELL_FOCUS_NEXT = 'CELL_FOCUS_NEXT';
export var CELL_CREATE_FALLBACK = 'CELL_CREATE_FALLBACK';
/**
 * An action creator for updating a cell's content data.
 *
 * @example
 * // const store = redux.createStore()
 * // const cell = { id: '1', ... }
 * store.dispatch(updateCellContent(cell.id, { foo: 'bar' }))
 *
 * @param {string} id The id of the cell that should be updated
 * @return {Action}
 */
export var updateCellContent = function (id) { return function (state, lang) {
    if (state === void 0) { state = {}; }
    return ({
        type: CELL_UPDATE_CONTENT,
        ts: new Date(),
        id: id,
        state: state,
        lang: lang,
    });
}; };
/**
 * An action creator for setting the cell's isDraft property
 *
 * @example
 * // const store = redux.createStore()
 * // const cell = { id: '1', ... }
 * store.dispatch(updateCellContent(cell.id, { foo: 'bar' }))
 *
 * @param {string} id The id of the cell that should be updated
 * @return {Action}
 */
export var updateCellIsDraft = function (id, isDraft, lang) {
    if (isDraft === void 0) { isDraft = false; }
    if (lang === void 0) { lang = null; }
    return ({
        type: CELL_UPDATE_IS_DRAFT,
        ts: new Date(),
        id: id,
        isDraft: isDraft,
        lang: lang,
    });
};
/**
 * An action creator for updating a cell's layout data.
 *
 * @example
 * // const store = redux.createStore()
 * // const cell = { id: '1', ... }
 * store.dispatch(updateCellLayout(cell.id, { foo: 'bar' }))
 *
 * @param {string} id The id of the cell that should be updated
 * @return {Action}
 */
export var updateCellLayout = function (id) { return function (state, lang) {
    if (state === void 0) { state = {}; }
    return ({
        type: CELL_UPDATE_LAYOUT,
        ts: new Date(),
        id: id,
        state: state,
        lang: lang,
    });
}; };
/**
 * An action creator for removing a cell.
 *
 * @example
 * // const store = redux.createStore()
 * // const cell = { id: '1', ... }
 * store.dispatch(removeCell(cell.id, ['1', '2', '3', '4', ...]))
 *
 * @param {string} id The id of the cell that should be removed.
 * @param {string} ids An object of IDs for new cells that might be created.
 * @return {Action}
 */
export var removeCell = function (id, ids) {
    if (ids === void 0) { ids = null; }
    return ({
        type: CELL_REMOVE,
        ts: new Date(),
        id: id,
        ids: ids ? ids : generateIds(),
    });
};
/**
 * An action creator for resizing a cell.
 *
 * @example
 * // const store = redux.createStore()
 * // const cell = { id: '1', ... }
 * store.dispatch(resizeCell(cell.id)(size))
 *
 * @param {string} id The id of the cell that should be removed.
 * @param {number} size The cell's new size.
 * @return {Function}
 */
export var resizeCell = function (id) { return function (size) {
    if (size === void 0) { size = 1; }
    return ({
        type: CELL_RESIZE,
        ts: new Date(),
        id: id,
        size: size,
    });
}; };
/**
 * Dispatch to focus a cell.
 */
export var focusCell = function (id, scrollToCell) {
    if (scrollToCell === void 0) { scrollToCell = false; }
    return function (_a) {
        var source = (_a === void 0 ? {} : _a).source;
        return ({
            type: CELL_FOCUS,
            ts: new Date(),
            id: id,
            scrollToCell: scrollToCell,
            source: source,
        });
    };
};
/**
 * Dispatch to focus a cell.
 */
export var focusNextCell = function (id) { return function () { return ({
    type: CELL_FOCUS_NEXT,
    ts: new Date(),
    id: id,
}); }; };
/**
 * Dispatch to focus a cell.
 */
export var focusPreviousCell = function (id) { return function () { return ({
    type: CELL_FOCUS_PREV,
    ts: new Date(),
    id: id,
}); }; };
/**
 * Dispatch to blur a cell.
 */
export var blurCell = function (id) { return function () { return ({
    type: CELL_BLUR,
    ts: new Date(),
    id: id,
}); }; };
/**
 * Dispatch to blur all cells. For example when clicking on document body.
 */
export var blurAllCells = function () { return ({
    type: CELL_BLUR_ALL,
    ts: new Date(),
}); };
/**
 * Creates a fallback cell, usually done when an editable is empty.
 */
export var createFallbackCell = function (
// eslint-disable-next-line @typescript-eslint/no-explicit-any
fallback, editable) { return ({
    type: CELL_CREATE_FALLBACK,
    ts: new Date(),
    editable: editable,
    ids: {
        cell: uuid.v4(),
    },
    fallback: fallback,
}); };
export var coreActions = {
    createFallbackCell: createFallbackCell,
    blurAllCells: blurAllCells,
    blurCell: blurCell,
    focusPreviousCell: focusPreviousCell,
    focusNextCell: focusNextCell,
    focusCell: focusCell,
    resizeCell: resizeCell,
    removeCell: removeCell,
    updateCellLayout: updateCellLayout,
    updateCellContent: updateCellContent,
    updateCellIsDraft: updateCellIsDraft,
};
//# sourceMappingURL=core.js.map