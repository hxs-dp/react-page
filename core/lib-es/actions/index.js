import { bindActionCreators } from 'redux';
import { updateCellContent, updateCellLayout, removeCell, resizeCell, focusCell, focusNextCell, focusPreviousCell, blurAllCells, } from './cell/core';
import { cellHoverLeftOf, cellHoverRightOf, cellHoverAbove, cellHoverBelow, cellHoverInlineLeft, cellHoverInlineRight, dragCell, clearHover, cancelCellDrag, } from './cell/drag';
import { insertCellBelow, insertCellAbove, insertCellRightOf, insertCellLeftOf, insertCellLeftInline, insertCellRightInline, } from './cell/insert';
import { undo, redo } from './undo';
import { updateEditable } from './editables';
import { insertMode, editMode, previewMode, layoutMode, resizeMode, } from './display';
import { cellActions } from './cell';
import { setLang } from './setting';
var Display = {
    insertMode: insertMode,
    editMode: editMode,
    previewMode: previewMode,
    layoutMode: layoutMode,
    resizeMode: resizeMode,
};
var Setting = {
    setLang: setLang,
};
var Cell = cellActions;
export var Actions = {
    Display: Display,
    Cell: Cell,
    Setting: Setting,
};
export var actions = function (dispatch) { return ({
    cell: {
        updateContent: function (id, state, lang) {
            return dispatch(updateCellContent(id)(state, lang));
        },
        updateLayout: function (id, state, lang) {
            return dispatch(updateCellLayout(id)(state, lang));
        },
        remove: bindActionCreators({ removeCell: removeCell }, dispatch).removeCell,
        resize: function (id, size) { return dispatch(resizeCell(id)(size)); },
        focus: function (id, source) { return dispatch(focusCell(id)(source)); },
        focusNext: function (id) { return dispatch(focusNextCell(id)()); },
        focusPrevious: function (id) { return dispatch(focusPreviousCell(id)()); },
        blurAll: bindActionCreators({ blurAllCells: blurAllCells }, dispatch).blurAllCells,
        drag: bindActionCreators({ dragCell: dragCell }, dispatch).dragCell,
        cancelDrag: bindActionCreators({ cancelCellDrag: cancelCellDrag }, dispatch).cancelCellDrag,
        hoverLeftOf: bindActionCreators({ cellHoverLeftOf: cellHoverLeftOf }, dispatch)
            .cellHoverLeftOf,
        hoverRightOf: bindActionCreators({ cellHoverRightOf: cellHoverRightOf }, dispatch)
            .cellHoverRightOf,
        hoverAbove: bindActionCreators({ cellHoverAbove: cellHoverAbove }, dispatch).cellHoverAbove,
        hoverBelow: bindActionCreators({ cellHoverBelow: cellHoverBelow }, dispatch).cellHoverBelow,
        hoverFloatingLeft: bindActionCreators({ cellHoverInlineLeft: cellHoverInlineLeft }, dispatch)
            .cellHoverInlineLeft,
        hoverFloatingRight: bindActionCreators({ cellHoverInlineRight: cellHoverInlineRight }, dispatch)
            .cellHoverInlineRight,
        clearHover: bindActionCreators({ clearHover: clearHover }, dispatch).clearHover,
        insertBelow: bindActionCreators({ insertCellBelow: insertCellBelow }, dispatch)
            .insertCellBelow,
        insertAbove: bindActionCreators({ insertCellAbove: insertCellAbove }, dispatch)
            .insertCellAbove,
        insertRightOf: bindActionCreators({ insertCellRightOf: insertCellRightOf }, dispatch)
            .insertCellRightOf,
        insertLeftOf: bindActionCreators({ insertCellLeftOf: insertCellLeftOf }, dispatch)
            .insertCellLeftOf,
        insertFloatingLeft: bindActionCreators({ insertCellLeftInline: insertCellLeftInline }, dispatch)
            .insertCellLeftInline,
        insertFloatingRight: bindActionCreators({ insertCellRightInline: insertCellRightInline }, dispatch)
            .insertCellRightInline,
    },
    editable: {
        add: bindActionCreators({ updateEditable: updateEditable }, dispatch).updateEditable,
        update: bindActionCreators({ updateEditable: updateEditable }, dispatch).updateEditable,
    },
    mode: {
        insert: bindActionCreators({ insertMode: insertMode }, dispatch).insertMode,
        edit: bindActionCreators({ editMode: editMode }, dispatch).editMode,
        preview: bindActionCreators({ previewMode: previewMode }, dispatch).previewMode,
        layout: bindActionCreators({ layoutMode: layoutMode }, dispatch).layoutMode,
        resize: bindActionCreators({ resizeMode: resizeMode }, dispatch).resizeMode,
    },
    undo: bindActionCreators({ undo: undo }, dispatch).undo,
    redo: bindActionCreators({ redo: redo }, dispatch).redo,
}); };
//# sourceMappingURL=index.js.map