import * as DisplaySelectors from './display';
import * as EditableSelectors from './editable';
import { editable, editables } from './editable';
import * as SettingSelectors from './setting';
export var Selectors = {
    Display: DisplaySelectors,
    Setting: SettingSelectors,
    Editable: EditableSelectors,
};
export var selectors = function (store) { return ({
    editable: function (id) { return editable(store.getState(), { id: id }); },
    editables: function (id) { return editables(store.getState()); },
}); };
export { editable, editables };
//# sourceMappingURL=index.js.map