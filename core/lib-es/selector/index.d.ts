import { Store } from 'redux';
import { RootState } from '../types/state';
import * as DisplaySelectors from './display';
import * as EditableSelectors from './editable';
import { editable, editables } from './editable';
import * as SettingSelectors from './setting';
export declare const Selectors: {
    Display: typeof DisplaySelectors;
    Setting: typeof SettingSelectors;
    Editable: typeof EditableSelectors;
};
export declare const selectors: (store: Store<RootState>) => {
    editable: (id: string) => import("../types/editable").AbstractEditable<import("../types/editable").AbstractCell<import("..").Row>>;
    editables: (id: string) => import("../types/editable").AbstractEditable<import("../types/editable").AbstractCell<import("..").Row>>[];
};
export { editable, editables, RootState };
//# sourceMappingURL=index.d.ts.map