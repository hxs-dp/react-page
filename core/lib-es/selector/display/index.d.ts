import { RootState } from '../../types/state';
export declare const isPreviewMode: ({ reactPage: { display: { mode }, }, }: RootState) => boolean;
export declare const isLayoutMode: ({ reactPage: { display: { mode }, }, }: RootState) => boolean;
export declare const isEditMode: ({ reactPage: { display: { mode }, }, }: RootState) => boolean;
export declare const isInsertMode: ({ reactPage: { display: { mode }, }, }: RootState) => boolean;
export declare const isResizeMode: ({ reactPage: { display: { mode }, }, }: RootState) => boolean;
//# sourceMappingURL=index.d.ts.map