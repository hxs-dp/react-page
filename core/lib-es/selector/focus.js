export var focus = function (state) {
    return state && state.reactPage && state.reactPage.focus;
};
//# sourceMappingURL=focus.js.map