/**
 * A list of positions in the layout space.
 */
export declare enum PositionEnum {
    LEFT_OF = "left-of",
    RIGHT_OF = "right-of",
    ABOVE = "above",
    BELOW = "below",
    INLINE_LEFT = "inline-left",
    INLINE_RIGHT = "inline-right"
}
/**
 * Is true if built in production mode.
 */
export declare const isProduction: boolean;
//# sourceMappingURL=const.d.ts.map