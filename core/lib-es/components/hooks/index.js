import { useDispatch } from '../../reduxConnect';
import { undo, redo } from '../../actions/undo';
import { useCallback, useContext, createContext } from 'react';
import { duplicateCell, insertCellAtTheEnd } from '../../actions/cell/insert';
import { isEditMode, isInsertMode, isLayoutMode } from '../../selector/display';
import { useSelector } from '../../reduxConnect';
import { node, parentCellSelector } from '../../selector/editable';
import { Selectors } from '../../selector';
import { EditorContext } from '../../Editor';
import { updateCellIsDraft, focusCell, updateCellContent, updateCellLayout, removeCell, } from '../../actions/cell/core';
import { setLang } from '../../actions/setting';
export var EditableContext = createContext(null);
export var useEditableId = function () { return useContext(EditableContext); };
export var useEditor = function () { return useContext(EditorContext); };
export var useNode = function (id) {
    var editableId = useEditableId();
    return useSelector(function (state) {
        return node(state, { id: id, editable: editableId });
    });
};
var isCell = function (node) {
    var _a, _b;
    return Boolean(((_a = node) === null || _a === void 0 ? void 0 : _a.content) || ((_b = node) === null || _b === void 0 ? void 0 : _b.layout));
};
export var useCell = function (id) {
    var node = useNode(id);
    if (isCell(node)) {
        return node;
    }
    return null;
};
export var useParentCell = function (id) {
    var editableId = useEditableId();
    return useSelector(function (state) {
        return parentCellSelector(state, { id: id, editable: editableId });
    });
};
export var useIsEditMode = function () {
    return useSelector(isEditMode);
};
export var useIsInsertMode = function () {
    return useSelector(isInsertMode);
};
export var useIsLayoutMode = function () {
    return useSelector(isLayoutMode);
};
export var useLang = function () {
    return useSelector(Selectors.Setting.getLang);
};
// actions
export var useUndo = function () {
    var dispatch = useDispatch();
    return useCallback(function () { return dispatch(undo()); }, [dispatch]);
};
export var useRedo = function () {
    var dispatch = useDispatch();
    return useCallback(function () { return dispatch(redo()); }, [dispatch]);
};
export var useSetDraft = function () {
    var dispatch = useDispatch();
    return useCallback(function (id, isDraft, lang) {
        return dispatch(updateCellIsDraft(id, isDraft, lang));
    }, [dispatch]);
};
export var useSetLang = function () {
    var dispatch = useDispatch();
    return useCallback(function (lang) { return dispatch(setLang(lang)); }, [dispatch]);
};
export var useUpdateCellContent = function () {
    var dispatch = useDispatch();
    return useCallback(
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    function (id, state, lang) {
        return dispatch(updateCellContent(id)(state, lang));
    }, [dispatch]);
};
export var useUpdateCellLayout = function () {
    var dispatch = useDispatch();
    return useCallback(
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    function (id, state, lang) {
        return dispatch(updateCellLayout(id)(state, lang));
    }, [dispatch]);
};
export var useRemoveCell = function () {
    var dispatch = useDispatch();
    return useCallback(
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    function (id) { return dispatch(removeCell(id)); }, [dispatch]);
};
export var useDuplicateCell = function () {
    var dispatch = useDispatch();
    var editor = useEditor();
    var editableId = useEditableId();
    return useCallback(function (nodeId) {
        return dispatch(duplicateCell(editor.getNode(editableId, nodeId)));
    }, [dispatch, editableId]);
};
export var useFocusCell = function () {
    var dispatch = useDispatch();
    return useCallback(function (id, scrollToCell) {
        dispatch(focusCell(id, scrollToCell)());
    }, [dispatch]);
};
export var useInsertCellAtTheEnd = function () {
    var dispatch = useDispatch();
    return useCallback(function (node) {
        dispatch(insertCellAtTheEnd(node, {}));
    }, [dispatch]);
};
//# sourceMappingURL=index.js.map