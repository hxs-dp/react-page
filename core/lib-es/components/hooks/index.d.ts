/// <reference types="react" />
import { RootState } from '../../selector';
import Editor from '../../Editor';
import { Cell, Row } from '../../types/editable';
export declare const EditableContext: import("react").Context<string>;
export declare const useEditableId: () => string;
export declare const useEditor: () => Editor<RootState>;
export declare const useNode: (id: string) => Row | import("../../types/editable").AbstractCell<Row>;
export declare const useCell: (id: string) => import("../../types/editable").AbstractCell<Row>;
export declare const useParentCell: (id: string) => import("../../types/editable").AbstractCell<Row>;
export declare const useIsEditMode: () => boolean;
export declare const useIsInsertMode: () => boolean;
export declare const useIsLayoutMode: () => boolean;
export declare const useLang: () => string;
export declare const useUndo: () => () => import("redux").Action<any>;
export declare const useRedo: () => () => import("redux").Action<any>;
export declare const useSetDraft: () => (id: string, isDraft: boolean, lang: string) => import("../../actions/cell/core").UpdateCellIsDraftAction;
export declare const useSetLang: () => (lang: string) => import("../../actions/setting").SetLangAction;
export declare const useUpdateCellContent: () => (id: string, state: any, lang?: string) => import("../../actions/cell/core").UpdateCellContentAction;
export declare const useUpdateCellLayout: () => (id: string, state: any, lang?: string) => import("../../actions/cell/core").UpdateCellLayoutAction;
export declare const useRemoveCell: () => (id: string) => import("../../actions/cell/core").RemoveCellAction;
export declare const useDuplicateCell: () => (nodeId: string) => (dispatch: any) => void;
export declare const useFocusCell: () => (id: string, scrollToCell?: boolean) => void;
export declare const useInsertCellAtTheEnd: () => (node: Partial<Cell>) => void;
//# sourceMappingURL=index.d.ts.map