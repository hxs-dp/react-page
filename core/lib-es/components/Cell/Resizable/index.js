var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import classNames from 'classnames';
import throttle from 'lodash/throttle';
import * as React from 'react';
import { Resizable as ReactResizeable } from 'react-resizable';
import { createStructuredSelector } from 'reselect';
import { editMode, resizeMode } from '../../../actions/display';
import { connect } from '../../../reduxConnect';
import { computeStepWidth, widthToSize } from './helper';
var Resizable = /** @class */ (function (_super) {
    __extends(Resizable, _super);
    function Resizable(props) {
        var _this = _super.call(this, props) || this;
        _this.onChangeSize = function (size) {
            if (isNaN(size.width)) {
                return;
            }
            var newSize = widthToSize(_this.state, _this.props, size);
            _this.props.onChange(newSize);
        };
        _this.onResize = function (event, _a) {
            var size = _a.size;
            if (isNaN(size.width)) {
                return;
            }
            _this.setState({ width: size.width });
            _this.onChangeSizeThrottled(size);
        };
        _this.onResizeStop = function (event, _a) {
            var size = _a.size;
            if (isNaN(size.width)) {
                return;
            }
            _this.onChangeSize(size);
            var newSize = widthToSize(_this.state, _this.props, size);
            _this.setState({ width: newSize * _this.state.stepWidth });
        };
        var sw = computeStepWidth(props);
        _this.onChangeSizeThrottled = throttle(_this.onChangeSize, 100);
        _this.state = {
            stepWidth: sw,
            width: props.node.size * sw,
            steps: props.steps - 1 || 11,
        };
        return _this;
    }
    Resizable.prototype.render = function () {
        var _a;
        var _b = this.props, _c = _b.node, bounds = _c.bounds, inline = _c.inline, children = _b.children;
        return (React.createElement(ReactResizeable, { className: classNames('ory-cell-inner', 'ory-cell-resizable', (_a = {},
                _a["ory-cell-resizable-inline-" + (inline || '')] = inline,
                _a)), onResize: this.onResize, onResizeStop: this.onResizeStop, minConstraints: inline ? null : [this.state.stepWidth, Infinity], maxConstraints: inline ? null : [bounds.right * this.state.stepWidth, Infinity], draggableOpts: { axis: 'none', offsetParent: document.body }, width: this.state.width, height: 0 },
            React.createElement("div", null, children)));
    };
    return Resizable;
}(React.PureComponent));
var mapStateToProps = createStructuredSelector({});
var mapDispatchToProps = { resizeMode: resizeMode, editMode: editMode };
export default connect(mapStateToProps, mapDispatchToProps)(Resizable);
//# sourceMappingURL=index.js.map