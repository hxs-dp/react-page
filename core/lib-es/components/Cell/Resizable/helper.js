export var computeStepWidth = function (_a) {
    var rowWidth = _a.rowWidth, steps = _a.steps;
    return Math.round(rowWidth / (steps || 12));
};
export var widthToSize = function (_a, _b, result) {
    var stepWidth = _a.stepWidth, steps = _a.steps;
    var inline = _b.node.inline;
    var size = Math.round(result.width / stepWidth);
    if (inline === 'right') {
        size = steps - size;
    }
    if (size > steps) {
        size = steps;
    }
    else if (size < 1) {
        size = 1;
    }
    return size;
};
//# sourceMappingURL=helper.js.map