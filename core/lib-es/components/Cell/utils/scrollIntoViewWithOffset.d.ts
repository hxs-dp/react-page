declare const _default: (element: HTMLElement, offset?: number, behavior?: ScrollBehavior) => void;
export default _default;
//# sourceMappingURL=scrollIntoViewWithOffset.d.ts.map