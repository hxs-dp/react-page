var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
import * as React from 'react';
var ErrorCell = function (_a) {
    var _b = _a.id, id = _b === void 0 ? 'no id given' : _b, error = _a.error, props = __rest(_a, ["id", "error"]);
    return (React.createElement("div", { className: "ory-cell-error" },
        React.createElement("strong", null, "An error occurred!"),
        React.createElement("small", null,
            React.createElement("dl", null,
                React.createElement("dt", null, "Cause:"),
                React.createElement("dd", null, error.message),
                React.createElement("dt", null, "Cell:"),
                React.createElement("dd", null, id))),
        props.isEditMode ? (React.createElement("button", { onClick: function () { return props.removeCell(); } }, "Remove")) : null));
};
export default ErrorCell;
//# sourceMappingURL=index.js.map