var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
import classNames from 'classnames';
import * as React from 'react';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { blurAllCells, focusCell, resizeCell, } from '../../actions/cell';
import { connect } from '../../reduxConnect';
import { Selectors } from '../../selector';
import { isEditMode, isInsertMode, isLayoutMode, isPreviewMode, isResizeMode, } from '../../selector/display';
import { editableConfig, node, purifiedNode, } from '../../selector/editable';
import { removeCell, } from './../../actions/cell/core';
import Inner from './Inner';
import Resizable from './Resizable';
var gridClass = function (_a) {
    var size = _a.node.size, rest = __rest(_a, ["node"]);
    if (rest.isPreviewMode || rest.isEditMode) {
        return "ory-cell-" + (rest.isPreviewMode || rest.isEditMode ? 'sm' : 'xs') + "-" + (size || 12) + " ory-cell-xs-12";
    }
    return "ory-cell-xs-" + (size || 12);
};
var stopClick = function (_isEditMode) { return function (e) { return (_isEditMode ? e.stopPropagation() : null); }; };
var Cell = /** @class */ (function (_super) {
    __extends(Cell, _super);
    function Cell() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Cell.prototype.render = function () {
        var _a;
        var _b;
        var _c = this.props, id = _c.id, rowWidth = _c.rowWidth, rowHeight = _c.rowHeight, updateDimensions = _c.updateDimensions, lang = _c.lang, _d = _c.node, inline = _d.inline, resizable = _d.resizable, hasInlineNeighbour = _d.hasInlineNeighbour, focused = _d.focused, isDraft = _d.isDraft, isDraftI18n = _d.isDraftI18n;
        var isDraftInLang = (_b = isDraftI18n === null || isDraftI18n === void 0 ? void 0 : isDraftI18n[lang]) !== null && _b !== void 0 ? _b : isDraft;
        if (isDraftInLang && this.props.isPreviewMode) {
            return null;
        }
        return (React.createElement("div", { className: classNames('ory-cell', gridClass(this.props), (_a = {
                    'ory-cell-has-inline-neighbour': hasInlineNeighbour
                },
                _a["ory-cell-inline-" + (inline || '')] = inline,
                _a['ory-cell-focused'] = focused,
                _a['ory-cell-is-draft'] = isDraftInLang,
                _a['ory-cell-resizing-overlay'] = this.props.isResizeMode,
                _a['ory-cell-bring-to-front'] = !this.props.isResizeMode && !this.props.isLayoutMode && inline,
                _a)), onClick: stopClick(this.props.isEditMode) }, resizable &&
            (this.props.isResizeMode || this.props.allowResizeInEditMode) &&
            rowWidth ? (React.createElement(Resizable, __assign({}, this.props, { id: id, rowWidth: rowWidth, rowHeight: rowHeight, updateDimensions: updateDimensions, node: this.props.node, steps: 12, onChange: this.props.resizeCell }),
            React.createElement(Inner, __assign({}, this.props, { styles: null })))) : (React.createElement(Inner, __assign({}, this.props, { styles: null })))));
    };
    return Cell;
}(React.PureComponent));
var mapStateToProps = createStructuredSelector({
    isPreviewMode: isPreviewMode,
    isEditMode: isEditMode,
    isResizeMode: isResizeMode,
    // required by sub-components
    isInsertMode: isInsertMode,
    isLayoutMode: isLayoutMode,
    config: editableConfig,
    node: purifiedNode,
    lang: Selectors.Setting.getLang,
    rawNode: function (state, props) { return function () { return node(state, props); }; },
});
var mapDispatchToProps = function (dispatch, _a) {
    var id = _a.id;
    return bindActionCreators({
        resizeCell: resizeCell(id),
        focusCell: focusCell(id),
        blurAllCells: blurAllCells,
        removeCell: function () { return removeCell(id); },
    }, 
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    dispatch);
};
export default connect(mapStateToProps, mapDispatchToProps)(Cell);
//# sourceMappingURL=index.js.map