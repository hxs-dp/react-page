import { DropTargetConnector, DropTargetMonitor } from 'react-dnd';
import { ComponetizedCell } from '../../../../types/editable';
export declare const target: {
    hover: any;
    canDrop: ({ id, ancestors }: ComponetizedCell, monitor: DropTargetMonitor) => boolean;
    drop(hover: ComponetizedCell, monitor: DropTargetMonitor, component: any): void;
};
export declare const connect: (connectInner: DropTargetConnector, monitor: DropTargetMonitor) => {
    connectDropTarget: import("react-dnd").DragElementWrapper<any>;
    isOver: boolean;
    isOverCurrent: boolean;
};
//# sourceMappingURL=dnd.d.ts.map