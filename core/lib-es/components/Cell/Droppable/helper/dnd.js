import throttle from 'lodash.throttle';
import { createNativeCellReplacement, isNativeHTMLElementDrag, } from '../../../../helper/nativeDragHelpers';
import { delay } from '../../../../helper/throttle';
import { computeAndDispatchHover, computeAndDispatchInsert, } from '../../../../service/hover/input';
import logger from '../../../../service/logger';
var last = { hover: '', drag: '' };
var clear = function (hover, drag) {
    if (hover.id === last.hover && drag === last.drag) {
        return;
    }
    last = { hover: hover.id, drag: drag };
    hover.clearHover();
};
export var target = {
    hover: throttle(function (hover, monitor, component) {
        var _a, _b, _c, _d;
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        var drag = monitor.getItem();
        if (!drag) {
            // item undefined, happens when throttle triggers after drop
            return;
        }
        if (isNativeHTMLElementDrag(monitor)) {
            drag = createNativeCellReplacement();
        }
        if (drag.id === hover.id) {
            // If hovering over itself, do nothing
            clear(hover, drag.id);
            return;
        }
        else if (!monitor.isOver({ shallow: true })) {
            // If hovering over ancestor cell, do nothing (we are going to propagate later in the tree anyways)
            return;
        }
        else if (hover.ancestors.indexOf(drag.id) > -1) {
            // If hovering over a child of itself
            clear(hover, drag.id);
            return;
        }
        else if (!hover.id) {
            // If hovering over something that isn't a cell or hasn't an id, do nothing. Should be an edge case
            logger.warn('Canceled cell drop, no id given.', hover, drag);
            return;
        }
        last = { hover: hover.id, drag: drag.id };
        var allowInlineNeighbours = (_d = (_c = (_b = (_a = hover === null || hover === void 0 ? void 0 : hover.node) === null || _a === void 0 ? void 0 : _a.content) === null || _b === void 0 ? void 0 : _b.plugin) === null || _c === void 0 ? void 0 : _c.allowInlineNeighbours) !== null && _d !== void 0 ? _d : false;
        computeAndDispatchHover(hover, drag, monitor, component, "10x10" + (allowInlineNeighbours ? '' : '-no-inline'));
    }, delay, { leading: false }),
    canDrop: function (_a, monitor) {
        var id = _a.id, ancestors = _a.ancestors;
        var item = monitor.getItem();
        return item.id !== id && ancestors.indexOf(item.id) === -1;
    },
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    drop: function (hover, monitor, component) {
        var _a, _b, _c, _d;
        var drag = monitor.getItem();
        if (isNativeHTMLElementDrag(monitor)) {
            var plugins = component.props.config.plugins;
            drag = plugins.createNativePlugin(hover, monitor, component);
        }
        if (monitor.didDrop() || !monitor.isOver({ shallow: true })) {
            // If the item drop occurred deeper down the tree, don't do anything
            return;
        }
        else if (drag.id === hover.id) {
            // If the item being dropped on itself do nothing
            hover.cancelCellDrag();
            return;
        }
        else if (hover.ancestors.indexOf(drag.id) > -1) {
            // If hovering over a child of itself, don't propagate further
            hover.cancelCellDrag();
            return;
        }
        last = { hover: hover.id, drag: drag.id };
        var allowInlineNeighbours = (_d = (_c = (_b = (_a = hover === null || hover === void 0 ? void 0 : hover.node) === null || _a === void 0 ? void 0 : _a.content) === null || _b === void 0 ? void 0 : _b.plugin) === null || _c === void 0 ? void 0 : _c.allowInlineNeighbours) !== null && _d !== void 0 ? _d : false;
        computeAndDispatchInsert(hover, drag, monitor, component, "10x10" + (allowInlineNeighbours ? '' : '-no-inline'));
    },
};
export var connect = function (connectInner, monitor) { return ({
    connectDropTarget: connectInner.dropTarget(),
    isOver: monitor.isOver(),
    isOverCurrent: monitor.isOver({ shallow: true }),
}); };
//# sourceMappingURL=dnd.js.map