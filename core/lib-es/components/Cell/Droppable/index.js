var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import classNames from 'classnames';
import * as React from 'react';
import { DropTarget as dropTarget } from 'react-dnd';
import { dragActions } from '../../../actions/cell/drag';
import { insertActions } from '../../../actions/cell/insert';
import { connect } from '../../../reduxConnect';
import { connect as monitorConnect, target } from './helper/dnd';
var Droppable = /** @class */ (function (_super) {
    __extends(Droppable, _super);
    function Droppable() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Droppable.prototype.render = function () {
        var _a;
        var _b = this.props, connectDropTarget = _b.connectDropTarget, isLayoutMode = _b.isLayoutMode, isInsertMode = _b.isInsertMode, className = _b.className, isLeaf = _b.isLeaf, hover = _b.node.hover, children = _b.children, allowMoveInEditMode = _b.allowMoveInEditMode;
        if (!(isLayoutMode || isInsertMode) && !allowMoveInEditMode) {
            return (React.createElement("div", { className: classNames(className, 'ory-cell-droppable-container') }, children));
        }
        return connectDropTarget(React.createElement("div", { className: classNames(className, 'ory-cell-droppable', (_a = {
                    'ory-cell-droppable-is-over-current': hover
                },
                _a["ory-cell-droppable-is-over-" + hover] = hover,
                _a['ory-cell-droppable-leaf'] = isLeaf,
                _a)) }, children));
    };
    return Droppable;
}(React.PureComponent));
var mapDispatchToProps = __assign(__assign({}, dragActions), insertActions);
export default connect(null, mapDispatchToProps)(dropTarget(function (_a) {
    var dropTypes = _a.dropTypes;
    return dropTypes;
}, target, monitorConnect)(Droppable));
//# sourceMappingURL=index.js.map