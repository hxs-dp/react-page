/// <reference types="react" />
import { DragSourceConnector, DragSourceMonitor } from 'react-dnd';
import { ComponetizedCell } from '../../../../types/editable';
export declare const source: {
    beginDrag(props: ComponetizedCell): {
        children: any;
        node: {
            rows: import("../../../../types/editable").Row[];
            id: string;
            content?: import("../../../../types/editable").Content<any>;
            layout?: import("../../../../types/editable").Layout<any>;
            size?: number;
            hover?: string;
            inline?: string;
            focused?: boolean;
            scrollToCell?: Number;
            isDraft?: boolean;
            isDraftI18n?: import("../../../../types/editable").I18nField<boolean>;
            focusSource?: string;
            resizable?: boolean;
            bounds?: {
                left: number;
                right: number;
            };
            hasInlineNeighbour?: string;
            levels?: import("../../../../types/editable").Levels;
        };
        id: string;
        editable: string;
        ancestors: string[];
        config: import("../../../../types/editable").Config;
        isInsertMode: boolean;
        isResizeMode: boolean;
        isDisplayMode: boolean;
        isEditMode: boolean;
        isLayoutMode: boolean;
        isPreviewMode: boolean;
        steps: number;
        rowHeight: number;
        rowWidth: number;
        updateDimensions: () => void;
        onResize: () => void;
        styles: import("react").CSSProperties;
        rawNode(): import("../../../../types/editable").AbstractCell<import("../../../../types/editable").Row>;
        clearHover(): void;
        removeCell(): void;
        resizeCell(id: string): void;
        focusCell(props: {
            source?: string;
        }): void;
        blurCell(id: string): void;
        blurAllCells(): void;
        updateCellContent(state: any, lang?: string): void;
        updateCellLayout(state: any, lang?: string): void;
        cancelCellDrag(): void;
        dragCell(drag: string): void;
        cellHoverAbove(drag: import("../../../../types/editable").AbstractCell<import("../../../../types/editable").Row>, hover: import("../../../../types/editable").AbstractCell<import("../../../../types/editable").Row>, level: number): void;
        cellHoverBelow(drag: import("../../../../types/editable").AbstractCell<import("../../../../types/editable").Row>, hover: import("../../../../types/editable").AbstractCell<import("../../../../types/editable").Row>, level: number): void;
        cellHoverLeftOf(drag: import("../../../../types/editable").AbstractCell<import("../../../../types/editable").Row>, hover: import("../../../../types/editable").AbstractCell<import("../../../../types/editable").Row>, level: number): void;
        cellHoverRightOf(drag: import("../../../../types/editable").AbstractCell<import("../../../../types/editable").Row>, hover: import("../../../../types/editable").AbstractCell<import("../../../../types/editable").Row>, level: number): void;
        cellHoverInlineLeft(drag: import("../../../../types/editable").AbstractCell<import("../../../../types/editable").Row>, hover: import("../../../../types/editable").AbstractCell<import("../../../../types/editable").Row>): void;
        cellHoverInlineRight(drag: import("../../../../types/editable").AbstractCell<import("../../../../types/editable").Row>, hover: import("../../../../types/editable").AbstractCell<import("../../../../types/editable").Row>): void;
        insertCellAbove(type: string): void;
        insertCellBelow(type: string): void;
        insertCellLeftInline(type: string): void;
        insertCellLeftOf(type: string): void;
        insertCellRightInline(type: string): void;
        insertCellRightOf(type: string): void;
        onChange(state: any): void;
    };
    endDrag({ cancelCellDrag, id }: ComponetizedCell, monitor: DragSourceMonitor): void;
};
export declare const collect: (connect: DragSourceConnector, monitor: DragSourceMonitor) => {
    connectDragSource: import("react-dnd").DragElementWrapper<import("react-dnd").DragSourceOptions>;
    isDragging: boolean;
    connectDragPreview: import("react-dnd").DragElementWrapper<import("react-dnd").DragPreviewOptions>;
};
//# sourceMappingURL=dnd.d.ts.map