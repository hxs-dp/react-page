var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
export var source = {
    beginDrag: function (props) {
        // Begin dragging the cell
        props.dragCell(props.id);
        return __assign(__assign({}, props), { 
            // we do not want to pass down the react children or we will risk circular dependencies.
            children: null, node: __assign(__assign({}, props.node), { rows: props.rawNode().rows }) });
    },
    endDrag: function (_a, monitor) {
        var cancelCellDrag = _a.cancelCellDrag, id = _a.id;
        if (monitor.didDrop()) {
            // If the item drop occurred deeper down the tree, don't do anything
            return;
        }
        // If drag ended but drop did not occur, cancel dragging
        cancelCellDrag();
    },
};
export var collect = function (connect, monitor) { return ({
    connectDragSource: connect.dragSource(),
    isDragging: monitor.isDragging(),
    connectDragPreview: connect.dragPreview(),
}); };
//# sourceMappingURL=dnd.js.map