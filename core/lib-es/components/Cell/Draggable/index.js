var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import classNames from 'classnames';
import * as React from 'react';
import { DragSource as dragSource } from 'react-dnd';
import { dragActions } from '../../../actions/cell/drag';
import { insertActions } from '../../../actions/cell/insert';
import { connect } from '../../../reduxConnect';
import { collect, source } from './helper/dnd';
var icon = 
// tslint:disable-next-line:max-line-length
'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAhCAYAAACbffiEAAAA6UlEQVRYhe2ZQQ6CMBBFX0njHg7ESXTp1p3uvIBewc3Em3AfdelSFwRDCAm01JRO+pa0lP8zzc9kMCKyAa7AFqhIixdwB44WuACHuHq8KWm1vwtgF1lMCPaWkevUNE3Qr9R17XTu1P5uvUdV+IpbG2qMGBH5xBYRAjUVUWPEjj10SS3XRFry3kha/VBTETVGcmqtDTVGFqdWn7k9ku96f88QNRVRYySn1tpQY8QptXz7qinmnpt7rZTIqbU21BgJ2mv1+XfCDVFTETVGjIg8SG8KP+RZ0I7lU+dmgRNgaKfyZVw9znT/R85fOHJJE77U6UcAAAAASUVORK5CYII=';
var defaultSmallHandle = (React.createElement("div", { className: "ory-cell-draggable-overlay-handle" },
    React.createElement("div", { className: "ory-cell-draggable-overlay-handle-icon" })));
var Draggable = /** @class */ (function (_super) {
    __extends(Draggable, _super);
    function Draggable(props) {
        var _this = _super.call(this, props) || this;
        _this.preventBlurWhenClickingOnHandle = _this.preventBlurWhenClickingOnHandle.bind(_this);
        return _this;
    }
    Draggable.prototype.componentDidMount = function () {
        var _this = this;
        var img = new Image();
        img.onload = function () { return _this.props.connectDragPreview(img); };
        img.src = icon;
    };
    Draggable.prototype.render = function () {
        var _a;
        var _b = this.props, isLeaf = _b.isLeaf, connectDragSource = _b.connectDragSource, isDragging = _b.isDragging, isLayoutMode = _b.isLayoutMode, _c = _b.node, inline = _c.inline, focused = _c.focused, children = _b.children, name = _b.name, allowMoveInEditMode = _b.allowMoveInEditMode;
        if (!isLayoutMode && !this.props.allowMoveInEditMode) {
            return (React.createElement("div", { className: "ory-cell-draggable-container" },
                React.createElement("div", { className: "ory-cell-draggable-overlay-placeholder" }),
                children));
        }
        if (this.props.allowMoveInEditMode && !this.props.isLayoutMode) {
            var handle = focused
                ? connectDragSource(this.props.editModeResizeHandle || defaultSmallHandle)
                : null;
            return (React.createElement("div", { className: classNames({
                    'ory-cell-draggable-in-edit': allowMoveInEditMode,
                    'ory-cell-draggable': isLayoutMode && !allowMoveInEditMode,
                    'ory-cell-draggable-is-dragging': isDragging,
                }), onMouseDown: this.preventBlurWhenClickingOnHandle },
                handle,
                React.createElement("div", null, children)));
        }
        return connectDragSource(React.createElement("div", { className: classNames({
                'ory-cell-draggable': isLayoutMode,
                'ory-cell-draggable-is-dragging': isDragging,
            }) },
            React.createElement("div", { className: classNames((_a = {
                        'ory-cell-draggable-overlay': isLayoutMode
                    },
                    _a["ory-cell-draggable-inline-" + inline] = inline,
                    _a['ory-cell-draggable-leaf'] = isLeaf,
                    _a)) },
                React.createElement("div", { className: "ory-cell-draggable-overlay-description" },
                    React.createElement("span", null, name))),
            React.createElement("div", null, children)));
    };
    Draggable.prototype.preventBlurWhenClickingOnHandle = function (e) {
        e.stopPropagation();
    };
    return Draggable;
}(React.PureComponent));
var mapDispatchToProps = __assign(__assign({}, dragActions), insertActions);
export default connect(null, mapDispatchToProps)(dragSource(function (_a) {
    var dragType = _a.dragType;
    return dragType;
}, source, collect)(Draggable));
//# sourceMappingURL=index.js.map