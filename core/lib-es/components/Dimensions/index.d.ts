declare const Dimensions: ({ className, elementResize }?: {
    className?: any;
    elementResize?: boolean;
}) => (ComposedComponent: any) => (props: any) => JSX.Element;
export default Dimensions;
//# sourceMappingURL=index.d.ts.map