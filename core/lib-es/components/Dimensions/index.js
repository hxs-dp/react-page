/* eslint-disable @typescript-eslint/ban-types */
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import classNames from 'classnames';
import * as React from 'react';
var getWidth = function (element) { return element.clientWidth; };
var getHeight = function (element) { return element.clientHeight; };
var Dimensions = function (_a) {
    var _b = _a === void 0 ? {} : _a, _c = _b.className, className = _c === void 0 ? null : _c, _d = _b.elementResize, elementResize = _d === void 0 ? false : _d;
    return function (ComposedComponent) {
        var Decorator = /** @class */ (function (_super) {
            __extends(Decorator, _super);
            function Decorator(props) {
                var _this = _super.call(this, props) || this;
                _this.updateDimensions = function () {
                    var container = _this.containerRef;
                    if (!container) {
                        return;
                    }
                    var containerWidth = getWidth(container);
                    var containerHeight = getHeight(container);
                    if (containerWidth !== _this.state.containerWidth ||
                        containerHeight !== _this.state.containerHeight) {
                        _this.setState({ containerWidth: containerWidth, containerHeight: containerHeight });
                    }
                };
                _this.onResize = function () {
                    if (_this.rqf) {
                        return;
                    }
                    _this.rqf = _this.getWindow().requestAnimationFrame(function () {
                        _this.rqf = null;
                        _this.updateDimensions();
                    });
                };
                _this.onContainerRef = function (ref) {
                    _this.containerRef = ref;
                    _this.updateDimensions();
                };
                _this.state = {};
                return _this;
            }
            Decorator.prototype.componentDidMount = function () {
                if (!this.containerRef) {
                    throw new Error('Cannot find container div');
                }
                // eslint-disable-next-line @typescript-eslint/no-explicit-any
                if (global.IntersectionObserver) {
                    this.observer = new IntersectionObserver(this.onResize);
                }
                this.observer.observe(this.containerRef);
                this.updateDimensions();
                this.getWindow().addEventListener('resize', this.onResize, false);
            };
            // This cann not be used here because it doesn't listen to state changes.
            Decorator.prototype.componentWillUnmount = function () {
                this.getWindow().removeEventListener('resize', this.onResize);
                if (this.observer) {
                    this.observer.disconnect();
                }
            };
            // If the component is mounted in a different window to the javascript
            // context, as with https://github.com/JakeGinnivan/react-popout
            // then the `window` global will be different from the `window` that
            // contains the component.
            // Depends on `defaultView` which is not supported <IE9
            Decorator.prototype.getWindow = function () {
                return this.containerRef
                    ? this.containerRef.ownerDocument.defaultView || window
                    : window;
            };
            Decorator.prototype.render = function () {
                return (React.createElement("div", { className: classNames(className, 'ory-dimensions'), ref: this.onContainerRef },
                    React.createElement(ComposedComponent, __assign({}, this.state, this.props, { updateDimensions: this.updateDimensions }))));
            };
            return Decorator;
        }(React.Component));
        return function (props) { return React.createElement(Decorator, __assign({}, props)); };
    };
};
export default Dimensions;
//# sourceMappingURL=index.js.map