import React, { useEffect } from 'react';
import { blurAllCells } from '../actions/cell';
import { setMode as setModeInternal, DISPLAY_MODE_EDIT, } from '../actions/display';
import { connect } from '../reduxConnect';
import { createStructuredSelector } from 'reselect';
import { isInsertMode } from '../selector/display';
// this might break in future, but its better than nothing
// eslint-disable-next-line @typescript-eslint/no-explicit-any
function findReactElement(node) {
    for (var key in node) {
        if (key.startsWith('__reactInternalInstance$')) {
            return node[key];
        }
    }
    return null;
}
// we go up the reac-tree. This works even through portals, which would not be possible with traversing the dom tree!
var isInSameTree = function (parent, child) {
    if (!parent) {
        return false;
    }
    var element = findReactElement(child);
    while (element) {
        if (element.stateNode === parent) {
            return true;
        }
        element = element.return;
    }
    return false;
};
var useBlurAll = function (blurAllCellsDispatch, setMode, isInInsertMode, defaultMode, disabled) {
    if (defaultMode === void 0) { defaultMode = DISPLAY_MODE_EDIT; }
    var ref = React.useRef();
    useEffect(function () {
        if (disabled) {
            return;
        }
        if (!ref.current) {
            return;
        }
        if (!document && !document.body) {
            return;
        }
        var onMouseDown = function (e) {
            if (!isInSameTree(ref.current, e.target)) {
                blurAllCellsDispatch();
                // set us in default mode if current mode is "insert"
                if (isInInsertMode) {
                    setMode(defaultMode);
                }
            }
        };
        document.body.addEventListener('mousedown', onMouseDown);
        return function () {
            document.body.removeEventListener('mousedown', onMouseDown);
        };
    }, [ref.current, disabled, isInInsertMode]);
    return ref;
};
var mapStateToProps = createStructuredSelector({ isInsertMode: isInsertMode });
var mapDispatchToProps = { blurAllCells: blurAllCells, setMode: setModeInternal };
var BlurGate = connect(mapStateToProps, mapDispatchToProps)(function (props) {
    var ref = useBlurAll(props.blurAllCells, props.setMode, props.defaultMode, props.isInsertMode, props.disabled);
    return React.createElement("div", { ref: ref }, props.children);
});
export default BlurGate;
//# sourceMappingURL=BlurGate.js.map