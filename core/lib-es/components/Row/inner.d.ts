import { ComponetizedRow, SimplifiedModesProps } from '../../types/editable';
declare const Inner: ({ editable, ancestors, node: { id, hover, cells }, containerHeight, blurAllCells, containerWidth, allowMoveInEditMode, allowResizeInEditMode, editModeResizeHandle, rawNode, }: ComponetizedRow & SimplifiedModesProps & {
    rawNode: any;
}) => JSX.Element;
export default Inner;
//# sourceMappingURL=inner.d.ts.map