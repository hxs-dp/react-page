var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import * as React from 'react';
import { connect } from '../../reduxConnect';
import { createStructuredSelector } from 'reselect';
import droppable from './Droppable';
import Inner from './inner';
import dimensions from '../Dimensions';
import { isLayoutMode, isEditMode, isResizeMode, isInsertMode, } from '../../selector/display';
import { editableConfig, purifiedNode, node } from '../../selector/editable';
import { blurAllCells } from '../../actions/cell';
var Row = /** @class */ (function (_super) {
    __extends(Row, _super);
    function Row(props) {
        var _this = _super.call(this, props) || this;
        var whitelist = props.config.whitelist;
        _this.Droppable = droppable(whitelist);
        return _this;
    }
    Row.prototype.render = function () {
        var Droppable = this.Droppable;
        var props = this.props;
        return (React.createElement(Droppable, __assign({}, props),
            React.createElement(Inner, __assign({}, props))));
    };
    return Row;
}(React.PureComponent));
var mapStateToProps = createStructuredSelector({
    isLayoutMode: isLayoutMode,
    config: editableConfig,
    isResizeMode: isResizeMode,
    isInsertMode: isInsertMode,
    isEditMode: isEditMode,
    node: purifiedNode,
    rawNode: function (state, props) { return function () {
        return node(state, props);
    }; },
});
var mapDispatchToProps = {
    blurAllCells: blurAllCells,
};
export default dimensions()(connect(mapStateToProps, mapDispatchToProps)(Row));
//# sourceMappingURL=index.js.map