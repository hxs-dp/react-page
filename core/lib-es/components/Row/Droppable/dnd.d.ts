import { DropTargetConnector, DropTargetMonitor } from 'react-dnd';
import { ComponetizedRow } from '../../../types/editable';
export declare const target: {
    hover: any;
    canDrop: ({ id, ancestors }: ComponetizedRow, monitor: DropTargetMonitor) => boolean;
    drop(hover: ComponetizedRow, monitor: DropTargetMonitor, component: any): void;
};
export declare const connect: (_connect: DropTargetConnector, monitor: DropTargetMonitor) => {
    connectDropTarget: import("react-dnd").DragElementWrapper<any>;
    isOverCurrent: boolean;
};
//# sourceMappingURL=dnd.d.ts.map