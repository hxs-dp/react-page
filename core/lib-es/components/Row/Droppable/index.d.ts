import * as React from 'react';
import { ConnectDropTarget } from 'react-dnd';
import { ComponetizedRow } from '../../../types/editable';
export declare type Props = ComponetizedRow & {
    children: any;
    isLayoutMode: boolean;
    isInsertMode: boolean;
    isOverCurrent: boolean;
    connectDropTarget: ConnectDropTarget;
};
export declare class Droppable extends React.Component<Props> {
    render(): JSX.Element;
}
declare const _default: (dropTypes?: string[]) => any;
export default _default;
//# sourceMappingURL=index.d.ts.map