var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import * as React from 'react';
import { DropTarget as dropTarget } from 'react-dnd';
import { dragActions } from '../../../actions/cell/drag';
import { insertActions } from '../../../actions/cell/insert';
import { connect } from '../../../reduxConnect';
import { connect as monitorConnect, target } from './dnd';
var Droppable = /** @class */ (function (_super) {
    __extends(Droppable, _super);
    function Droppable() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Droppable.prototype.render = function () {
        if (!(this.props.isLayoutMode || this.props.isInsertMode)) {
            return (React.createElement("div", { className: "ory-row-droppable-container" }, this.props.children));
        }
        return this.props.connectDropTarget(React.createElement("div", { className: "ory-row-droppable" }, this.props.children));
    };
    return Droppable;
}(React.Component));
export { Droppable };
var mapDispatchToProps = __assign(__assign({}, dragActions), insertActions);
export default (function (dropTypes) {
    if (dropTypes === void 0) { dropTypes = ['CELL']; }
    return connect(null, mapDispatchToProps)(dropTarget(dropTypes, target, monitorConnect)(Droppable));
});
//# sourceMappingURL=index.js.map