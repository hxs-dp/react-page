import throttle from 'lodash.throttle';
import { createNativeCellReplacement, isNativeHTMLElementDrag, } from '../../../helper/nativeDragHelpers';
import { delay } from '../../../helper/throttle';
import { computeAndDispatchHover, computeAndDispatchInsert, } from '../../../service/hover/input';
import logger from '../../../service/logger';
var last = {
    hover: '',
    drag: '',
};
var clear = function (hover, drag) {
    if (hover.id === last.hover && drag === last.drag) {
        return;
    }
    last = { hover: hover.id, drag: drag };
    hover.clearHover(drag);
};
export var target = {
    hover: throttle(function (hover, monitor, component) {
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        var drag = monitor.getItem();
        if (!drag) {
            // item undefined, happens when throttle triggers after drop
            return;
        }
        if (isNativeHTMLElementDrag(monitor)) {
            drag = createNativeCellReplacement();
        }
        if (!drag) {
            return;
        }
        else if (drag.id === hover.id) {
            clear(hover, drag.id);
            return;
        }
        else if (!monitor.isOver({ shallow: true })) {
            return;
        }
        else if (hover.ancestors.indexOf(drag.id) > -1) {
            // If hovering over a child of itself
            clear(hover, drag.id);
            return;
        }
        else if (!hover.id) {
            // If hovering over something that isn't a cell or hasn't an id, do nothing. Should be an edge case
            logger.warn('Canceled cell.drop.target.hover: no id given.', hover, drag);
            return;
        }
        computeAndDispatchHover(
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        hover, drag, monitor, component, '10x10-no-inline');
    }, delay, { leading: false }),
    canDrop: function (_a, monitor) {
        var id = _a.id, ancestors = _a.ancestors;
        var item = monitor.getItem();
        return item.id !== id || ancestors.indexOf(item.id) === -1;
    },
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    drop: function (hover, monitor, component) {
        var drag = monitor.getItem();
        if (isNativeHTMLElementDrag(monitor)) {
            var plugins = component.props.config.plugins;
            drag = plugins.createNativePlugin(hover, monitor, component);
        }
        if (monitor.didDrop() || !monitor.isOver({ shallow: true })) {
            // If the item drop occurred deeper down the tree, don't do anything
            return;
        }
        else if (hover.ancestors.indexOf(drag.id) > -1) {
            // If hovering over a child of itself
            hover.cancelCellDrag(drag.id);
            return;
        }
        else if (drag.id === hover.id) {
            hover.cancelCellDrag(drag.id);
            return;
        }
        computeAndDispatchInsert(
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        hover, drag, monitor, component, '10x10-no-inline');
    },
};
export var connect = function (_connect, monitor) { return ({
    connectDropTarget: _connect.dropTarget(),
    isOverCurrent: monitor.isOver({ shallow: true }),
}); };
//# sourceMappingURL=dnd.js.map