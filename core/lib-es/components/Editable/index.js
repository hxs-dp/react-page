var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
import equals from 'fast-deep-equal';
import React, { useEffect, useRef } from 'react';
import { editable } from '../../selector/editable';
import { EditableContext, useEditor } from '../hooks';
import HotKeyDecorator from '../HotKey/Decorator';
import FallbackDropArea from './FallbackDropArea';
import Inner from './Inner';
var Editable = function (_a) {
    var id = _a.id, onChange = _a.onChange, onChangeLang = _a.onChangeLang, lang = _a.lang, t = _a.t, rest = __rest(_a, ["id", "onChange", "onChangeLang", "lang", "t"]);
    var editor = useEditor();
    // update lang when changed from outside
    useEffect(function () {
        editor.setLang(lang);
    }, [lang]);
    var previousSerializedRef = useRef();
    useEffect(function () {
        var oldLang = lang;
        var handleChanges = function () {
            // notify outsiders to new language, when chagned in ui
            var newLang = editor.store.getState().reactPage.settings.lang;
            if (newLang !== oldLang || newLang !== lang) {
                oldLang = newLang;
                onChangeLang === null || onChangeLang === void 0 ? void 0 : onChangeLang(newLang);
            }
            // check also if lang has changed internally, to call callback when controled from outside
            var state = editable(editor.store.getState(), {
                id: id,
            });
            if (!state) {
                return;
            }
            // prevent uneeded updates
            var serialized = editor.plugins.serialize(state);
            var serializedEqual = equals(previousSerializedRef.current, serialized);
            if (serializedEqual) {
                return;
            }
            previousSerializedRef.current = serialized;
            onChange(serialized);
        };
        var unsubscribe = editor.store.subscribe(handleChanges);
        return function () {
            unsubscribe();
        };
    }, [editor, id, onChange]);
    return (React.createElement(EditableContext.Provider, { value: id },
        React.createElement(HotKeyDecorator, { id: id },
            React.createElement(FallbackDropArea, null,
                React.createElement(Inner, __assign({ id: id, defaultPlugin: editor.defaultPlugin }, rest, { t: t }))))));
};
export default React.memo(Editable);
//# sourceMappingURL=index.js.map