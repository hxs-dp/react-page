var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
import React from 'react';
import { useDrop } from 'react-dnd';
import { useEditor, useInsertCellAtTheEnd } from '../hooks';
var FallbackDropArea = function (_a) {
    var children = _a.children;
    var insertAtTheEnd = useInsertCellAtTheEnd();
    var editor = useEditor();
    var _b = __read(useDrop({
        accept: editor.plugins.getRegisteredNames(),
        drop: function (item, monitor) {
            // fallback drop
            if (!monitor.didDrop()) {
                insertAtTheEnd(item.node);
            }
        },
    }), 2), dropRef = _b[1];
    return React.createElement("div", { ref: dropRef }, children);
};
export default FallbackDropArea;
//# sourceMappingURL=FallbackDropArea.js.map