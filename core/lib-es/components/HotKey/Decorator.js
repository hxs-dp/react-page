var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
var __spread = (this && this.__spread) || function () {
    for (var ar = [], i = 0; i < arguments.length; i++) ar = ar.concat(__read(arguments[i]));
    return ar;
};
/* eslint-disable @typescript-eslint/ban-types */
import isHotkey from 'is-hotkey';
import React, { useCallback, useEffect, useMemo } from 'react';
import { createStructuredSelector } from 'reselect';
import { blurAllCells, focusCell, removeCell } from '../../actions/cell';
import { redo, undo } from '../../actions/undo';
import { connect } from '../../reduxConnect';
import { isEditMode } from '../../selector/display';
import { editable, editables, node, searchNodeEverywhere, } from '../../selector/editable';
import { focus } from '../../selector/focus';
var nextLeaf = function (order, current) {
    if (order === void 0) { order = []; }
    var last;
    return order.find(function (c) {
        if (last === current) {
            return c.isLeaf;
        }
        last = c.id;
        return false;
    });
};
var previousLeaf = function (order, current) {
    return nextLeaf(__spread(order).reverse(), current);
};
var delegateToPlugin = function (event, n, handlerName) { return __awaiter(void 0, void 0, void 0, function () {
    var plugin;
    var _a, _b, _c;
    return __generator(this, function (_d) {
        switch (_d.label) {
            case 0:
                plugin = (_b = (_a = n === null || n === void 0 ? void 0 : n.layout) === null || _a === void 0 ? void 0 : _a.plugin) !== null && _b !== void 0 ? _b : (_c = n === null || n === void 0 ? void 0 : n.content) === null || _c === void 0 ? void 0 : _c.plugin;
                if (!(plugin === null || plugin === void 0 ? void 0 : plugin[handlerName])) return [3 /*break*/, 2];
                return [4 /*yield*/, plugin[handlerName](event, n)];
            case 1:
                _d.sent();
                _d.label = 2;
            case 2: return [2 /*return*/];
        }
    });
}); };
var Decorator = function (props) {
    var delegateToFoundPlugin = useCallback(function (event, nodeId, handlerName, defaultHandler) { return __awaiter(void 0, void 0, void 0, function () {
        var cellNode, e_1;
        var _a, _b;
        return __generator(this, function (_c) {
            switch (_c.label) {
                case 0:
                    cellNode = (_b = (_a = props.searchNodeEverywhere(nodeId)) === null || _a === void 0 ? void 0 : _a.node) === null || _b === void 0 ? void 0 : _b.node;
                    _c.label = 1;
                case 1:
                    _c.trys.push([1, 5, , 6]);
                    if (!cellNode) return [3 /*break*/, 3];
                    return [4 /*yield*/, delegateToPlugin(event, cellNode, handlerName)];
                case 2:
                    _c.sent();
                    _c.label = 3;
                case 3: 
                // if the plugin handler resolve or there is no, they do not handle it, so do the default
                return [4 /*yield*/, defaultHandler(event, cellNode)];
                case 4:
                    // if the plugin handler resolve or there is no, they do not handle it, so do the default
                    _c.sent();
                    return [3 /*break*/, 6];
                case 5:
                    e_1 = _c.sent();
                    if (e_1) {
                        // tslint:disable-next-line:no-console
                        console.error(e_1);
                    }
                    return [3 /*break*/, 6];
                case 6: return [2 /*return*/];
            }
        });
    }); }, []);
    var handlers = useMemo(function () { return [
        {
            hotkeys: ['ctrl+z', 'command+z'],
            handler: function () {
                props.undo(props.id);
            },
        },
        {
            hotkeys: ['ctrl+shift+z', 'ctrl+y', 'command+shift+z', 'command+y'],
            handler: function () {
                props.redo(props.id);
            },
        },
        {
            hotkeys: ['alt+del', 'alt+backspace'],
            handler: function (event) {
                delegateToFoundPlugin(event, props.focus, 'handleRemoveHotKey', function () {
                    return props.removeCell(props.focus);
                });
            },
        },
        {
            hotkeys: ['alt+down', 'alt+right'],
            handler: function (event) {
                delegateToFoundPlugin(event, props.focus, 'handleFocusNextHotKey', function () {
                    var found = nextLeaf(props.editable.cellOrder, props.focus);
                    if (found) {
                        props.blurAllCells();
                        props.focusCell(found.id);
                    }
                });
            },
        },
        {
            hotkeys: ['alt+up', 'alt+left'],
            handler: function (event) {
                delegateToFoundPlugin(event, props.focus, 'handleFocusPreviousHotKey', function () {
                    var found = previousLeaf(props.editable.cellOrder, props.focus);
                    if (found) {
                        props.blurAllCells();
                        props.focusCell(found.id);
                    }
                });
            },
        },
    ]; }, [props.id, props.focus, props.editable]);
    useEffect(function () {
        // in editmode we only allow hotkeys if a cell is focused.
        // this is because if we have multiple editors on the same page, we don't want to interfer with others
        // and also to make it more explicit
        // The BlurGate currently guarantees that only one editor ever has a focused cell, so we already know on which one we can apply hotkeys
        // If the editor is in another mode, all hot keys are allowed (which is useful for undo/redo, e.g. when resizing)
        // because the BlurGate also guarantees that only one editor will be in another mode then editMode
        // This is not totally clean, but it works very well.
        if (props.isEditMode && !props.focus) {
            return;
        }
        var keyHandler = function (event) {
            var matchingHandler = handlers.find(function (handler) {
                return handler.hotkeys.some(function (hotkey) { return isHotkey(hotkey, event); });
            });
            matchingHandler === null || matchingHandler === void 0 ? void 0 : matchingHandler.handler(event);
        };
        document.addEventListener('keydown', keyHandler);
        return function () {
            document.removeEventListener('keydown', keyHandler);
        };
    }, [handlers, props.focus, props.isEditMode]);
    return React.createElement(React.Fragment, null, props.children);
};
/** FIXME: we should start using hooks for redux. this would drastically simplify this whole thing */
var mapStateToProps = createStructuredSelector({
    isEditMode: isEditMode,
    focus: focus,
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    node: function (state) { return function (id, _editable) {
        return node(state, { id: id, editable: _editable });
    }; },
    searchNodeEverywhere: function (state) { return function (id) {
        return searchNodeEverywhere(state, id);
    }; },
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    editable: function (state, props) { return editable(state, { id: props.id }); },
    editables: editables,
});
var mapDispatchToProps = {
    undo: undo,
    redo: redo,
    removeCell: removeCell,
    focusCell: function (id) { return focusCell(id)(); },
    blurAllCells: blurAllCells,
};
export default connect(mapStateToProps, mapDispatchToProps)(Decorator);
//# sourceMappingURL=Decorator.js.map