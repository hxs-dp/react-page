import { Editables } from './editable';
import { Display } from './display';
export declare type RootState = {
    reactPage: {
        editables: Editables;
        display: Display;
        focus: string;
        settings: {
            lang?: string;
        };
    };
};
//# sourceMappingURL=state.d.ts.map