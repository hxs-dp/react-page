export declare type Display = {
    previous: string;
    mode: string;
};
export declare type DisplayAction = {
    type: string;
    mode: string;
    fallback: string;
    remember: boolean;
};
//# sourceMappingURL=display.d.ts.map