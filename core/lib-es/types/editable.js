export var createCell = function () { return ({
    id: '',
    rows: [],
    size: 12,
    hover: null,
    inline: null,
    focused: false,
    focusSource: '',
    resizable: false,
    bounds: { left: 0, right: 0 },
    hasInlineNeighbour: null,
    levels: {
        above: 0,
        below: 0,
        right: 0,
        left: 0,
    },
}); };
export var createRow = function () { return ({
    id: '',
    hover: null,
    cells: [],
}); };
//# sourceMappingURL=editable.js.map