var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
/* eslint-disable @typescript-eslint/ban-types */
import { ContentPlugin, LayoutPlugin, } from '../../service/plugin/classes';
import { v4 } from 'uuid';
var withDefaultState = function (layoutOrContent, PluginClass) {
    var plugin = new PluginClass(layoutOrContent.plugin);
    return {
        plugin: plugin,
        state: layoutOrContent.state || plugin.createInitialState(),
    };
};
export default (function (rows) { return ({
    id: v4(),
    rows: rows.map(function (row) { return ({
        id: v4(),
        cells: row.map(function (_a) {
            var layout = _a.layout, content = _a.content, rest = __rest(_a, ["layout", "content"]);
            return __assign({ id: v4(), layout: layout ? withDefaultState(layout, LayoutPlugin) : undefined, content: content ? withDefaultState(content, ContentPlugin) : undefined }, rest);
        }),
    }); }),
}); });
//# sourceMappingURL=index.js.map