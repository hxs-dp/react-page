import createInitialChildren from '../createInitialChildren';
export default (function (childrenOrRowDef) {
    if (Array.isArray(childrenOrRowDef)) {
        return createInitialChildren(childrenOrRowDef);
    }
    return childrenOrRowDef;
});
//# sourceMappingURL=index.js.map