import { NativeTypes } from 'react-dnd-html5-backend';
export var isNativeHTMLElementDrag = function (monitor) {
    switch (monitor.getItemType()) {
        case NativeTypes.URL:
        case NativeTypes.FILE:
        case NativeTypes.TEXT:
            return true;
        default:
            return false;
    }
};
export var createNativeCellReplacement = function () {
    var id = 'ory-native-drag';
    return {
        id: id,
        rawNode: function () { return ({ id: id }); },
        node: { content: { plugin: { isInlineable: false } } },
    };
};
//# sourceMappingURL=index.js.map