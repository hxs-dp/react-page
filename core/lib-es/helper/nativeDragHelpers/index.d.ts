import { DropTargetMonitor } from 'react-dnd';
export declare const isNativeHTMLElementDrag: (monitor: Pick<DropTargetMonitor, 'getItemType'>) => boolean;
export declare const createNativeCellReplacement: () => {
    id: string;
    rawNode: () => {
        id: string;
    };
    node: {
        content: {
            plugin: {
                isInlineable: boolean;
            };
        };
    };
};
//# sourceMappingURL=index.d.ts.map