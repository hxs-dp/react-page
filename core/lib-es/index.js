import { DragSource, DropTarget } from 'react-dnd';
import { Actions } from './actions';
import Editable from './components/Editable';
import Editor, { createEmptyState } from './Editor';
import lazyLoad from './helper/lazyLoad';
import sanitizeInitialChildren from './helper/sanitizeInitialChildren';
import Provider from './Provider';
import { reducer } from './reducer';
import { editable as editableReducer } from './reducer/editable';
import { connect, ReduxContext, ReduxProvider } from './reduxConnect';
import { Selectors } from './selector';
import i18n from './service/i18n';
import PluginService from './service/plugin';
import { ContentPlugin, LayoutPlugin, Migration, Plugin, } from './service/plugin/classes';
export * from './components/hooks';
import { setAllSizesAndOptimize } from './reducer/editable/helper/setAllSizesAndOptimize';
export { DropTarget, DragSource };
export { setAllSizesAndOptimize, Migration, createEmptyState, Plugin, Actions, Selectors, i18n, sanitizeInitialChildren, PluginService, ContentPlugin, LayoutPlugin, Editor, reducer, editableReducer, // deprecated
ReduxProvider, connect, ReduxContext, };
// newer api
export { Provider, Editable, lazyLoad };
export default Editor;
//# sourceMappingURL=index.js.map