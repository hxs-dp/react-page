// tslint:disable:no-console
var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
var __spread = (this && this.__spread) || function () {
    for (var ar = [], i = 0; i < arguments.length; i++) ar = ar.concat(__read(arguments[i]));
    return ar;
};
var trace = function () {
    var e = new Error('dummy');
    return e.stack
        .replace(/^[^(]+?[\n$]/gm, '')
        .replace(/^\s+at\s+/gm, '')
        .replace(/^Object.<anonymous>\s*\(/gm, '{anonymous}()@')
        .split('\n');
};
var Logger = /** @class */ (function () {
    function Logger() {
    }
    /**
     * Logs a warning. Warnings are things that are exceptional, but easily to recover from.
     */
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    Logger.prototype.warn = function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        console.warn.apply(console, __spread(['Warning:'], args));
    };
    /**
     * Logs a debug message. Debug messages are things that help developers debugging things.
     */
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    Logger.prototype.debug = function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        console.log.apply(console, __spread(['Debug:'], args));
    };
    /**
     * Logs an info. Infos are things that might be interesting for someone who needs to take a closer look.
     */
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    Logger.prototype.info = function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        console.log.apply(console, __spread(['Info:'], args));
    };
    /**
     * Logs an error. Error are things that are exceptional, but can be recovered from.
     */
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    Logger.prototype.error = function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        console.error.apply(console, __spread(['Error:'], args));
        console.error('Trace:', trace());
    };
    /**
     * Logs a fatal error. Fatal errors are things that are exceptional and can not be recovered from.
     */
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    Logger.prototype.fatal = function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        console.error.apply(console, __spread(['Fatal:'], args));
        console.error('Trace:', trace());
        throw new Error(args.join(' '));
    };
    /**
     * Logs a message.
     */
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    Logger.prototype.log = function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        console.log.apply(console, __spread(['Fatal:'], args));
        console.log('Trace:', trace());
    };
    return Logger;
}());
export { Logger };
var instance = new Logger();
export default instance;
//# sourceMappingURL=index.js.map