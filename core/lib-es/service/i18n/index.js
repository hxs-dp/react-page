var I18n = /** @class */ (function () {
    function I18n() {
        this.t = function (message) { return message; };
    }
    return I18n;
}());
export { I18n };
export default new I18n();
//# sourceMappingURL=index.js.map