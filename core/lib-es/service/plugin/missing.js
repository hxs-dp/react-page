var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
import * as React from 'react';
var ContentMissingComponent = function (props) { return (React.createElement("div", { style: {
        backgroundColor: 'red',
        padding: '8px',
        border: '1px solid black',
        margin: '2px',
        overflowX: 'scroll',
    } },
    "The requested content plugin could not be found.",
    React.createElement("button", { onClick: props.remove }, "Delete Plugin"),
    React.createElement("pre", null, JSON.stringify(props, null, 2)))); };
export var contentMissing = function (_a) {
    var name = _a.name, version = _a.version;
    return ({
        Component: ContentMissingComponent,
        name: name,
        version: version,
    });
};
var LayoutMissingComponent = function (_a) {
    var children = _a.children, props = __rest(_a, ["children"]);
    return (React.createElement("div", null,
        React.createElement("div", { style: {
                backgroundColor: 'red',
                padding: '8px',
                border: '1px solid black',
                margin: '2px',
                overflowX: 'scroll',
            } },
            "The requested layout plugin could not be found.",
            React.createElement("button", { onClick: props.remove }, "Delete Plugin"),
            React.createElement("pre", null, JSON.stringify(props, null, 2))),
        children));
};
export var layoutMissing = function (_a) {
    var name = _a.name, version = _a.version;
    return ({
        Component: LayoutMissingComponent,
        name: name,
        version: version,
    });
};
//# sourceMappingURL=missing.js.map