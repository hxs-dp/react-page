import * as React from 'react';
var handleChange = function (onChange) { return function (e) {
    if (e.target instanceof HTMLInputElement) {
        onChange({ value: e.target.value });
    }
}; };
var Default = function (_a) {
    var readOnly = _a.readOnly, value = _a.state.value, onChange = _a.onChange;
    return readOnly ? (React.createElement("div", null, value)) : (React.createElement("textarea", { style: { width: '100%' }, value: value, onChange: handleChange(onChange) }));
};
var _defaultContentPlugin = {
    Component: Default,
    name: 'ory/editor/core/default',
    version: '0.0.1',
    createInitialState: function () { return ({
        value: 'This is the default plugin from the core package. To replace it, set the "defaultPlugin" value in the editor config.',
    }); },
};
export default _defaultContentPlugin;
//# sourceMappingURL=default.js.map