var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
var __spread = (this && this.__spread) || function () {
    for (var ar = [], i = 0; i < arguments.length; i++) ar = ar.concat(__read(arguments[i]));
    return ar;
};
import semver, { satisfies } from 'semver';
import { v4 } from 'uuid';
import { ContentPlugin, LayoutPlugin, NativePlugin, } from './classes';
import defaultPlugin from './default';
import { contentMissing, layoutMissing } from './missing';
var find = function (name, version) {
    if (version === void 0) { version = '*'; }
    return function (plugin) {
        return plugin.name === name && satisfies(plugin.version, version);
    };
};
/**
 * Iterate through an editable content tree and generate ids where missing.
 */
export var generateMissingIds = function (props) {
    var rows = props.rows, cells = props.cells, id = props.id;
    if ((rows || []).length > 0) {
        props.rows = rows.map(generateMissingIds);
    }
    else if ((cells || []).length > 0) {
        props.cells = cells.map(generateMissingIds);
    }
    return __assign(__assign({}, props), { id: id || v4() });
};
/**
 * PluginService is a registry of all content and layout plugins known to the editor.
 */
var PluginService = /** @class */ (function () {
    /**
     * Instantiate a new PluginService instance. You can provide your own set of content and layout plugins here.
     */
    function PluginService(_a) {
        var _this = this;
        var _b = _a === void 0 ? {} : _a, _c = _b.content, content = _c === void 0 ? [] : _c, _d = _b.layout, layout = _d === void 0 ? [] : _d, native = _b.native;
        this.hasNativePlugin = function () { return Boolean(_this.plugins.native); };
        this.getNativePlugin = function () { return _this.plugins.native; };
        this.createNativePlugin = function (
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        hover, 
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        monitor, 
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        component) {
            var native = _this.plugins.native;
            if (!native) {
                var insert_1 = new NativePlugin({});
                // eslint-disable-next-line @typescript-eslint/no-explicit-any
                var cell = { node: insert_1, rawNode: function () { return insert_1; } };
                return cell;
            }
            else {
                var plugin = new NativePlugin(native(hover, monitor, component));
                var initialState = plugin.createInitialState();
                // eslint-disable-next-line @typescript-eslint/no-explicit-any
                var insert_2 = { content: { plugin: plugin, state: initialState } };
                /*if (plugin === 'layout') {
                  insert = { layout: { plugin, state: initialState } };
                }*/
                // eslint-disable-next-line @typescript-eslint/no-explicit-any
                var cell = { node: insert_2, rawNode: function () { return insert_2; } };
                return cell;
            }
        };
        this.setLayoutPlugins = function (plugins) {
            if (plugins === void 0) { plugins = []; }
            _this.plugins.layout = [];
            plugins.forEach(function (plugin) { return _this.addLayoutPlugin(plugin); });
        };
        this.addLayoutPlugin = function (config) {
            _this.plugins.layout.push(new LayoutPlugin(config));
        };
        this.removeLayoutPlugin = function (name) {
            _this.plugins.layout = _this.plugins.layout.filter(function (plugin) { return plugin.name !== name; });
        };
        this.setContentPlugins = function (plugins) {
            if (plugins === void 0) { plugins = []; }
            _this.plugins.content = [];
            // semicolon is required to avoid syntax error
            __spread([defaultPlugin], plugins).forEach(function (plugin) {
                return _this.addContentPlugin(plugin);
            });
        };
        this.addContentPlugin = function (config) {
            _this.plugins.content.push(new ContentPlugin(config));
        };
        this.removeContentPlugin = function (name) {
            _this.plugins.content = _this.plugins.content.filter(function (plugin) { return plugin.name !== name; });
        };
        /**
         * Finds a layout plugin based on its name and version.
         */
        this.findLayoutPlugin = function (name, version) {
            var plugin = _this.plugins.layout.find(find(name, version));
            var pluginWrongVersion = undefined;
            if (!plugin) {
                pluginWrongVersion = _this.plugins.layout.find(find(name, '*'));
            }
            return {
                plugin: plugin || new LayoutPlugin(layoutMissing({ name: name, version: version })),
                pluginWrongVersion: pluginWrongVersion,
            };
        };
        /**
         * Finds a content plugin based on its name and version.
         */
        this.findContentPlugin = function (name, version) {
            var plugin = _this.plugins.content.find(find(name, version));
            var pluginWrongVersion = undefined;
            if (!plugin) {
                pluginWrongVersion = _this.plugins.content.find(find(name, '*'));
            }
            return {
                plugin: plugin || new ContentPlugin(contentMissing({ name: name, version: version })),
                pluginWrongVersion: pluginWrongVersion,
            };
        };
        /**
         * Returns a list of all known plugin names.
         */
        this.getRegisteredNames = function () { return __spread(_this.plugins.content.map(function (_a) {
            var name = _a.name;
            return name;
        }), _this.plugins.layout.map(function (_a) {
            var name = _a.name;
            return name;
        })); };
        this.migratePluginState = function (
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        state, plugin, dataVersion
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        ) {
            if (!plugin || !dataVersion || semver.valid(dataVersion) === null) {
                return state;
            }
            var currentDataVersion = dataVersion;
            var migrations = plugin.migrations ? plugin.migrations : [];
            // eslint-disable-next-line no-constant-condition
            while (true) {
                var migration = migrations.find(function (m) {
                    return semver.satisfies(currentDataVersion, m.fromVersionRange);
                });
                migrations = migrations.filter(function (m) { return !semver.satisfies(currentDataVersion, m.fromVersionRange); });
                if (!migration) {
                    // We assume all migrations necessary for the current version of plugin to work are provided
                    // Therefore if we don't find any, that means we are done and state is up to date
                    break;
                }
                currentDataVersion = migration.toVersion;
                state = migration.migrate(state);
            }
            return state;
        };
        this.getNewPluginState = function (found, state, stateI18n, version) {
            var getResult = function (plugin, shouldMigrate) {
                // Attempt to migrate
                if (shouldMigrate === void 0) { shouldMigrate = false; }
                var transformState = function (s) {
                    if (shouldMigrate) {
                        var migratedState = _this.migratePluginState(s, found.pluginWrongVersion, version);
                        return plugin.unserialize(migratedState);
                    }
                    return plugin.unserialize(s);
                };
                var result = {
                    plugin: plugin,
                    state: transformState(state),
                    stateI18n: stateI18n
                        ? Object.keys(stateI18n).reduce(function (acc, lang) {
                            var _a;
                            return (__assign(__assign({}, acc), (_a = {}, _a[lang] = transformState(stateI18n[lang]), _a)));
                        }, {})
                        : undefined,
                };
                return result;
            };
            if (!found.pluginWrongVersion ||
                semver.lt(found.pluginWrongVersion.version, version)) {
                // Standard case
                return getResult(found.plugin);
            }
            else {
                if (found.pluginWrongVersion) {
                    return getResult(found.pluginWrongVersion, true);
                }
                else {
                    return getResult(found.plugin);
                }
            }
        };
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        this.unserialize = function (state) {
            if (!state) {
                return;
            }
            var _a = state.rows, rows = _a === void 0 ? [] : _a, _b = state.cells, cells = _b === void 0 ? [] : _b, _c = state.content, content = _c === void 0 ? {} : _c, _d = state.layout, layout = _d === void 0 ? {} : _d, inline = state.inline, size = state.size, isDraft = state.isDraft, isDraftI18n = state.isDraftI18n, id = state.id;
            var newState = { id: id, inline: inline, size: size, isDraft: isDraft, isDraftI18n: isDraftI18n };
            var _e = (content || {}).plugin, _f = _e === void 0 ? {} : _e, _g = _f.name, contentName = _g === void 0 ? null : _g, _h = _f.version, contentVersion = _h === void 0 ? '*' : _h;
            var _j = (layout || {}).plugin, _k = _j === void 0 ? {} : _j, _l = _k.name, layoutName = _l === void 0 ? null : _l, _m = _k.version, layoutVersion = _m === void 0 ? '*' : _m;
            if (contentName) {
                var found = _this.findContentPlugin(contentName, contentVersion);
                var newContentState = _this.getNewPluginState(found, content.state, content.stateI18n, contentVersion);
                newState.content = newContentState;
            }
            if (layoutName) {
                var found = _this.findLayoutPlugin(layoutName, layoutVersion);
                var newLayoutState = _this.getNewPluginState(found, layout.state, layout.stateI18n, layoutVersion);
                newState.layout = newLayoutState;
            }
            if ((rows || []).length) {
                newState.rows = rows.map(_this.unserialize);
            }
            if ((cells || []).length) {
                newState.cells = cells.map(_this.unserialize);
            }
            return generateMissingIds(newState);
        };
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        this.serialize = function (state) {
            var _a = state.rows, rows = _a === void 0 ? [] : _a, _b = state.cells, cells = _b === void 0 ? [] : _b, content = state.content, layout = state.layout, inline = state.inline, isDraft = state.isDraft, isDraftI18n = state.isDraftI18n, size = state.size, id = state.id;
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            var newState = { id: id, inline: inline, size: size, isDraft: isDraft, isDraftI18n: isDraftI18n };
            if (content && content.plugin) {
                newState.content = {
                    plugin: { name: content.plugin.name, version: content.plugin.version },
                    state: content.plugin.serialize(content.state),
                    stateI18n: content.stateI18n
                        ? Object.keys(content.stateI18n).reduce(function (acc, lang) {
                            var _a;
                            return (__assign(__assign({}, acc), (_a = {}, _a[lang] = content.plugin.serialize(content.stateI18n[lang]), _a)));
                        }, {})
                        : undefined,
                };
            }
            if (layout && layout.plugin) {
                newState.layout = {
                    plugin: { name: layout.plugin.name, version: layout.plugin.version },
                    state: layout.plugin.serialize(layout.state),
                    stateI18n: layout.stateI18n
                        ? Object.keys(layout.stateI18n).reduce(function (acc, lang) {
                            var _a;
                            return (__assign(__assign({}, acc), (_a = {}, _a[lang] = layout.plugin.serialize(layout.stateI18n[lang]), _a)));
                        }, {})
                        : undefined,
                };
            }
            if (rows.length) {
                newState.rows = rows.map(_this.serialize);
            }
            if (cells.length) {
                newState.cells = cells.map(_this.serialize);
            }
            return newState;
        };
        this.plugins = {
            content: __spread([defaultPlugin], content).map(
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            function (config) { return new ContentPlugin(config); }),
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            layout: layout.map(function (config) { return new LayoutPlugin(config); }),
            native: native,
        };
    }
    return PluginService;
}());
export default PluginService;
//# sourceMappingURL=index.js.map