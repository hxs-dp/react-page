/// <reference types="react" />
import { Middleware, Store } from 'redux';
import { ActionsTypes } from './actions';
import PluginService from './service/plugin';
import { ContentPluginConfig, LayoutPluginConfig, Plugins } from './service/plugin/classes';
import { EditableType } from './types/editable';
import { RootState } from './types/state';
export declare const EditorContext: import("react").Context<Editor<RootState>>;
export declare type Languages = Array<{
    lang: string;
    label: string;
}>;
export interface CoreEditorProps<T extends RootState = RootState> {
    plugins?: Plugins;
    middleware?: [];
    editables?: EditableType[];
    defaultPlugin?: ContentPluginConfig | LayoutPluginConfig;
    store?: Store<T>;
    languages?: Languages;
    lang?: string;
}
/**
 * Editor is the core interface for dealing with the editor.
 */
declare class Editor<T extends RootState = RootState> {
    store: Store<RootState>;
    plugins: PluginService;
    middleware: Middleware[];
    defaultPlugin: ContentPluginConfig | LayoutPluginConfig;
    trigger: ActionsTypes;
    query: {};
    languages?: Languages;
    constructor({ plugins, middleware, editables, defaultPlugin, store, languages, lang, }?: CoreEditorProps<T>);
    setLang(lang: string): void;
    /**
     * @deprecated in order to reduce api surface, this api gets removed in the future. Please file an issue with your use case if you still need it
     */
    refreshEditables: () => void;
    getNode: (editableId: string, nodeId: string) => import("./types/editable").Row | import("./types/editable").AbstractCell<import("./types/editable").Row>;
}
export declare const createEmptyState: () => EditableType;
export default Editor;
//# sourceMappingURL=Editor.d.ts.map