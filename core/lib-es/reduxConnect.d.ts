import React from 'react';
export declare const ReduxContext: React.Context<any>;
export declare const ReduxProvider: ({ store, ...props }: {
    [x: string]: any;
    store: any;
}) => JSX.Element;
export declare const useStore: typeof import("react-redux").useStore;
export declare const useDispatch: typeof import("react-redux").useDispatch;
export declare const useSelector: typeof import("react-redux").useSelector;
export declare const connect: (mapStateToProps?: any, mapDispatchToProps?: Function | Object, mergeProps?: any, options?: {}) => (C: any) => any;
//# sourceMappingURL=reduxConnect.d.ts.map