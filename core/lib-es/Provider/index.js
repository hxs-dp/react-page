import * as React from 'react';
import { DndProvider } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';
import BlurGate from '../components/BlurGate';
import { EditorContext } from '../Editor';
import { ReduxProvider } from '../reduxConnect';
var Provider = function (_a) {
    var editor = _a.editor, _b = _a.children, children = _b === void 0 ? [] : _b, _c = _a.dndBackend, dndBackend = _c === void 0 ? HTML5Backend : _c, _d = _a.blurGateDisabled, blurGateDisabled = _d === void 0 ? false : _d, blurGateDefaultMode = _a.blurGateDefaultMode;
    return (React.createElement(DndProvider, { backend: dndBackend },
        React.createElement(ReduxProvider, { store: editor.store },
            React.createElement(EditorContext.Provider, { value: editor },
                React.createElement(BlurGate, { disabled: blurGateDisabled, defaultMode: blurGateDefaultMode }, children)))));
};
export default Provider;
//# sourceMappingURL=index.js.map