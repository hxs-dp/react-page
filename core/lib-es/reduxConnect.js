var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
/* eslint-disable @typescript-eslint/ban-types */
import React from 'react';
import { connect as reduxConnect, Provider, createStoreHook, createDispatchHook, createSelectorHook, } from 'react-redux';
export var ReduxContext = React.createContext(null);
export var ReduxProvider = function (_a) {
    var store = _a.store, props = __rest(_a, ["store"]);
    return (React.createElement(Provider, __assign({ store: store, context: ReduxContext }, props)));
};
export var useStore = createStoreHook(ReduxContext);
export var useDispatch = createDispatchHook(ReduxContext);
export var useSelector = createSelectorHook(ReduxContext);
export var connect = function (
// eslint-disable-next-line @typescript-eslint/no-explicit-any
mapStateToProps, mapDispatchToProps, 
// eslint-disable-next-line @typescript-eslint/no-explicit-any
mergeProps, options) {
    if (options === void 0) { options = {}; }
    return reduxConnect(mapStateToProps, mapDispatchToProps, mergeProps, __assign(__assign({}, options), { context: ReduxContext }));
};
//# sourceMappingURL=reduxConnect.js.map