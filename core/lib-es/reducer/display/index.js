var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import { DEFAULT_DISPLAY_MODE, SET_DISPLAY_MODE, SET_PREVIOUS_DISPLAY_MODE, } from '../../actions/display';
export var display = function (state, action) {
    if (state === void 0) { state = {
        previous: DEFAULT_DISPLAY_MODE,
        mode: DEFAULT_DISPLAY_MODE,
    }; }
    switch (action.type) {
        case SET_PREVIOUS_DISPLAY_MODE:
            return __assign(__assign({}, state), { mode: state.previous === state.mode ? action.fallback : state.previous });
        case SET_DISPLAY_MODE:
            return {
                previous: action.mode === state.mode && action.remember
                    ? state.previous
                    : action.mode,
                mode: action.mode,
            };
        default:
            return state;
    }
};
//# sourceMappingURL=index.js.map