import { computeSizes, computeInlines, computeBounds, computeResizeable, } from './sizing';
import { optimizeCell, optimizeRow, optimizeRows, optimizeCells, } from './optimize';
import { computeDropLevels } from './level';
export var setAllSizesAndOptimize = function (cells) {
    if (cells === void 0) { cells = []; }
    return computeInlines(computeResizeable(computeBounds(computeSizes(optimizeCells(cells))))).map(function (cell) {
        if (cell.rows) {
            cell.rows = optimizeRows(cell.rows).map(function (r) {
                var optimized = optimizeRow(r);
                if (optimized.cells) {
                    optimized.cells = setAllSizesAndOptimize(optimized.cells);
                }
                return optimized;
            });
        }
        return computeDropLevels(optimizeCell(cell));
    });
};
//# sourceMappingURL=setAllSizesAndOptimize.js.map