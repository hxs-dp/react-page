export var isEmpty = function (_a) {
    var cells = _a.cells, rows = _a.rows, _b = _a.layout, _c = (_b === void 0 ? {} : _b).plugin, _d = (_c === void 0 ? {} : _c).name, layout = _d === void 0 ? undefined : _d, _e = _a.content, _f = (_e === void 0 ? {} : _e).plugin, _g = (_f === void 0 ? {} : _f).name, content = _g === void 0 ? undefined : _g;
    return !(cells || []).filter(emptyFilter).length &&
        !(rows || []).filter(emptyFilter).length &&
        !content &&
        !(layout && (rows || []).filter(emptyFilter).length);
};
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export var emptyFilter = function (state) { return !isEmpty(state); };
//# sourceMappingURL=empty.js.map