var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
var __spread = (this && this.__spread) || function () {
    for (var ar = [], i = 0; i < arguments.length; i++) ar = ar.concat(__read(arguments[i]));
    return ar;
};
import { CELL_BLUR, CELL_BLUR_ALL, CELL_DRAG_HOVER, CELL_FOCUS, CELL_INSERT_ABOVE, CELL_INSERT_BELOW, CELL_INSERT_INLINE_LEFT, CELL_INSERT_INLINE_RIGHT, CELL_INSERT_LEFT_OF, CELL_INSERT_RIGHT_OF, CELL_REMOVE, CELL_RESIZE, CELL_UPDATE_CONTENT, CELL_UPDATE_IS_DRAFT, CELL_UPDATE_LAYOUT, CELL_INSERT_AT_END, } from '../../actions/cell';
import { createCell, createRow } from '../../types/editable';
import { isHoveringThis } from './helper/hover';
import { flatten, optimizeCell, optimizeCells, optimizeRow, optimizeRows, } from './helper/optimize';
import { resizeCells } from './helper/sizing';
var inner = function (cb, action) { return function (state) {
    return cb(state, action);
}; };
var identity = function (state) { return state; };
export var cell = function (s, a) {
    return optimizeCell((function (state, action) {
        var _a, _b, _c;
        var _d, _e, _f, _g, _h, _j;
        var reduce = function () {
            var _a, _b, _c, _d, _e, _f;
            var content = (_c = (_b = (_a = state === null || state === void 0 ? void 0 : state.content) === null || _a === void 0 ? void 0 : _a.plugin) === null || _b === void 0 ? void 0 : _b.reducer) !== null && _c !== void 0 ? _c : identity;
            var layout = (_f = (_e = (_d = state === null || state === void 0 ? void 0 : state.layout) === null || _d === void 0 ? void 0 : _d.plugin) === null || _e === void 0 ? void 0 : _e.reducer) !== null && _f !== void 0 ? _f : identity;
            return content(layout(__assign(__assign({}, state), { hover: null, rows: rows(state.rows, action) }), action), action);
        };
        switch (action.type) {
            case CELL_UPDATE_IS_DRAFT:
                if (action.id === state.id) {
                    // If this cell is being focused, set the data
                    var reduced = reduce();
                    if (action.lang) {
                        return __assign(__assign({}, reduced), { isDraftI18n: __assign(__assign({}, reduced.isDraftI18n), (_a = {}, _a[action.lang] = action.isDraft, _a)) });
                    }
                    else {
                        return __assign(__assign({}, reduced), { isDraft: action.isDraft });
                    }
                }
                return __assign(__assign({}, reduce()), { focused: false, focusSource: null });
            case CELL_UPDATE_CONTENT:
                if (action.id === state.id) {
                    // If this cell is being updated, set the data
                    var reduced = reduce();
                    var emptyValue = action.state == null;
                    if (action.lang && emptyValue) {
                        (_d = reduced.content.stateI18n) === null || _d === void 0 ? true : delete _d[action.lang];
                    }
                    return __assign(__assign({}, reduced), { content: __assign(__assign({}, ((_e = state.content) !== null && _e !== void 0 ? _e : {})), (action.lang
                            ? {
                                stateI18n: __assign(__assign({}, ((_f = reduced.content.stateI18n) !== null && _f !== void 0 ? _f : {})), (!emptyValue ? (_b = {}, _b[action.lang] = action.state, _b) : {})),
                            }
                            : {
                                state: action.state,
                            })) });
                }
                return reduce();
            case CELL_UPDATE_LAYOUT:
                if (action.id === state.id) {
                    // If this cell is being updated, set the data
                    var reduced = reduce();
                    var emptyValue = action.state == null;
                    if (action.lang && emptyValue) {
                        (_g = reduced.layout.stateI18n) === null || _g === void 0 ? true : delete _g[action.lang];
                    }
                    return __assign(__assign({}, reduced), { layout: __assign(__assign({}, ((_h = state.layout) !== null && _h !== void 0 ? _h : {})), (action.lang
                            ? {
                                stateI18n: __assign(__assign({}, ((_j = reduced.layout.stateI18n) !== null && _j !== void 0 ? _j : {})), (!emptyValue ? (_c = {}, _c[action.lang] = action.state, _c) : {})),
                            }
                            : {
                                state: action.state,
                            })) });
                }
                return reduce();
            case CELL_FOCUS:
                if (action.id === state.id) {
                    // If this cell is being focused, set the data
                    return __assign(__assign({}, reduce()), { focused: true, scrollToCell: action.scrollToCell ? new Date().getTime() : null, focusSource: action.source });
                }
                return __assign(__assign({}, reduce()), { focused: null, scrollToCell: null, focusSource: null });
            case CELL_BLUR:
                if (action.id === state.id) {
                    // If this cell is being blurred, set the data
                    return __assign(__assign({}, reduce()), { focused: false, focusSource: null });
                }
                return reduce();
            case CELL_BLUR_ALL:
                return __assign(__assign({}, reduce()), { focused: false, scrollToCell: null });
            case CELL_DRAG_HOVER:
                if (isHoveringThis(state, action)) {
                    // if this is the cell we're hovering, set the hover attribute
                    return __assign(__assign({}, reduce()), { hover: action.position });
                }
                // or remove it if not
                return reduce();
            case CELL_INSERT_ABOVE:
                if (isHoveringThis(state, action)) {
                    return __assign(__assign({}, createCell()), { id: action.ids.cell, hover: null, rows: rows([
                            __assign(__assign({}, createRow()), { id: action.ids.others[0], cells: [
                                    __assign(__assign({}, action.item), { id: action.ids.item, inline: null }),
                                ] }),
                            __assign(__assign({}, createRow()), { id: action.ids.others[1], cells: [__assign(__assign({}, reduce()), { id: action.ids.others[2] })] }),
                        ], __assign(__assign({}, action), { hover: null })) });
                }
                return reduce();
            case CELL_INSERT_BELOW:
                if (isHoveringThis(state, action)) {
                    return __assign(__assign({}, createCell()), { id: action.ids.cell, hover: null, rows: rows([
                            __assign(__assign({}, createRow()), { id: action.ids.others[0], cells: [__assign(__assign({}, reduce()), { id: action.ids.others[1] })] }),
                            __assign(__assign({}, createRow()), { id: action.ids.others[2], cells: [
                                    __assign(__assign({}, action.item), { id: action.ids.item, inline: null }),
                                ] }),
                        ], __assign(__assign({}, action), { hover: null })) });
                }
                return reduce();
            default:
                return reduce();
        }
    })(s, a));
};
export var cells = function (s, a) {
    if (s === void 0) { s = []; }
    return optimizeCells((function (state, action) {
        switch (action.type) {
            case CELL_RESIZE:
                // eslint-disable-next-line @typescript-eslint/no-explicit-any
                return resizeCells(state.map(inner(cell, action)), action);
            case CELL_INSERT_AT_END:
            case CELL_INSERT_BELOW:
            case CELL_INSERT_ABOVE:
                return state
                    .filter(function (c) { return c.id !== action.item.id; })
                    .map(inner(cell, action));
            case CELL_INSERT_LEFT_OF:
                return state
                    .filter(function (c) { return c.id !== action.item.id; })
                    .map(function (c) {
                    return isHoveringThis(c, action)
                        ? [
                            __assign(__assign({}, action.item), { id: action.ids.item, inline: null }),
                            __assign(__assign({}, c), { id: action.ids.others[0] }),
                        ]
                        : [c];
                })
                    .reduce(flatten, [])
                    .map(inner(cell, action));
            case CELL_INSERT_RIGHT_OF:
                return state
                    .filter(function (c) { return c.id !== action.item.id; })
                    .map(function (c) {
                    return isHoveringThis(c, action)
                        ? [
                            __assign(__assign({}, c), { id: action.ids.others[0] }),
                            __assign(__assign({}, action.item), { id: action.ids.item, inline: null }),
                        ]
                        : [c];
                })
                    .reduce(flatten, [])
                    .map(inner(cell, action));
            case CELL_INSERT_INLINE_RIGHT:
            case CELL_INSERT_INLINE_LEFT:
                return state
                    .filter(function (c) { return c.id !== action.item.id; })
                    .map(function (c) {
                    if (isHoveringThis(c, action)) {
                        return [
                            __assign(__assign({}, createCell()), { id: action.ids.cell, rows: [
                                    __assign(__assign({}, createRow()), { id: action.ids.others[0], cells: [
                                            __assign(__assign({}, action.item), { inline: action.type === CELL_INSERT_INLINE_RIGHT
                                                    ? 'right'
                                                    : 'left', id: action.ids.item, size: 0 }),
                                            __assign(__assign({}, c), { id: action.ids.others[1], inline: null, hasInlineNeighbour: action.ids.item, size: 0 }),
                                        ] }),
                                ] }),
                        ];
                    }
                    return [c];
                })
                    .reduce(flatten, [])
                    .map(inner(cell, action));
            case CELL_REMOVE:
                return state
                    .filter(function (_a) {
                    var id = _a.id;
                    return id !== action.id;
                })
                    .map(inner(cell, action));
            default:
                return state.map(inner(cell, action));
        }
    })(s, a));
};
export var row = function (s, a) {
    return optimizeRow((function (state, action) {
        var reduce = function () { return (__assign(__assign({}, state), { hover: null, cells: cells(state.cells, action) })); };
        switch (action.type) {
            case CELL_INSERT_LEFT_OF:
                if (!isHoveringThis(state, action)) {
                    return reduce();
                }
                return __assign(__assign({}, state), { hover: null, cells: cells(__spread([
                        __assign(__assign({}, action.item), { id: action.ids.item, inline: null })
                    ], state.cells), __assign(__assign({}, action), { hover: null })) });
            case CELL_INSERT_RIGHT_OF:
                if (!isHoveringThis(state, action)) {
                    return reduce();
                }
                return __assign(__assign({}, state), { hover: null, cells: cells(__spread(state.cells, [
                        __assign(__assign({}, action.item), { id: action.ids.item, inline: null }),
                    ]), __assign(__assign({}, action), { hover: null })) });
            case CELL_DRAG_HOVER:
                if (isHoveringThis(state, action)) {
                    return __assign(__assign({}, reduce()), { hover: action.position });
                }
                return reduce();
            default:
                return reduce();
        }
    })(s, a));
};
export var rows = function (s, a) {
    if (s === void 0) { s = []; }
    return optimizeRows(
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    (function (state, action) {
        var reduce = function () { return state.map(inner(row, action)); };
        switch (action.type) {
            case CELL_INSERT_ABOVE:
                return state
                    .map(function (r) {
                    return isHoveringThis(r, action)
                        ? [
                            __assign(__assign({}, createRow()), { cells: [
                                    __assign(__assign({}, action.item), { id: action.ids.item, inline: null }),
                                ], id: action.ids.others[0] }),
                            __assign(__assign({}, r), { id: action.ids.others[1] }),
                        ]
                        : [r];
                })
                    .reduce(flatten, [])
                    .map(inner(row, action));
            case CELL_INSERT_BELOW:
                return state
                    .map(function (r) {
                    return isHoveringThis(r, action)
                        ? [
                            __assign(__assign({}, r), { id: action.ids.others[0] }),
                            __assign(__assign({}, createRow()), { cells: [
                                    __assign(__assign({}, action.item), { id: action.ids.item, inline: null }),
                                ], id: action.ids.others[1] }),
                        ]
                        : [r];
                })
                    .reduce(flatten, [])
                    .map(inner(row, action));
            case CELL_INSERT_AT_END:
                return __spread(state, [
                    __assign(__assign({}, createRow()), { cells: [__assign(__assign({}, action.item), { id: action.ids.item, inline: null })], id: action.ids.others[1] }),
                ]);
            default:
                return reduce();
        }
    })(s, a));
};
//# sourceMappingURL=tree.js.map