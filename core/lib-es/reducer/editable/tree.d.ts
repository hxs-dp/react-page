import { AnyAction } from 'redux';
import { Cell, Row } from '../../types/editable';
export declare const cell: (s: Cell, a: AnyAction) => Cell;
export declare const cells: (s: Cell[], a: AnyAction) => Cell[];
export declare const row: (s: Row, a: AnyAction) => Row;
export declare const rows: (s: Row[], a: AnyAction) => Row[];
//# sourceMappingURL=tree.d.ts.map