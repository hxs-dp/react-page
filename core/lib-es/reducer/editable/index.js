var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import { CELL_CREATE_FALLBACK } from '../../actions/cell';
import sanitizeInitialChildren from '../../helper/sanitizeInitialChildren';
import { createCell } from '../../types/editable';
import { cellOrder } from './helper/order';
import { setAllSizesAndOptimize } from './helper/setAllSizesAndOptimize';
import { cells } from './tree';
export var rawEditableReducer = function (state, action) {
    if (state === void 0) { state = {
        id: null,
        cells: [],
        config: {
            whitelist: [],
        },
    }; }
    var newCells = setAllSizesAndOptimize(cells(state.cells, action));
    // eslint-disable-next-line default-case
    switch (action.type) {
        case CELL_CREATE_FALLBACK:
            if (action.editable === state.id) {
                if (action.fallback.createInitialChildren) {
                    var children = sanitizeInitialChildren(action.fallback.createInitialChildren());
                    var c = __assign(__assign(__assign({}, createCell()), children), { layout: {
                            plugin: action.fallback,
                            state: action.fallback.createInitialState(),
                        }, id: action.ids.cell });
                    newCells = setAllSizesAndOptimize(cells([c], action));
                }
                else {
                    var c = __assign(__assign({}, createCell()), { content: {
                            plugin: action.fallback,
                            state: action.fallback.createInitialState(),
                        }, id: action.ids.cell });
                    newCells = setAllSizesAndOptimize(cells([c], action));
                }
            }
            break;
        default:
            break;
    }
    return __assign(__assign({}, state), { cells: newCells, cellOrder: cellOrder(newCells || []) });
};
export var editable = rawEditableReducer;
//# sourceMappingURL=index.js.map