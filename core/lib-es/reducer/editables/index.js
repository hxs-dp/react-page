var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
var __spread = (this && this.__spread) || function () {
    for (var ar = [], i = 0; i < arguments.length; i++) ar = ar.concat(__read(arguments[i]));
    return ar;
};
import undoable, { includeAction } from 'redux-undo';
import { set } from 'redux-undo/lib/debug';
import { CELL_REMOVE, CELL_RESIZE, CELL_UPDATE_CONTENT, CELL_UPDATE_LAYOUT, } from '../../actions/cell/core';
import { CELL_INSERT_ABOVE, CELL_INSERT_BELOW, CELL_INSERT_INLINE_LEFT, CELL_INSERT_INLINE_RIGHT, CELL_INSERT_LEFT_OF, CELL_INSERT_RIGHT_OF, CELL_INSERT_AT_END, } from '../../actions/cell/insert';
import { UPDATE_EDITABLE } from '../../actions/editables';
import { isProduction } from '../../const';
import { editable } from '../editable';
if (!isProduction) {
    set(true);
}
var inner = undoable(function (
// eslint-disable-next-line @typescript-eslint/no-explicit-any
state, action) {
    if (state === void 0) { state = []; }
    switch (action.type) {
        default:
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            return state.map(function (e) { return editable(e, action); });
    }
}, {
    filter: includeAction([
        CELL_UPDATE_CONTENT,
        CELL_UPDATE_LAYOUT,
        CELL_REMOVE,
        CELL_RESIZE,
        CELL_INSERT_ABOVE,
        CELL_INSERT_BELOW,
        CELL_INSERT_LEFT_OF,
        CELL_INSERT_RIGHT_OF,
        CELL_INSERT_INLINE_LEFT,
        CELL_INSERT_INLINE_RIGHT,
        CELL_INSERT_AT_END,
    ]),
    // initTypes: [UPDATE_EDITABLE],
    neverSkipReducer: true,
});
export var editables = function (state, action) {
    if (state === void 0) { state = {
        past: [],
        present: [],
        future: [],
    }; }
    var _a = state.past, past = _a === void 0 ? [] : _a, _b = state.present, present = _b === void 0 ? [] : _b, _c = state.future, future = _c === void 0 ? [] : _c;
    switch (action.type) {
        case UPDATE_EDITABLE:
            return inner({
                past: past.map(function (e) { return __spread(e.filter(function (_a) {
                    var id = _a.id;
                    return id !== action.editable.id;
                }), [
                    // we need to run the rawreducer once or the history initial state will be inconsistent.
                    // resolves https://github.com/ory/editor/pull/117#issuecomment-242942796
                    // ...past,
                    editable(action.editable, action),
                ]); }),
                present: inner(__spread(present.filter(function (_a) {
                    var id = _a.id;
                    return id !== action.editable.id;
                }), [
                    // we need to run the rawreducer once or the history initial state will be inconsistent.
                    // resolves https://github.com/ory/editor/pull/117#issuecomment-242942796
                    editable(action.editable, action),
                ]), undefined),
                future: future,
            }, undefined);
        default:
            return inner(state, action);
    }
};
//# sourceMappingURL=index.js.map