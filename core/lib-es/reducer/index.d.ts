/// <reference types="redux-undo" />
declare const reducer: import("redux").Reducer<import("redux").CombinedState<{
    editables: import("../types/editable").Editables | import("redux-undo").StateWithHistory<any>;
    display: import("../types/display").Display;
    focus: string;
    settings: {
        lang: any;
    };
}>, import("redux").AnyAction>;
/**
 * @example
 * import { reducer } from '@react-page/core'
 * const reducer = combineReducers({
 *   reactPage: reducer,
 *   // ...
 * })
 * const store = createStore(reducer, null, middleware)
 * new Editor({ store })
 */
export { reducer };
declare const _default: import("redux").Reducer<import("redux").CombinedState<{
    reactPage: import("redux").CombinedState<{
        editables: any;
        display: any;
        focus: any;
        settings: any;
    }>;
}>, import("redux").AnyAction>;
export default _default;
//# sourceMappingURL=index.d.ts.map