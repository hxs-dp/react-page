"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@react-page/core");
var ui_1 = __importDefault(require("@react-page/ui"));
var react_1 = __importStar(require("react"));
var StickyWrapper_1 = __importDefault(require("./StickyWrapper"));
var fast_deep_equal_1 = __importDefault(require("fast-deep-equal"));
var EditableEditor = function (_a) {
    var plugins = _a.plugins, defaultPlugin = _a.defaultPlugin, value = _a.value, onChange = _a.onChange, dndBackend = _a.dndBackend, defaultDisplayMode = _a.defaultDisplayMode, blurGateDisabled = _a.blurGateDisabled, lang = _a.lang, languages = _a.languages, onChangeLang = _a.onChangeLang, hideEditorSidebar = _a.hideEditorSidebar, rest = __rest(_a, ["plugins", "defaultPlugin", "value", "onChange", "dndBackend", "defaultDisplayMode", "blurGateDisabled", "lang", "languages", "onChangeLang", "hideEditorSidebar"]);
    var theValue = value || core_1.createEmptyState();
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    var lastValueRef = react_1.useRef();
    var editorRef = react_1.useRef(new core_1.Editor({ defaultPlugin: defaultPlugin, plugins: plugins, languages: languages, lang: lang }));
    var onChangeCallback = react_1.useCallback(function (newValue) {
        lastValueRef.current = newValue;
        onChange(newValue);
    }, [onChange]);
    var equal = fast_deep_equal_1.default(theValue, lastValueRef === null || lastValueRef === void 0 ? void 0 : lastValueRef.current);
    react_1.useEffect(function () {
        if (!equal) {
            lastValueRef.current = theValue;
            editorRef.current.trigger.editable.update(theValue);
        }
    }, [equal]);
    var editor = editorRef.current;
    return (react_1.default.createElement(core_1.Provider, { editor: editor, dndBackend: dndBackend, blurGateDisabled: blurGateDisabled, blurGateDefaultMode: defaultDisplayMode },
        react_1.default.createElement(StickyWrapper_1.default, null, function (stickyNess) { return (react_1.default.createElement(react_1.default.Fragment, null,
            react_1.default.createElement(core_1.Editable, __assign({ lang: lang, onChangeLang: onChangeLang, id: theValue === null || theValue === void 0 ? void 0 : theValue.id, onChange: onChangeCallback }, rest)),
            react_1.default.createElement(ui_1.default, { stickyNess: stickyNess, hideEditorSidebar: hideEditorSidebar }))); })));
};
exports.default = EditableEditor;
//# sourceMappingURL=EditableEditor.js.map