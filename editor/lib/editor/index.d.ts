import { ContentPluginConfig, DndBackend, EditableType, LayoutPluginConfig, Plugins, Languages, DisplayModes, SimplifiedModesProps } from '@react-page/core';
import React from 'react';
export declare type EditorProps = {
    plugins?: Plugins;
    defaultPlugin?: ContentPluginConfig | LayoutPluginConfig;
    dndBackend?: DndBackend;
    value?: EditableType | null;
    onChange?: (v: EditableType) => void;
    onChangeLang?: (l: string) => void;
    readOnly?: boolean;
    defaultDisplayMode?: DisplayModes;
    blurGateDisabled?: boolean;
    languages?: Languages;
    lang?: string;
    hideEditorSidebar?: boolean;
} & SimplifiedModesProps;
declare const _default: React.NamedExoticComponent<EditorProps>;
export default _default;
//# sourceMappingURL=index.d.ts.map