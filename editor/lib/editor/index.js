"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@react-page/core");
var renderer_1 = require("@react-page/renderer");
var react_1 = __importDefault(require("react"));
var EditableEditor = core_1.lazyLoad(function () { return Promise.resolve().then(function () { return __importStar(require('./EditableEditor')); }); });
var Editor = function (_a) {
    var plugins = _a.plugins, defaultPlugin = _a.defaultPlugin, readOnly = _a.readOnly, value = _a.value, onChange = _a.onChange, dndBackend = _a.dndBackend, blurGateDisabled = _a.blurGateDisabled, defaultDisplayMode = _a.defaultDisplayMode, lang = _a.lang, rest = __rest(_a, ["plugins", "defaultPlugin", "readOnly", "value", "onChange", "dndBackend", "blurGateDisabled", "defaultDisplayMode", "lang"]);
    return readOnly ? (react_1.default.createElement(renderer_1.HTMLRenderer, { state: value, plugins: plugins, lang: lang })) : (react_1.default.createElement(EditableEditor, __assign({ plugins: plugins, defaultPlugin: defaultPlugin, value: value, onChange: onChange, dndBackend: dndBackend, blurGateDisabled: blurGateDisabled, defaultDisplayMode: defaultDisplayMode, lang: lang }, rest)));
};
exports.default = react_1.default.memo(Editor);
//# sourceMappingURL=index.js.map