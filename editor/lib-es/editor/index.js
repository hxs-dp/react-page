var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
import { lazyLoad, } from '@react-page/core';
import { HTMLRenderer } from '@react-page/renderer';
import React from 'react';
var EditableEditor = lazyLoad(function () { return import('./EditableEditor'); });
var Editor = function (_a) {
    var plugins = _a.plugins, defaultPlugin = _a.defaultPlugin, readOnly = _a.readOnly, value = _a.value, onChange = _a.onChange, dndBackend = _a.dndBackend, blurGateDisabled = _a.blurGateDisabled, defaultDisplayMode = _a.defaultDisplayMode, lang = _a.lang, rest = __rest(_a, ["plugins", "defaultPlugin", "readOnly", "value", "onChange", "dndBackend", "blurGateDisabled", "defaultDisplayMode", "lang"]);
    return readOnly ? (React.createElement(HTMLRenderer, { state: value, plugins: plugins, lang: lang })) : (React.createElement(EditableEditor, __assign({ plugins: plugins, defaultPlugin: defaultPlugin, value: value, onChange: onChange, dndBackend: dndBackend, blurGateDisabled: blurGateDisabled, defaultDisplayMode: defaultDisplayMode, lang: lang }, rest)));
};
export default React.memo(Editor);
//# sourceMappingURL=index.js.map