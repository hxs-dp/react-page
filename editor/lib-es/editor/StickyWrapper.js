var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
import React, { useState } from 'react';
export default (function (_a) {
    var children = _a.children;
    var ref = React.createRef();
    var stickyElRef = React.createRef();
    var _b = __read(useState(false), 2), shouldStickToTop = _b[0], setShouldStickToTop = _b[1];
    var _c = __read(useState(true), 2), shouldStickToBottom = _c[0], setShouldStickToBottom = _c[1];
    var _d = __read(useState(0), 2), rightOffset = _d[0], setRightOffset = _d[1];
    React.useEffect(function () {
        var calc = function () {
            if (ref.current) {
                var _a = ref.current.getBoundingClientRect(), top_1 = _a.top, left = _a.left;
                var bottom = top_1 + ref.current.clientHeight;
                // document.documentElement.clientWidth is without scrollbars, so better for us
                var right = document.documentElement.clientWidth - left - ref.current.clientWidth;
                var uiHeight = stickyElRef.current
                    ? stickyElRef.current.clientHeight
                    : 400;
                setRightOffset(right);
                setShouldStickToTop(top_1 > window.innerHeight - uiHeight);
                setShouldStickToBottom(bottom < window.innerHeight);
            }
        };
        document.addEventListener('scroll', calc);
        window.addEventListener('resize', calc);
        var observer = null;
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        if (global.IntersectionObserver) {
            observer = new IntersectionObserver(calc);
            if (ref.current) {
                observer.observe(ref.current);
            }
        }
        // do it once
        calc();
        return function () {
            document.removeEventListener('scroll', calc);
            window.removeEventListener('resize', calc);
            observer === null || observer === void 0 ? void 0 : observer.disconnect();
        };
    }, [ref, stickyElRef]);
    return (React.createElement("div", { style: { position: 'relative' }, ref: ref }, children({
        rightOffset: rightOffset,
        stickyElRef: stickyElRef,
        shouldStickToTop: shouldStickToTop,
        shouldStickToBottom: shouldStickToBottom,
    })));
});
//# sourceMappingURL=StickyWrapper.js.map