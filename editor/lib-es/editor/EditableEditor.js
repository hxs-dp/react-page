var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
import { createEmptyState, Editable, Editor, Provider, } from '@react-page/core';
import EditorUI from '@react-page/ui';
import React, { useEffect, useRef, useCallback } from 'react';
import StickyWrapper from './StickyWrapper';
import equals from 'fast-deep-equal';
var EditableEditor = function (_a) {
    var plugins = _a.plugins, defaultPlugin = _a.defaultPlugin, value = _a.value, onChange = _a.onChange, dndBackend = _a.dndBackend, defaultDisplayMode = _a.defaultDisplayMode, blurGateDisabled = _a.blurGateDisabled, lang = _a.lang, languages = _a.languages, onChangeLang = _a.onChangeLang, hideEditorSidebar = _a.hideEditorSidebar, rest = __rest(_a, ["plugins", "defaultPlugin", "value", "onChange", "dndBackend", "defaultDisplayMode", "blurGateDisabled", "lang", "languages", "onChangeLang", "hideEditorSidebar"]);
    var theValue = value || createEmptyState();
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    var lastValueRef = useRef();
    var editorRef = useRef(new Editor({ defaultPlugin: defaultPlugin, plugins: plugins, languages: languages, lang: lang }));
    var onChangeCallback = useCallback(function (newValue) {
        lastValueRef.current = newValue;
        onChange(newValue);
    }, [onChange]);
    var equal = equals(theValue, lastValueRef === null || lastValueRef === void 0 ? void 0 : lastValueRef.current);
    useEffect(function () {
        if (!equal) {
            lastValueRef.current = theValue;
            editorRef.current.trigger.editable.update(theValue);
        }
    }, [equal]);
    var editor = editorRef.current;
    return (React.createElement(Provider, { editor: editor, dndBackend: dndBackend, blurGateDisabled: blurGateDisabled, blurGateDefaultMode: defaultDisplayMode },
        React.createElement(StickyWrapper, null, function (stickyNess) { return (React.createElement(React.Fragment, null,
            React.createElement(Editable, __assign({ lang: lang, onChangeLang: onChangeLang, id: theValue === null || theValue === void 0 ? void 0 : theValue.id, onChange: onChangeCallback }, rest)),
            React.createElement(EditorUI, { stickyNess: stickyNess, hideEditorSidebar: hideEditorSidebar }))); })));
};
export default EditableEditor;
//# sourceMappingURL=EditableEditor.js.map