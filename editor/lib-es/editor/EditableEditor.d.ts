import { Plugins, ContentPluginConfig, LayoutPluginConfig, DndBackend, EditableType, Languages, SimplifiedModesProps, DisplayModes } from '@react-page/core';
import React from 'react';
export declare type EditableEditorProps = {
    plugins?: Plugins;
    defaultPlugin?: ContentPluginConfig | LayoutPluginConfig;
    dndBackend?: DndBackend;
    value?: EditableType;
    onChange?: (v: EditableType) => void;
    defaultDisplayMode?: DisplayModes;
    blurGateDisabled?: boolean;
    languages?: Languages;
    lang?: string;
    onChangeLang?: (l: string) => void;
    hideEditorSidebar?: boolean;
} & SimplifiedModesProps;
declare const EditableEditor: React.FC<EditableEditorProps>;
export default EditableEditor;
//# sourceMappingURL=EditableEditor.d.ts.map