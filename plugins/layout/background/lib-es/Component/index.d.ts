import * as React from 'react';
import { BackgroundProps } from '../types/component';
import { ImageLoaded, RGBColor } from '@react-page/ui';
import { ModeEnum } from '../types/ModeEnum';
import { BackgroundApi } from '../types/api';
export declare type BackgroundState = {
    backgroundColorPreview?: RGBColor;
    gradientDegPreview?: number;
    gradientDegPreviewIndex?: number;
    gradientOpacityPreview?: number;
    gradientOpacityPreviewIndex?: number;
    gradientColorPreview?: RGBColor;
    gradientColorPreviewIndex?: number;
    gradientColorPreviewColorIndex?: number;
    darkenPreview?: number;
    lightenPreview?: number;
    imagePreview?: ImageLoaded;
};
declare class BackgroundComponent extends React.Component<BackgroundProps, BackgroundState> implements BackgroundApi {
    constructor(props: BackgroundProps);
    handleChangeDarken: () => void;
    handleChangeDarkenPreview: (value: number) => void;
    handleChangeLighten: () => void;
    handleChangeLightenPreview: (value: number) => void;
    handleChangeHasPadding: () => void;
    handleChangeBackgroundColorPreview: (e: RGBColor) => void;
    handleChangeGradientDegPreview: (gradientDegPreview: number, gradientDegPreviewIndex?: number) => void;
    handleChangeGradientOpacityPreview: (gradientOpacityPreview: number, gradientOpacityPreviewIndex?: number) => void;
    handleChangeGradientColorPreview: (gradientColorPreview: RGBColor, gradientColorPreviewIndex?: number, gradientColorPreviewColorIndex?: number) => void;
    handleImageLoaded: (imagePreview: ImageLoaded) => void;
    handleImageUploaded: () => void;
    handleChangeModeSwitch: (mode: ModeEnum, modeFlag: ModeEnum) => () => void;
    render(): JSX.Element;
}
export default BackgroundComponent;
//# sourceMappingURL=index.d.ts.map