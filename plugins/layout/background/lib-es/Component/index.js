var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import * as React from 'react';
var BackgroundComponent = /** @class */ (function (_super) {
    __extends(BackgroundComponent, _super);
    function BackgroundComponent(props) {
        var _this = _super.call(this, props) || this;
        _this.handleChangeDarken = function () {
            _this.props.onChange({ darken: _this.state.darkenPreview });
            _this.setState({ darkenPreview: undefined });
        };
        _this.handleChangeDarkenPreview = function (value) {
            _this.setState({ darkenPreview: value });
        };
        _this.handleChangeLighten = function () {
            _this.props.onChange({ lighten: _this.state.lightenPreview });
            _this.setState({ lightenPreview: undefined });
        };
        _this.handleChangeLightenPreview = function (value) {
            _this.setState({ lightenPreview: value });
        };
        _this.handleChangeHasPadding = function () {
            _this.props.onChange({
                hasPadding: _this.props.state.hasPadding === undefined
                    ? !_this.props.defaultHasPadding
                    : !_this.props.state.hasPadding,
            });
        };
        _this.handleChangeBackgroundColorPreview = function (e) {
            return _this.setState({ backgroundColorPreview: e });
        };
        _this.handleChangeGradientDegPreview = function (gradientDegPreview, gradientDegPreviewIndex) { return _this.setState({ gradientDegPreview: gradientDegPreview, gradientDegPreviewIndex: gradientDegPreviewIndex }); };
        _this.handleChangeGradientOpacityPreview = function (gradientOpacityPreview, gradientOpacityPreviewIndex) { return _this.setState({ gradientOpacityPreview: gradientOpacityPreview, gradientOpacityPreviewIndex: gradientOpacityPreviewIndex }); };
        _this.handleChangeGradientColorPreview = function (gradientColorPreview, gradientColorPreviewIndex, gradientColorPreviewColorIndex) {
            return _this.setState({
                gradientColorPreview: gradientColorPreview,
                gradientColorPreviewIndex: gradientColorPreviewIndex,
                gradientColorPreviewColorIndex: gradientColorPreviewColorIndex,
            });
        };
        _this.handleImageLoaded = function (imagePreview) {
            return _this.setState({ imagePreview: imagePreview });
        };
        _this.handleImageUploaded = function () { return _this.setState({ imagePreview: undefined }); };
        _this.handleChangeModeSwitch = function (mode, modeFlag) { return function () {
            modeFlag ^= mode;
            _this.props.onChange({ modeFlag: modeFlag });
        }; };
        _this.state = {};
        return _this;
    }
    BackgroundComponent.prototype.render = function () {
        var _a = this.props, Controls = _a.Controls, Renderer = _a.Renderer, readOnly = _a.readOnly;
        return (React.createElement(React.Fragment, null,
            !readOnly ? (React.createElement(Controls, __assign({}, this.props, { handleChangeDarken: this.handleChangeDarken, handleChangeDarkenPreview: this.handleChangeDarkenPreview, handleChangeLighten: this.handleChangeLighten, handleChangeLightenPreview: this.handleChangeLightenPreview, handleChangeHasPadding: this.handleChangeHasPadding, handleChangeModeSwitch: this.handleChangeModeSwitch, handleChangeBackgroundColorPreview: this.handleChangeBackgroundColorPreview, handleChangeGradientDegPreview: this.handleChangeGradientDegPreview, handleChangeGradientOpacityPreview: this.handleChangeGradientOpacityPreview, handleChangeGradientColorPreview: this.handleChangeGradientColorPreview, handleImageLoaded: this.handleImageLoaded, handleImageUploaded: this.handleImageUploaded }, this.state))) : null,
            React.createElement(Renderer, __assign({}, this.props))));
    };
    return BackgroundComponent;
}(React.Component));
export default BackgroundComponent;
//# sourceMappingURL=index.js.map