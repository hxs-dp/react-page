var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import * as React from 'react';
import BackgroundComponent from './Component';
import { defaultSettings } from './default/settings';
import { lazyLoad } from '@react-page/core';
var Icon = lazyLoad(function () { return import('@material-ui/icons/CropLandscape'); });
var createPlugin = function (settings) {
    var mergedSettings = __assign(__assign({}, defaultSettings), settings);
    var plugin = {
        Component: function (props) { return (React.createElement(BackgroundComponent, __assign({}, props, mergedSettings))); },
        name: 'ory/editor/core/layout/background',
        version: '0.0.1',
        text: mergedSettings.translations.pluginName,
        description: mergedSettings.translations.pluginDescription,
        IconComponent: React.createElement(Icon, null),
        createInitialChildren: settings.getInitialChildren ||
            (function () { return [[{ content: { plugin: settings.defaultPlugin } }]]; }),
        handleFocusNextHotKey: function () { return Promise.reject(); },
        handleFocusPreviousHotKey: function () { return Promise.reject(); },
    };
    return plugin;
};
export default createPlugin;
//# sourceMappingURL=createPlugin.js.map