import { BackgroundSettings } from './types/settings';
import { MakeOptional } from './types/makeOptional';
import { ModeEnum } from './types/ModeEnum';
export { ModeEnum };
declare const _default: (settings: MakeOptional<BackgroundSettings, 'Renderer' | 'Controls'>) => Pick<import("@react-page/core").LayoutPluginProps<import("./types/state").BackgroundState>, "Component" | "name" | "text" | "description" | "lang" | "version" | "IconComponent" | "hideInMenu" | "serialize" | "unserialize" | "handleRemoveHotKey" | "handleFocusNextHotKey" | "handleFocusPreviousHotKey" | "handleFocus" | "handleBlur" | "reducer" | "migrations" | "createInitialState" | "createInitialChildren">;
export default _default;
//# sourceMappingURL=index.d.ts.map