var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import { colorToString } from '@react-page/ui';
import * as React from 'react';
import { ModeEnum } from '../types/ModeEnum';
var getStyles = function (props) {
    var _a = props.state, _b = _a.background, background = _b === void 0 ? '' : _b, _c = _a.modeFlag, modeFlag = _c === void 0 ? props.defaultModeFlag : _c, _d = _a.isParallax, isParallax = _d === void 0 ? true : _d, _e = _a.backgroundColor, backgroundColor = _e === void 0 ? props.defaultBackgroundColor : _e, _f = _a.gradients, gradients = _f === void 0 ? [] : _f;
    var styles = {};
    if (modeFlag & ModeEnum.GRADIENT_MODE_FLAG) {
        var usedGradients = gradients.filter(function (g) { return g.colors && g.colors.length; });
        var usedGradientsString = usedGradients
            .map(function (g, i) {
            var firstColor = g.colors[0].color;
            var firstColorStr = colorToString(firstColor);
            var deg = i === props.gradientDegPreviewIndex &&
                props.gradientDegPreview !== undefined
                ? props.gradientDegPreview
                : g.deg;
            var opacity = i === props.gradientOpacityPreviewIndex &&
                props.gradientOpacityPreview !== undefined
                ? props.gradientOpacityPreview
                : g.opacity;
            return ('linear-gradient(' +
                deg +
                'deg, ' +
                (g.colors.length !== 1
                    ? g.colors
                        .map(function (c, cpIndex) {
                        var color = i === props.gradientColorPreviewIndex &&
                            cpIndex === props.gradientColorPreviewColorIndex &&
                            props.gradientColorPreview !== undefined
                            ? props.gradientColorPreview
                            : c.color;
                        var colorWithOpacity = __assign(__assign({}, color), { a: color.a !== undefined ? color.a * opacity : opacity });
                        return colorToString(colorWithOpacity);
                    })
                        .join(', ')
                    : firstColorStr + ', ' + firstColorStr) +
                ')');
        })
            .join(', ');
        if (usedGradientsString !== '') {
            styles = __assign(__assign({}, styles), { background: usedGradientsString });
        }
    }
    if (modeFlag & ModeEnum.COLOR_MODE_FLAG) {
        var colorStr = colorToString(props.backgroundColorPreview
            ? props.backgroundColorPreview
            : backgroundColor);
        var modeStr = "linear-gradient(" + colorStr + ", " + colorStr + ")";
        styles = __assign(__assign({}, styles), { background: styles.background
                ? styles.background + ', ' + modeStr
                : modeStr });
    }
    if (modeFlag & ModeEnum.IMAGE_MODE_FLAG) {
        var backgroundFinal = props.imagePreview
            ? props.imagePreview.dataUrl
            : background;
        var modeStr = "url('" + backgroundFinal + "') center / cover no-repeat" +
            (isParallax ? ' fixed' : '');
        styles = __assign(__assign({}, styles), { background: styles.background
                ? styles.background + ', ' + modeStr
                : modeStr });
    }
    return styles;
};
var BackgroundHtmlRenderer = function (props) {
    var children = props.children, _a = props.state, _b = _a.darken, darken = _b === void 0 ? props.defaultDarken : _b, _c = _a.lighten, lighten = _c === void 0 ? props.defaultLighten : _c, _d = _a.hasPadding, hasPadding = _d === void 0 ? props.defaultHasPadding : _d;
    var darkenFinal = props.darkenPreview !== undefined ? props.darkenPreview : darken;
    var lightenFinal = props.lightenPreview !== undefined ? props.lightenPreview : lighten;
    var containerStyles = getStyles(props);
    return (React.createElement("div", { className: "ory-plugins-layout-background", style: __assign(__assign({}, containerStyles), (hasPadding ? {} : { padding: 0 })) },
        React.createElement("div", { className: "ory-plugins-layout-background__backstretch", style: {
                // tslint:disable-next-line:max-line-length
                backgroundImage: "linear-gradient(rgba(0, 0, 0, " + darkenFinal + "), rgba(0, 0, 0, " + darkenFinal + ")),linear-gradient(rgba(255, 255, 255, " + lightenFinal + "), rgba(255, 255, 255, " + lightenFinal + "))",
            } }),
        children));
};
export default BackgroundHtmlRenderer;
//# sourceMappingURL=BackgroundHtmlRenderer.js.map