var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import * as React from 'react';
import Switch from '@material-ui/core/Switch';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import { ImageUpload } from '@react-page/ui';
import Typography from '@material-ui/core/Typography';
var ImageComponent = /** @class */ (function (_super) {
    __extends(ImageComponent, _super);
    function ImageComponent() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.handleChangeBackground = function (e) {
            _this.props.ensureModeOn();
            _this.props.onChange({ background: e.target.value });
        };
        _this.handleChangeIsParallax = function (e) {
            _this.props.ensureModeOn();
            _this.props.onChange({
                isParallax: _this.props.state.isParallax === undefined
                    ? false
                    : !_this.props.state.isParallax,
            });
        };
        _this.handleImageLoaded = function (image) {
            _this.props.ensureModeOn();
            _this.props.onImageLoaded(image);
        };
        _this.handleImageUploaded = function (resp) {
            _this.props.onImageUploaded();
            _this.props.onChange({ background: resp.url });
        };
        return _this;
    }
    ImageComponent.prototype.render = function () {
        var _a = this.props.state, _b = _a.isParallax, isParallax = _b === void 0 ? true : _b, _c = _a.background, background = _c === void 0 ? '' : _c;
        return (React.createElement("div", null,
            React.createElement("div", { style: { display: 'flex' } },
                this.props.imageUpload && (React.createElement(React.Fragment, null,
                    React.createElement(ImageUpload, { imageUpload: this.props.imageUpload, imageLoaded: this.handleImageLoaded, imageUploaded: this.handleImageUploaded }),
                    React.createElement(Typography, { variant: "body1", style: { marginLeft: '20px', marginRight: '20px' } }, "OR"))),
                React.createElement(TextField, { placeholder: "http://example.com/image.png", label: this.props.imageUpload ? 'I have a URL' : 'Image URL', style: { width: '256px' }, value: background, onChange: this.handleChangeBackground })),
            React.createElement("br", null),
            React.createElement("div", { style: { display: 'flex' } },
                React.createElement(FormControlLabel, { control: React.createElement(Switch, { onChange: this.handleChangeIsParallax, checked: isParallax }), label: "Is parallax" }))));
    };
    return ImageComponent;
}(React.Component));
export default ImageComponent;
//# sourceMappingURL=Image.js.map