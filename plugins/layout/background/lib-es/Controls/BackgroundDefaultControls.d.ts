import * as React from 'react';
import { BackgroundControlsProps } from '../types/controls';
import { ModeEnum } from '../types/ModeEnum';
export interface BackgroundDefaultControlsState {
    mode: ModeEnum;
}
declare class BackgroundDefaultControls extends React.Component<BackgroundControlsProps, BackgroundDefaultControlsState> {
    constructor(props: BackgroundControlsProps);
    render(): JSX.Element;
    renderModeSwitch: () => JSX.Element;
    renderUI: () => JSX.Element;
    ensureModeOn: (mode: ModeEnum) => () => void;
    handleChangeMode: (e: React.ChangeEvent, mode: number) => void;
}
export default BackgroundDefaultControls;
//# sourceMappingURL=BackgroundDefaultControls.d.ts.map