var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Slider from '@material-ui/core/Slider';
import Switch from '@material-ui/core/Switch';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import Typography from '@material-ui/core/Typography';
import ColorIcon from '@material-ui/icons/ColorLens';
import GradientIcon from '@material-ui/icons/Gradient';
import ImageIcon from '@material-ui/icons/Landscape';
import { BottomToolbar } from '@react-page/ui';
import * as React from 'react';
import { ModeEnum } from '../types/ModeEnum';
import ColorComponent from './sub/Color';
import ImageComponent from './sub/Image';
import LinearGradientComponent from './sub/LinearGradient';
var BackgroundDefaultControls = /** @class */ (function (_super) {
    __extends(BackgroundDefaultControls, _super);
    function BackgroundDefaultControls(props) {
        var _this = _super.call(this, props) || this;
        _this.renderModeSwitch = function () {
            var _a = _this.props.state.modeFlag, modeFlag = _a === void 0 ? _this.props.defaultModeFlag : _a;
            var label = _this.props.translations.onOff;
            switch (_this.state.mode) {
                case ModeEnum.COLOR_MODE_FLAG:
                    // label = 'Use color'
                    break;
                case ModeEnum.IMAGE_MODE_FLAG:
                    // label = 'Use image'
                    break;
                case ModeEnum.GRADIENT_MODE_FLAG:
                    // label = 'Use gradient'
                    break;
                default:
                    label = 'Unknown mode';
                    break;
            }
            return (React.createElement(FormControlLabel, { control: React.createElement(Switch, { onChange: _this.props.handleChangeModeSwitch(_this.state.mode, modeFlag), checked: Boolean(modeFlag & _this.state.mode) }), label: label }));
        };
        _this.renderUI = function () {
            switch (_this.state.mode) {
                case ModeEnum.COLOR_MODE_FLAG:
                    return (React.createElement(React.Fragment, null,
                        _this.renderModeSwitch(),
                        React.createElement(ColorComponent, __assign({}, _this.props, { ensureModeOn: _this.ensureModeOn(ModeEnum.COLOR_MODE_FLAG), onChangeBackgroundColorPreview: _this.props.handleChangeBackgroundColorPreview, backgroundColorPreview: _this.props.backgroundColorPreview }))));
                case ModeEnum.GRADIENT_MODE_FLAG:
                    return (React.createElement(React.Fragment, null,
                        _this.renderModeSwitch(),
                        React.createElement(LinearGradientComponent, __assign({}, _this.props, { ensureModeOn: _this.ensureModeOn(ModeEnum.GRADIENT_MODE_FLAG), gradientDegPreview: _this.props.gradientDegPreview, gradientDegPreviewIndex: _this.props.gradientDegPreviewIndex, gradientOpacityPreview: _this.props.gradientOpacityPreview, gradientOpacityPreviewIndex: _this.props.gradientOpacityPreviewIndex, gradientColorPreview: _this.props.gradientColorPreview, gradientColorPreviewIndex: _this.props.gradientColorPreviewIndex, gradientColorPreviewColorIndex: _this.props.gradientColorPreviewColorIndex, onChangeGradientDegPreview: _this.props.handleChangeGradientDegPreview, onChangeGradientOpacityPreview: _this.props.handleChangeGradientOpacityPreview, onChangeGradientColorPreview: _this.props.handleChangeGradientColorPreview }))));
                case ModeEnum.IMAGE_MODE_FLAG:
                default:
                    return (React.createElement(React.Fragment, null,
                        _this.renderModeSwitch(),
                        React.createElement(ImageComponent, __assign({}, _this.props, { onImageLoaded: _this.props.handleImageLoaded, onImageUploaded: _this.props.handleImageUploaded, ensureModeOn: _this.ensureModeOn(ModeEnum.IMAGE_MODE_FLAG) }))));
            }
        };
        _this.ensureModeOn = function (mode) { return function () {
            var _a = _this.props.state.modeFlag, modeFlag = _a === void 0 ? _this.props.defaultModeFlag : _a;
            if ((modeFlag & mode) === 0) {
                _this.props.handleChangeModeSwitch(mode, modeFlag)();
            }
        }; };
        _this.handleChangeMode = function (e, mode) {
            return _this.setState({ mode: mode });
        };
        _this.state = {
            mode: props.defaultMode,
        };
        return _this;
    }
    BackgroundDefaultControls.prototype.render = function () {
        var _this = this;
        var _a = this.props, focused = _a.focused, remove = _a.remove, _b = _a.state, _c = _b.hasPadding, hasPadding = _c === void 0 ? this.props.defaultHasPadding : _c, _d = _b.modeFlag, modeFlag = _d === void 0 ? this.props.defaultModeFlag : _d, _e = _b.darken, darken = _e === void 0 ? this.props.defaultDarken : _e, _f = _b.lighten, lighten = _f === void 0 ? this.props.defaultLighten : _f;
        var darkenFinal = this.props.darkenPreview !== undefined
            ? this.props.darkenPreview
            : darken;
        var lightenFinal = this.props.lightenPreview !== undefined
            ? this.props.lightenPreview
            : lighten;
        return (React.createElement(BottomToolbar, __assign({ open: focused, title: this.props.translations.pluginName, icon: this.props.IconComponent, onDelete: remove }, this.props),
            React.createElement(Tabs, { value: this.state.mode, onChange: this.handleChangeMode, centered: true },
                (this.props.enabledModes & ModeEnum.IMAGE_MODE_FLAG) > 0 && (React.createElement(Tab, { icon: React.createElement(ImageIcon, { color: (modeFlag & ModeEnum.IMAGE_MODE_FLAG) > 0
                            ? 'secondary'
                            : undefined }), label: this.props.translations.imageMode, value: ModeEnum.IMAGE_MODE_FLAG })),
                (this.props.enabledModes & ModeEnum.COLOR_MODE_FLAG) > 0 && (React.createElement(Tab, { icon: React.createElement(ColorIcon, { color: (modeFlag & ModeEnum.COLOR_MODE_FLAG) > 0
                            ? 'secondary'
                            : undefined }), label: this.props.translations.colorMode, value: ModeEnum.COLOR_MODE_FLAG })),
                (this.props.enabledModes & ModeEnum.GRADIENT_MODE_FLAG) > 0 && (React.createElement(Tab, { icon: React.createElement(GradientIcon, { color: (modeFlag & ModeEnum.GRADIENT_MODE_FLAG) > 0
                            ? 'secondary'
                            : undefined }), label: this.props.translations.gradientMode, value: ModeEnum.GRADIENT_MODE_FLAG }))),
            this.renderUI(),
            React.createElement("br", null),
            React.createElement("div", { style: { display: 'flex' } },
                React.createElement("div", { style: { flex: '1', marginRight: '8px' } },
                    React.createElement(Typography, { variant: "body1", id: "linear-gradient-darken-label" },
                        this.props.translations.darken,
                        " (",
                        (darkenFinal * 100).toFixed(0),
                        "%)"),
                    React.createElement(Slider, { "aria-labelledby": "linear-gradient-darken-label", value: darkenFinal, onChange: function (e, value) {
                            return _this.props.handleChangeDarkenPreview(value instanceof Array ? value[0] : value);
                        }, onChangeCommitted: this.props.handleChangeDarken, step: 0.01, min: 0, max: 1 })),
                React.createElement("div", { style: { flex: '1', marginLeft: '8px' } },
                    React.createElement(Typography, { variant: "body1", id: "linear-gradient-lighten-label" },
                        this.props.translations.lighten,
                        " (",
                        (lightenFinal * 100).toFixed(0),
                        "%)"),
                    React.createElement(Slider, { "aria-labelledby": "linear-gradient-lighten-label", value: lightenFinal, onChange: function (e, value) {
                            return _this.props.handleChangeLightenPreview(value instanceof Array ? value[0] : value);
                        }, onChangeCommitted: this.props.handleChangeLighten, step: 0.01, min: 0, max: 1 }))),
            React.createElement("div", { style: { display: 'flex' } },
                React.createElement(FormControlLabel, { control: React.createElement(Switch, { onChange: this.props.handleChangeHasPadding, checked: hasPadding }), label: this.props.translations.usePadding }))));
    };
    return BackgroundDefaultControls;
}(React.Component));
export default BackgroundDefaultControls;
//# sourceMappingURL=BackgroundDefaultControls.js.map