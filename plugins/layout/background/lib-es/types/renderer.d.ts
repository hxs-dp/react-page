import { BackgroundProps } from './component';
import { ImageLoaded, RGBColor } from '@react-page/ui';
export interface BackgroundRendererExtraProps {
    backgroundColorPreview?: RGBColor;
    gradientDegPreview?: number;
    gradientDegPreviewIndex?: number;
    gradientOpacityPreview?: number;
    gradientOpacityPreviewIndex?: number;
    gradientColorPreview?: RGBColor;
    gradientColorPreviewIndex?: number;
    gradientColorPreviewColorIndex?: number;
    darkenPreview?: number;
    lightenPreview?: number;
    imagePreview?: ImageLoaded;
}
export declare type BackgroundRendererProps = BackgroundProps & BackgroundRendererExtraProps;
//# sourceMappingURL=renderer.d.ts.map