import { LayoutPluginProps } from '@react-page/core';
import { BackgroundSettings } from './settings';
import { BackgroundState } from './state';
export declare type BackgroundProps = LayoutPluginProps<BackgroundState> & BackgroundSettings;
//# sourceMappingURL=component.d.ts.map