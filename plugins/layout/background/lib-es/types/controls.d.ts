import { BackgroundProps } from './component';
import { BackgroundApi } from './api';
import { BackgroundRendererExtraProps } from './renderer';
export declare type BackgroundControlsProps = BackgroundProps & BackgroundApi & BackgroundRendererExtraProps;
//# sourceMappingURL=controls.d.ts.map