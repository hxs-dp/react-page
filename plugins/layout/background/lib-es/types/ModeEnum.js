export var ModeEnum;
(function (ModeEnum) {
    ModeEnum[ModeEnum["IMAGE_MODE_FLAG"] = 1] = "IMAGE_MODE_FLAG";
    ModeEnum[ModeEnum["COLOR_MODE_FLAG"] = 2] = "COLOR_MODE_FLAG";
    ModeEnum[ModeEnum["GRADIENT_MODE_FLAG"] = 4] = "GRADIENT_MODE_FLAG";
})(ModeEnum || (ModeEnum = {}));
//# sourceMappingURL=ModeEnum.js.map