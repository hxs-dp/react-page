import { Omit } from './omit';
export declare type MakeOptional<T, K extends keyof T> = Omit<T, K> & Partial<T>;
//# sourceMappingURL=makeOptional.d.ts.map