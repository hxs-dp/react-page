var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import * as React from 'react';
import { IMAGE_MODE_FLAG, COLOR_MODE_FLAG, GRADIENT_MODE_FLAG, } from './../const/mode';
import { defaultTranslations as defaultImageUploadTranslations } from '@react-page/ui/lib/ImageUpload/defaultTranslations';
export var defaultTranslations = __assign(__assign({}, defaultImageUploadTranslations), { imageMode: 'Image', colorMode: 'Color', gradientMode: 'Gradient', lighten: 'Lighten', darken: 'Darken', usePadding: 'Use Padding', onOff: 'ON/OFF', gradientRotation: 'Gradient rotation', degrees: 'deg', gradientOpacity: 'Gradient opacity', addColor: 'Add color', addGradient: 'Add gradient', pluginName: 'Background', pluginDescription: '' });
export var defaultSettings = {
    defaultBackgroundColor: { r: 245, g: 0, b: 87, a: 1 },
    defaultGradientColor: { r: 245, g: 0, b: 87, a: 1 },
    defaultGradientSecondaryColor: { r: 71, g: 245, b: 87, a: 1 },
    defaultMode: 1,
    defaultModeFlag: 1,
    defaultDarken: 0.1,
    defaultLighten: 0,
    defaultHasPadding: true,
    defaultIsParallax: true,
    translations: defaultTranslations,
    enabledModes: IMAGE_MODE_FLAG | COLOR_MODE_FLAG | GRADIENT_MODE_FLAG,
    Controls: function () { return React.createElement(React.Fragment, null, " Controls for this plugin were not provided"); },
    Renderer: function () { return React.createElement(React.Fragment, null, "Renderer; for this plugin was not provided "); },
};
//# sourceMappingURL=settings.js.map