import { RGBColor } from '@react-page/ui';
export declare type Gradient = {
    opacity: number;
    deg: number;
    colors?: {
        color: RGBColor;
    }[];
};
//# sourceMappingURL=gradient.d.ts.map