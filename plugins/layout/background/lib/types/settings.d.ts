/// <reference types="react" />
import { ContentPluginConfig, InitialChildrenDef } from '@react-page/core';
import { ImageUploadType, RGBColor } from '@react-page/ui';
import { BackgroundControlsProps } from './controls';
import { ModeEnum } from './ModeEnum';
import { BackgroundRendererProps } from './renderer';
import { Translations } from './translations';
export declare type BackgroundSettings = {
    Renderer: React.ComponentType<BackgroundRendererProps>;
    Controls: React.ComponentType<BackgroundControlsProps>;
    defaultPlugin: ContentPluginConfig;
    enabledModes?: ModeEnum;
    getInitialChildren?: () => InitialChildrenDef;
    defaultBackgroundColor?: RGBColor;
    defaultGradientColor?: RGBColor;
    defaultGradientSecondaryColor?: RGBColor;
    defaultMode?: ModeEnum;
    defaultModeFlag?: ModeEnum;
    defaultDarken?: number;
    defaultLighten?: number;
    defaultHasPadding?: boolean;
    defaultIsParallax?: boolean;
    imageUpload?: ImageUploadType;
    translations?: Translations;
};
//# sourceMappingURL=settings.d.ts.map