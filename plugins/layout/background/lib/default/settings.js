"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.defaultSettings = exports.defaultTranslations = void 0;
var React = __importStar(require("react"));
var mode_1 = require("./../const/mode");
var defaultTranslations_1 = require("@react-page/ui/lib/ImageUpload/defaultTranslations");
exports.defaultTranslations = __assign(__assign({}, defaultTranslations_1.defaultTranslations), { imageMode: 'Image', colorMode: 'Color', gradientMode: 'Gradient', lighten: 'Lighten', darken: 'Darken', usePadding: 'Use Padding', onOff: 'ON/OFF', gradientRotation: 'Gradient rotation', degrees: 'deg', gradientOpacity: 'Gradient opacity', addColor: 'Add color', addGradient: 'Add gradient', pluginName: 'Background', pluginDescription: '' });
exports.defaultSettings = {
    defaultBackgroundColor: { r: 245, g: 0, b: 87, a: 1 },
    defaultGradientColor: { r: 245, g: 0, b: 87, a: 1 },
    defaultGradientSecondaryColor: { r: 71, g: 245, b: 87, a: 1 },
    defaultMode: 1,
    defaultModeFlag: 1,
    defaultDarken: 0.1,
    defaultLighten: 0,
    defaultHasPadding: true,
    defaultIsParallax: true,
    translations: exports.defaultTranslations,
    enabledModes: mode_1.IMAGE_MODE_FLAG | mode_1.COLOR_MODE_FLAG | mode_1.GRADIENT_MODE_FLAG,
    Controls: function () { return React.createElement(React.Fragment, null, " Controls for this plugin were not provided"); },
    Renderer: function () { return React.createElement(React.Fragment, null, "Renderer; for this plugin was not provided "); },
};
//# sourceMappingURL=settings.js.map