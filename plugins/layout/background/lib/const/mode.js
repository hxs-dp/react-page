"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GRADIENT_MODE_FLAG = exports.COLOR_MODE_FLAG = exports.IMAGE_MODE_FLAG = void 0;
exports.IMAGE_MODE_FLAG = 1;
exports.COLOR_MODE_FLAG = 2;
exports.GRADIENT_MODE_FLAG = 4;
//# sourceMappingURL=mode.js.map