import { BackgroundSettings } from './types/settings';
import { BackgroundState } from './types/state';
declare const createPlugin: (settings: BackgroundSettings) => Pick<import("@react-page/core").LayoutPluginProps<BackgroundState>, "Component" | "name" | "text" | "description" | "lang" | "version" | "IconComponent" | "hideInMenu" | "serialize" | "unserialize" | "handleRemoveHotKey" | "handleFocusNextHotKey" | "handleFocusPreviousHotKey" | "handleFocus" | "handleBlur" | "reducer" | "migrations" | "createInitialState" | "createInitialChildren">;
export default createPlugin;
//# sourceMappingURL=createPlugin.d.ts.map