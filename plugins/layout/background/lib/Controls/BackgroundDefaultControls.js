"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var FormControlLabel_1 = __importDefault(require("@material-ui/core/FormControlLabel"));
var Slider_1 = __importDefault(require("@material-ui/core/Slider"));
var Switch_1 = __importDefault(require("@material-ui/core/Switch"));
var Tab_1 = __importDefault(require("@material-ui/core/Tab"));
var Tabs_1 = __importDefault(require("@material-ui/core/Tabs"));
var Typography_1 = __importDefault(require("@material-ui/core/Typography"));
var ColorLens_1 = __importDefault(require("@material-ui/icons/ColorLens"));
var Gradient_1 = __importDefault(require("@material-ui/icons/Gradient"));
var Landscape_1 = __importDefault(require("@material-ui/icons/Landscape"));
var ui_1 = require("@react-page/ui");
var React = __importStar(require("react"));
var ModeEnum_1 = require("../types/ModeEnum");
var Color_1 = __importDefault(require("./sub/Color"));
var Image_1 = __importDefault(require("./sub/Image"));
var LinearGradient_1 = __importDefault(require("./sub/LinearGradient"));
var BackgroundDefaultControls = /** @class */ (function (_super) {
    __extends(BackgroundDefaultControls, _super);
    function BackgroundDefaultControls(props) {
        var _this = _super.call(this, props) || this;
        _this.renderModeSwitch = function () {
            var _a = _this.props.state.modeFlag, modeFlag = _a === void 0 ? _this.props.defaultModeFlag : _a;
            var label = _this.props.translations.onOff;
            switch (_this.state.mode) {
                case ModeEnum_1.ModeEnum.COLOR_MODE_FLAG:
                    // label = 'Use color'
                    break;
                case ModeEnum_1.ModeEnum.IMAGE_MODE_FLAG:
                    // label = 'Use image'
                    break;
                case ModeEnum_1.ModeEnum.GRADIENT_MODE_FLAG:
                    // label = 'Use gradient'
                    break;
                default:
                    label = 'Unknown mode';
                    break;
            }
            return (React.createElement(FormControlLabel_1.default, { control: React.createElement(Switch_1.default, { onChange: _this.props.handleChangeModeSwitch(_this.state.mode, modeFlag), checked: Boolean(modeFlag & _this.state.mode) }), label: label }));
        };
        _this.renderUI = function () {
            switch (_this.state.mode) {
                case ModeEnum_1.ModeEnum.COLOR_MODE_FLAG:
                    return (React.createElement(React.Fragment, null,
                        _this.renderModeSwitch(),
                        React.createElement(Color_1.default, __assign({}, _this.props, { ensureModeOn: _this.ensureModeOn(ModeEnum_1.ModeEnum.COLOR_MODE_FLAG), onChangeBackgroundColorPreview: _this.props.handleChangeBackgroundColorPreview, backgroundColorPreview: _this.props.backgroundColorPreview }))));
                case ModeEnum_1.ModeEnum.GRADIENT_MODE_FLAG:
                    return (React.createElement(React.Fragment, null,
                        _this.renderModeSwitch(),
                        React.createElement(LinearGradient_1.default, __assign({}, _this.props, { ensureModeOn: _this.ensureModeOn(ModeEnum_1.ModeEnum.GRADIENT_MODE_FLAG), gradientDegPreview: _this.props.gradientDegPreview, gradientDegPreviewIndex: _this.props.gradientDegPreviewIndex, gradientOpacityPreview: _this.props.gradientOpacityPreview, gradientOpacityPreviewIndex: _this.props.gradientOpacityPreviewIndex, gradientColorPreview: _this.props.gradientColorPreview, gradientColorPreviewIndex: _this.props.gradientColorPreviewIndex, gradientColorPreviewColorIndex: _this.props.gradientColorPreviewColorIndex, onChangeGradientDegPreview: _this.props.handleChangeGradientDegPreview, onChangeGradientOpacityPreview: _this.props.handleChangeGradientOpacityPreview, onChangeGradientColorPreview: _this.props.handleChangeGradientColorPreview }))));
                case ModeEnum_1.ModeEnum.IMAGE_MODE_FLAG:
                default:
                    return (React.createElement(React.Fragment, null,
                        _this.renderModeSwitch(),
                        React.createElement(Image_1.default, __assign({}, _this.props, { onImageLoaded: _this.props.handleImageLoaded, onImageUploaded: _this.props.handleImageUploaded, ensureModeOn: _this.ensureModeOn(ModeEnum_1.ModeEnum.IMAGE_MODE_FLAG) }))));
            }
        };
        _this.ensureModeOn = function (mode) { return function () {
            var _a = _this.props.state.modeFlag, modeFlag = _a === void 0 ? _this.props.defaultModeFlag : _a;
            if ((modeFlag & mode) === 0) {
                _this.props.handleChangeModeSwitch(mode, modeFlag)();
            }
        }; };
        _this.handleChangeMode = function (e, mode) {
            return _this.setState({ mode: mode });
        };
        _this.state = {
            mode: props.defaultMode,
        };
        return _this;
    }
    BackgroundDefaultControls.prototype.render = function () {
        var _this = this;
        var _a = this.props, focused = _a.focused, remove = _a.remove, _b = _a.state, _c = _b.hasPadding, hasPadding = _c === void 0 ? this.props.defaultHasPadding : _c, _d = _b.modeFlag, modeFlag = _d === void 0 ? this.props.defaultModeFlag : _d, _e = _b.darken, darken = _e === void 0 ? this.props.defaultDarken : _e, _f = _b.lighten, lighten = _f === void 0 ? this.props.defaultLighten : _f;
        var darkenFinal = this.props.darkenPreview !== undefined
            ? this.props.darkenPreview
            : darken;
        var lightenFinal = this.props.lightenPreview !== undefined
            ? this.props.lightenPreview
            : lighten;
        return (React.createElement(ui_1.BottomToolbar, __assign({ open: focused, title: this.props.translations.pluginName, icon: this.props.IconComponent, onDelete: remove }, this.props),
            React.createElement(Tabs_1.default, { value: this.state.mode, onChange: this.handleChangeMode, centered: true },
                (this.props.enabledModes & ModeEnum_1.ModeEnum.IMAGE_MODE_FLAG) > 0 && (React.createElement(Tab_1.default, { icon: React.createElement(Landscape_1.default, { color: (modeFlag & ModeEnum_1.ModeEnum.IMAGE_MODE_FLAG) > 0
                            ? 'secondary'
                            : undefined }), label: this.props.translations.imageMode, value: ModeEnum_1.ModeEnum.IMAGE_MODE_FLAG })),
                (this.props.enabledModes & ModeEnum_1.ModeEnum.COLOR_MODE_FLAG) > 0 && (React.createElement(Tab_1.default, { icon: React.createElement(ColorLens_1.default, { color: (modeFlag & ModeEnum_1.ModeEnum.COLOR_MODE_FLAG) > 0
                            ? 'secondary'
                            : undefined }), label: this.props.translations.colorMode, value: ModeEnum_1.ModeEnum.COLOR_MODE_FLAG })),
                (this.props.enabledModes & ModeEnum_1.ModeEnum.GRADIENT_MODE_FLAG) > 0 && (React.createElement(Tab_1.default, { icon: React.createElement(Gradient_1.default, { color: (modeFlag & ModeEnum_1.ModeEnum.GRADIENT_MODE_FLAG) > 0
                            ? 'secondary'
                            : undefined }), label: this.props.translations.gradientMode, value: ModeEnum_1.ModeEnum.GRADIENT_MODE_FLAG }))),
            this.renderUI(),
            React.createElement("br", null),
            React.createElement("div", { style: { display: 'flex' } },
                React.createElement("div", { style: { flex: '1', marginRight: '8px' } },
                    React.createElement(Typography_1.default, { variant: "body1", id: "linear-gradient-darken-label" },
                        this.props.translations.darken,
                        " (",
                        (darkenFinal * 100).toFixed(0),
                        "%)"),
                    React.createElement(Slider_1.default, { "aria-labelledby": "linear-gradient-darken-label", value: darkenFinal, onChange: function (e, value) {
                            return _this.props.handleChangeDarkenPreview(value instanceof Array ? value[0] : value);
                        }, onChangeCommitted: this.props.handleChangeDarken, step: 0.01, min: 0, max: 1 })),
                React.createElement("div", { style: { flex: '1', marginLeft: '8px' } },
                    React.createElement(Typography_1.default, { variant: "body1", id: "linear-gradient-lighten-label" },
                        this.props.translations.lighten,
                        " (",
                        (lightenFinal * 100).toFixed(0),
                        "%)"),
                    React.createElement(Slider_1.default, { "aria-labelledby": "linear-gradient-lighten-label", value: lightenFinal, onChange: function (e, value) {
                            return _this.props.handleChangeLightenPreview(value instanceof Array ? value[0] : value);
                        }, onChangeCommitted: this.props.handleChangeLighten, step: 0.01, min: 0, max: 1 }))),
            React.createElement("div", { style: { display: 'flex' } },
                React.createElement(FormControlLabel_1.default, { control: React.createElement(Switch_1.default, { onChange: this.props.handleChangeHasPadding, checked: hasPadding }), label: this.props.translations.usePadding }))));
    };
    return BackgroundDefaultControls;
}(React.Component));
exports.default = BackgroundDefaultControls;
//# sourceMappingURL=BackgroundDefaultControls.js.map