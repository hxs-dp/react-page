import { RGBColor } from '@react-page/ui';
import * as React from 'react';
import { BackgroundProps } from '../../types/component';
export interface LinearGradientComponentProps {
    ensureModeOn: () => void;
    onChangeGradientDegPreview: (value: number, index: number) => void;
    onChangeGradientOpacityPreview: (value: number, index: number) => void;
    onChangeGradientColorPreview: (color: RGBColor, index: number, cIndex: number) => void;
    gradientDegPreview: number;
    gradientDegPreviewIndex: number;
    gradientOpacityPreview: number;
    gradientOpacityPreviewIndex: number;
    gradientColorPreview: RGBColor;
    gradientColorPreviewIndex: number;
    gradientColorPreviewColorIndex: number;
}
declare class LinearGradientComponent extends React.Component<LinearGradientComponentProps & BackgroundProps> {
    addGradient: () => void;
    handleChangeDeg: (index: number, value: number) => () => void;
    handleChangeDegPreview: (index: number) => (e: React.ChangeEvent, value: number) => void;
    handleChangeOpacity: (index: number, value: number) => () => void;
    handleChangeOpacityPreview: (index: number) => (e: React.ChangeEvent, value: number) => void;
    handleChangeGradientColor: (index: number, cpIndex: number) => (e: RGBColor) => void;
    handleChangeGradientColorPreview: (index: number, cpIndex: number) => (e: RGBColor) => void;
    addColor: (index: number) => () => void;
    removeColor: (index: number, cpIndex: number) => () => void;
    removeGradient: (index: number) => () => void;
    render(): JSX.Element;
}
export default LinearGradientComponent;
//# sourceMappingURL=LinearGradient.d.ts.map