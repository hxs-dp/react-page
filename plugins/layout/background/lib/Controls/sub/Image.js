"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = __importStar(require("react"));
var Switch_1 = __importDefault(require("@material-ui/core/Switch"));
var TextField_1 = __importDefault(require("@material-ui/core/TextField"));
var FormControlLabel_1 = __importDefault(require("@material-ui/core/FormControlLabel"));
var ui_1 = require("@react-page/ui");
var Typography_1 = __importDefault(require("@material-ui/core/Typography"));
var ImageComponent = /** @class */ (function (_super) {
    __extends(ImageComponent, _super);
    function ImageComponent() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.handleChangeBackground = function (e) {
            _this.props.ensureModeOn();
            _this.props.onChange({ background: e.target.value });
        };
        _this.handleChangeIsParallax = function (e) {
            _this.props.ensureModeOn();
            _this.props.onChange({
                isParallax: _this.props.state.isParallax === undefined
                    ? false
                    : !_this.props.state.isParallax,
            });
        };
        _this.handleImageLoaded = function (image) {
            _this.props.ensureModeOn();
            _this.props.onImageLoaded(image);
        };
        _this.handleImageUploaded = function (resp) {
            _this.props.onImageUploaded();
            _this.props.onChange({ background: resp.url });
        };
        return _this;
    }
    ImageComponent.prototype.render = function () {
        var _a = this.props.state, _b = _a.isParallax, isParallax = _b === void 0 ? true : _b, _c = _a.background, background = _c === void 0 ? '' : _c;
        return (React.createElement("div", null,
            React.createElement("div", { style: { display: 'flex' } },
                this.props.imageUpload && (React.createElement(React.Fragment, null,
                    React.createElement(ui_1.ImageUpload, { imageUpload: this.props.imageUpload, imageLoaded: this.handleImageLoaded, imageUploaded: this.handleImageUploaded }),
                    React.createElement(Typography_1.default, { variant: "body1", style: { marginLeft: '20px', marginRight: '20px' } }, "OR"))),
                React.createElement(TextField_1.default, { placeholder: "http://example.com/image.png", label: this.props.imageUpload ? 'I have a URL' : 'Image URL', style: { width: '256px' }, value: background, onChange: this.handleChangeBackground })),
            React.createElement("br", null),
            React.createElement("div", { style: { display: 'flex' } },
                React.createElement(FormControlLabel_1.default, { control: React.createElement(Switch_1.default, { onChange: this.handleChangeIsParallax, checked: isParallax }), label: "Is parallax" }))));
    };
    return ImageComponent;
}(React.Component));
exports.default = ImageComponent;
//# sourceMappingURL=Image.js.map