import { Component } from 'react';
import { RGBColor } from '@react-page/ui';
import { BackgroundProps } from '../../types/component';
export interface ColorComponentProps {
    onChangeBackgroundColorPreview: (color?: RGBColor) => void;
    backgroundColorPreview: RGBColor;
    ensureModeOn: () => void;
}
declare class ColorComponent extends Component<BackgroundProps & ColorComponentProps> {
    handleChangePickerBackgroundColor: (e: RGBColor) => void;
    handleChangePickerBackgroundColorComplete: (e: RGBColor) => void;
    render(): JSX.Element;
}
export default ColorComponent;
//# sourceMappingURL=Color.d.ts.map