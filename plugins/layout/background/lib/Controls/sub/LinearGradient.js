"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var Button_1 = __importDefault(require("@material-ui/core/Button"));
var IconButton_1 = __importDefault(require("@material-ui/core/IconButton"));
var Slider_1 = __importDefault(require("@material-ui/core/Slider"));
var Typography_1 = __importDefault(require("@material-ui/core/Typography"));
var Delete_1 = __importDefault(require("@material-ui/icons/Delete"));
var ui_1 = require("@react-page/ui");
var React = __importStar(require("react"));
var LinearGradientComponent = /** @class */ (function (_super) {
    __extends(LinearGradientComponent, _super);
    function LinearGradientComponent() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.addGradient = function () {
            _this.props.ensureModeOn();
            _this.props.onChange({
                gradients: (_this.props.state.gradients
                    ? _this.props.state.gradients
                    : []).concat({
                    deg: 45,
                    opacity: 1,
                }),
            });
        };
        _this.handleChangeDeg = function (index, value) { return function () {
            _this.props.onChangeGradientDegPreview &&
                _this.props.onChangeGradientDegPreview(undefined, undefined);
            _this.props.onChange({
                gradients: (_this.props.state.gradients
                    ? _this.props.state.gradients
                    : []).map(function (g, i) { return (i === index ? __assign(__assign({}, g), { deg: value }) : g); }),
            });
        }; };
        _this.handleChangeDegPreview = function (index) { return function (e, value) {
            _this.props.onChangeGradientDegPreview &&
                _this.props.onChangeGradientDegPreview(value, index);
        }; };
        _this.handleChangeOpacity = function (index, value) { return function () {
            _this.props.onChangeGradientOpacityPreview &&
                _this.props.onChangeGradientOpacityPreview(undefined, undefined);
            _this.props.onChange({
                gradients: (_this.props.state.gradients
                    ? _this.props.state.gradients
                    : []).map(function (g, i) { return (i === index ? __assign(__assign({}, g), { opacity: value }) : g); }),
            });
        }; };
        _this.handleChangeOpacityPreview = function (index) { return function (e, value) {
            _this.props.onChangeGradientOpacityPreview &&
                _this.props.onChangeGradientOpacityPreview(value, index);
        }; };
        _this.handleChangeGradientColor = function (index, cpIndex) { return function (e) {
            _this.props.onChangeGradientColorPreview &&
                _this.props.onChangeGradientColorPreview(undefined, undefined, undefined);
            _this.props.onChange({
                gradients: []
                    .concat(_this.props.state.gradients ? _this.props.state.gradients : [])
                    .map(function (g, i) {
                    return i === index
                        ? __assign(__assign({}, g), { colors: (g.colors ? g.colors : []).map(function (c, cpI) {
                                return cpI === cpIndex ? __assign(__assign({}, c), { color: e }) : c;
                            }) }) : g;
                }),
            });
        }; };
        _this.handleChangeGradientColorPreview = function (index, cpIndex) { return function (e) {
            _this.props.onChangeGradientColorPreview &&
                _this.props.onChangeGradientColorPreview(e, index, cpIndex);
        }; };
        _this.addColor = function (index) { return function () {
            _this.props.ensureModeOn();
            _this.props.onChange({
                gradients: (_this.props.state.gradients
                    ? _this.props.state.gradients
                    : []).map(function (g, i) {
                    return i === index
                        ? __assign(__assign({}, g), { colors: (g.colors ? g.colors : []).concat({
                                color: (g.colors ? g.colors : []).length % 2 === index % 2
                                    ? _this.props.defaultGradientColor
                                    : _this.props.defaultGradientSecondaryColor,
                            }) }) : g;
                }),
            });
        }; };
        _this.removeColor = function (index, cpIndex) { return function () {
            _this.props.onChange({
                gradients: []
                    .concat(_this.props.state.gradients ? _this.props.state.gradients : [])
                    .map(function (g, i) {
                    return i === index
                        ? __assign(__assign({}, g), { colors: (g.colors ? g.colors : []).filter(function (c, cpI) { return cpI !== cpIndex; }) }) : g;
                }),
            });
        }; };
        _this.removeGradient = function (index) { return function () {
            _this.props.onChange({
                gradients: []
                    .concat(_this.props.state.gradients ? _this.props.state.gradients : [])
                    .filter(function (item, i) { return i !== index; }),
            });
        }; };
        return _this;
    }
    LinearGradientComponent.prototype.render = function () {
        var _this = this;
        var _a = this.props, gradientDegPreview = _a.gradientDegPreview, gradientDegPreviewIndex = _a.gradientDegPreviewIndex, gradientOpacityPreview = _a.gradientOpacityPreview, gradientOpacityPreviewIndex = _a.gradientOpacityPreviewIndex, gradientColorPreview = _a.gradientColorPreview, gradientColorPreviewIndex = _a.gradientColorPreviewIndex, gradientColorPreviewColorIndex = _a.gradientColorPreviewColorIndex, _b = _a.state.gradients, gradients = _b === void 0 ? [] : _b;
        return (React.createElement("div", null,
            gradients.map(function (gradient, i) {
                var colors = gradient.colors ? gradient.colors : [];
                var deg = i === gradientDegPreviewIndex && gradientDegPreview !== undefined
                    ? gradientDegPreview
                    : gradient.deg;
                var opacity = i === gradientOpacityPreviewIndex &&
                    gradientOpacityPreview !== undefined
                    ? gradientOpacityPreview
                    : gradient.opacity;
                return (React.createElement("div", { key: i, style: {
                        marginBottom: '8px',
                        borderLeft: '2px',
                        borderLeftStyle: 'solid',
                        paddingLeft: '8px',
                    } },
                    React.createElement("div", null,
                        React.createElement(Typography_1.default, { variant: "body1", id: "linear-gradient-degree-label" },
                            _this.props.translations.gradientRotation,
                            " (",
                            deg,
                            _this.props.translations.degrees,
                            ")"),
                        React.createElement(Slider_1.default, { "aria-labelledby": "linear-gradient-degree-label", value: deg, onChange: _this.handleChangeDegPreview(i), onChangeCommitted: _this.handleChangeDeg(i, deg), step: 5, min: 0, max: 360 })),
                    React.createElement("div", null,
                        React.createElement(Typography_1.default, { variant: "body1", id: "linear-gradient-opacity-label" },
                            _this.props.translations.gradientOpacity,
                            " (",
                            (opacity * 100).toFixed(0),
                            "%)"),
                        React.createElement(Slider_1.default, { "aria-labelledby": "linear-gradient-opacity-label", value: opacity, onChange: _this.handleChangeOpacityPreview(i), onChangeCommitted: _this.handleChangeOpacity(i, opacity), step: 0.01, min: 0, max: 1 })),
                    colors.map(function (c, cpIndex) {
                        var color = i === gradientColorPreviewIndex &&
                            cpIndex === gradientColorPreviewColorIndex &&
                            gradientColorPreview !== undefined
                            ? gradientColorPreview
                            : c.color;
                        return (React.createElement(React.Fragment, { key: cpIndex },
                            React.createElement(ui_1.ColorPicker, { style: { marginLeft: '8px' }, color: color, onChange: _this.handleChangeGradientColorPreview(i, cpIndex), onChangeComplete: _this.handleChangeGradientColor(i, cpIndex) }),
                            React.createElement(IconButton_1.default, { "aria-label": "Delete", onClick: _this.removeColor(i, cpIndex) },
                                React.createElement(Delete_1.default, null))));
                    }),
                    React.createElement(Button_1.default, { variant: "contained", onClick: _this.addColor(i), style: { marginLeft: '8px' } }, _this.props.translations.addColor),
                    React.createElement(IconButton_1.default, { "aria-label": "Delete", onClick: _this.removeGradient(i) },
                        React.createElement(Delete_1.default, null))));
            }),
            React.createElement("div", { style: { display: 'flex' } },
                React.createElement(Button_1.default, { style: {
                        margin: 'auto',
                    }, variant: "contained", onClick: this.addGradient, disabled: gradients.length > 5 }, this.props.translations.addGradient))));
    };
    return LinearGradientComponent;
}(React.Component));
exports.default = LinearGradientComponent;
//# sourceMappingURL=LinearGradient.js.map