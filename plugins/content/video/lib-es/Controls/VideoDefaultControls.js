var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import TextField from '@material-ui/core/TextField';
import { BottomToolbar } from '@react-page/ui';
import * as React from 'react';
import { defaultVideoState } from '../default/state';
var Form = function (props) {
    var focused = props.focused, changeSrcPreview = props.changeSrcPreview, commitSrc = props.commitSrc, remove = props.remove, _a = props.state, src = (_a === void 0 ? defaultVideoState : _a).src;
    return (React.createElement(BottomToolbar, __assign({ open: focused, title: props.translations.pluginName, icon: props.IconComponent, onDelete: remove }, props),
        React.createElement(TextField, { placeholder: props.translations.placeholder, label: props.translations.label, style: { width: '512px' }, value: src, onChange: function (e) { return changeSrcPreview(e.target.value); }, onBlur: commitSrc })));
};
export default Form;
//# sourceMappingURL=VideoDefaultControls.js.map