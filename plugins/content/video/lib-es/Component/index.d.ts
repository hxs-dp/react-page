import * as React from 'react';
import { VideoProps } from './../types/component';
export interface VideoState {
    src: string;
}
declare class Video extends React.PureComponent<VideoProps, VideoState> {
    constructor(props: VideoProps);
    render(): JSX.Element;
    private changeSrcPreview;
    private commitSrc;
}
export default Video;
//# sourceMappingURL=index.d.ts.map