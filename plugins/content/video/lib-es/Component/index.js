var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import * as React from 'react';
var Video = /** @class */ (function (_super) {
    __extends(Video, _super);
    function Video(props) {
        var _this = _super.call(this, props) || this;
        _this.state = {
            src: undefined,
        };
        _this.changeSrcPreview = _this.changeSrcPreview.bind(_this);
        _this.commitSrc = _this.commitSrc.bind(_this);
        return _this;
    }
    Video.prototype.render = function () {
        var _a = this.props, Controls = _a.Controls, readOnly = _a.readOnly, Renderer = _a.Renderer;
        return (React.createElement(React.Fragment, null,
            !readOnly ? (React.createElement(Controls, __assign({}, this.props, { state: __assign(__assign({}, this.props.state), { src: this.state.src ? this.state.src : this.props.state.src }), changeSrcPreview: this.changeSrcPreview, commitSrc: this.commitSrc }))) : null,
            React.createElement(Renderer, __assign({}, this.props))));
    };
    Video.prototype.changeSrcPreview = function (src) {
        this.setState({ src: src });
    };
    Video.prototype.commitSrc = function () {
        var _this = this;
        var src = this.state.src;
        this.setState({ src: undefined }, function () { return _this.props.onChange({ src: src }); });
    };
    return Video;
}(React.PureComponent));
export default Video;
// <div className="ory-content-plugin--video">
//# sourceMappingURL=index.js.map