import createPlugin from './createPlugin';
import { lazyLoad } from '@react-page/core';
import VideoHtmlRenderer from './Renderer/VideoHtmlRenderer';
var VideoDefaultControls = lazyLoad(function () {
    return import('./Controls/VideoDefaultControls');
});
var plugin = createPlugin({
    Renderer: VideoHtmlRenderer,
    Controls: VideoDefaultControls,
});
export default plugin;
//# sourceMappingURL=index.js.map