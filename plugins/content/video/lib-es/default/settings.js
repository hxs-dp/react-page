import * as React from 'react';
import { lazyLoad } from '@react-page/core';
var PlayArrow = lazyLoad(function () { return import('@material-ui/icons/PlayArrow'); });
export var defaultTranslations = {
    pluginName: 'Video',
    pluginDescription: 'Include videos from Vimeo or YouTube',
    label: 'Video location (YouTube / Vimeo)',
    placeholder: 'https://www.youtube.com/watch?v=ER97mPHhgtM',
};
export var defaultSettings = {
    Controls: function () { return React.createElement(React.Fragment, null, " Controls for this plugin were not provided"); },
    Renderer: function () { return React.createElement(React.Fragment, null, "Renderer; for this plugin was not provided "); },
    translations: defaultTranslations,
    IconComponent: React.createElement(PlayArrow, null),
};
//# sourceMappingURL=settings.js.map