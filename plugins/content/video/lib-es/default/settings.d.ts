import { VideoSettings } from '../types/settings';
export declare const defaultTranslations: {
    pluginName: string;
    pluginDescription: string;
    label: string;
    placeholder: string;
};
export declare const defaultSettings: VideoSettings;
//# sourceMappingURL=settings.d.ts.map