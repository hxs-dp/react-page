"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = __importStar(require("react"));
var PlayArrow_1 = __importDefault(require("@material-ui/icons/PlayArrow"));
var styles_1 = require("../common/styles");
var core_1 = require("@react-page/core");
// react player is big, better lazy load it.
var ReactPlayer = core_1.lazyLoad(function () { return Promise.resolve().then(function () { return __importStar(require('react-player')); }); });
var Display = function (_a) {
    var src = _a.state.src, readOnly = _a.readOnly;
    return src ? (React.createElement("div", { style: { position: 'relative', height: 0, paddingBottom: '65.25%' } },
        readOnly ? null : (React.createElement("div", { style: {
                position: 'absolute',
                top: 0,
                left: 0,
                right: 0,
                bottom: 0,
                zIndex: 10,
            } })),
        React.createElement(ReactPlayer, { url: src, height: "100%", width: "100%", style: {
                position: 'absolute',
                width: '100%',
                height: '100%',
            } }))) : (React.createElement("div", { className: "ory-plugins-content-video-placeholder" },
        React.createElement(PlayArrow_1.default, { style: styles_1.iconStyle })));
};
exports.default = Display;
//# sourceMappingURL=VideoHtmlRenderer.js.map