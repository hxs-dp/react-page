import { ContentPluginProps } from '@react-page/core';
import { VideoSettings } from './settings';
import { VideoState } from './state';
export declare type VideoProps = ContentPluginProps<VideoState> & VideoSettings;
//# sourceMappingURL=component.d.ts.map