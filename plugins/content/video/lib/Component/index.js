"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = __importStar(require("react"));
var Video = /** @class */ (function (_super) {
    __extends(Video, _super);
    function Video(props) {
        var _this = _super.call(this, props) || this;
        _this.state = {
            src: undefined,
        };
        _this.changeSrcPreview = _this.changeSrcPreview.bind(_this);
        _this.commitSrc = _this.commitSrc.bind(_this);
        return _this;
    }
    Video.prototype.render = function () {
        var _a = this.props, Controls = _a.Controls, readOnly = _a.readOnly, Renderer = _a.Renderer;
        return (React.createElement(React.Fragment, null,
            !readOnly ? (React.createElement(Controls, __assign({}, this.props, { state: __assign(__assign({}, this.props.state), { src: this.state.src ? this.state.src : this.props.state.src }), changeSrcPreview: this.changeSrcPreview, commitSrc: this.commitSrc }))) : null,
            React.createElement(Renderer, __assign({}, this.props))));
    };
    Video.prototype.changeSrcPreview = function (src) {
        this.setState({ src: src });
    };
    Video.prototype.commitSrc = function () {
        var _this = this;
        var src = this.state.src;
        this.setState({ src: undefined }, function () { return _this.props.onChange({ src: src }); });
    };
    return Video;
}(React.PureComponent));
exports.default = Video;
// <div className="ory-content-plugin--video">
//# sourceMappingURL=index.js.map