import { ContentPluginConfig } from '@react-page/core';
import { VideoSettings } from './types/settings';
import { VideoState } from './types/state';
declare const createPlugin: (settings: VideoSettings) => ContentPluginConfig<VideoState>;
export default createPlugin;
//# sourceMappingURL=createPlugin.d.ts.map