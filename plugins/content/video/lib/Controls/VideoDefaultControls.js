"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var TextField_1 = __importDefault(require("@material-ui/core/TextField"));
var ui_1 = require("@react-page/ui");
var React = __importStar(require("react"));
var state_1 = require("../default/state");
var Form = function (props) {
    var focused = props.focused, changeSrcPreview = props.changeSrcPreview, commitSrc = props.commitSrc, remove = props.remove, _a = props.state, src = (_a === void 0 ? state_1.defaultVideoState : _a).src;
    return (React.createElement(ui_1.BottomToolbar, __assign({ open: focused, title: props.translations.pluginName, icon: props.IconComponent, onDelete: remove }, props),
        React.createElement(TextField_1.default, { placeholder: props.translations.placeholder, label: props.translations.label, style: { width: '512px' }, value: src, onChange: function (e) { return changeSrcPreview(e.target.value); }, onBlur: commitSrc })));
};
exports.default = Form;
//# sourceMappingURL=VideoDefaultControls.js.map