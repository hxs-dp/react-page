import { SlateComponentPluginDefinition } from '../types/slatePluginDefinitions';
import { HtmlBlockData } from './createSimpleHtmlBlockPlugin';
export declare type HeadingsDef<T> = {
    level: 1 | 2 | 3 | 4 | 5 | 6;
} & Pick<SlateComponentPluginDefinition<HtmlBlockData<T>>, 'type' | 'getInitialData' | 'icon'>;
declare function createHeadingsPlugin<T = {}>(def: HeadingsDef<T>): {
    <CT = HtmlBlockData<T>>(customize?: (t: SlateComponentPluginDefinition<HtmlBlockData<T>>) => SlateComponentPluginDefinition<CT>): {
        <CT_1 = CT>(customize?: (t: SlateComponentPluginDefinition<CT>) => SlateComponentPluginDefinition<CT_1>): {
            <CT_2 = CT_1>(customize?: (t: SlateComponentPluginDefinition<CT_1>) => SlateComponentPluginDefinition<CT_2>): {
                <CT_3 = CT_2>(customize?: (t: SlateComponentPluginDefinition<CT_2>) => SlateComponentPluginDefinition<CT_3>): {
                    <CT_4 = CT_3>(customize?: (t: SlateComponentPluginDefinition<CT_3>) => SlateComponentPluginDefinition<CT_4>): {
                        <CT_5 = CT_4>(customize?: (t: SlateComponentPluginDefinition<CT_4>) => SlateComponentPluginDefinition<CT_5>): {
                            <CT_6 = CT_5>(customize?: (t: SlateComponentPluginDefinition<CT_5>) => SlateComponentPluginDefinition<CT_6>): {
                                <CT_7 = CT_6>(customize?: (t: SlateComponentPluginDefinition<CT_6>) => SlateComponentPluginDefinition<CT_7>): {
                                    <CT_8 = CT_7>(customize?: (t: SlateComponentPluginDefinition<CT_7>) => SlateComponentPluginDefinition<CT_8>): {
                                        <CT_9 = CT_8>(customize?: (t: SlateComponentPluginDefinition<CT_8>) => SlateComponentPluginDefinition<CT_9>): {
                                            <CT_10 = CT_9>(customize?: (t: SlateComponentPluginDefinition<CT_9>) => SlateComponentPluginDefinition<CT_10>): any;
                                            toPlugin(): import("../types/slatePluginDefinitions").SlatePluginDefinition<any>;
                                        };
                                        toPlugin(): import("../types/slatePluginDefinitions").SlatePluginDefinition<any>;
                                    };
                                    toPlugin(): import("../types/slatePluginDefinitions").SlatePluginDefinition<any>;
                                };
                                toPlugin(): import("../types/slatePluginDefinitions").SlatePluginDefinition<any>;
                            };
                            toPlugin(): import("../types/slatePluginDefinitions").SlatePluginDefinition<any>;
                        };
                        toPlugin(): import("../types/slatePluginDefinitions").SlatePluginDefinition<any>;
                    };
                    toPlugin(): import("../types/slatePluginDefinitions").SlatePluginDefinition<any>;
                };
                toPlugin(): import("../types/slatePluginDefinitions").SlatePluginDefinition<any>;
            };
            toPlugin(): import("../types/slatePluginDefinitions").SlatePluginDefinition<any>;
        };
        toPlugin(): import("../types/slatePluginDefinitions").SlatePluginDefinition<any>;
    };
    toPlugin(): import("../types/slatePluginDefinitions").SlatePluginDefinition<any>;
};
export default createHeadingsPlugin;
//# sourceMappingURL=createHeadingsPlugin.d.ts.map