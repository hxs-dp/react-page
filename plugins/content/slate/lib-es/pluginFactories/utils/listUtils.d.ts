import { Editor } from 'slate';
import { ReactEditor } from 'slate-react';
declare type ListBaseDef = {
    allListTypes: string[];
    listItemType: string;
};
export declare const getActiveList: (editor: Editor, allListTypes: string[]) => import("slate").NodeEntry<import("slate").Node>;
export declare const getActiveListType: (editor: Editor, allListTypes: string[]) => unknown;
export declare const getPreviousListItem: (editor: Editor, listItemType: string) => import("slate").NodeEntry<import("slate").Node>;
export declare const increaseListIndention: (editor: Editor, def: ListBaseDef, listType?: string) => void;
export declare const decreaseListIndention: (editor: ReactEditor, def: ListBaseDef) => void;
export {};
//# sourceMappingURL=listUtils.d.ts.map