/// <reference types="react" />
import { SlatePlugin } from '../types/SlatePlugin';
import { SlateComponentPluginDefinition } from '../types/slatePluginDefinitions';
import { HtmlBlockData } from './createSimpleHtmlBlockPlugin';
declare type ListDef = {
    type: string;
    icon?: JSX.Element;
    label?: string;
    hotKey?: string;
    tagName: keyof JSX.IntrinsicElements;
    noButton?: boolean;
    allListTypes: string[];
    listItem: {
        type: string;
        tagName: keyof JSX.IntrinsicElements;
    };
};
declare type ListItemDef<T> = SlateComponentPluginDefinition<HtmlBlockData<T>>;
declare type CustomizeFunction<T, CT> = (def: ListItemDef<T>) => ListItemDef<CT & T>;
declare type ListCustomizers<T, CT> = {
    customizeList?: CustomizeFunction<T, CT>;
    customizeListItem?: CustomizeFunction<T, CT>;
};
declare function createListPlugin<T = {}>(def: ListDef): {
    <CT>(customizers: ListCustomizers<T, CT>): {
        <CT_1>(customizers: ListCustomizers<unknown, CT_1>): any;
        toPlugin(): SlatePlugin[];
    };
    toPlugin(): SlatePlugin[];
};
export default createListPlugin;
//# sourceMappingURL=createListPlugin.d.ts.map