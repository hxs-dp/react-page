import { decreaseListIndention, getActiveListType, getPreviousListItem, increaseListIndention, } from './utils/listUtils';
var ceateSlatePlugin = function (def) {
    return [
        {
            pluginType: 'custom',
            addToolbarButton: true,
            addHoverButton: false,
            icon: def.iconIncrease,
            label: def.labelIncrease,
            customAdd: function (editor) {
                increaseListIndention(editor, {
                    allListTypes: def.allListTypes,
                    listItemType: def.listItemType,
                });
            },
            customRemove: function (editor) {
                decreaseListIndention(editor, {
                    allListTypes: def.allListTypes,
                    listItemType: def.listItemType,
                });
            },
            isDisabled: function (editor) {
                var previous = getPreviousListItem(editor, def.listItemType);
                return !previous;
            },
        },
        {
            pluginType: 'custom',
            addToolbarButton: true,
            addHoverButton: false,
            icon: def.iconDecrease,
            label: def.labelDecrease,
            customAdd: function (editor) {
                decreaseListIndention(editor, {
                    allListTypes: def.allListTypes,
                    listItemType: def.listItemType,
                });
            },
            customRemove: function (editor) {
                increaseListIndention(editor, {
                    allListTypes: def.allListTypes,
                    listItemType: def.listItemType,
                });
            },
            isDisabled: function (editor) {
                return !getActiveListType(editor, def.allListTypes);
            },
        },
    ];
};
function createListIndentionPlugin(def) {
    var customizablePlugin = function (customize) {
        return createListIndentionPlugin(customize(def));
    };
    customizablePlugin.toPlugin = function () { return ceateSlatePlugin(def); };
    return customizablePlugin;
}
export default createListIndentionPlugin;
//# sourceMappingURL=createListIndentionPlugin.js.map