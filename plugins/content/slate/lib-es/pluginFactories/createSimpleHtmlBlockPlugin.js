/* eslint-disable @typescript-eslint/ban-types */
import { getAlignmentFromElement } from '../plugins/paragraphs';
import createComponentPlugin from './createComponentPlugin';
function createSimpleHtmlBlockPlugin(def) {
    return createComponentPlugin({
        type: def.type,
        object: 'block',
        hotKey: def.hotKey,
        replaceWithDefaultOnRemove: def.replaceWithDefaultOnRemove,
        icon: def.icon,
        label: def.label,
        onKeyDown: def.onKeyDown,
        addToolbarButton: !def.noButton,
        customAdd: def.customAdd,
        customRemove: def.customRemove,
        schema: def.schema,
        addHoverButton: false,
        deserialize: {
            tagName: def.tagName,
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            getData: def.getData || getAlignmentFromElement,
        },
        getStyle: function (_a) {
            var align = _a.align;
            return ({ textAlign: align });
        },
        Component: def.tagName,
    });
}
export default createSimpleHtmlBlockPlugin;
//# sourceMappingURL=createSimpleHtmlBlockPlugin.js.map