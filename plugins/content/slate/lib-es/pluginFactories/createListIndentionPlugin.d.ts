/// <reference types="react" />
import { SlatePlugin } from '../types/SlatePlugin';
declare type Definition = {
    iconIncrease: JSX.Element;
    iconDecrease: JSX.Element;
    listItemType: string;
    allListTypes: string[];
    labelIncrease?: string;
    labelDecrease?: string;
};
declare function createListIndentionPlugin(def: Definition): {
    (customize: (def2: Definition) => Definition): any;
    toPlugin(): SlatePlugin[];
};
export default createListIndentionPlugin;
//# sourceMappingURL=createListIndentionPlugin.d.ts.map