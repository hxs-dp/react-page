/// <reference types="react" />
import { SlatePluginControls } from '../../types/slatePluginDefinitions';
declare function Controls<T>(props: SlatePluginControls<T>): JSX.Element;
export default Controls;
//# sourceMappingURL=UniformsControls.d.ts.map