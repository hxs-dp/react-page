/// <reference types="react" />
import { SlateComponentPluginDefinition } from '../types/slatePluginDefinitions';
declare type Def<T extends {}> = Pick<SlateComponentPluginDefinition<HtmlBlockData<T>>, 'type' | 'icon' | 'label' | 'customAdd' | 'customRemove' | 'isDisabled' | 'hotKey' | 'onKeyDown' | 'getInitialData' | 'schema'> & {
    replaceWithDefaultOnRemove?: boolean;
    tagName: keyof JSX.IntrinsicElements;
    getData?: SlateComponentPluginDefinition<HtmlBlockData<T>>['deserialize']['getData'];
    noButton?: boolean;
};
export declare type DefaultBlockDataType = {
    align: 'left' | 'right' | 'center' | 'justify';
};
export declare type HtmlBlockData<T> = T & DefaultBlockDataType;
declare function createSimpleHtmlBlockPlugin<T = {}>(def: Def<HtmlBlockData<T>>): {
    <CT = HtmlBlockData<T>>(customize?: (t: SlateComponentPluginDefinition<HtmlBlockData<T>>) => SlateComponentPluginDefinition<CT>): {
        <CT_1 = CT>(customize?: (t: SlateComponentPluginDefinition<CT>) => SlateComponentPluginDefinition<CT_1>): {
            <CT_2 = CT_1>(customize?: (t: SlateComponentPluginDefinition<CT_1>) => SlateComponentPluginDefinition<CT_2>): {
                <CT_3 = CT_2>(customize?: (t: SlateComponentPluginDefinition<CT_2>) => SlateComponentPluginDefinition<CT_3>): {
                    <CT_4 = CT_3>(customize?: (t: SlateComponentPluginDefinition<CT_3>) => SlateComponentPluginDefinition<CT_4>): {
                        <CT_5 = CT_4>(customize?: (t: SlateComponentPluginDefinition<CT_4>) => SlateComponentPluginDefinition<CT_5>): {
                            <CT_6 = CT_5>(customize?: (t: SlateComponentPluginDefinition<CT_5>) => SlateComponentPluginDefinition<CT_6>): {
                                <CT_7 = CT_6>(customize?: (t: SlateComponentPluginDefinition<CT_6>) => SlateComponentPluginDefinition<CT_7>): {
                                    <CT_8 = CT_7>(customize?: (t: SlateComponentPluginDefinition<CT_7>) => SlateComponentPluginDefinition<CT_8>): {
                                        <CT_9 = CT_8>(customize?: (t: SlateComponentPluginDefinition<CT_8>) => SlateComponentPluginDefinition<CT_9>): {
                                            <CT_10 = CT_9>(customize?: (t: SlateComponentPluginDefinition<CT_9>) => SlateComponentPluginDefinition<CT_10>): any;
                                            toPlugin(): import("../types/slatePluginDefinitions").SlatePluginDefinition<any>;
                                        };
                                        toPlugin(): import("../types/slatePluginDefinitions").SlatePluginDefinition<any>;
                                    };
                                    toPlugin(): import("../types/slatePluginDefinitions").SlatePluginDefinition<any>;
                                };
                                toPlugin(): import("../types/slatePluginDefinitions").SlatePluginDefinition<any>;
                            };
                            toPlugin(): import("../types/slatePluginDefinitions").SlatePluginDefinition<any>;
                        };
                        toPlugin(): import("../types/slatePluginDefinitions").SlatePluginDefinition<any>;
                    };
                    toPlugin(): import("../types/slatePluginDefinitions").SlatePluginDefinition<any>;
                };
                toPlugin(): import("../types/slatePluginDefinitions").SlatePluginDefinition<any>;
            };
            toPlugin(): import("../types/slatePluginDefinitions").SlatePluginDefinition<any>;
        };
        toPlugin(): import("../types/slatePluginDefinitions").SlatePluginDefinition<any>;
    };
    toPlugin(): import("../types/slatePluginDefinitions").SlatePluginDefinition<any>;
};
export default createSimpleHtmlBlockPlugin;
//# sourceMappingURL=createSimpleHtmlBlockPlugin.d.ts.map