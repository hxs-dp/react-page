import { Transforms } from 'slate';
import createListItemPlugin from './createListItemPlugin';
import createSimpleHtmlBlockPlugin from './createSimpleHtmlBlockPlugin';
import { decreaseListIndention, getActiveList, increaseListIndention, } from './utils/listUtils';
function createSlatePlugins(def, customizers) {
    if (customizers === void 0) { customizers = {}; }
    return [
        createSimpleHtmlBlockPlugin({
            type: def.type,
            icon: def.icon,
            label: def.label,
            noButton: def.noButton,
            tagName: def.tagName,
            customAdd: function (editor) {
                var currentList = getActiveList(editor, def.allListTypes);
                if (!currentList) {
                    increaseListIndention(editor, {
                        allListTypes: def.allListTypes,
                        listItemType: def.listItem.type,
                    }, def.type);
                }
                else {
                    // change type
                    Transforms.setNodes(editor, {
                        type: def.type,
                    }, {
                        at: currentList[1],
                    });
                }
            },
            customRemove: function (editor) {
                decreaseListIndention(editor, {
                    allListTypes: def.allListTypes,
                    listItemType: def.listItem.type,
                });
            },
        })(customizers.customizeList),
        createListItemPlugin(def.listItem)(customizers.customizeListItem),
    ];
}
// eslint-disable-next-line @typescript-eslint/no-explicit-any
function mergeCustomizer(c1, c2) {
    return {
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        customizeList: function (def) {
            var def2 = (c1 === null || c1 === void 0 ? void 0 : c1.customizeList) ? c1.customizeList(def) : def;
            return (c2 === null || c2 === void 0 ? void 0 : c2.customizeList) ? c2.customizeList(def2) : def2;
        },
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        customizeListItem: function (def) {
            var def2 = (c1 === null || c1 === void 0 ? void 0 : c1.customizeList) ? c1.customizeListItem(def) : def;
            return (c2 === null || c2 === void 0 ? void 0 : c2.customizeList) ? c2.customizeListItem(def2) : def2;
        },
    };
}
// eslint-disable-next-line @typescript-eslint/ban-types
function createListPlugin(def) {
    var inner = function (innerdef, customizersIn) {
        var customizablePlugin = function (customizers) {
            return inner(innerdef, mergeCustomizer(customizersIn, customizers));
        };
        customizablePlugin.toPlugin = function () {
            return createSlatePlugins(innerdef, customizersIn).map(function (plugin) {
                return plugin.toPlugin();
            });
        };
        return customizablePlugin;
    };
    return inner(def);
}
export default createListPlugin;
//# sourceMappingURL=createListPlugin.js.map