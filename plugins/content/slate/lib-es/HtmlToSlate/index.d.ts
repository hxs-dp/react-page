import { SlatePlugin } from '../types/SlatePlugin';
import { SlateState } from '../types/state';
declare const HtmlToSlate: ({ plugins }: {
    plugins: SlatePlugin[];
}) => (htmlString: string) => SlateState;
export default HtmlToSlate;
//# sourceMappingURL=index.d.ts.map