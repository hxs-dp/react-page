import { ContentPluginConfig } from '@react-page/core';
import * as React from 'react';
import { defaultTranslations } from './default/settings';
import HtmlToSlate from './HtmlToSlate';
import * as pluginFactories from './pluginFactories/index';
import * as defaultPlugins from './plugins/index';
import { SlateControlsProps } from './types/controls';
import { InitialSlateStateDef } from './types/initialSlateState';
import { SlateRendererProps } from './types/renderer';
import { SlatePluginCollection } from './types/SlatePlugin';
import { SlateState } from './types/state';
declare const slatePlugins: typeof defaultPlugins;
export { defaultPlugins, slatePlugins, pluginFactories, HtmlToSlate };
declare const migrations: import("@react-page/core").Migration[];
declare type SlateDefinition<TPlugins extends SlatePluginCollection> = {
    icon: JSX.Element;
    plugins: TPlugins;
    defaultPluginType: string;
    Renderer: React.ComponentType<SlateRendererProps>;
    Controls: React.ComponentType<SlateControlsProps>;
    name: string;
    version: string;
    translations: typeof defaultTranslations;
    migrations: typeof migrations;
    allowInlineNeighbours: boolean;
    hideInMenu?: boolean;
};
declare type DefaultPlugins = typeof defaultPlugins;
declare type DefaultSlateDefinition = SlateDefinition<DefaultPlugins>;
export declare type CreateInitialStateCustomizer<TPlugins> = ({ plugins, }: {
    plugins: TPlugins;
}) => InitialSlateStateDef;
declare type CreateInitialSlateState<TPlugins> = (custom?: CreateInitialStateCustomizer<TPlugins>) => SlateState;
export declare type SlatePlugin<TPlugins> = ContentPluginConfig<SlateState> & {
    createInitialSlateState: CreateInitialSlateState<TPlugins>;
    htmlToSlate: (html: string) => SlateState;
};
export declare type SlateCustomizeFunction<TPlugins extends SlatePluginCollection> = (def: DefaultSlateDefinition) => SlateDefinition<TPlugins>;
declare function plugin<TPlugins extends SlatePluginCollection = DefaultPlugins>(customize?: SlateCustomizeFunction<TPlugins>): SlatePlugin<TPlugins>;
export default plugin;
//# sourceMappingURL=index.d.ts.map