import { ReactEditor } from 'slate-react';
import { SlatePluginDefinition } from '../types/slatePluginDefinitions';
export declare const addPlugin: <T>(editor: ReactEditor, plugin: SlatePluginDefinition<T>, props?: {
    data?: T;
    text?: string;
}) => void;
declare const _default: <T>(plugin: SlatePluginDefinition<T>) => (props?: {
    data?: T;
    text?: string;
}) => void;
export default _default;
//# sourceMappingURL=useAddPlugin.d.ts.map