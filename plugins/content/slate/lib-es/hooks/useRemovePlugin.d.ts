import { ReactEditor } from 'slate-react';
import { SlatePluginDefinition } from '../types/slatePluginDefinitions';
export declare const removePlugin: <T>(editor: ReactEditor, plugin: SlatePluginDefinition<T>) => void;
declare const _default: <T>(plugin: SlatePluginDefinition<T>) => () => void;
export default _default;
//# sourceMappingURL=useRemovePlugin.d.ts.map