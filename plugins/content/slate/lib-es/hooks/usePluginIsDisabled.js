import { useSlate } from 'slate-react';
export default (function (plugin) {
    try {
        var editor = useSlate();
        if (!editor) {
            return true;
        }
        return plugin.isDisabled ? plugin.isDisabled(editor) : false;
    }
    catch (e) {
        // slate sometimes throws when dom node cant be found in undo
        return false;
    }
});
//# sourceMappingURL=usePluginIsDisabled.js.map