var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import { BottomToolbar } from '@react-page/ui';
import React from 'react';
import PluginButton from './PluginButton';
var Toolbar = function (props) {
    var show = props.show, removeSlate = props.removeSlate, plugins = props.plugins, translations = props.translations;
    var bottomToolbarProps = {
        open: show,
        dark: true,
        onDelete: removeSlate,
        editable: props.editable,
        name: props.name,
        id: props.id,
    };
    // useWhyDidYouUpdate('Toolbar' + props.id, props);
    return (React.createElement(BottomToolbar, __assign({}, bottomToolbarProps),
        React.createElement("div", null, plugins &&
            plugins.map(function (plugin, i) {
                return plugin.addToolbarButton ? (React.createElement(PluginButton, { key: i, translations: translations, plugin: plugin })) : null;
            }))));
};
export default React.memo(Toolbar);
//# sourceMappingURL=Toolbar.js.map