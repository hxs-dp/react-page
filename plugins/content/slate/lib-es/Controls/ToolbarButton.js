import { lazyLoad } from '@react-page/core';
import * as React from 'react';
import { ConditionalWrapper } from './ConditionalWrapper';
var IconButton = lazyLoad(function () { return import('@material-ui/core/IconButton'); });
var Tooltip = lazyLoad(function () { return import('@material-ui/core/Tooltip'); });
var ToolbarButton = function (_a) {
    var icon = _a.icon, isActive = _a.isActive, onClick = _a.onClick, _b = _a.disabled, disabled = _b === void 0 ? false : _b, _c = _a.toolTip, toolTip = _c === void 0 ? '' : _c;
    return (React.createElement(ConditionalWrapper, { condition: !disabled, wrapper: function (children) { return React.createElement(Tooltip, { title: toolTip }, children); } },
        React.createElement(IconButton, { onMouseDown: onClick, style: isActive
                ? { color: 'rgb(0, 188, 212)' }
                : disabled
                    ? { color: 'gray' }
                    : { color: 'white' }, disabled: disabled }, icon)));
};
export default React.memo(ToolbarButton);
//# sourceMappingURL=ToolbarButton.js.map