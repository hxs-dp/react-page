export var ConditionalWrapper = function (_a) {
    var condition = _a.condition, wrapper = _a.wrapper, children = _a.children;
    return condition ? wrapper(children) : children;
};
//# sourceMappingURL=ConditionalWrapper.js.map