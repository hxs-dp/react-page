var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
import { lazyLoad } from '@react-page/core';
import debounce from 'lodash.debounce';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { createEditor, Transforms } from 'slate';
import { Editable, Slate, withReact } from 'slate-react';
import withInline from '../slateEnhancer/withInline';
import withPaste from '../slateEnhancer/withPaste';
import { useOnKeyDown } from './hotkeyHooks';
import { useRenderElement, useRenderLeave } from './renderHooks';
var HoverButtons = lazyLoad(function () { return import('./HoverButtons'); });
var Toolbar = lazyLoad(function () { return import('./Toolbar'); });
var SlateEditable = React.memo(function (props) {
    var plugins = props.plugins, defaultPluginType = props.defaultPluginType, readOnly = props.readOnly, placeholder = props.placeholder;
    var renderElement = useRenderElement({ plugins: plugins, defaultPluginType: defaultPluginType }, []);
    var renderLeaf = useRenderLeave({ plugins: plugins }, []);
    var onKeyDown = readOnly ? undefined : useOnKeyDown({ plugins: plugins }, []);
    return (React.createElement(Editable, { placeholder: readOnly ? undefined : placeholder, readOnly: readOnly, renderElement: renderElement, renderLeaf: renderLeaf, onKeyDown: onKeyDown }));
});
var SlateControls = function (props) {
    var state = props.state, plugins = props.plugins, focused = props.focused, readOnly = props.readOnly, remove = props.remove, id = props.id, defaultPluginType = props.defaultPluginType;
    var editor = useMemo(function () {
        return withPaste(plugins, defaultPluginType)(withReact(withInline(plugins)(createEditor())));
    }, []);
    // useWhyDidYouUpdate('SlateControls' + id, props);
    var onChangeDebounced = useMemo(function () { return debounce(props.onChange, 200); }, [
        props.onChange,
    ]);
    var _a = __read(useState(state === null || state === void 0 ? void 0 : state.slate), 2), value = _a[0], setValue = _a[1];
    useEffect(function () {
        if (state.selection) {
            // update seleciton, if changed from outside (e.g. through undo)
            Transforms.select(editor, state.selection);
        }
        else {
            // deselect, otherwise slate might throw an eerror if cursor is now on a non existing dom node
            Transforms.deselect(editor);
        }
        setValue(state === null || state === void 0 ? void 0 : state.slate);
    }, [state === null || state === void 0 ? void 0 : state.slate, state === null || state === void 0 ? void 0 : state.selection]);
    var onChange = useCallback(function (v) {
        if (editor.selection) {
            setValue(v);
            onChangeDebounced({
                slate: v,
                selection: editor.selection,
            });
        }
    }, [onChangeDebounced]);
    var showBottomToolbar = Boolean(focused);
    return (React.createElement(Slate, { editor: editor, value: value, onChange: onChange },
        !readOnly && focused && (React.createElement(HoverButtons, { plugins: props.plugins, translations: props.translations })),
        React.createElement(SlateEditable, { placeholder: props.translations.placeholder, readOnly: readOnly, plugins: plugins, defaultPluginType: props.defaultPluginType }),
        !readOnly ? (React.createElement(Toolbar, { plugins: plugins, show: showBottomToolbar, removeSlate: remove, id: id, name: props.name, translations: props.translations, editable: props.editable })) : null));
};
export default React.memo(SlateControls);
//# sourceMappingURL=index.js.map