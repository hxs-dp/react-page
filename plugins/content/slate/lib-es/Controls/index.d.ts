import React from 'react';
import { SlateProps } from '../types/component';
declare const _default: React.MemoExoticComponent<(props: SlateProps) => JSX.Element>;
export default _default;
//# sourceMappingURL=index.d.ts.map