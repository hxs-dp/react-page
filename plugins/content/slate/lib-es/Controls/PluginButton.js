var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
/* eslint-disable @typescript-eslint/ban-types */
import React, { useCallback, useRef, useState } from 'react';
import { Range, Transforms } from 'slate';
import { useSlate } from 'slate-react';
import useAddPlugin from '../hooks/useAddPlugin';
import { getCurrentNodeDataWithPlugin } from '../hooks/useCurrentNodeDataWithPlugin';
import usePluginIsActive from '../hooks/usePluginIsActive';
import usePluginIsDisabled from '../hooks/usePluginIsDisabled';
import useRemovePlugin from '../hooks/useRemovePlugin';
import UniformsControls from '../pluginFactories/components/UniformsControls';
import ToolbarButton from './ToolbarButton';
function PluginButton(props) {
    var _a, _b, _c, _d, _e;
    var plugin = props.plugin;
    var hasControls = Boolean(plugin.Controls) || Boolean(plugin.schema);
    var _f = __read(useState(false), 2), showControls = _f[0], setShowControls = _f[1];
    var storedPropsRef = useRef();
    var shouldInsertWithText = plugin.pluginType === 'component' &&
        plugin.object === 'inline' &&
        (!((_a = storedPropsRef === null || storedPropsRef === void 0 ? void 0 : storedPropsRef.current) === null || _a === void 0 ? void 0 : _a.selection) ||
            Range.isCollapsed((_b = storedPropsRef === null || storedPropsRef === void 0 ? void 0 : storedPropsRef.current) === null || _b === void 0 ? void 0 : _b.selection)) &&
        !((_c = storedPropsRef === null || storedPropsRef === void 0 ? void 0 : storedPropsRef.current) === null || _c === void 0 ? void 0 : _c.isActive);
    var close = useCallback(function () { return setShowControls(false); }, []);
    var isActive = usePluginIsActive(plugin);
    var add = useAddPlugin(plugin);
    var remove = useRemovePlugin(plugin);
    var editor = useSlate();
    var onClick = React.useCallback(function (e) {
        e.preventDefault();
        if (hasControls || shouldInsertWithText) {
            if (!showControls) {
                // store props
                storedPropsRef.current = {
                    selection: editor.selection,
                    isActive: isActive,
                    data: getCurrentNodeDataWithPlugin(editor, plugin),
                };
            }
            setShowControls(!showControls);
        }
        else {
            if (isActive) {
                remove();
            }
            else {
                add();
            }
        }
    }, [isActive, hasControls, showControls, shouldInsertWithText]);
    var PassedControls = plugin.Controls;
    var Controls = PassedControls || UniformsControls;
    var isDisabled = usePluginIsDisabled(plugin);
    return (React.createElement(React.Fragment, null,
        React.createElement(ToolbarButton, { onClick: onClick, disabled: isDisabled, isActive: isActive, icon: plugin.icon ||
                (plugin.pluginType === 'component' && plugin.deserialize.tagName), toolTip: plugin.label }),
        hasControls || shouldInsertWithText ? (React.createElement(Controls, __assign({ schema: plugin.schema, close: close, open: showControls, add: function (p) {
                var _a;
                if ((_a = storedPropsRef === null || storedPropsRef === void 0 ? void 0 : storedPropsRef.current) === null || _a === void 0 ? void 0 : _a.selection) {
                    // restore selection before adding
                    Transforms.select(editor, storedPropsRef === null || storedPropsRef === void 0 ? void 0 : storedPropsRef.current.selection);
                }
                add(p);
            }, remove: function () {
                var _a;
                if ((_a = storedPropsRef === null || storedPropsRef === void 0 ? void 0 : storedPropsRef.current) === null || _a === void 0 ? void 0 : _a.selection) {
                    // restore selection before removing
                    Transforms.select(editor, storedPropsRef === null || storedPropsRef === void 0 ? void 0 : storedPropsRef.current.selection);
                }
                remove();
            }, isActive: (_d = storedPropsRef === null || storedPropsRef === void 0 ? void 0 : storedPropsRef.current) === null || _d === void 0 ? void 0 : _d.isActive, shouldInsertWithText: shouldInsertWithText, data: (_e = storedPropsRef === null || storedPropsRef === void 0 ? void 0 : storedPropsRef.current) === null || _e === void 0 ? void 0 : _e.data }, props))) : null));
}
export default React.memo(PluginButton);
//# sourceMappingURL=PluginButton.js.map