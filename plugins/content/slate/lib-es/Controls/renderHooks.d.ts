import React from 'react';
import { RenderElementProps, RenderLeafProps } from 'slate-react';
import { SlatePlugin } from '../types/SlatePlugin';
export declare const useRenderElement: ({ plugins, defaultPluginType, }: {
    plugins: SlatePlugin[];
    defaultPluginType: string;
}, deps: React.DependencyList) => ({ element: { type, data, children: childNodes }, children, attributes, }: RenderElementProps) => JSX.Element;
export declare const useRenderLeave: ({ plugins }: {
    plugins: SlatePlugin[];
}, deps: React.DependencyList) => ({ leaf: { text, ...leaveTypes }, attributes, children, }: RenderLeafProps) => JSX.Element;
//# sourceMappingURL=renderHooks.d.ts.map