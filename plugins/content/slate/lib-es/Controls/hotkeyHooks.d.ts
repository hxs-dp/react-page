import React from 'react';
import { SlatePlugin } from '../types/SlatePlugin';
export declare const useOnKeyDown: ({ plugins, }: {
    plugins: SlatePlugin[];
}, deps: React.DependencyList) => (event: any) => boolean;
//# sourceMappingURL=hotkeyHooks.d.ts.map