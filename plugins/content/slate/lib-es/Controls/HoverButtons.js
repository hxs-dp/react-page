import React, { useEffect, useRef } from 'react';
import { Portal } from 'react-portal';
import { useSlate } from 'slate-react';
import useTextIsSelected from '../hooks/useTextIsSelected';
import PluginButton from './PluginButton';
var HoverButtons = function (_a) {
    var plugins = _a.plugins, translations = _a.translations;
    var showHoverToolbar = useTextIsSelected();
    var toolbarRef = useRef();
    var editor = useSlate();
    useEffect(function () {
        var toolbar = toolbarRef.current;
        if (!showHoverToolbar) {
            return;
        }
        var s = window.getSelection();
        var oRange = s.getRangeAt(0); // get the text range
        var oRect = oRange.getBoundingClientRect();
        if (oRect) {
            var left = oRect.left, top_1 = oRect.top, width = oRect.width;
            toolbar.style.opacity = '1';
            toolbar.style.top = top_1 + window.scrollY - toolbar.offsetHeight + "px";
            toolbar.style.left = left + window.scrollX - toolbar.offsetWidth / 2 + width / 2 + "px";
        }
    }, [editor, showHoverToolbar]);
    return (React.createElement(Portal, null,
        React.createElement("div", { className: 'ory-plugins-content-slate-inline-toolbar ' +
                (showHoverToolbar
                    ? ''
                    : 'ory-plugins-content-slate-inline-toolbar--hidden'), style: { padding: 0 }, ref: toolbarRef }, plugins &&
            plugins.map(function (plugin, i) {
                return plugin.addHoverButton ? (React.createElement(PluginButton, { translations: translations, key: i, plugin: plugin })) : null;
            }))));
};
export default HoverButtons;
//# sourceMappingURL=HoverButtons.js.map