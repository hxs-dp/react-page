import React from 'react';
import { lazyLoad } from '@react-page/core';
import createMarkPlugin from '../../pluginFactories/createMarkPlugin';
var UnderlinedIcon = lazyLoad(function () {
    return import('@material-ui/icons/FormatUnderlined');
});
export default createMarkPlugin({
    type: 'EMPHASIZE/U',
    tagName: 'u',
    icon: React.createElement(UnderlinedIcon, null),
    label: 'Underline',
    hotKey: 'mod+u',
});
//# sourceMappingURL=underline.js.map