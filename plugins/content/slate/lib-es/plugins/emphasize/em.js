import React from 'react';
import { lazyLoad } from '@react-page/core';
import createMarkPlugin from '../../pluginFactories/createMarkPlugin';
var ItalicIcon = lazyLoad(function () { return import('@material-ui/icons/FormatItalic'); });
export default createMarkPlugin({
    type: 'EMPHASIZE/EM',
    tagName: 'em',
    icon: React.createElement(ItalicIcon, null),
    label: 'Italic',
    hotKey: 'mod+i',
});
//# sourceMappingURL=em.js.map