import em from './em';
import strong from './strong';
import underline from './underline';
export default { em: em, strong: strong, underline: underline };
//# sourceMappingURL=index.js.map