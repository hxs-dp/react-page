import { lazyLoad } from '@react-page/core';
import React from 'react';
import { createListItemPlugin } from '../../pluginFactories';
import createIndentionPlugin from '../../pluginFactories/createListIndentionPlugin';
import createListsPlugin from '../../pluginFactories/createListPlugin';
var ListIcon = lazyLoad(function () {
    return import('@material-ui/icons/FormatListBulleted');
});
var OrderedListIcon = lazyLoad(function () {
    return import('@material-ui/icons/FormatListNumbered');
});
export var UL = 'LISTS/UNORDERED-LIST';
export var OL = 'LISTS/ORDERED-LIST';
export var LI = 'LISTS/LIST-ITEM';
var IncreaseIndentIcon = lazyLoad(function () {
    return import('@material-ui/icons/FormatIndentIncrease');
});
var DecreaseIndentIcon = lazyLoad(function () {
    return import('@material-ui/icons/FormatIndentDecrease');
});
var ol = createListsPlugin({
    type: OL,
    allListTypes: [UL, OL],
    icon: React.createElement(OrderedListIcon, null),
    label: 'Ordered List',
    tagName: 'ol',
    listItem: {
        tagName: 'li',
        type: LI,
    },
});
var ul = createListsPlugin({
    type: UL,
    allListTypes: [UL, OL],
    icon: React.createElement(ListIcon, null),
    label: 'Unordered List',
    tagName: 'ul',
    listItem: {
        tagName: 'li',
        type: LI,
    },
});
// only used for easier access on createInitialSlateState
var li = createListItemPlugin({
    tagName: 'li',
    type: LI,
});
var indention = createIndentionPlugin({
    iconIncrease: React.createElement(IncreaseIndentIcon, null),
    iconDecrease: React.createElement(DecreaseIndentIcon, null),
    listItemType: LI,
    allListTypes: [UL, OL],
    labelIncrease: 'Increase Indentation',
    labelDecrease: 'Decrease Indentation',
});
export default {
    ol: ol,
    ul: ul,
    li: li,
    indention: indention,
};
//# sourceMappingURL=index.js.map