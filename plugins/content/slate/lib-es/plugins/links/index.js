import link from './link';
import anchor from './anchor';
export default {
    anchor: anchor,
    link: link,
};
//# sourceMappingURL=index.js.map