import React from 'react';
import { lazyLoad } from '@react-page/core';
import createSimpleHtmlBlockPlugin from '../pluginFactories/createSimpleHtmlBlockPlugin';
var BlockquoteIcon = lazyLoad(function () { return import('@material-ui/icons/FormatQuote'); });
export default {
    blockQuote: createSimpleHtmlBlockPlugin({
        type: 'BLOCKQUOTE/BLOCKQUOTE',
        icon: React.createElement(BlockquoteIcon, null),
        label: 'Quote',
        tagName: 'blockquote',
    }),
};
//# sourceMappingURL=quotes.js.map