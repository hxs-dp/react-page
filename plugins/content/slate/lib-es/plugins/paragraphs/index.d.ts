declare type Align = 'left' | 'right' | 'center' | 'justify';
export declare const getAlignmentFromElement: (el: HTMLElement) => {
    align: Align;
};
declare const _default: {
    paragraph: {
        <CT = {
            align: Align;
        }>(customize?: (t: import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<{
            align: Align;
        }>) => import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT>): {
            <CT_1 = CT>(customize?: (t: import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT>) => import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_1>): {
                <CT_2 = CT_1>(customize?: (t: import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_1>) => import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_2>): {
                    <CT_3 = CT_2>(customize?: (t: import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_2>) => import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_3>): {
                        <CT_4 = CT_3>(customize?: (t: import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_3>) => import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_4>): {
                            <CT_5 = CT_4>(customize?: (t: import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_4>) => import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_5>): {
                                <CT_6 = CT_5>(customize?: (t: import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_5>) => import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_6>): {
                                    <CT_7 = CT_6>(customize?: (t: import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_6>) => import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_7>): {
                                        <CT_8 = CT_7>(customize?: (t: import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_7>) => import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_8>): {
                                            <CT_9 = CT_8>(customize?: (t: import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_8>) => import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_9>): {
                                                <CT_10 = CT_9>(customize?: (t: import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_9>) => import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_10>): any;
                                                toPlugin(): import("../../types/slatePluginDefinitions").SlatePluginDefinition<any>;
                                            };
                                            toPlugin(): import("../../types/slatePluginDefinitions").SlatePluginDefinition<any>;
                                        };
                                        toPlugin(): import("../../types/slatePluginDefinitions").SlatePluginDefinition<any>;
                                    };
                                    toPlugin(): import("../../types/slatePluginDefinitions").SlatePluginDefinition<any>;
                                };
                                toPlugin(): import("../../types/slatePluginDefinitions").SlatePluginDefinition<any>;
                            };
                            toPlugin(): import("../../types/slatePluginDefinitions").SlatePluginDefinition<any>;
                        };
                        toPlugin(): import("../../types/slatePluginDefinitions").SlatePluginDefinition<any>;
                    };
                    toPlugin(): import("../../types/slatePluginDefinitions").SlatePluginDefinition<any>;
                };
                toPlugin(): import("../../types/slatePluginDefinitions").SlatePluginDefinition<any>;
            };
            toPlugin(): import("../../types/slatePluginDefinitions").SlatePluginDefinition<any>;
        };
        toPlugin(): import("../../types/slatePluginDefinitions").SlatePluginDefinition<any>;
    };
    pre: {
        <CT_11 = {
            align: Align;
        }>(customize?: (t: import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<{
            align: Align;
        }>) => import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_11>): {
            <CT_12 = CT_11>(customize?: (t: import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_11>) => import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_12>): {
                <CT_13 = CT_12>(customize?: (t: import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_12>) => import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_13>): {
                    <CT_14 = CT_13>(customize?: (t: import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_13>) => import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_14>): {
                        <CT_15 = CT_14>(customize?: (t: import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_14>) => import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_15>): {
                            <CT_16 = CT_15>(customize?: (t: import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_15>) => import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_16>): {
                                <CT_17 = CT_16>(customize?: (t: import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_16>) => import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_17>): {
                                    <CT_18 = CT_17>(customize?: (t: import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_17>) => import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_18>): {
                                        <CT_19 = CT_18>(customize?: (t: import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_18>) => import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_19>): {
                                            <CT_20 = CT_19>(customize?: (t: import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_19>) => import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_20>): {
                                                <CT_21 = CT_20>(customize?: (t: import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_20>) => import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_21>): any;
                                                toPlugin(): import("../../types/slatePluginDefinitions").SlatePluginDefinition<any>;
                                            };
                                            toPlugin(): import("../../types/slatePluginDefinitions").SlatePluginDefinition<any>;
                                        };
                                        toPlugin(): import("../../types/slatePluginDefinitions").SlatePluginDefinition<any>;
                                    };
                                    toPlugin(): import("../../types/slatePluginDefinitions").SlatePluginDefinition<any>;
                                };
                                toPlugin(): import("../../types/slatePluginDefinitions").SlatePluginDefinition<any>;
                            };
                            toPlugin(): import("../../types/slatePluginDefinitions").SlatePluginDefinition<any>;
                        };
                        toPlugin(): import("../../types/slatePluginDefinitions").SlatePluginDefinition<any>;
                    };
                    toPlugin(): import("../../types/slatePluginDefinitions").SlatePluginDefinition<any>;
                };
                toPlugin(): import("../../types/slatePluginDefinitions").SlatePluginDefinition<any>;
            };
            toPlugin(): import("../../types/slatePluginDefinitions").SlatePluginDefinition<any>;
        };
        toPlugin(): import("../../types/slatePluginDefinitions").SlatePluginDefinition<any>;
    };
};
export default _default;
//# sourceMappingURL=index.d.ts.map