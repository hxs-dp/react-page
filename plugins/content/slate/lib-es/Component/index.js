var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import * as React from 'react';
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export default React.memo(function (props) {
    var Controls = props.Controls, Renderer = props.Renderer, readOnly = props.readOnly;
    // slate controls currently contain everything
    return React.createElement(React.Fragment, null, !readOnly ? React.createElement(Controls, __assign({}, props)) : React.createElement(Renderer, __assign({}, props)));
});
//# sourceMappingURL=index.js.map