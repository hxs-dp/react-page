var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
var __spread = (this && this.__spread) || function () {
    for (var ar = [], i = 0; i < arguments.length; i++) ar = ar.concat(__read(arguments[i]));
    return ar;
};
import flattenDeep from './flattenDeep';
export default (function (plugins) {
    return Object.keys(plugins).reduce(function (acc, groupKey) {
        var group = plugins[groupKey];
        var groupPlugins = Object.keys(group).reduce(function (innerAcc, key) {
            var pluginOrFactory = plugins[groupKey][key];
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            var result = pluginOrFactory.toPlugin
                ? // eslint-disable-next-line @typescript-eslint/no-explicit-any
                    pluginOrFactory.toPlugin()
                : pluginOrFactory;
            return __spread(innerAcc, flattenDeep(result));
        }, []);
        return __spread(acc, groupPlugins);
    }, []);
});
//# sourceMappingURL=makeSlatePluginsFromDef.js.map