var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
var __spread = (this && this.__spread) || function () {
    for (var ar = [], i = 0; i < arguments.length; i++) ar = ar.concat(__read(arguments[i]));
    return ar;
};
export var getTextContents = function (nodes) {
    return nodes.reduce(function (acc, node) {
        if (node.text) {
            return __spread(acc, [node.text]);
        }
        else if (node.children) {
            // see https://github.com/ianstormtaylor/slate/issues/3769
            return __spread(acc, getTextContents(node.children));
        }
        else {
            return acc;
        }
    }, []);
};
//# sourceMappingURL=getTextContent.js.map