import { Node } from 'slate';
import { InitialSlateStateDef } from '../types/initialSlateState';
declare const _default: (def: InitialSlateStateDef) => {
    slate: Node[];
};
export default _default;
//# sourceMappingURL=transformInitialSlateState.d.ts.map