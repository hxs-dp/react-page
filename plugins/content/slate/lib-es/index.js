var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import { lazyLoad } from '@react-page/core';
import * as React from 'react';
import { ActionTypes } from 'redux-undo';
import Component from './Component';
import { defaultTranslations } from './default/settings';
import HtmlToSlate from './HtmlToSlate';
import v002 from './migrations/v002';
import v003 from './migrations/v003';
import v004 from './migrations/v004';
import * as pluginFactories from './pluginFactories/index';
import * as defaultPlugins from './plugins/index';
import Renderer from './Renderer';
import makeSlatePluginsFromDef from './utils/makeSlatePluginsFromDef';
import transformInitialSlateState from './utils/transformInitialSlateState';
var slatePlugins = defaultPlugins;
export { defaultPlugins, slatePlugins, pluginFactories, HtmlToSlate };
var Subject = lazyLoad(function () { return import('@material-ui/icons/Subject'); });
var Controls = lazyLoad(function () { return import('./Controls/'); });
var migrations = [v002, v003, v004];
var defaultConfig = {
    icon: React.createElement(Subject, null),
    plugins: defaultPlugins,
    defaultPluginType: 'PARAGRAPH/PARAGRAPH',
    Renderer: Renderer,
    Controls: Controls,
    name: 'ory/editor/core/content/slate',
    version: '0.0.4',
    translations: defaultTranslations,
    migrations: migrations,
    allowInlineNeighbours: true,
};
function plugin(customize) {
    var settings = (customize
        ? customize(defaultConfig)
        : defaultConfig);
    var createInitialState = function (customizeInitialSlateState) {
        var defaultInitialState = function (_a) {
            var cplugins = _a.plugins;
            return ({
                children: [
                    {
                        plugin: cplugins.paragraphs.paragraph,
                        children: [''],
                    },
                ],
            });
        };
        var func = customizeInitialSlateState
            ? customizeInitialSlateState
            : defaultInitialState;
        return transformInitialSlateState(func({ plugins: settings.plugins }));
    };
    // plugins should be flatten
    // NEW: to make it easier to manage and group plugins,
    // they now need to be an object of object with group and keys, see type SlatePluginCollection
    var plugins = makeSlatePluginsFromDef(settings.plugins);
    var htmlToSlate = HtmlToSlate({ plugins: plugins });
    return {
        Component: function (props) { return (React.createElement(Component, __assign({ Renderer: settings.Renderer, Controls: settings.Controls, plugins: plugins, translations: settings.translations, defaultPluginType: settings.defaultPluginType }, props))); },
        name: settings.name,
        version: settings.version,
        IconComponent: settings.icon,
        text: settings.translations.pluginName,
        description: settings.translations.pluginDescription,
        hideInMenu: settings.hideInMenu,
        allowInlineNeighbours: settings.allowInlineNeighbours,
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        reducer: function (state, action) {
            var _a, _b, _c;
            if ((action.type === ActionTypes.UNDO ||
                action.type === ActionTypes.REDO) &&
                ((_c = (_b = (_a = state === null || state === void 0 ? void 0 : state.content) === null || _a === void 0 ? void 0 : _a.state) === null || _b === void 0 ? void 0 : _b.slate) !== null && _c !== void 0 ? _c : false)) {
                return __assign(__assign({}, state), { content: __assign(__assign({}, state.content), { state: __assign({}, state.content.state) }) });
            }
            return state;
        },
        handleRemoveHotKey: function () { return Promise.reject(); },
        handleFocusPreviousHotKey: function () { return Promise.reject(); },
        handleFocusNextHotKey: function () { return Promise.reject(); },
        createInitialState: createInitialState,
        createInitialSlateState: createInitialState,
        htmlToSlate: htmlToSlate,
        serialize: function (s) { return (s ? { slate: s.slate } : null); },
        unserialize: function (s) {
            if (s === null || s === void 0 ? void 0 : s.importFromHtml) {
                return htmlToSlate(s.importFromHtml);
            }
            if (s === null || s === void 0 ? void 0 : s.slate) {
                return {
                    slate: s.slate,
                };
            }
            return createInitialState();
        },
        // TODO this is disabled because of #207
        // merge = hooks.merge
        // split = hooks.split
        migrations: settings.migrations,
    };
}
export default plugin;
//# sourceMappingURL=index.js.map