import { SlatePluginDefinition } from './slatePluginDefinitions';
export declare type SlatePlugin = SlatePluginDefinition<any>;
export declare type SlatePluginOrListOfPlugins = SlatePlugin | SlatePlugin[];
export declare type SlatePluginOrFactory = {
    toPlugin: () => SlatePluginOrListOfPlugins;
} | SlatePluginOrListOfPlugins;
export declare type SlatePluginCollection = {
    [group: string]: {
        [key: string]: SlatePluginOrFactory;
    };
};
//# sourceMappingURL=SlatePlugin.d.ts.map