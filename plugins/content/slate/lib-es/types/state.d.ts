import { Node, Range } from 'slate';
export declare type SlateState = {
    slate: Node[];
    selection?: Range;
};
//# sourceMappingURL=state.d.ts.map