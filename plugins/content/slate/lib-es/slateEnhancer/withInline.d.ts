import { Editor } from 'slate';
import { SlatePlugin } from '../types/SlatePlugin';
declare const withInline: (plugins: SlatePlugin[]) => (editor: Editor) => Editor;
export default withInline;
//# sourceMappingURL=withInline.d.ts.map