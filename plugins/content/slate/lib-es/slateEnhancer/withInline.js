var withInline = function (plugins) { return function (editor) {
    var isInline = editor.isInline;
    editor.isInline = function (element) {
        return plugins.some(function (plugin) {
            return plugin.pluginType === 'component' &&
                plugin.object === 'inline' &&
                plugin.type === element.type;
        })
            ? true
            : isInline(element);
    };
    return editor;
}; };
export default withInline;
//# sourceMappingURL=withInline.js.map