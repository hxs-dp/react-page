import { Transforms } from 'slate';
import HtmlToSlate from '../HtmlToSlate';
var withPaste = function (plugins, defaultPluginType) { return function (editor) {
    var insertData = editor.insertData;
    var htmlToSlate = HtmlToSlate({ plugins: plugins });
    editor.insertData = function (data) {
        var slateData = data.getData('application/x-slate-fragment');
        if (slateData) {
            insertData(data);
            return;
        }
        var html = data.getData('text/html');
        if (html) {
            var slate = htmlToSlate(html).slate;
            Transforms.insertFragment(editor, slate);
            return;
        }
        var text = data.getData('text/plain');
        if (text) {
            // if there are two subsequent line breks, insert paragraph, otherway insert soft line break
            var lines = text.split('\n');
            var nextWillbeParagraph = false;
            for (var i = 0; i < lines.length; i++) {
                var thisLine = lines[i];
                var nextLine = lines[i + 1];
                // add a \n, unless the next line is empty, then its either the last entry or the following wil be a paragraph
                var nextIsEmpty = !nextLine || !nextLine.trim();
                var thisLineText = thisLine + (nextIsEmpty ? '' : '\n');
                if (!thisLine.trim()) {
                    // this line is empty,
                    nextWillbeParagraph = true;
                }
                else if (nextWillbeParagraph) {
                    Transforms.insertNodes(editor, {
                        type: defaultPluginType,
                        children: [{ text: thisLineText }],
                    });
                    nextWillbeParagraph = false;
                }
                else {
                    Transforms.insertText(editor, thisLineText);
                    nextWillbeParagraph = false;
                }
            }
            return;
        }
        insertData(data);
    };
    return editor;
}; };
export default withPaste;
//# sourceMappingURL=withPaste.js.map