import { SlatePluginOrFactory } from './SlatePlugin';
export declare type SlatePluginNode = {
    plugin: SlatePluginOrFactory;
    children?: SlateDefNode[];
    data?: object;
};
export declare type SlateDefNode = SlatePluginNode | string;
export declare type InitialSlateStateDef = {
    children: SlateDefNode[];
};
//# sourceMappingURL=initialSlateState.d.ts.map