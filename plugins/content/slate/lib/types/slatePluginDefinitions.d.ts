/// <reference types="react" />
import { JsonSchema } from '@react-page/create-plugin-materialui';
import { ReactEditor } from 'slate-react';
import { NextType } from '../types/next';
import { Translations } from './translations';
import { Node } from 'slate';
export interface PluginButtonProps {
    translations?: Partial<Translations>;
}
export declare type SlatePluginControls<T extends {}> = {
    open: boolean;
    close: () => void;
    isActive: boolean;
    cancelLabel?: string;
    submitLabel?: string;
    removeLabel?: string;
    schema?: JsonSchema<T>;
    data: T;
    add: (p: {
        data?: T;
        text?: string;
    }) => void;
    remove: () => void;
    shouldInsertWithText: boolean;
    getInitialData?: () => T;
} & PluginButtonProps;
export declare type SlateBasePluginDefinition<T extends {}> = {
    hotKey?: string;
    onKeyDown?: (e: React.KeyboardEvent, editor: ReactEditor, next: NextType) => void;
    schema?: JsonSchema<T>;
    Controls?: React.ComponentType<SlatePluginControls<T>>;
    icon?: JSX.Element;
    label?: string;
    addHoverButton: boolean;
    addToolbarButton: boolean;
    customAdd?: (editor: ReactEditor) => void;
    customRemove?: (editor: ReactEditor) => void;
    isDisabled?: (editor: ReactEditor) => boolean;
    getInitialData?: () => T;
};
export declare type SlateNodeBasePluginDefinition<T extends {}> = {
    object: SlateNodeObjectType;
} & SlateBasePluginDefinition<T>;
export declare type SlateNodeObjectType = 'inline' | 'block' | 'mark';
export declare type SlateDataPluginDefinition<T extends {}> = SlateNodeBasePluginDefinition<T> & {
    dataMatches: (data: T) => boolean;
    /**
     * if defined these properties will be removed from data when plugin gets disabled
     */
    properties?: Array<keyof T>;
};
export declare type SlateCustomPluginDefinition<T extends {}> = SlateBasePluginDefinition<T> & {};
export declare type MapLike<T extends {}> = {
    get<K extends keyof T>(key: K): T[K];
};
declare type ObjectProps = {
    object: 'block';
    replaceWithDefaultOnRemove?: boolean;
};
declare type InlineProps = {
    object: 'inline';
    addExtraSpace?: boolean;
};
declare type MarkProps = {
    object: 'mark';
};
declare type NoInfer<T> = [T][T extends any ? 0 : never];
export declare type SlateComponentPluginDefinition<T extends {}> = SlateNodeBasePluginDefinition<T> & {
    type: string;
    getStyle?: (s: T) => React.CSSProperties;
    deserialize?: {
        tagName: string;
        getData?: (el: HTMLElement) => T;
    };
    Component: keyof JSX.IntrinsicElements | React.ComponentType<NoInfer<{
        attributes?: object;
        style?: React.CSSProperties;
        className?: string;
        childNodes: Node[];
        getTextContents: () => string[];
        useFocused: () => boolean;
        useSelected: () => boolean;
    } & T>>;
} & (ObjectProps | InlineProps | MarkProps);
export declare type SlatePluginDefinition<T extends {
    [key: string]: any;
}> = (SlateComponentPluginDefinition<T> & {
    pluginType: 'component';
}) | (SlateDataPluginDefinition<T> & {
    pluginType: 'data';
}) | (SlateCustomPluginDefinition<T> & {
    pluginType: 'custom';
});
export {};
//# sourceMappingURL=slatePluginDefinitions.d.ts.map