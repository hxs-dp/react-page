import { ReactEditor } from 'slate-react';
import { SlatePlugin } from '../types/SlatePlugin';
declare const withPaste: (plugins: SlatePlugin[], defaultPluginType: string) => (editor: ReactEditor) => ReactEditor;
export default withPaste;
//# sourceMappingURL=withPaste.d.ts.map