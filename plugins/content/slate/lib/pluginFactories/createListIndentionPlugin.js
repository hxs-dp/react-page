"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var listUtils_1 = require("./utils/listUtils");
var ceateSlatePlugin = function (def) {
    return [
        {
            pluginType: 'custom',
            addToolbarButton: true,
            addHoverButton: false,
            icon: def.iconIncrease,
            label: def.labelIncrease,
            customAdd: function (editor) {
                listUtils_1.increaseListIndention(editor, {
                    allListTypes: def.allListTypes,
                    listItemType: def.listItemType,
                });
            },
            customRemove: function (editor) {
                listUtils_1.decreaseListIndention(editor, {
                    allListTypes: def.allListTypes,
                    listItemType: def.listItemType,
                });
            },
            isDisabled: function (editor) {
                var previous = listUtils_1.getPreviousListItem(editor, def.listItemType);
                return !previous;
            },
        },
        {
            pluginType: 'custom',
            addToolbarButton: true,
            addHoverButton: false,
            icon: def.iconDecrease,
            label: def.labelDecrease,
            customAdd: function (editor) {
                listUtils_1.decreaseListIndention(editor, {
                    allListTypes: def.allListTypes,
                    listItemType: def.listItemType,
                });
            },
            customRemove: function (editor) {
                listUtils_1.increaseListIndention(editor, {
                    allListTypes: def.allListTypes,
                    listItemType: def.listItemType,
                });
            },
            isDisabled: function (editor) {
                return !listUtils_1.getActiveListType(editor, def.allListTypes);
            },
        },
    ];
};
function createListIndentionPlugin(def) {
    var customizablePlugin = function (customize) {
        return createListIndentionPlugin(customize(def));
    };
    customizablePlugin.toPlugin = function () { return ceateSlatePlugin(def); };
    return customizablePlugin;
}
exports.default = createListIndentionPlugin;
//# sourceMappingURL=createListIndentionPlugin.js.map