"use strict";
/* eslint-disable @typescript-eslint/ban-types */
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var paragraphs_1 = require("../plugins/paragraphs");
var createComponentPlugin_1 = __importDefault(require("./createComponentPlugin"));
function createSimpleHtmlBlockPlugin(def) {
    return createComponentPlugin_1.default({
        type: def.type,
        object: 'block',
        hotKey: def.hotKey,
        replaceWithDefaultOnRemove: def.replaceWithDefaultOnRemove,
        icon: def.icon,
        label: def.label,
        onKeyDown: def.onKeyDown,
        addToolbarButton: !def.noButton,
        customAdd: def.customAdd,
        customRemove: def.customRemove,
        schema: def.schema,
        addHoverButton: false,
        deserialize: {
            tagName: def.tagName,
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            getData: def.getData || paragraphs_1.getAlignmentFromElement,
        },
        getStyle: function (_a) {
            var align = _a.align;
            return ({ textAlign: align });
        },
        Component: def.tagName,
    });
}
exports.default = createSimpleHtmlBlockPlugin;
//# sourceMappingURL=createSimpleHtmlBlockPlugin.js.map