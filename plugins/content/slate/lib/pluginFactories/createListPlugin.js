"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var slate_1 = require("slate");
var createListItemPlugin_1 = __importDefault(require("./createListItemPlugin"));
var createSimpleHtmlBlockPlugin_1 = __importDefault(require("./createSimpleHtmlBlockPlugin"));
var listUtils_1 = require("./utils/listUtils");
function createSlatePlugins(def, customizers) {
    if (customizers === void 0) { customizers = {}; }
    return [
        createSimpleHtmlBlockPlugin_1.default({
            type: def.type,
            icon: def.icon,
            label: def.label,
            noButton: def.noButton,
            tagName: def.tagName,
            customAdd: function (editor) {
                var currentList = listUtils_1.getActiveList(editor, def.allListTypes);
                if (!currentList) {
                    listUtils_1.increaseListIndention(editor, {
                        allListTypes: def.allListTypes,
                        listItemType: def.listItem.type,
                    }, def.type);
                }
                else {
                    // change type
                    slate_1.Transforms.setNodes(editor, {
                        type: def.type,
                    }, {
                        at: currentList[1],
                    });
                }
            },
            customRemove: function (editor) {
                listUtils_1.decreaseListIndention(editor, {
                    allListTypes: def.allListTypes,
                    listItemType: def.listItem.type,
                });
            },
        })(customizers.customizeList),
        createListItemPlugin_1.default(def.listItem)(customizers.customizeListItem),
    ];
}
// eslint-disable-next-line @typescript-eslint/no-explicit-any
function mergeCustomizer(c1, c2) {
    return {
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        customizeList: function (def) {
            var def2 = (c1 === null || c1 === void 0 ? void 0 : c1.customizeList) ? c1.customizeList(def) : def;
            return (c2 === null || c2 === void 0 ? void 0 : c2.customizeList) ? c2.customizeList(def2) : def2;
        },
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        customizeListItem: function (def) {
            var def2 = (c1 === null || c1 === void 0 ? void 0 : c1.customizeList) ? c1.customizeListItem(def) : def;
            return (c2 === null || c2 === void 0 ? void 0 : c2.customizeList) ? c2.customizeListItem(def2) : def2;
        },
    };
}
// eslint-disable-next-line @typescript-eslint/ban-types
function createListPlugin(def) {
    var inner = function (innerdef, customizersIn) {
        var customizablePlugin = function (customizers) {
            return inner(innerdef, mergeCustomizer(customizersIn, customizers));
        };
        customizablePlugin.toPlugin = function () {
            return createSlatePlugins(innerdef, customizersIn).map(function (plugin) {
                return plugin.toPlugin();
            });
        };
        return customizablePlugin;
    };
    return inner(def);
}
exports.default = createListPlugin;
//# sourceMappingURL=createListPlugin.js.map