import React from 'react';
import { SlateProps } from '../types/component';
declare const _default: React.MemoExoticComponent<(props: Pick<SlateProps, "plugins" | "translations" | "id" | "name" | "editable"> & {
    show: boolean;
    removeSlate: () => void;
}) => JSX.Element>;
export default _default;
//# sourceMappingURL=Toolbar.d.ts.map