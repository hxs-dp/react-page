"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@react-page/core");
var lodash_debounce_1 = __importDefault(require("lodash.debounce"));
var react_1 = __importStar(require("react"));
var slate_1 = require("slate");
var slate_react_1 = require("slate-react");
var withInline_1 = __importDefault(require("../slateEnhancer/withInline"));
var withPaste_1 = __importDefault(require("../slateEnhancer/withPaste"));
var hotkeyHooks_1 = require("./hotkeyHooks");
var renderHooks_1 = require("./renderHooks");
var HoverButtons = core_1.lazyLoad(function () { return Promise.resolve().then(function () { return __importStar(require('./HoverButtons')); }); });
var Toolbar = core_1.lazyLoad(function () { return Promise.resolve().then(function () { return __importStar(require('./Toolbar')); }); });
var SlateEditable = react_1.default.memo(function (props) {
    var plugins = props.plugins, defaultPluginType = props.defaultPluginType, readOnly = props.readOnly, placeholder = props.placeholder;
    var renderElement = renderHooks_1.useRenderElement({ plugins: plugins, defaultPluginType: defaultPluginType }, []);
    var renderLeaf = renderHooks_1.useRenderLeave({ plugins: plugins }, []);
    var onKeyDown = readOnly ? undefined : hotkeyHooks_1.useOnKeyDown({ plugins: plugins }, []);
    return (react_1.default.createElement(slate_react_1.Editable, { placeholder: readOnly ? undefined : placeholder, readOnly: readOnly, renderElement: renderElement, renderLeaf: renderLeaf, onKeyDown: onKeyDown }));
});
var SlateControls = function (props) {
    var state = props.state, plugins = props.plugins, focused = props.focused, readOnly = props.readOnly, remove = props.remove, id = props.id, defaultPluginType = props.defaultPluginType;
    var editor = react_1.useMemo(function () {
        return withPaste_1.default(plugins, defaultPluginType)(slate_react_1.withReact(withInline_1.default(plugins)(slate_1.createEditor())));
    }, []);
    // useWhyDidYouUpdate('SlateControls' + id, props);
    var onChangeDebounced = react_1.useMemo(function () { return lodash_debounce_1.default(props.onChange, 200); }, [
        props.onChange,
    ]);
    var _a = __read(react_1.useState(state === null || state === void 0 ? void 0 : state.slate), 2), value = _a[0], setValue = _a[1];
    react_1.useEffect(function () {
        if (state.selection) {
            // update seleciton, if changed from outside (e.g. through undo)
            slate_1.Transforms.select(editor, state.selection);
        }
        else {
            // deselect, otherwise slate might throw an eerror if cursor is now on a non existing dom node
            slate_1.Transforms.deselect(editor);
        }
        setValue(state === null || state === void 0 ? void 0 : state.slate);
    }, [state === null || state === void 0 ? void 0 : state.slate, state === null || state === void 0 ? void 0 : state.selection]);
    var onChange = react_1.useCallback(function (v) {
        if (editor.selection) {
            setValue(v);
            onChangeDebounced({
                slate: v,
                selection: editor.selection,
            });
        }
    }, [onChangeDebounced]);
    var showBottomToolbar = Boolean(focused);
    return (react_1.default.createElement(slate_react_1.Slate, { editor: editor, value: value, onChange: onChange },
        !readOnly && focused && (react_1.default.createElement(HoverButtons, { plugins: props.plugins, translations: props.translations })),
        react_1.default.createElement(SlateEditable, { placeholder: props.translations.placeholder, readOnly: readOnly, plugins: plugins, defaultPluginType: props.defaultPluginType }),
        !readOnly ? (react_1.default.createElement(Toolbar, { plugins: plugins, show: showBottomToolbar, removeSlate: remove, id: id, name: props.name, translations: props.translations, editable: props.editable })) : null));
};
exports.default = react_1.default.memo(SlateControls);
//# sourceMappingURL=index.js.map