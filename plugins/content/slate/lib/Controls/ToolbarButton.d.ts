import * as React from 'react';
declare const _default: React.NamedExoticComponent<{
    icon: string | JSX.Element;
    isActive: boolean;
    disabled?: boolean;
    onClick: (event: React.MouseEvent<Element, MouseEvent>) => void;
    toolTip?: string;
}>;
export default _default;
//# sourceMappingURL=ToolbarButton.d.ts.map