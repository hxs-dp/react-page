"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var ui_1 = require("@react-page/ui");
var react_1 = __importDefault(require("react"));
var PluginButton_1 = __importDefault(require("./PluginButton"));
var Toolbar = function (props) {
    var show = props.show, removeSlate = props.removeSlate, plugins = props.plugins, translations = props.translations;
    var bottomToolbarProps = {
        open: show,
        dark: true,
        onDelete: removeSlate,
        editable: props.editable,
        name: props.name,
        id: props.id,
    };
    // useWhyDidYouUpdate('Toolbar' + props.id, props);
    return (react_1.default.createElement(ui_1.BottomToolbar, __assign({}, bottomToolbarProps),
        react_1.default.createElement("div", null, plugins &&
            plugins.map(function (plugin, i) {
                return plugin.addToolbarButton ? (react_1.default.createElement(PluginButton_1.default, { key: i, translations: translations, plugin: plugin })) : null;
            }))));
};
exports.default = react_1.default.memo(Toolbar);
//# sourceMappingURL=Toolbar.js.map