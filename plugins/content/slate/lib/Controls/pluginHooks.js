"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.useComponentMarkPlugins = exports.useComponentNodePlugins = void 0;
var react_1 = require("react");
exports.useComponentNodePlugins = function (_a, deps) {
    var plugins = _a.plugins;
    return react_1.useMemo(function () {
        return plugins.filter(function (plugin) {
            return plugin.pluginType === 'component' && plugin.object !== 'mark';
        }
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        );
    }, deps);
};
exports.useComponentMarkPlugins = function (_a, deps) {
    var plugins = _a.plugins;
    return react_1.useMemo(function () {
        return plugins.filter(function (plugin) {
            return plugin.pluginType === 'component' && plugin.object === 'mark';
        }
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        );
    }, deps);
};
//# sourceMappingURL=pluginHooks.js.map