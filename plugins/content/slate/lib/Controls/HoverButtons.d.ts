/// <reference types="react" />
import { SlateProps } from '../types/component';
declare const HoverButtons: ({ plugins, translations, }: Pick<SlateProps, 'plugins' | 'translations'>) => JSX.Element;
export default HoverButtons;
//# sourceMappingURL=HoverButtons.d.ts.map