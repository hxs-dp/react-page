"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
/* eslint-disable @typescript-eslint/ban-types */
var react_1 = __importStar(require("react"));
var slate_1 = require("slate");
var slate_react_1 = require("slate-react");
var useAddPlugin_1 = __importDefault(require("../hooks/useAddPlugin"));
var useCurrentNodeDataWithPlugin_1 = require("../hooks/useCurrentNodeDataWithPlugin");
var usePluginIsActive_1 = __importDefault(require("../hooks/usePluginIsActive"));
var usePluginIsDisabled_1 = __importDefault(require("../hooks/usePluginIsDisabled"));
var useRemovePlugin_1 = __importDefault(require("../hooks/useRemovePlugin"));
var UniformsControls_1 = __importDefault(require("../pluginFactories/components/UniformsControls"));
var ToolbarButton_1 = __importDefault(require("./ToolbarButton"));
function PluginButton(props) {
    var _a, _b, _c, _d, _e;
    var plugin = props.plugin;
    var hasControls = Boolean(plugin.Controls) || Boolean(plugin.schema);
    var _f = __read(react_1.useState(false), 2), showControls = _f[0], setShowControls = _f[1];
    var storedPropsRef = react_1.useRef();
    var shouldInsertWithText = plugin.pluginType === 'component' &&
        plugin.object === 'inline' &&
        (!((_a = storedPropsRef === null || storedPropsRef === void 0 ? void 0 : storedPropsRef.current) === null || _a === void 0 ? void 0 : _a.selection) ||
            slate_1.Range.isCollapsed((_b = storedPropsRef === null || storedPropsRef === void 0 ? void 0 : storedPropsRef.current) === null || _b === void 0 ? void 0 : _b.selection)) &&
        !((_c = storedPropsRef === null || storedPropsRef === void 0 ? void 0 : storedPropsRef.current) === null || _c === void 0 ? void 0 : _c.isActive);
    var close = react_1.useCallback(function () { return setShowControls(false); }, []);
    var isActive = usePluginIsActive_1.default(plugin);
    var add = useAddPlugin_1.default(plugin);
    var remove = useRemovePlugin_1.default(plugin);
    var editor = slate_react_1.useSlate();
    var onClick = react_1.default.useCallback(function (e) {
        e.preventDefault();
        if (hasControls || shouldInsertWithText) {
            if (!showControls) {
                // store props
                storedPropsRef.current = {
                    selection: editor.selection,
                    isActive: isActive,
                    data: useCurrentNodeDataWithPlugin_1.getCurrentNodeDataWithPlugin(editor, plugin),
                };
            }
            setShowControls(!showControls);
        }
        else {
            if (isActive) {
                remove();
            }
            else {
                add();
            }
        }
    }, [isActive, hasControls, showControls, shouldInsertWithText]);
    var PassedControls = plugin.Controls;
    var Controls = PassedControls || UniformsControls_1.default;
    var isDisabled = usePluginIsDisabled_1.default(plugin);
    return (react_1.default.createElement(react_1.default.Fragment, null,
        react_1.default.createElement(ToolbarButton_1.default, { onClick: onClick, disabled: isDisabled, isActive: isActive, icon: plugin.icon ||
                (plugin.pluginType === 'component' && plugin.deserialize.tagName), toolTip: plugin.label }),
        hasControls || shouldInsertWithText ? (react_1.default.createElement(Controls, __assign({ schema: plugin.schema, close: close, open: showControls, add: function (p) {
                var _a;
                if ((_a = storedPropsRef === null || storedPropsRef === void 0 ? void 0 : storedPropsRef.current) === null || _a === void 0 ? void 0 : _a.selection) {
                    // restore selection before adding
                    slate_1.Transforms.select(editor, storedPropsRef === null || storedPropsRef === void 0 ? void 0 : storedPropsRef.current.selection);
                }
                add(p);
            }, remove: function () {
                var _a;
                if ((_a = storedPropsRef === null || storedPropsRef === void 0 ? void 0 : storedPropsRef.current) === null || _a === void 0 ? void 0 : _a.selection) {
                    // restore selection before removing
                    slate_1.Transforms.select(editor, storedPropsRef === null || storedPropsRef === void 0 ? void 0 : storedPropsRef.current.selection);
                }
                remove();
            }, isActive: (_d = storedPropsRef === null || storedPropsRef === void 0 ? void 0 : storedPropsRef.current) === null || _d === void 0 ? void 0 : _d.isActive, shouldInsertWithText: shouldInsertWithText, data: (_e = storedPropsRef === null || storedPropsRef === void 0 ? void 0 : storedPropsRef.current) === null || _e === void 0 ? void 0 : _e.data }, props))) : null));
}
exports.default = react_1.default.memo(PluginButton);
//# sourceMappingURL=PluginButton.js.map