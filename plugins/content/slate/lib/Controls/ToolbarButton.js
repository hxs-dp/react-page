"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@react-page/core");
var React = __importStar(require("react"));
var ConditionalWrapper_1 = require("./ConditionalWrapper");
var IconButton = core_1.lazyLoad(function () { return Promise.resolve().then(function () { return __importStar(require('@material-ui/core/IconButton')); }); });
var Tooltip = core_1.lazyLoad(function () { return Promise.resolve().then(function () { return __importStar(require('@material-ui/core/Tooltip')); }); });
var ToolbarButton = function (_a) {
    var icon = _a.icon, isActive = _a.isActive, onClick = _a.onClick, _b = _a.disabled, disabled = _b === void 0 ? false : _b, _c = _a.toolTip, toolTip = _c === void 0 ? '' : _c;
    return (React.createElement(ConditionalWrapper_1.ConditionalWrapper, { condition: !disabled, wrapper: function (children) { return React.createElement(Tooltip, { title: toolTip }, children); } },
        React.createElement(IconButton, { onMouseDown: onClick, style: isActive
                ? { color: 'rgb(0, 188, 212)' }
                : disabled
                    ? { color: 'gray' }
                    : { color: 'white' }, disabled: disabled }, icon)));
};
exports.default = React.memo(ToolbarButton);
//# sourceMappingURL=ToolbarButton.js.map