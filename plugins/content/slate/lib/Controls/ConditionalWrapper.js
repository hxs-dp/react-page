"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConditionalWrapper = void 0;
exports.ConditionalWrapper = function (_a) {
    var condition = _a.condition, wrapper = _a.wrapper, children = _a.children;
    return condition ? wrapper(children) : children;
};
//# sourceMappingURL=ConditionalWrapper.js.map