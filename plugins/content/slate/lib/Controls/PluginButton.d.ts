import React from 'react';
import { PluginButtonProps, SlatePluginDefinition } from '../types/slatePluginDefinitions';
declare type Props<T extends {}> = {
    plugin: SlatePluginDefinition<T>;
} & PluginButtonProps;
declare function PluginButton<T>(props: Props<T>): JSX.Element;
declare const _default: React.MemoExoticComponent<typeof PluginButton>;
export default _default;
//# sourceMappingURL=PluginButton.d.ts.map