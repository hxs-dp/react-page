/// <reference types="react" />
export declare const UL = "LISTS/UNORDERED-LIST";
export declare const OL = "LISTS/ORDERED-LIST";
export declare const LI = "LISTS/LIST-ITEM";
declare const _default: {
    ol: {
        <CT>(customizers: {
            customizeList?: (def: import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<import("../../pluginFactories/createSimpleHtmlBlockPlugin").DefaultBlockDataType>) => import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<import("../../pluginFactories/createSimpleHtmlBlockPlugin").HtmlBlockData<CT & {}>>;
            customizeListItem?: (def: import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<import("../../pluginFactories/createSimpleHtmlBlockPlugin").DefaultBlockDataType>) => import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<import("../../pluginFactories/createSimpleHtmlBlockPlugin").HtmlBlockData<CT & {}>>;
        }): {
            <CT_1>(customizers: {
                customizeList?: (def: import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<import("../../pluginFactories/createSimpleHtmlBlockPlugin").DefaultBlockDataType>) => import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<import("../../pluginFactories/createSimpleHtmlBlockPlugin").HtmlBlockData<CT_1>>;
                customizeListItem?: (def: import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<import("../../pluginFactories/createSimpleHtmlBlockPlugin").DefaultBlockDataType>) => import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<import("../../pluginFactories/createSimpleHtmlBlockPlugin").HtmlBlockData<CT_1>>;
            }): any;
            toPlugin(): import("../../types/slatePluginDefinitions").SlatePluginDefinition<any>[];
        };
        toPlugin(): import("../../types/slatePluginDefinitions").SlatePluginDefinition<any>[];
    };
    ul: {
        <CT>(customizers: {
            customizeList?: (def: import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<import("../../pluginFactories/createSimpleHtmlBlockPlugin").DefaultBlockDataType>) => import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<import("../../pluginFactories/createSimpleHtmlBlockPlugin").HtmlBlockData<CT & {}>>;
            customizeListItem?: (def: import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<import("../../pluginFactories/createSimpleHtmlBlockPlugin").DefaultBlockDataType>) => import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<import("../../pluginFactories/createSimpleHtmlBlockPlugin").HtmlBlockData<CT & {}>>;
        }): {
            <CT_1>(customizers: {
                customizeList?: (def: import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<import("../../pluginFactories/createSimpleHtmlBlockPlugin").DefaultBlockDataType>) => import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<import("../../pluginFactories/createSimpleHtmlBlockPlugin").HtmlBlockData<CT_1>>;
                customizeListItem?: (def: import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<import("../../pluginFactories/createSimpleHtmlBlockPlugin").DefaultBlockDataType>) => import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<import("../../pluginFactories/createSimpleHtmlBlockPlugin").HtmlBlockData<CT_1>>;
            }): any;
            toPlugin(): import("../../types/slatePluginDefinitions").SlatePluginDefinition<any>[];
        };
        toPlugin(): import("../../types/slatePluginDefinitions").SlatePluginDefinition<any>[];
    };
    li: {
        <CT_2 = import("../../pluginFactories/createSimpleHtmlBlockPlugin").DefaultBlockDataType>(customize?: (t: import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<import("../../pluginFactories/createSimpleHtmlBlockPlugin").DefaultBlockDataType>) => import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_2>): {
            <CT_3 = CT_2>(customize?: (t: import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_2>) => import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_3>): {
                <CT_4 = CT_3>(customize?: (t: import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_3>) => import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_4>): {
                    <CT_5 = CT_4>(customize?: (t: import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_4>) => import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_5>): {
                        <CT_6 = CT_5>(customize?: (t: import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_5>) => import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_6>): {
                            <CT_7 = CT_6>(customize?: (t: import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_6>) => import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_7>): {
                                <CT_8 = CT_7>(customize?: (t: import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_7>) => import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_8>): {
                                    <CT_9 = CT_8>(customize?: (t: import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_8>) => import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_9>): {
                                        <CT_10 = CT_9>(customize?: (t: import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_9>) => import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_10>): {
                                            <CT_11 = CT_10>(customize?: (t: import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_10>) => import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_11>): {
                                                <CT_12 = CT_11>(customize?: (t: import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_11>) => import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_12>): any;
                                                toPlugin(): import("../../types/slatePluginDefinitions").SlatePluginDefinition<any>;
                                            };
                                            toPlugin(): import("../../types/slatePluginDefinitions").SlatePluginDefinition<any>;
                                        };
                                        toPlugin(): import("../../types/slatePluginDefinitions").SlatePluginDefinition<any>;
                                    };
                                    toPlugin(): import("../../types/slatePluginDefinitions").SlatePluginDefinition<any>;
                                };
                                toPlugin(): import("../../types/slatePluginDefinitions").SlatePluginDefinition<any>;
                            };
                            toPlugin(): import("../../types/slatePluginDefinitions").SlatePluginDefinition<any>;
                        };
                        toPlugin(): import("../../types/slatePluginDefinitions").SlatePluginDefinition<any>;
                    };
                    toPlugin(): import("../../types/slatePluginDefinitions").SlatePluginDefinition<any>;
                };
                toPlugin(): import("../../types/slatePluginDefinitions").SlatePluginDefinition<any>;
            };
            toPlugin(): import("../../types/slatePluginDefinitions").SlatePluginDefinition<any>;
        };
        toPlugin(): import("../../types/slatePluginDefinitions").SlatePluginDefinition<any>;
    };
    indention: {
        (customize: (def2: {
            iconIncrease: JSX.Element;
            iconDecrease: JSX.Element;
            listItemType: string;
            allListTypes: string[];
            labelIncrease?: string;
            labelDecrease?: string;
        }) => {
            iconIncrease: JSX.Element;
            iconDecrease: JSX.Element;
            listItemType: string;
            allListTypes: string[];
            labelIncrease?: string;
            labelDecrease?: string;
        }): any;
        toPlugin(): import("../../types/slatePluginDefinitions").SlatePluginDefinition<any>[];
    };
};
export default _default;
//# sourceMappingURL=index.d.ts.map