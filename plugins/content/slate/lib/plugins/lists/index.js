"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LI = exports.OL = exports.UL = void 0;
var core_1 = require("@react-page/core");
var react_1 = __importDefault(require("react"));
var pluginFactories_1 = require("../../pluginFactories");
var createListIndentionPlugin_1 = __importDefault(require("../../pluginFactories/createListIndentionPlugin"));
var createListPlugin_1 = __importDefault(require("../../pluginFactories/createListPlugin"));
var ListIcon = core_1.lazyLoad(function () {
    return Promise.resolve().then(function () { return __importStar(require('@material-ui/icons/FormatListBulleted')); });
});
var OrderedListIcon = core_1.lazyLoad(function () {
    return Promise.resolve().then(function () { return __importStar(require('@material-ui/icons/FormatListNumbered')); });
});
exports.UL = 'LISTS/UNORDERED-LIST';
exports.OL = 'LISTS/ORDERED-LIST';
exports.LI = 'LISTS/LIST-ITEM';
var IncreaseIndentIcon = core_1.lazyLoad(function () {
    return Promise.resolve().then(function () { return __importStar(require('@material-ui/icons/FormatIndentIncrease')); });
});
var DecreaseIndentIcon = core_1.lazyLoad(function () {
    return Promise.resolve().then(function () { return __importStar(require('@material-ui/icons/FormatIndentDecrease')); });
});
var ol = createListPlugin_1.default({
    type: exports.OL,
    allListTypes: [exports.UL, exports.OL],
    icon: react_1.default.createElement(OrderedListIcon, null),
    label: 'Ordered List',
    tagName: 'ol',
    listItem: {
        tagName: 'li',
        type: exports.LI,
    },
});
var ul = createListPlugin_1.default({
    type: exports.UL,
    allListTypes: [exports.UL, exports.OL],
    icon: react_1.default.createElement(ListIcon, null),
    label: 'Unordered List',
    tagName: 'ul',
    listItem: {
        tagName: 'li',
        type: exports.LI,
    },
});
// only used for easier access on createInitialSlateState
var li = pluginFactories_1.createListItemPlugin({
    tagName: 'li',
    type: exports.LI,
});
var indention = createListIndentionPlugin_1.default({
    iconIncrease: react_1.default.createElement(IncreaseIndentIcon, null),
    iconDecrease: react_1.default.createElement(DecreaseIndentIcon, null),
    listItemType: exports.LI,
    allListTypes: [exports.UL, exports.OL],
    labelIncrease: 'Increase Indentation',
    labelDecrease: 'Decrease Indentation',
});
exports.default = {
    ol: ol,
    ul: ul,
    li: li,
    indention: indention,
};
//# sourceMappingURL=index.js.map