"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@react-page/core");
var React = __importStar(require("react"));
var createDataPlugin_1 = __importDefault(require("../pluginFactories/createDataPlugin"));
var AlignLeftIcon = core_1.lazyLoad(function () {
    return Promise.resolve().then(function () { return __importStar(require('@material-ui/icons/FormatAlignLeft')); });
});
var AlignCenterIcon = core_1.lazyLoad(function () {
    return Promise.resolve().then(function () { return __importStar(require('@material-ui/icons/FormatAlignCenter')); });
});
var AlignRightIcon = core_1.lazyLoad(function () {
    return Promise.resolve().then(function () { return __importStar(require('@material-ui/icons/FormatAlignRight')); });
});
var AlignJustifyIcon = core_1.lazyLoad(function () {
    return Promise.resolve().then(function () { return __importStar(require('@material-ui/icons/FormatAlignJustify')); });
});
var left = createDataPlugin_1.default({
    icon: React.createElement(AlignLeftIcon, null),
    label: 'Align Left',
    object: 'block',
    addToolbarButton: true,
    addHoverButton: false,
    dataMatches: function (data) { return (data === null || data === void 0 ? void 0 : data.align) === 'left'; },
    getInitialData: function () { return ({ align: 'left' }); },
});
var center = createDataPlugin_1.default({
    icon: React.createElement(AlignCenterIcon, null),
    label: 'Align Center',
    object: 'block',
    addToolbarButton: true,
    addHoverButton: false,
    dataMatches: function (data) { return (data === null || data === void 0 ? void 0 : data.align) === 'center'; },
    getInitialData: function () { return ({ align: 'center' }); },
});
var right = createDataPlugin_1.default({
    icon: React.createElement(AlignRightIcon, null),
    label: 'Align Right',
    object: 'block',
    addToolbarButton: true,
    addHoverButton: false,
    dataMatches: function (data) { return (data === null || data === void 0 ? void 0 : data.align) === 'right'; },
    getInitialData: function () { return ({ align: 'right' }); },
});
var justify = createDataPlugin_1.default({
    icon: React.createElement(AlignJustifyIcon, null),
    label: 'Align Justify',
    object: 'block',
    addToolbarButton: true,
    addHoverButton: false,
    dataMatches: function (data) { return (data === null || data === void 0 ? void 0 : data.align) === 'justify'; },
    getInitialData: function () { return ({ align: 'justify' }); },
});
exports.default = {
    left: left,
    center: center,
    right: right,
    justify: justify,
};
//# sourceMappingURL=alignment.js.map