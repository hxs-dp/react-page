declare const _default: {
    <CT = {}>(customize?: (t: import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<{}>) => import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT>): {
        <CT_1 = CT>(customize?: (t: import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT>) => import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_1>): {
            <CT_2 = CT_1>(customize?: (t: import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_1>) => import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_2>): {
                <CT_3 = CT_2>(customize?: (t: import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_2>) => import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_3>): {
                    <CT_4 = CT_3>(customize?: (t: import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_3>) => import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_4>): {
                        <CT_5 = CT_4>(customize?: (t: import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_4>) => import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_5>): {
                            <CT_6 = CT_5>(customize?: (t: import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_5>) => import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_6>): {
                                <CT_7 = CT_6>(customize?: (t: import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_6>) => import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_7>): {
                                    <CT_8 = CT_7>(customize?: (t: import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_7>) => import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_8>): {
                                        <CT_9 = CT_8>(customize?: (t: import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_8>) => import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_9>): {
                                            <CT_10 = CT_9>(customize?: (t: import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_9>) => import("../../types/slatePluginDefinitions").SlateComponentPluginDefinition<CT_10>): any;
                                            toPlugin(): import("../../types/slatePluginDefinitions").SlatePluginDefinition<any>;
                                        };
                                        toPlugin(): import("../../types/slatePluginDefinitions").SlatePluginDefinition<any>;
                                    };
                                    toPlugin(): import("../../types/slatePluginDefinitions").SlatePluginDefinition<any>;
                                };
                                toPlugin(): import("../../types/slatePluginDefinitions").SlatePluginDefinition<any>;
                            };
                            toPlugin(): import("../../types/slatePluginDefinitions").SlatePluginDefinition<any>;
                        };
                        toPlugin(): import("../../types/slatePluginDefinitions").SlatePluginDefinition<any>;
                    };
                    toPlugin(): import("../../types/slatePluginDefinitions").SlatePluginDefinition<any>;
                };
                toPlugin(): import("../../types/slatePluginDefinitions").SlatePluginDefinition<any>;
            };
            toPlugin(): import("../../types/slatePluginDefinitions").SlatePluginDefinition<any>;
        };
        toPlugin(): import("../../types/slatePluginDefinitions").SlatePluginDefinition<any>;
    };
    toPlugin(): import("../../types/slatePluginDefinitions").SlatePluginDefinition<any>;
};
export default _default;
//# sourceMappingURL=em.d.ts.map