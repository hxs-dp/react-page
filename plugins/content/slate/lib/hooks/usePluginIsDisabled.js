"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var slate_react_1 = require("slate-react");
exports.default = (function (plugin) {
    try {
        var editor = slate_react_1.useSlate();
        if (!editor) {
            return true;
        }
        return plugin.isDisabled ? plugin.isDisabled(editor) : false;
    }
    catch (e) {
        // slate sometimes throws when dom node cant be found in undo
        return false;
    }
});
//# sourceMappingURL=usePluginIsDisabled.js.map