import { NodeEntry } from 'slate';
import { ReactEditor } from 'slate-react';
import { SlatePluginDefinition } from '../types/slatePluginDefinitions';
export declare const getCurrentNodeWithPlugin: <T>(editor: ReactEditor, plugin: SlatePluginDefinition<T>) => NodeEntry<import("slate").Node>;
declare const _default: <T>(plugin: SlatePluginDefinition<T>) => NodeEntry<import("slate").Node>;
export default _default;
//# sourceMappingURL=useCurrentNodeWithPlugin.d.ts.map