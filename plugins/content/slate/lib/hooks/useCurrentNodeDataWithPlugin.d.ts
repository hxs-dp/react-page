import { ReactEditor } from 'slate-react';
import { SlatePluginDefinition } from '../types/slatePluginDefinitions';
export declare const getCurrentNodeDataWithPlugin: <T>(editor: ReactEditor, plugin: SlatePluginDefinition<T>) => T;
declare const _default: <T>(plugin: SlatePluginDefinition<T>) => T;
export default _default;
//# sourceMappingURL=useCurrentNodeDataWithPlugin.d.ts.map