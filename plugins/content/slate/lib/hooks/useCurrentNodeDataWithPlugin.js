"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getCurrentNodeDataWithPlugin = void 0;
var slate_react_1 = require("slate-react");
var useCurrentNodeWithPlugin_1 = require("./useCurrentNodeWithPlugin");
exports.getCurrentNodeDataWithPlugin = function (editor, plugin) {
    var currentNodeEntry = useCurrentNodeWithPlugin_1.getCurrentNodeWithPlugin(editor, plugin);
    if (currentNodeEntry) {
        var currentNode = currentNodeEntry[0];
        if (plugin.pluginType === 'component' && plugin.object === 'mark') {
            return currentNode[plugin.type];
        }
        var data = currentNode.data;
        return data;
    }
    else if (plugin.getInitialData) {
        return plugin.getInitialData();
    }
    else {
        return {};
    }
};
exports.default = (function (plugin) {
    var editor = slate_react_1.useSlate();
    return exports.getCurrentNodeDataWithPlugin(editor, plugin);
});
//# sourceMappingURL=useCurrentNodeDataWithPlugin.js.map