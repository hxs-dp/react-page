"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.HtmlToSlate = exports.pluginFactories = exports.slatePlugins = exports.defaultPlugins = void 0;
var core_1 = require("@react-page/core");
var React = __importStar(require("react"));
var redux_undo_1 = require("redux-undo");
var Component_1 = __importDefault(require("./Component"));
var settings_1 = require("./default/settings");
var HtmlToSlate_1 = __importDefault(require("./HtmlToSlate"));
exports.HtmlToSlate = HtmlToSlate_1.default;
var v002_1 = __importDefault(require("./migrations/v002"));
var v003_1 = __importDefault(require("./migrations/v003"));
var v004_1 = __importDefault(require("./migrations/v004"));
var pluginFactories = __importStar(require("./pluginFactories/index"));
exports.pluginFactories = pluginFactories;
var defaultPlugins = __importStar(require("./plugins/index"));
exports.defaultPlugins = defaultPlugins;
var Renderer_1 = __importDefault(require("./Renderer"));
var makeSlatePluginsFromDef_1 = __importDefault(require("./utils/makeSlatePluginsFromDef"));
var transformInitialSlateState_1 = __importDefault(require("./utils/transformInitialSlateState"));
var slatePlugins = defaultPlugins;
exports.slatePlugins = slatePlugins;
var Subject = core_1.lazyLoad(function () { return Promise.resolve().then(function () { return __importStar(require('@material-ui/icons/Subject')); }); });
var Controls = core_1.lazyLoad(function () { return Promise.resolve().then(function () { return __importStar(require('./Controls/')); }); });
var migrations = [v002_1.default, v003_1.default, v004_1.default];
var defaultConfig = {
    icon: React.createElement(Subject, null),
    plugins: defaultPlugins,
    defaultPluginType: 'PARAGRAPH/PARAGRAPH',
    Renderer: Renderer_1.default,
    Controls: Controls,
    name: 'ory/editor/core/content/slate',
    version: '0.0.4',
    translations: settings_1.defaultTranslations,
    migrations: migrations,
    allowInlineNeighbours: true,
};
function plugin(customize) {
    var settings = (customize
        ? customize(defaultConfig)
        : defaultConfig);
    var createInitialState = function (customizeInitialSlateState) {
        var defaultInitialState = function (_a) {
            var cplugins = _a.plugins;
            return ({
                children: [
                    {
                        plugin: cplugins.paragraphs.paragraph,
                        children: [''],
                    },
                ],
            });
        };
        var func = customizeInitialSlateState
            ? customizeInitialSlateState
            : defaultInitialState;
        return transformInitialSlateState_1.default(func({ plugins: settings.plugins }));
    };
    // plugins should be flatten
    // NEW: to make it easier to manage and group plugins,
    // they now need to be an object of object with group and keys, see type SlatePluginCollection
    var plugins = makeSlatePluginsFromDef_1.default(settings.plugins);
    var htmlToSlate = HtmlToSlate_1.default({ plugins: plugins });
    return {
        Component: function (props) { return (React.createElement(Component_1.default, __assign({ Renderer: settings.Renderer, Controls: settings.Controls, plugins: plugins, translations: settings.translations, defaultPluginType: settings.defaultPluginType }, props))); },
        name: settings.name,
        version: settings.version,
        IconComponent: settings.icon,
        text: settings.translations.pluginName,
        description: settings.translations.pluginDescription,
        hideInMenu: settings.hideInMenu,
        allowInlineNeighbours: settings.allowInlineNeighbours,
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        reducer: function (state, action) {
            var _a, _b, _c;
            if ((action.type === redux_undo_1.ActionTypes.UNDO ||
                action.type === redux_undo_1.ActionTypes.REDO) &&
                ((_c = (_b = (_a = state === null || state === void 0 ? void 0 : state.content) === null || _a === void 0 ? void 0 : _a.state) === null || _b === void 0 ? void 0 : _b.slate) !== null && _c !== void 0 ? _c : false)) {
                return __assign(__assign({}, state), { content: __assign(__assign({}, state.content), { state: __assign({}, state.content.state) }) });
            }
            return state;
        },
        handleRemoveHotKey: function () { return Promise.reject(); },
        handleFocusPreviousHotKey: function () { return Promise.reject(); },
        handleFocusNextHotKey: function () { return Promise.reject(); },
        createInitialState: createInitialState,
        createInitialSlateState: createInitialState,
        htmlToSlate: htmlToSlate,
        serialize: function (s) { return (s ? { slate: s.slate } : null); },
        unserialize: function (s) {
            if (s === null || s === void 0 ? void 0 : s.importFromHtml) {
                return htmlToSlate(s.importFromHtml);
            }
            if (s === null || s === void 0 ? void 0 : s.slate) {
                return {
                    slate: s.slate,
                };
            }
            return createInitialState();
        },
        // TODO this is disabled because of #207
        // merge = hooks.merge
        // split = hooks.split
        migrations: settings.migrations,
    };
}
exports.default = plugin;
//# sourceMappingURL=index.js.map