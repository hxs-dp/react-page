var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import { BottomToolbar, ImageUpload } from '@react-page/ui';
import * as React from 'react';
var ImageDefaultControls = function (props) {
    var Renderer = props.Renderer, handleImageLoaded = props.handleImageLoaded, handleImageUploaded = props.handleImageUploaded, handleChange = props.handleChange, readOnly = props.readOnly, focused = props.focused, remove = props.remove;
    return (React.createElement("div", null,
        React.createElement(Renderer, __assign({}, props, { imagePreview: props.imagePreview })),
        !readOnly && focused && (React.createElement(BottomToolbar, __assign({ icon: props.IconComponent, open: props.focused, title: props.translations.pluginName, onDelete: remove }, props),
            React.createElement("div", { style: { display: 'flex' } },
                props.imageUpload && (React.createElement(React.Fragment, null,
                    React.createElement(ImageUpload, { translations: props.translations, imageUpload: props.imageUpload, imageLoaded: handleImageLoaded, imageUploaded: handleImageUploaded }),
                    React.createElement(Typography, { variant: "body1", style: { marginLeft: '20px', marginRight: '20px' } }, props.translations.or))),
                React.createElement(TextField, { placeholder: props.translations.srcPlaceholder, label: props.imageUpload
                        ? props.translations.haveUrl
                        : props.translations.imageUrl, name: "src", 
                    // style={{ flex: 1 }}
                    value: props.state.src, onChange: handleChange })),
            React.createElement(TextField, { placeholder: props.translations.hrefPlaceholder, label: props.translations.hrefLabel, name: "href", style: { width: '512px' }, value: props.state.href, onChange: handleChange }),
            React.createElement("br", null),
            React.createElement("br", null),
            React.createElement(FormControlLabel, { control: React.createElement(Checkbox, { checked: props.state.target === '_blank', name: "target", onChange: handleChange }), label: props.translations.openNewWindow })))));
};
export default ImageDefaultControls;
//# sourceMappingURL=ImageDefaultControls.js.map