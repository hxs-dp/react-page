import { ImageProps } from './component';
import { ImageLoaded } from '@react-page/ui';
export interface ImageRendererExtraProps {
    imagePreview?: ImageLoaded;
}
export declare type ImageRendererProps = ImageProps & ImageRendererExtraProps;
//# sourceMappingURL=renderer.d.ts.map