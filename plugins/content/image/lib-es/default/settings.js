import * as React from 'react';
import { lazyLoad } from '@react-page/core';
var Panorama = lazyLoad(function () { return import('@material-ui/icons/Panorama'); });
export var defaultTranslations = {
    pluginName: 'Image',
    pluginDescription: 'Loads an image from an url.',
    or: 'OR',
    haveUrl: 'I have a URL',
    imageUrl: 'Image URL',
    hrefPlaceholder: 'http://example.com',
    hrefLabel: 'Link location (url)',
    openNewWindow: 'Open in new window',
    srcPlaceholder: 'http://example.com/image.png',
};
export var defaultSettings = {
    Controls: function () { return React.createElement(React.Fragment, null, " Controls for this plugin were not provided"); },
    Renderer: function () { return React.createElement(React.Fragment, null, "Renderer; for this plugin was not provided "); },
    translations: defaultTranslations,
    IconComponent: React.createElement(Panorama, null),
};
//# sourceMappingURL=settings.js.map