import * as React from 'react';
import { iconStyle } from './../common/styles';
import { lazyLoad } from '@react-page/core';
var ImageIcon = lazyLoad(function () { return import('@material-ui/icons/Landscape'); });
var ImageHtmlRenderer = function (props) {
    var isEditMode = props.isEditMode, state = props.state, imagePreview = props.imagePreview;
    var src = imagePreview ? imagePreview.dataUrl : state.src;
    var Image = React.createElement("img", { className: "ory-plugins-content-image", alt: "", src: src });
    return src ? (React.createElement("div", null, state.href && !isEditMode ? (React.createElement("a", { href: state.href, target: state.target, rel: state.rel }, Image)) : (Image))) : (React.createElement("div", null,
        React.createElement("div", { className: "ory-plugins-content-image-placeholder" },
            React.createElement(ImageIcon, { style: iconStyle }))));
};
export default ImageHtmlRenderer;
//# sourceMappingURL=ImageHtmlRenderer.js.map