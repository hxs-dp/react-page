var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import * as React from 'react';
import Component from './Component/index';
import { defaultSettings } from './default/settings';
var createPlugin = function (settings) {
    var mergedSettings = __assign(__assign({}, defaultSettings), settings);
    return {
        Component: function (props) { return (React.createElement(Component, __assign({}, props, mergedSettings))); },
        name: 'ory/editor/core/content/image',
        version: '0.0.1',
        IconComponent: mergedSettings.IconComponent,
        text: mergedSettings.translations.pluginName,
        isInlineable: true,
        description: mergedSettings.translations.pluginDescription,
        handleRemoveHotKey: function (_, __) {
            return Promise.reject();
        },
        handleFocusPreviousHotKey: function (_, __) { return Promise.reject(); },
        handleFocusNextHotKey: function (_, __) {
            return Promise.reject();
        },
        // We need this because otherwise we lose hotkey focus on elements like spoilers.
        // This could probably be solved in an easier way by listening to window.document?
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        handleFocus: function (props, source, ref) {
            if (!ref) {
                return;
            }
            setTimeout(function () { return ref.focus(); });
        },
    };
};
export default createPlugin;
//# sourceMappingURL=createPlugin.js.map