"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var Checkbox_1 = __importDefault(require("@material-ui/core/Checkbox"));
var FormControlLabel_1 = __importDefault(require("@material-ui/core/FormControlLabel"));
var TextField_1 = __importDefault(require("@material-ui/core/TextField"));
var Typography_1 = __importDefault(require("@material-ui/core/Typography"));
var ui_1 = require("@react-page/ui");
var React = __importStar(require("react"));
var ImageDefaultControls = function (props) {
    var Renderer = props.Renderer, handleImageLoaded = props.handleImageLoaded, handleImageUploaded = props.handleImageUploaded, handleChange = props.handleChange, readOnly = props.readOnly, focused = props.focused, remove = props.remove;
    return (React.createElement("div", null,
        React.createElement(Renderer, __assign({}, props, { imagePreview: props.imagePreview })),
        !readOnly && focused && (React.createElement(ui_1.BottomToolbar, __assign({ icon: props.IconComponent, open: props.focused, title: props.translations.pluginName, onDelete: remove }, props),
            React.createElement("div", { style: { display: 'flex' } },
                props.imageUpload && (React.createElement(React.Fragment, null,
                    React.createElement(ui_1.ImageUpload, { translations: props.translations, imageUpload: props.imageUpload, imageLoaded: handleImageLoaded, imageUploaded: handleImageUploaded }),
                    React.createElement(Typography_1.default, { variant: "body1", style: { marginLeft: '20px', marginRight: '20px' } }, props.translations.or))),
                React.createElement(TextField_1.default, { placeholder: props.translations.srcPlaceholder, label: props.imageUpload
                        ? props.translations.haveUrl
                        : props.translations.imageUrl, name: "src", 
                    // style={{ flex: 1 }}
                    value: props.state.src, onChange: handleChange })),
            React.createElement(TextField_1.default, { placeholder: props.translations.hrefPlaceholder, label: props.translations.hrefLabel, name: "href", style: { width: '512px' }, value: props.state.href, onChange: handleChange }),
            React.createElement("br", null),
            React.createElement("br", null),
            React.createElement(FormControlLabel_1.default, { control: React.createElement(Checkbox_1.default, { checked: props.state.target === '_blank', name: "target", onChange: handleChange }), label: props.translations.openNewWindow })))));
};
exports.default = ImageDefaultControls;
//# sourceMappingURL=ImageDefaultControls.js.map