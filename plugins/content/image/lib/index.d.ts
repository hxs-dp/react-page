import { ContentPluginConfig } from '@react-page/core';
import { ImageSettings } from './types/settings';
import { ImageState } from './types/state';
import { ImageUploadType } from '@react-page/ui';
declare const imagePlugin: (settings?: Partial<ImageSettings>) => ContentPluginConfig<ImageState>;
declare const image: Pick<import("@react-page/core").ContentPluginProps<ImageState>, "text" | "lang" | "name" | "version" | "allowInlineNeighbours" | "isInlineable" | "Component" | "description" | "IconComponent" | "hideInMenu" | "serialize" | "unserialize" | "handleRemoveHotKey" | "handleFocusNextHotKey" | "handleFocusPreviousHotKey" | "handleFocus" | "handleBlur" | "reducer" | "migrations" | "createInitialState">;
export default image;
export { ImageUploadType };
export { imagePlugin };
//# sourceMappingURL=index.d.ts.map