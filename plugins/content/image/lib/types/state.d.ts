export interface ImageState {
    src: string;
    caption?: string;
    href?: string;
    target?: string;
    rel?: string;
}
//# sourceMappingURL=state.d.ts.map