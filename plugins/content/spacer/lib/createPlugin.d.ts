import { ContentPluginConfig } from '@react-page/core';
import { SpacerSettings } from './types/settings';
import { SpacerState } from './types/state';
declare const createPlugin: (settings: SpacerSettings) => ContentPluginConfig<SpacerState>;
export default createPlugin;
//# sourceMappingURL=createPlugin.d.ts.map