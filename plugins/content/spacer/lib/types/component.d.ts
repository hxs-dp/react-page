import { ContentPluginProps } from '@react-page/core';
import { SpacerSettings } from './settings';
import { SpacerState } from './state';
export declare type SpacerProps = ContentPluginProps<SpacerState> & SpacerSettings;
//# sourceMappingURL=component.d.ts.map