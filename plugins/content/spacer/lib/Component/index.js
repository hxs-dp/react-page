"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = __importStar(require("react"));
var Spacer = /** @class */ (function (_super) {
    __extends(Spacer, _super);
    function Spacer(props) {
        var _this = _super.call(this, props) || this;
        _this.state = {
            height: undefined,
        };
        _this.changeHeightPreview = _this.changeHeightPreview.bind(_this);
        _this.commitHeight = _this.commitHeight.bind(_this);
        return _this;
    }
    Spacer.prototype.render = function () {
        var Controls = this.props.Controls;
        return (React.createElement(Controls, __assign({}, this.props, { state: __assign(__assign({}, this.props.state), { height: this.state.height
                    ? this.state.height
                    : this.props.state.height }), changeHeightPreview: this.changeHeightPreview, commitHeight: this.commitHeight })));
    };
    Spacer.prototype.changeHeightPreview = function (height) {
        if (!height || height < 24) {
            height = 24;
        }
        this.setState({ height: height });
    };
    Spacer.prototype.commitHeight = function (height) {
        var _this = this;
        var h = height ? height : this.state.height;
        if (!h || h < 24) {
            h = 24;
        }
        this.setState({ height: undefined }, function () {
            return _this.props.onChange({ height: h });
        });
    };
    return Spacer;
}(React.PureComponent));
exports.default = Spacer;
//# sourceMappingURL=index.js.map