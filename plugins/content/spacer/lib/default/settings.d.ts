import { SpacerSettings } from '../types/settings';
export declare const defaultTranslations: {
    pluginName: string;
    pluginDescription: string;
    elementHeightLabel: string;
};
export declare const defaultSettings: SpacerSettings;
//# sourceMappingURL=settings.d.ts.map