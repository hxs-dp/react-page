var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import TextField from '@material-ui/core/TextField';
import { BottomToolbar } from '@react-page/ui';
import classNames from 'classnames';
import * as React from 'react';
import { Resizable } from 'react-resizable';
import { defaultSpacerState } from './../default/state';
var faintBlack = 'rgba(0, 0, 0, 0.12)';
var SpacerDefaultControls = function (props) {
    var isPreviewMode = props.isPreviewMode, isEditMode = props.isEditMode, remove = props.remove, Renderer = props.Renderer, changeHeightPreview = props.changeHeightPreview, commitHeight = props.commitHeight, _a = props.state, height = (_a === void 0 ? defaultSpacerState : _a).height;
    return (React.createElement("div", { style: { border: 'solid 1px', borderColor: faintBlack }, className: classNames('ory-plugins-content-spacer', {
            'ory-plugins-content-spacer-read-only': isPreviewMode,
        }) }, !isEditMode ? (React.createElement(Renderer, __assign({}, props))) : (React.createElement(Resizable, { onResize: function (e, data) { return changeHeightPreview(data.size.height); }, onResizeStop: function (e, data) { return commitHeight(data.size.height); }, height: height, width: 0 },
        React.createElement("div", { style: { height: height, position: 'relative' } },
            React.createElement(BottomToolbar, __assign({}, props, { icon: props.IconComponent, open: props.focused, title: props.translations.pluginName, onDelete: remove }),
                React.createElement(TextField, { placeholder: "24", label: props.translations.elementHeightLabel, style: { width: '512px' }, value: height, onChange: function (e) {
                        return changeHeightPreview(parseInt(e.target.value, 10));
                    }, onBlur: function () { return commitHeight(); }, type: "number" })),
            React.createElement("div", { style: {
                    position: 'absolute',
                    bottom: '0',
                    height: '24px',
                    width: '100%',
                    background: faintBlack,
                    textAlign: 'center',
                } },
                React.createElement("svg", { viewBox: "0 0 24 24", style: { color: 'white', width: 24, height: 24 } },
                    React.createElement("path", { d: "M20 9H4v2h16V9zM4 15h16v-2H4v2z" }))))))));
};
export default SpacerDefaultControls;
//# sourceMappingURL=SpacerDefaultControls.js.map