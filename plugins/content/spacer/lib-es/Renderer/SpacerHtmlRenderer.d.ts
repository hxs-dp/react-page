import * as React from 'react';
import { SpacerHtmlRendererProps } from '../types/renderer';
declare const SpacerHtmlRenderer: React.SFC<SpacerHtmlRendererProps>;
export default SpacerHtmlRenderer;
//# sourceMappingURL=SpacerHtmlRenderer.d.ts.map