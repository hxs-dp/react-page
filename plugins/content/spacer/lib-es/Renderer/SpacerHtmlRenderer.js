import * as React from 'react';
import { defaultSpacerState } from './../default/state';
var SpacerHtmlRenderer = function (_a) {
    var _b = _a.state, height = (_b === void 0 ? defaultSpacerState : _b).height;
    return React.createElement("div", { style: { height: (height || 0).toString() + "px" } });
};
export default SpacerHtmlRenderer;
//# sourceMappingURL=SpacerHtmlRenderer.js.map