import { SpacerProps } from './component';
import { SpacerApi } from './api';
export declare type SpacerControlsProps = SpacerProps & SpacerApi;
//# sourceMappingURL=controls.d.ts.map