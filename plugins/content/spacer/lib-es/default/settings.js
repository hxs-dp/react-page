import * as React from 'react';
export var defaultTranslations = {
    pluginName: 'Spacer',
    pluginDescription: 'Resizeable, empty space.',
    elementHeightLabel: 'Element height (px)',
};
export var defaultSettings = {
    Controls: function () { return React.createElement(React.Fragment, null, " Controls for this plugin were not provided"); },
    Renderer: function () { return React.createElement(React.Fragment, null, "Renderer; for this plugin was not provided "); },
    translations: defaultTranslations,
};
//# sourceMappingURL=settings.js.map