import createPlugin from './createPlugin';
import SpacerHtmlRenderer from './Renderer/SpacerHtmlRenderer';
import { lazyLoad } from '@react-page/core';
var SpacerDefaultControls = lazyLoad(function () {
    return import('./Controls/SpacerDefaultControls');
});
var plugin = createPlugin({
    Renderer: SpacerHtmlRenderer,
    Controls: SpacerDefaultControls,
});
export default plugin;
//# sourceMappingURL=index.js.map