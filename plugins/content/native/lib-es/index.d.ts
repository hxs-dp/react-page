import { NativeState } from './types/state';
import { NativeFactory } from '@react-page/core';
export interface NativeProps {
    state: NativeState;
}
/**
 *
 * @param hover the item which the native element was dropped on
 * @param monitor the DropTargetMonitor as provided by react-dnd
 * @param component the React component of the item which the native element was dropped on
 */
declare const plugin: NativeFactory;
export default plugin;
//# sourceMappingURL=index.d.ts.map