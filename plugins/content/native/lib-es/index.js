import * as React from 'react';
var Native = function (_a) {
    var _b = _a.state, item = _b.item, itemType = _b.itemType;
    return (React.createElement("div", null,
        React.createElement("p", null,
            "This is a default plugin that handles native drag events of type",
            ' ',
            React.createElement("code", null, itemType),
            ".",
            React.createElement("br", null),
            "It contained the following payload:"),
        React.createElement("pre", null, JSON.stringify(item, null, 2))));
};
/**
 *
 * @param hover the item which the native element was dropped on
 * @param monitor the DropTargetMonitor as provided by react-dnd
 * @param component the React component of the item which the native element was dropped on
 */
var plugin = function (hover, monitor, component) { return ({
    Component: Native,
    name: 'ory/editor/core/content/default-native',
    version: '0.0.1',
    createInitialState: function () { return ({
        item: monitor && monitor.getItem(),
        itemType: monitor && monitor.getItemType(),
    }); },
}); };
export default plugin;
//# sourceMappingURL=index.js.map