import * as React from 'react';
import { lazyLoad } from '@react-page/core';
var PlayArrow = lazyLoad(function () { return import('@material-ui/icons/PlayArrow'); });
export var defaultTranslations = {
    pluginName: 'HTML 5 Video',
    pluginDescription: 'Add webm, ogg and other HTML5 video',
    urlLabel: 'Video url',
    urlPlaceholder: 'https://example.com/video.webm',
};
export var defaultSettings = {
    Controls: function () { return React.createElement(React.Fragment, null, " Controls for this plugin were not provided"); },
    Renderer: function () { return React.createElement(React.Fragment, null, "Renderer; for this plugin was not provided "); },
    translations: defaultTranslations,
    IconComponent: React.createElement(PlayArrow, null),
};
//# sourceMappingURL=settings.js.map