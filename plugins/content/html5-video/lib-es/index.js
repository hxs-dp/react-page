import createPlugin from './createPlugin';
import { lazyLoad } from '@react-page/core';
import Html5VideoHtmlRenderer from './Renderer/Html5VideoHtmlRenderer';
var Html5VideoDefaultControls = lazyLoad(function () {
    return import('./Controls/Html5VideoDefaultControls');
});
var plugin = createPlugin({
    Renderer: Html5VideoHtmlRenderer,
    Controls: Html5VideoDefaultControls,
});
export default plugin;
//# sourceMappingURL=index.js.map