import { Html5VideoProps } from './component';
import { Html5VideoApi } from './api';
export declare type Html5VideoControlsProps = Html5VideoProps & Html5VideoApi;
//# sourceMappingURL=controls.d.ts.map