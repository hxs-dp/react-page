var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
import React, { useEffect, useState } from 'react';
var HTML5Video = function (props) {
    var _a;
    var Controls = props.Controls, readOnly = props.readOnly, Renderer = props.Renderer;
    var _b = __read(useState(props.state.url), 2), url = _b[0], setUrl = _b[1];
    useEffect(function () {
        var _a;
        setUrl((_a = props.state) === null || _a === void 0 ? void 0 : _a.url);
    }, [(_a = props.state) === null || _a === void 0 ? void 0 : _a.url]);
    return (React.createElement(React.Fragment, null,
        !readOnly ? (React.createElement(Controls, __assign({}, props, { state: { url: url }, changeUrlPreview: setUrl, commitUrl: function () { return props.onChange({ url: url }); } }))) : null,
        React.createElement(Renderer, __assign({}, props))));
};
export default React.memo(HTML5Video);
//# sourceMappingURL=index.js.map