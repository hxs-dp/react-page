import * as React from 'react';
import { defaultHtml5VideoState } from '../default/state';
var Html5VideoHtmlRenderer = function (_a) {
    var _b = _a.state, _c = (_b === void 0 ? defaultHtml5VideoState : _b).url, url = _c === void 0 ? '' : _c;
    return (React.createElement("div", { className: "ory-content-plugin-html5-video" },
        React.createElement("video", { autoPlay: true, controls: true, loop: true, muted: true, width: "100%", key: url },
            React.createElement("source", { src: url, type: "video/" + url.split('.').pop() }))));
};
export default Html5VideoHtmlRenderer;
//# sourceMappingURL=Html5VideoHtmlRenderer.js.map