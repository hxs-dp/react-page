var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import * as React from 'react';
import Html5Video from './Component';
import { defaultSettings } from './default/settings';
var rejectPromise = function (e, props) { return Promise.reject(); };
var createPlugin = function (settings) {
    var mergedSettings = __assign(__assign({}, defaultSettings), settings);
    var WrappedComponent = function (props) { return (React.createElement(Html5Video, __assign({}, props, mergedSettings))); };
    return {
        Component: WrappedComponent,
        name: 'ory/sites/plugin/content/html5-video',
        version: '0.0.1',
        text: mergedSettings.translations.pluginName,
        description: mergedSettings.translations.pluginDescription,
        IconComponent: mergedSettings.IconComponent,
        handleFocusNextHotKey: rejectPromise,
        handleFocusPreviousHotKey: rejectPromise,
        createInitialState: function () { return ({
            url: '',
        }); },
    };
};
export default createPlugin;
//# sourceMappingURL=createPlugin.js.map