var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import TextField from '@material-ui/core/TextField';
import { BottomToolbar } from '@react-page/ui';
import * as React from 'react';
import { defaultHtml5VideoState } from '../default/state';
/*const changeUrl = (onChange: (state: Html5VideoState) => void) => (
  event: React.ChangeEvent<HTMLInputElement>
) => event.target && onChange({ url: event.target.value });*/
var Html5VideoDefaultControls = function (props) {
    var _a = props.state, url = (_a === void 0 ? defaultHtml5VideoState : _a).url, commitUrl = props.commitUrl, changeUrlPreview = props.changeUrlPreview, focused = props.focused, remove = props.remove;
    return (React.createElement(BottomToolbar, __assign({ open: focused, title: props.translations.pluginName, onDelete: remove }, props),
        React.createElement(TextField, { placeholder: props.translations.urlPlaceholder, label: props.translations.urlLabel, onChange: function (e) { return changeUrlPreview(e.target.value); }, onBlur: commitUrl, value: url, style: { width: '512px' } })));
};
export default Html5VideoDefaultControls;
//# sourceMappingURL=Html5VideoDefaultControls.js.map