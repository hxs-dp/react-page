"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importStar(require("react"));
var HTML5Video = function (props) {
    var _a;
    var Controls = props.Controls, readOnly = props.readOnly, Renderer = props.Renderer;
    var _b = __read(react_1.useState(props.state.url), 2), url = _b[0], setUrl = _b[1];
    react_1.useEffect(function () {
        var _a;
        setUrl((_a = props.state) === null || _a === void 0 ? void 0 : _a.url);
    }, [(_a = props.state) === null || _a === void 0 ? void 0 : _a.url]);
    return (react_1.default.createElement(react_1.default.Fragment, null,
        !readOnly ? (react_1.default.createElement(Controls, __assign({}, props, { state: { url: url }, changeUrlPreview: setUrl, commitUrl: function () { return props.onChange({ url: url }); } }))) : null,
        react_1.default.createElement(Renderer, __assign({}, props))));
};
exports.default = react_1.default.memo(HTML5Video);
//# sourceMappingURL=index.js.map