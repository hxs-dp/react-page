import React from 'react';
import { Html5VideoProps } from './../types/component';
export interface HTML5VideoState {
    url: string;
}
declare const _default: React.NamedExoticComponent<Html5VideoProps>;
export default _default;
//# sourceMappingURL=index.d.ts.map