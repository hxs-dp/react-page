export interface Html5VideoApi {
    changeUrlPreview: (url: string) => void;
    commitUrl: () => void;
}
//# sourceMappingURL=api.d.ts.map