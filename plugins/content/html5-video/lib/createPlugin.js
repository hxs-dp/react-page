"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = __importStar(require("react"));
var Component_1 = __importDefault(require("./Component"));
var settings_1 = require("./default/settings");
var rejectPromise = function (e, props) { return Promise.reject(); };
var createPlugin = function (settings) {
    var mergedSettings = __assign(__assign({}, settings_1.defaultSettings), settings);
    var WrappedComponent = function (props) { return (React.createElement(Component_1.default, __assign({}, props, mergedSettings))); };
    return {
        Component: WrappedComponent,
        name: 'ory/sites/plugin/content/html5-video',
        version: '0.0.1',
        text: mergedSettings.translations.pluginName,
        description: mergedSettings.translations.pluginDescription,
        IconComponent: mergedSettings.IconComponent,
        handleFocusNextHotKey: rejectPromise,
        handleFocusPreviousHotKey: rejectPromise,
        createInitialState: function () { return ({
            url: '',
        }); },
    };
};
exports.default = createPlugin;
//# sourceMappingURL=createPlugin.js.map