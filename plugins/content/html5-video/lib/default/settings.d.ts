import { Html5VideoSettings } from '../types/settings';
export declare const defaultTranslations: {
    pluginName: string;
    pluginDescription: string;
    urlLabel: string;
    urlPlaceholder: string;
};
export declare const defaultSettings: Html5VideoSettings;
//# sourceMappingURL=settings.d.ts.map