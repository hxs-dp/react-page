import { ContentPluginConfig, ContentPluginProps } from '@react-page/core';
import { Html5VideoSettings } from './types/settings';
import { Html5VideoState } from './types/state';
export declare type Props = ContentPluginProps;
declare const createPlugin: (settings: Html5VideoSettings) => ContentPluginConfig<Html5VideoState>;
export default createPlugin;
//# sourceMappingURL=createPlugin.d.ts.map