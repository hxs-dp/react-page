import createPlugin from './createPlugin';
import DividerHtmlRenderer from './Renderer/DividerHtmlRenderer';
var plugin = createPlugin({
    Renderer: DividerHtmlRenderer,
    Controls: function () { return null; },
});
export default plugin;
//# sourceMappingURL=index.js.map