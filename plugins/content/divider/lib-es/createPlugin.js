var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import { lazyLoad } from '@react-page/core';
import * as React from 'react';
import Divider from './Component';
import { defaultSettings } from './default/settings';
var Remove = lazyLoad(function () { return import('@material-ui/icons/Remove'); });
var createPlugin = function (settings) {
    var mergedSettings = __assign(__assign({}, defaultSettings), settings);
    var WrappedComponent = function (props) { return (React.createElement(Divider, __assign({}, props, mergedSettings))); };
    return {
        Component: WrappedComponent,
        name: 'ory/editor/core/content/divider',
        version: '0.0.1',
        IconComponent: React.createElement(Remove, null),
        text: mergedSettings.translations.pluginName,
        description: mergedSettings.translations.pluginDescription,
    };
};
export default createPlugin;
//# sourceMappingURL=createPlugin.js.map