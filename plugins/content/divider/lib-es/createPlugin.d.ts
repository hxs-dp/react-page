import { ContentPluginConfig } from '@react-page/core';
import { DividerSettings } from './types/settings';
import { DividerState } from './types/state';
declare const createPlugin: (settings: DividerSettings) => ContentPluginConfig<DividerState>;
export default createPlugin;
//# sourceMappingURL=createPlugin.d.ts.map