import { DividerProps } from './component';
import { DividerApi } from './api';
export declare type DividerControlsProps = DividerProps & Partial<DividerApi>;
//# sourceMappingURL=controls.d.ts.map