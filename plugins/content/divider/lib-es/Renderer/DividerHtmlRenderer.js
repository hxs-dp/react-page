import * as React from 'react';
var DividerHtmlRenderer = function () {
    return React.createElement("hr", { className: "ory-plugins-content-divider" });
};
export default DividerHtmlRenderer;
//# sourceMappingURL=DividerHtmlRenderer.js.map