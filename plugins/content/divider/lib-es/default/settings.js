import React from 'react';
export var defaultTranslations = {
    pluginName: 'Divider',
    pluginDescription: 'A horizontal divider',
};
export var defaultSettings = {
    translations: defaultTranslations,
    Controls: function () { return React.createElement(React.Fragment, null, "Controls for this plugin were not provided"); },
    Renderer: function () { return React.createElement(React.Fragment, null, "Renderer for this plugin was not provided"); },
};
//# sourceMappingURL=settings.js.map