import { ContentPluginProps } from '@react-page/core';
import { DividerSettings } from './settings';
import { DividerState } from './state';
export declare type DividerProps = ContentPluginProps<DividerState> & DividerSettings;
//# sourceMappingURL=component.d.ts.map