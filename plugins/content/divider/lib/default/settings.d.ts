export declare const defaultTranslations: {
    pluginName: string;
    pluginDescription: string;
};
export declare const defaultSettings: {
    translations: {
        pluginName: string;
        pluginDescription: string;
    };
    Controls: () => JSX.Element;
    Renderer: () => JSX.Element;
};
//# sourceMappingURL=settings.d.ts.map