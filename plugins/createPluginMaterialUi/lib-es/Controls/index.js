var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
import { BottomToolbar } from '@react-page/ui';
import debounce from 'lodash.debounce';
import React, { Fragment, useCallback, useEffect, useMemo, useState, } from 'react';
import { AutoFields as AutoFieldsOrg, AutoForm as AutoFormOrg, } from 'uniforms-material';
import makeUniformsSchema from '../utils/makeUniformsSchema';
// see https://github.com/vazco/uniforms/issues/617
// eslint-disable-next-line @typescript-eslint/no-explicit-any
var AutoForm = AutoFormOrg;
// eslint-disable-next-line @typescript-eslint/no-explicit-any
var AutoFields = AutoFieldsOrg;
var defaultControlsLayout = {
    columnCount: 2,
};
var getDefaultValue = function (bridge) {
    return bridge.getSubfields(null).reduce(function (acc, fieldName) {
        var _a;
        return (__assign(__assign({}, acc), (_a = {}, _a[fieldName] = bridge.getInitialValue(fieldName), _a)));
    }, {});
};
function Controls(props) {
    var saveDebounced = useCallback(debounce(function (m) { return props.onChange(m); }, 1000), [props.onChange]);
    var _a = __read(useState(), 2), preview = _a[0], setPreview = _a[1];
    useEffect(function () {
        setPreview(props.state);
    }, [props.lang, props.state]);
    var onSubmit = useCallback(function (model) {
        setPreview(model);
        saveDebounced(model);
    }, []);
    var focused = props.focused, state = props.state, schema = props.schema, _b = props.controlsLayout, controlsLayout = _b === void 0 ? defaultControlsLayout : _b, Renderer = props.Renderer, remove = props.remove;
    var bridge = useMemo(function () { return makeUniformsSchema(schema); }, [schema]);
    useEffect(function () {
        var _a;
        onSubmit(__assign(__assign({}, getDefaultValue(bridge)), ((_a = preview !== null && preview !== void 0 ? preview : state) !== null && _a !== void 0 ? _a : {})));
    }, [bridge]);
    return (React.createElement(React.Fragment, null,
        React.createElement(Renderer, __assign({}, props, { state: preview !== null && preview !== void 0 ? preview : state })),
        React.createElement(BottomToolbar, __assign({ open: focused, title: props.text, onDelete: remove, icon: props.IconComponent }, props),
            React.createElement("div", { style: { marginBottom: 24, maxHeight: '50vh', overflow: 'auto' } },
                React.createElement(AutoForm, { model: preview || state, autosave: true, schema: bridge, onSubmit: onSubmit },
                    React.createElement("div", { style: {
                            columnCount: controlsLayout.columnCount || 2,
                            columnRule: '1px solid #E0E0E0',
                            columnGap: 48,
                        } },
                        React.createElement(AutoFields, { element: Fragment })))))));
}
export default Controls;
//# sourceMappingURL=index.js.map