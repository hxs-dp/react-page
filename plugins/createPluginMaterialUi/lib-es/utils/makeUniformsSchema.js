var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
/* eslint-disable @typescript-eslint/ban-types */
import Ajv from 'ajv';
import { JSONSchemaBridge } from 'uniforms-bridge-json-schema';
var ajv = new Ajv({ allErrors: true, useDefaults: true });
function createValidator(schema) {
    var validator = ajv.compile(schema);
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    return function (model) {
        validator(model);
        if (validator.errors && validator.errors.length) {
            throw { details: validator.errors };
        }
    };
}
function makeUniformsSchema(jsonSchema) {
    var fullSchema = __assign({ type: 'object' }, jsonSchema);
    return new JSONSchemaBridge(fullSchema, createValidator(fullSchema));
}
export default makeUniformsSchema;
//# sourceMappingURL=makeUniformsSchema.js.map