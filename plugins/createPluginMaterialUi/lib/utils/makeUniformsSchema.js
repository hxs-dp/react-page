"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
/* eslint-disable @typescript-eslint/ban-types */
var ajv_1 = __importDefault(require("ajv"));
var uniforms_bridge_json_schema_1 = require("uniforms-bridge-json-schema");
var ajv = new ajv_1.default({ allErrors: true, useDefaults: true });
function createValidator(schema) {
    var validator = ajv.compile(schema);
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    return function (model) {
        validator(model);
        if (validator.errors && validator.errors.length) {
            throw { details: validator.errors };
        }
    };
}
function makeUniformsSchema(jsonSchema) {
    var fullSchema = __assign({ type: 'object' }, jsonSchema);
    return new uniforms_bridge_json_schema_1.JSONSchemaBridge(fullSchema, createValidator(fullSchema));
}
exports.default = makeUniformsSchema;
//# sourceMappingURL=makeUniformsSchema.js.map