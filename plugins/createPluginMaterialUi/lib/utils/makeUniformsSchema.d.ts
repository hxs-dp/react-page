import { JsonSchema } from '../types/jsonSchema';
import { JSONSchemaBridge } from 'uniforms-bridge-json-schema';
declare function makeUniformsSchema<T extends {}>(jsonSchema: Omit<JsonSchema<T>, 'type'>): JSONSchemaBridge;
export default makeUniformsSchema;
//# sourceMappingURL=makeUniformsSchema.d.ts.map