import { ContentPluginConfig, ContentPluginProps, LayoutPluginConfig, LayoutPluginProps, PluginProps } from '@react-page/core';
import { JsonSchema } from './jsonSchema';
export declare type ControlsType<T> = React.ComponentType<ControlProps<T>>;
export declare type ControlsLayout = {
    columnCount: number;
};
declare type CommonProps<T extends {}> = {
    schema?: Omit<JsonSchema<T>, 'type'>;
    controlsLayout?: ControlsLayout;
};
declare type CommonContentPluginProps<T> = {
    Renderer: React.ComponentType<ContentPluginProps<T>>;
};
declare type CommonLayoutPluginProps<T> = {
    Renderer: React.ComponentType<LayoutPluginProps<T>>;
};
export declare type ControlProps<T> = CommonProps<T> & {
    Renderer: React.ComponentType<PluginProps<T>>;
} & PluginProps<T>;
export declare type ContentPluginDefinition<T> = CommonProps<T> & CommonContentPluginProps<T> & ContentPluginConfig<T>;
export declare type LayoutPluginDefinition<T> = CommonProps<T> & CommonLayoutPluginProps<T> & LayoutPluginConfig<T>;
export {};
//# sourceMappingURL=index.d.ts.map