declare type CommonPropertyProps = {
    title?: string;
    uniforms?: object;
    [key: string]: any;
};
declare type BooleanType = {
    type: 'boolean';
} & CommonPropertyProps;
declare type StringProperty = {
    type: 'string';
    minLength?: number;
    maxLength?: number;
    pattern?: string;
} & CommonPropertyProps;
declare type NumberProperty = {
    type: 'number' | 'integer';
    minimum?: number;
    maximum?: number;
    multipleOf?: number;
    exclusiveMaximum?: boolean;
} & CommonPropertyProps;
declare type ObjectProperty<T extends object> = JsonSchema<T> & CommonPropertyProps;
declare type ArrayProperty<T> = {
    type: 'array';
    items: JsonSchemaProperty<T>;
};
export declare type JsonSchemaProperty<T> = T extends (infer U)[] ? ArrayProperty<U> : T extends object ? ObjectProperty<T> : T extends string ? StringProperty : T extends number ? NumberProperty : T extends boolean ? BooleanType : never;
export declare type JsonSchema<T extends object> = {
    title?: string;
    type: 'object';
    properties: {
        [K in keyof T]-?: JsonSchemaProperty<T[K]>;
    };
    /** union to tuple conversion is expensive, we do a poor mans version here */
    required: Array<keyof T>;
};
export {};
//# sourceMappingURL=jsonSchema.d.ts.map