import { ControlProps } from '../types';
declare function Controls<T>(props: ControlProps<T>): JSX.Element;
export default Controls;
//# sourceMappingURL=index.d.ts.map