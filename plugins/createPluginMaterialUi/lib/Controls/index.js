"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var ui_1 = require("@react-page/ui");
var lodash_debounce_1 = __importDefault(require("lodash.debounce"));
var react_1 = __importStar(require("react"));
var uniforms_material_1 = require("uniforms-material");
var makeUniformsSchema_1 = __importDefault(require("../utils/makeUniformsSchema"));
// see https://github.com/vazco/uniforms/issues/617
// eslint-disable-next-line @typescript-eslint/no-explicit-any
var AutoForm = uniforms_material_1.AutoForm;
// eslint-disable-next-line @typescript-eslint/no-explicit-any
var AutoFields = uniforms_material_1.AutoFields;
var defaultControlsLayout = {
    columnCount: 2,
};
var getDefaultValue = function (bridge) {
    return bridge.getSubfields(null).reduce(function (acc, fieldName) {
        var _a;
        return (__assign(__assign({}, acc), (_a = {}, _a[fieldName] = bridge.getInitialValue(fieldName), _a)));
    }, {});
};
function Controls(props) {
    var saveDebounced = react_1.useCallback(lodash_debounce_1.default(function (m) { return props.onChange(m); }, 1000), [props.onChange]);
    var _a = __read(react_1.useState(), 2), preview = _a[0], setPreview = _a[1];
    react_1.useEffect(function () {
        setPreview(props.state);
    }, [props.lang, props.state]);
    var onSubmit = react_1.useCallback(function (model) {
        setPreview(model);
        saveDebounced(model);
    }, []);
    var focused = props.focused, state = props.state, schema = props.schema, _b = props.controlsLayout, controlsLayout = _b === void 0 ? defaultControlsLayout : _b, Renderer = props.Renderer, remove = props.remove;
    var bridge = react_1.useMemo(function () { return makeUniformsSchema_1.default(schema); }, [schema]);
    react_1.useEffect(function () {
        var _a;
        onSubmit(__assign(__assign({}, getDefaultValue(bridge)), ((_a = preview !== null && preview !== void 0 ? preview : state) !== null && _a !== void 0 ? _a : {})));
    }, [bridge]);
    return (react_1.default.createElement(react_1.default.Fragment, null,
        react_1.default.createElement(Renderer, __assign({}, props, { state: preview !== null && preview !== void 0 ? preview : state })),
        react_1.default.createElement(ui_1.BottomToolbar, __assign({ open: focused, title: props.text, onDelete: remove, icon: props.IconComponent }, props),
            react_1.default.createElement("div", { style: { marginBottom: 24, maxHeight: '50vh', overflow: 'auto' } },
                react_1.default.createElement(AutoForm, { model: preview || state, autosave: true, schema: bridge, onSubmit: onSubmit },
                    react_1.default.createElement("div", { style: {
                            columnCount: controlsLayout.columnCount || 2,
                            columnRule: '1px solid #E0E0E0',
                            columnGap: 48,
                        } },
                        react_1.default.createElement(AutoFields, { element: react_1.Fragment })))))));
}
exports.default = Controls;
//# sourceMappingURL=index.js.map