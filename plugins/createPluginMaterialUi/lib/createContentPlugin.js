"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@react-page/core");
var React = __importStar(require("react"));
// eslint-disable-next-line @typescript-eslint/ban-types
function createPluginWithDef(_a) {
    var schema = _a.schema, Renderer = _a.Renderer, controlsLayout = _a.controlsLayout, pluginSettings = __rest(_a, ["schema", "Renderer", "controlsLayout"]);
    var Controls = core_1.lazyLoad(function () { return Promise.resolve().then(function () { return __importStar(require('./Controls')); }); });
    return __assign({ Component: function (props) {
            return (React.createElement(React.Fragment, null, !props.readOnly ? (React.createElement(Controls, __assign({ controlsLayout: controlsLayout, schema: schema, Renderer: Renderer }, props))) : (React.createElement(Renderer, __assign({}, props)))));
        } }, pluginSettings);
}
function createContentPlugin(definition) {
    return function (customize) {
        if (customize) {
            return createPluginWithDef(customize(definition));
        }
        return createPluginWithDef(definition);
    };
}
exports.default = createContentPlugin;
//# sourceMappingURL=createContentPlugin.js.map